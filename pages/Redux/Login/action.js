import { LOGIN_USER_FAILURE, LOGIN_USER_REQUEST, LOGIN_USER_SUCCESS } from './actionTypes'

// import axios from 'axios'
export const loginUserRequest = payload => ({
    type: LOGIN_USER_REQUEST,
    payload: payload
})

export const loginUserSuccess = payload => ({
    type: LOGIN_USER_SUCCESS,
    payload: payload
})

export const loginUserFailure = payload => ({
    type: LOGIN_USER_FAILURE,
    payload: payload
})

// export const getUser = payload => ({
//     type: GET_USERDATA,
//     payload
// })

// export const userDataRequest = payload  => dispatch => {
//     console.log(payload)
//     axios.get("http://localhost:3000/dev/login", {
//         params: {
//             token: payload
//         }
//     })
//         .then(res => dispatch(getUser(res.data)))

// }

export const loginUserCheck =  payload => dispatch => {
   dispatch(loginUserRequest(payload))
    // console.log(payload, 'called login')


    // axios.post("https://3psw0hg3a6.execute-api.us-east-1.amazonaws.com/dev/login", JSON.stringify(payload))
    //     .then(res => res.data)
    //     .then(res => dispatch(loginUserSuccess(res)))
    //     .catch(err => dispatch(loginUserFailure(err)));
    var axios = require('axios');
var data = JSON.stringify(payload);

var config = {
  method: 'post',
  // url: 'https://1cwejt4ixe.execute-api.us-east-1.amazonaws.com/dev/login',
  // url: 'http://localhost:3000/dev/login',
  url: "https://8w79l0t0b6.execute-api.ap-south-1.amazonaws.com/dev/login",
  headers: { 
    'Content-Type': 'application/json'
  },
  data : data
};

axios(config)
.then(res => res.data
//   console.log(JSON.stringify(response.data));
  )
  .then(res => dispatch(loginUserSuccess(res)))
  .catch(err => dispatch(loginUserFailure(err)));

}


