import { LOGIN_USER_FAILURE, LOGIN_USER_REQUEST, LOGIN_USER_SUCCESS } from './actionTypes'


// function getData(key) {
//     try {
//         let data = localStorage.getItem(key)
//         data = JSON.parse(data)
//         return data
//     }
//     catch{
//         return undefined
//     }
// }


function saveData(key, data) {
    localStorage.setItem(key, JSON.stringify(data))
}


const initState = {
    isLoginError: true,
    // admin:false,
    user_type:'',
    username: '',
    token: 'x',
    userData: "",
    data:null,
    errorMessage: false
}

const loginReducer = (state = initState, { type, payload }) => {
    switch (type) {
        case LOGIN_USER_REQUEST:
            return {
                ...state
            }
        case LOGIN_USER_SUCCESS:
            // console.log(payload, 'reducer')
            // axios.get("http://94e1f8c3880d.ngrok.io/auth/get_user_info?auth_token=",+ this.props.token )
// let {error} = payload
            // token(payload.token)
            if(!payload.error) {
                saveData('customerExist', {username:payload.data.username, user_type:payload.data.user_type, code: payload.data.code, email: payload.data.email })
            }
            else {
                // console.log("enter")
                // saveData('loginStatus', {isLoginError: payload.error, errorMessage: payload.error})
                return {
                    ...state,
                    isLoginError: payload.error,
                    errorMessage: payload.error
                }
            }
         
            return {
                ...state,
                isLoginError: payload.error,
                user_type:payload.data.user_type,
                errorMessage: payload.error,
                // data:payload.data,
                // username:payload.data.username
                // messageLogin: payload.message,
                // token: payload.token
            }
        case LOGIN_USER_FAILURE:
            return {
                ...state,
                isLoginError: payload.error,
                messageLogin: payload.message
            }

        // case GET_USERDATA:
        //     console.log(payload)
        //     saveData('user', payload.data)
        //     return {
        //         ...state,
        //         isLoginError:payload.error,
        //         userData: payload.data
        //     }
        default:
            return state
    }
}

export default loginReducer