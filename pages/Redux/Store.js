import {createStore, combineReducers, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import loginReducer from './Login/loginReducer';
import sidebarReducer from './Sidebar/sideBarReducer';
import orderReducer from './Orders/orderReducer'
import {composeWithDevTools} from 'redux-devtools-extension'


const rootReducer = combineReducers({
     login: loginReducer,  
     sidebar: sidebarReducer,
     orders:orderReducer

})

// const createComposer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
    rootReducer, 
    // createComposer(applyMiddleware(thunk))
    // applyMiddleware(thunk)
    composeWithDevTools(applyMiddleware(thunk))
    )

export default store;
