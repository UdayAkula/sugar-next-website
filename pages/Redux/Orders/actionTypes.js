export const ORDER_REQUEST = "ORDER_REQUEST";
export const ORDER_SUCCESS = "ORDER_SUCCESS";
export const ORDER_FAILURE = "ORDER_FAILURE";
export const DATES = "DATES";
export const TOTAL_ORDERS = "TOTAL_ORDERS"
export const API_CALL = "API_CALL"