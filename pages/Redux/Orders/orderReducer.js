import {ORDER_FAILURE, ORDER_REQUEST, ORDER_SUCCESS,  DATES, TOTAL_ORDERS, API_CALL } from './actionTypes'


function getData(key) {
    try {
        let data = localStorage.getItem(key)
        data = JSON.parse(data)
        return data
    }
    catch{
        return undefined
    }
}


function saveData(key, data) {
    localStorage.setItem(key, JSON.stringify(data))
}


const initState = {
    // isLoginError: true,
    // admin:false,
    // username: '',
    // token: 'x',
    // userData: "",
    // data:null,
    // errorMessage: false,
    // dates:{check:false},
    apiOrders:[],
    apiCall: true
}

const orderReducer = (state = initState, { type, payload }) => {
    switch (type) {
        // case LOGIN_USER_REQUEST:
        //     return {
        //         ...state
        //     }
        // case LOGIN_USER_SUCCESS:
        //     console.log(payload, 'reducer')
        //     // axios.get("http://94e1f8c3880d.ngrok.io/auth/get_user_info?auth_token=",+ this.props.token )

        //     // token(payload.token)
        //     if(!payload.error) {
        //         saveData('customerExist', payload.username)
        //     }
         
        //     return {
        //         ...state,
        //         isLoginError: payload.error,
        //         admin:payload.admin,
        //         data:payload.data,
        //         errorMessage: payload.error
        //         // messageLogin: payload.message,
        //         // token: payload.token
        //     }
        // case LOGIN_USER_FAILURE:
        //     return {
        //         ...state,
        //         isLoginError: payload.error,
        //         messageLogin: payload.message
        //     }

        // case DATES:
        //     return {
        //             ...state,
        //            dates: payload
        //      }

        case TOTAL_ORDERS: 
             return {
                 ...state,
                 apiOrders: payload

             }

             case API_CALL: 
             return {
                 ...state,
                 apiCall: payload

             }

        // case GET_USERDATA:
        //     console.log(payload)
        //     saveData('user', payload.data)
        //     return {
        //         ...state,
        //         isLoginError:payload.error,
        //         userData: payload.data
        //     }
        default:
            return state
    }
}

export default orderReducer