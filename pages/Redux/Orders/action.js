import { ORDER_FAILURE, ORDER_REQUEST, ORDER_SUCCESS, GET_USERDATA, DATES, TOTAL_ORDERS, API_CALL } from './actionTypes'

import axios from 'axios'
export const orderRequest = payload => ({
    type: ORDER_REQUEST,
    payload: payload
})

export const orderSuccess = payload => ({
    type: ORDER_SUCCESS,
    payload: payload
})

export const orderFailure = payload => ({
    type: ORDER_FAILURE,
    payload: payload
})

export const dates = payload => ({
    type: DATES,
    payload: payload
})

export const total_orders = payload => ({
    type: TOTAL_ORDERS,
    payload
})

export const api_call_check = payload => ({
    type: API_CALL,
    payload
})

// export const getUser = payload => ({
//     type: GET_USERDATA,
//     payload
// })

// export const userDataRequest = payload  => dispatch => {
//     console.log(payload)
//     axios.get("http://localhost:3000/dev/login", {
//         params: {
//             token: payload
//         }
//     })
//         .then(res => dispatch(getUser(res.data)))

// }

export const orderCheck =  payload => dispatch => {
   dispatch(orderRequest(payload))
    // console.log(payload, 'called login')


    axios.get("https://3psw0hg3a6.execute-api.us-east-1.amazonaws.com/dev/login", JSON.stringify(payload))
        .then(res => res.data)
        .then(res => dispatch(orderSuccess(res)))
        .catch(err => dispatch(orderFailure(err)));
  
}

export const datesRequest =  payload => dispatch => {
    dispatch(dates(payload))
    //  console.log(payload, 'called login')
 
 
    //  axios.get("https://3psw0hg3a6.execute-api.us-east-1.amazonaws.com/dev/login", JSON.stringify(payload))
    //      .then(res => res.data)
    //      .then(res => dispatch(orderSuccess(res)))
    //      .catch(err => dispatch(orderFailure(err)));
   
 }


