import { MANAGE_ACTIVE_ITEM } from './actionTypes'

// function saveData(key, data) {
//     localStorage.setItem(key, JSON.stringify(data))
// }

const initState = {
   overview:true,
   affiliates:false
}

const sidebarReducer = (state = initState, { type, payload }) => {
    switch (type) {
        case MANAGE_ACTIVE_ITEM:
            // saveData('manageItem', {
            //     overview: payload.overview,
            //     affiliates:payload.affiliates
            // })

            //console.log("payload", payload)
            return {
                ...state,
                overview: payload.overview,
                affiliates:payload.affiliates
            }
       
        default:
            return state
    }
}

export default sidebarReducer