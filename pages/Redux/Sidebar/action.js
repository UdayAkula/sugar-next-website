import { MANAGE_ACTIVE_ITEM} from './actionTypes'


export const manageItem = payload => ({
    type: MANAGE_ACTIVE_ITEM,
    payload: payload
})

