import Head from 'next/head'
// import styles from '../styles/Home.module.css'
import MyApp from './_app'
import Homepage from './Components/Home'
export default function Home() {
  return (
    <div className=''>
      <Head>
        <title>SUGAR Cosmetics</title>
        <link rel="icon" href="/sugar-logo.png" />
      </Head>
<main>
<Homepage />

</main>
  
    </div>
  )
}
