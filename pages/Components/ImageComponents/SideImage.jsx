import React from "react";
import styles from "../../../styles/SideImage.module.css";
// import {Link} from 'react-router-dom';
import Link from "next/link";
import { v4 as uuidv4 } from "uuid";
export default function sideImage(props) {
  console.log(props.contentData)
  var content = props.contentData ? props.contentData : []
  console.log(content, "contData")
  return (
    <div style={{ overflowX: "hidden", color: "white" }}>
      {/* <Link as="/product/lipstick/" href="/[products]/[ProductPage]" > */}
      <div className={styles["container-box"]}>
        <div
          className={styles.Box}
          style={{ backgroundColor: "rgb(32, 32, 32)" }}
        ></div>
        <div
          className={`${styles["stack-top"]} ${styles.box}`}
          style={{ backgroundColor: "rgb(32, 32, 32)", display: "flex" }}
        >
          { content.length > 0 && content?.map((data) => <>
          <div key={uuidv4()}>
          <Link
              // as={`${data.redirectUrl.split('.com')[1]}`}
              as={`/product/${data.product_json.handle}`}
              href="/Components/ProductPage"
            >
              <div>
                <img src={data.product_json.image.src} width="130" height="180" className="" />
                <h6>{data.product_json.title}</h6>
              </div>
            </Link>
            {/* <h1>{data.product_json.title}</h1> */}
          </div>
          </>)}
          {/* <div>
            <Link
              as="asjidfjasd/fkjasfkakjsdkf/askdjfalsd/ndaskfj/akjsdfaf/kdsjfakldf/jvsadfks"
              href="/Components/ProductPage"
            >
              <div>
                <img src="https://via.placeholder.com/130x200" />
                <span>Product</span>
              </div>
            </Link>
          </div> */}
          {/* <div>
            <Link as="collectiosn/face" href="/Components/ProductPage">
              <div>
                <img src="https://via.placeholder.com/130x200" />
                <span>Product</span>
              </div>
            </Link>
          </div> */}
          {/* <div>
            <img src="https://via.placeholder.com/130x200" />
            <span>Product</span>
          </div> */}
          {/* <div>
            <img src="https://via.placeholder.com/130x200" />
            <span>Product</span>
          </div> */}
          {/* <div>
            <img src="https://via.placeholder.com/130x200" />
            <span>Product</span>
          </div> */}
          {/* <div> 
             <img src="https://via.placeholder.com/130x200" />
            <span>Product</span>
          </div> */}
        </div>
      </div>
    </div>
  );
}
