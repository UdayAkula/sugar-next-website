import React from "react";
// import {Link} from 'react-router-dom'
// import Link from 'next/link'
const CarouselPage = () => {
  return (
    
<div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
  <ol className="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
  </ol>
  <div className="carousel-inner">
    <div className="carousel-item active">
      {/* <Link href="/Components/ProductPage" as='/image'> */}
      <img className="d-block w-100" src="https://via.placeholder.com/150" alt="First slide"/>
      {/* </Link> */}
    </div>
    <div className="carousel-item">
      <img className="d-block w-100" src="https://via.placeholder.com/250" alt="Second slide"/>
    </div>
    <div className="carousel-item">
      <img className="d-block w-100" src="https://via.placeholder.com/350" alt="Third slide"/>
    </div>
    <div className="carousel-item">
      <img className="d-block w-100" src="https://via.placeholder.com/450" alt="Third slide"/>
    </div>
    <div className="carousel-item">
      <img className="d-block w-100" src="https://via.placeholder.com/550" alt="Third slide"/>
    </div>
  </div>

</div>

  );
}

export default CarouselPage;