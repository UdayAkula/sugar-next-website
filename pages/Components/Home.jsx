import React, { useEffect } from "react";
import SideImage from "./ImageComponents/SideImage";
import TopBannerCarousel from "./ImageComponents/Carousel";
import Navbar from "./Navbar";
import Footer from "./Footer";
import Link from "next/link";
import { useRouter } from "next/router";
import { v4 as uuidv4 } from "uuid";
import axios from "axios";
import styles from "../../styles/SideImage.module.css";

 function Redirect({ to, as }) {
  const router = useRouter();
  console.log(to, as);
  useEffect(() => {
    router.replace(to, as, { shallow: true });
  }, [to]);

  return null;
}
var data = {
  statusId: 1,
  resbody: {
    totalSectionCount: 12,
    sections: [
      {
        id: 1,
        title: "Slider",
        sequence: 10,
        layoutType: 2,
        isTitleEnabled: 0,
        backgroundImg: null,
        backgroundColor: "#000000",
        text: null,
        contentType: 1,
        contentData: [
          {
            id: 182,
            sectionId: 1,
            webViewTitle: null,
            contentType: 1,
            mediaUrl:
              "https://d32baadbbpueqt.cloudfront.net/dashboard/1610562629Double-Dibs---Homepage-Banner-600x500.jpg",
            product_json: null,
            mediaText: null,
            redirectUrl:
              "https://in.sugarcosmetics.com/collections/double-dibs-jan",
          },
          {
            id: 33,
            sectionId: 1,
            webViewTitle: null,
            contentType: 1,
            mediaUrl:
              "https://d32baadbbpueqt.cloudfront.net/dashboard/1610534510Beginners-Must-Have-Kit---Homepage-Banner_MOB.jpg",
            product_json: null,
            mediaText: null,
            redirectUrl:
              "https://in.sugarcosmetics.com/products/beginners-must-have-kit",
          },
          {
            id: 36,
            sectionId: 1,
            webViewTitle: null,
            contentType: 1,
            mediaUrl:
              "https://d32baadbbpueqt.cloudfront.net/dashboard/1610524126Eyes-&-Shine-Shadow-Crayon---Clearance--mobile-Homepage-banner.jpg",
            product_json: null,
            mediaText: null,
            redirectUrl:
              "https://in.sugarcosmetics.com/collections/sugar-clearance",
          },
          {
            id: 178,
            sectionId: 1,
            webViewTitle: null,
            contentType: 1,
            mediaUrl:
              "https://d32baadbbpueqt.cloudfront.net/dashboard/1610529942Bride_Tribe_Kit_-_Homepage_Banner_mob_(1).jpg",
            product_json: null,
            mediaText: null,
            redirectUrl:
              "https://in.sugarcosmetics.com/products/bride-tribe-kit",
          },
          {
            id: 34,
            sectionId: 1,
            webViewTitle: null,
            contentType: 1,
            mediaUrl:
              "https://d32baadbbpueqt.cloudfront.net/dashboard/1609825996Blog-homepage-banner-mobile-(1).jpg",
            product_json: null,
            mediaText: null,
            redirectUrl:
              "https://blog.sugarcosmetics.com/ace-the-perfect-eyeliner-wings/",
          },
        ],
      },
      {
        id: 17,
        title: "Refer Your Friends",
        sequence: 16,
        layoutType: 4,
        isTitleEnabled: 1,
        backgroundImg: null,
        backgroundColor: "#000000",
        text: null,
        contentType: 1,
        contentData: [
          {
            id: 175,
            sectionId: 17,
            webViewTitle: null,
            contentType: 1,
            mediaUrl:
              "https://d32baadbbpueqt.cloudfront.net/dashboard/1606967422Refer_and_Earn_Banner_op_2.jpg",
            product_json: null,
            mediaText: null,
            redirectUrl: "https://in.sugarcosmetics.com/referral",
          },
        ],
      },
      {
        id: 2,
        title: "HOT DEALS",
        sequence: 20,
        layoutType: 1,
        isTitleEnabled: 1,
        backgroundImg: null,
        backgroundColor: "#000000",
        text: null,
        contentType: 4,
        contentData: [
          {
            id: 107,
            sectionId: 2,
            webViewTitle: null,
            contentType: 1,
            mediaUrl:
              "https://d32baadbbpueqt.cloudfront.net/dashboard/1610522940Get-Set-Glow2.jpg",
            product_json: null,
            mediaText: null,
            redirectUrl:
              "https://in.sugarcosmetics.com/products/get-set-glow-kit",
          },
          {
            id: 106,
            sectionId: 2,
            webViewTitle: null,
            contentType: 1,
            mediaUrl:
              "https://d32baadbbpueqt.cloudfront.net/dashboard/1609992753The-499-store_(1).jpg",
            product_json: null,
            mediaText: null,
            redirectUrl:
              "https://in.sugarcosmetics.com/collections/under-499-store",
          },
          {
            id: 37,
            sectionId: 2,
            webViewTitle: null,
            contentType: 1,
            mediaUrl:
              "https://d32baadbbpueqt.cloudfront.net/dashboard/1609439209999-tile-banner_(1).jpg",
            product_json: null,
            mediaText: null,
            redirectUrl:
              "https://in.sugarcosmetics.com/collections/featured-collection-999",
          },
          {
            id: 98,
            sectionId: 2,
            webViewTitle: null,
            contentType: 1,
            mediaUrl:
              "https://d32baadbbpueqt.cloudfront.net/dashboard/16094392281499-tile-banner_(1).jpg",
            product_json: null,
            mediaText: null,
            redirectUrl:
              "https://in.sugarcosmetics.com/collections/featured-collection-1499",
          },
          {
            id: 40,
            sectionId: 2,
            webViewTitle: null,
            contentType: 1,
            mediaUrl:
              "https://d32baadbbpueqt.cloudfront.net/dashboard/16094392431999-tile-banner_(1).jpg",
            product_json: null,
            mediaText: null,
            redirectUrl:
              "https://in.sugarcosmetics.com/collections/featured-collection-1999",
          },
          {
            id: 39,
            sectionId: 2,
            webViewTitle: null,
            contentType: 1,
            mediaUrl:
              "https://d32baadbbpueqt.cloudfront.net/dashboard/1609439133499-tile-banner_(1).jpg",
            product_json: null,
            mediaText: null,
            redirectUrl:
              "https://in.sugarcosmetics.com/collections/featured-collection-499",
          },
        ],
      },
      {
        id: 15,
        title: "BESTSELLERS",
        sequence: 30,
        layoutType: 3,
        isTitleEnabled: 1,
        backgroundImg: null,
        backgroundColor: "#ffffff",
        text: null,
        contentType: 3,
        contentData: [
          {
            id: 121,
            sectionId: 15,
            webViewTitle: null,
            contentType: 3,
            mediaUrl: null,
            product_json: {
              id: 2350278148179,
              title: "Smudge Me Not Liquid Lipstick Minis Set",
              body_html:
                '<p>Ladies, your wish is our command! We are here with the most-wanted, most wished for lipstick minis – And that too, as a CUSTOMISABLE SET. Yeah, you read it right. <strong>SUGAR Smudge Me Not Liquid Lipstick Minis Set</strong> is here!</p>\n<p>And, IT IS the treasure of your dreams. The cult-favourites, coveted colours of the “one-coat wonder” <strong>Smudge Me Not Liquid Lipstick</strong> range have become exponentially cuter now, in miniature form. Not just that, these teeny-tiny, super instagrammable versions of the lovable lip range give everyone bigger bang for the buck as they are sold in a specially-crafted, classy, black matte box that’s super stylish and travel friendly – the perfect box to gift to your girlfriends or the best treat for self-indulgence, you are never going to run out of options with this one, and that’s not all. With this on-the-go customizable fab box in your bag, say bye to all the lippie hassles and say hi to gorgeous, sorted lips!</p>\n<p>Now, you get everything you would ever need to get those ‘lips of your dreams’ in one chic place. With these stunning and bold lipcolours that pep your pout up, slay every day, all day!</p>\n<p>Checkout and choose from 24 cutesy minis with stunning shades that are perfect for all skin tones.</p>\n<ul>\n<li><strong>01 Brazen Raisin (Burgundy)</strong></li>\n<li><strong>02 Brink Of Pink (Plum Rose)</strong></li>\n<li><strong>03 Tan Fan (Mauve Nude)</strong></li>\n<li><strong>04 Plum Yum (Muted Plum)</strong></li>\n<li><strong>05 Rust Lust (Red Terracotta)</strong></li>\n<li><strong><span>06 Tangerine Queen (Orange Coral)</span></strong></li>\n<li><strong>07 Rethink Pink (Fuchsia)</strong></li>\n<li><strong>08 Wine And Shine (Sangria)</strong></li>\n<li><strong>09 Suave Mauve (<span>Mauve)</span></strong></li>\n<li><strong>10 Drop Dead Red (Red)</strong></li>\n<li><strong>12 Don Fawn (Yellow Brown)</strong></li>\n<li><strong>13 Wooed By Nude (Peach Nude)</strong></li>\n<li><strong>14 Teak Mystique (Warm Brown)</strong></li>\n<li><strong>17 Fiery Berry (Marsala)</strong></li>\n<li><strong>21 Aubergine Queen <span>(Blackened Burgundy)</span></strong></li>\n<li><strong>25 Very Mulberry<span> (Deep Berry)</span></strong></li>\n<li><strong>29 Scarlet Starlet<span> (Orange Red)</span></strong></li>\n<li><strong>30 Peony Genie <span>(Medium Pink)</span></strong></li>\n<li><strong><span>37 Hot Apricot (Peachy Nude)</span></strong></li>\n<li><strong><span>38 Dose Of Rose (Rosy Mauve)</span></strong></li>\n<li><strong><span>39 Pink Sync (Rosy Magenta)</span></strong></li>\n<li><strong><span>42 Toast Roast (Deep Reddish Brown)</span></strong></li>\n<li><strong><span>43 Hot Shot (Hot Pink / Dark Fuchsia Pink)</span></strong></li>\n<li><strong><span>44 Preach Peach (Peach Pink)</span></strong></li>\n</ul>\n<p>So ladies, Why have 1 when you can get your hands on 4?</p>\n<p><strong>NET VOLUME: (1.1ml/ 0.03 Fl.Oz.) X 4</strong></p>\n<p><span><strong>Note</strong>: Any single product in the kit/set cannot be replaced or returned.</span></p>\n<p><span><b data-stringify-type="bold">Maximum Retail Price:</b> RS. 999 (incl. all taxes)<br><b data-stringify-type="bold">Country of Origin</b>: India<br><b data-stringify-type="bold">Company Name</b>: Viva Cosmetics<br><b data-stringify-type="bold">Company Address</b>: A/19 G4 Happy Valley, Nr Tikujiniwadi, Manpada, Thane W - 400610</span></p>',
              vendor: "SUGAR Cosmetics",
              product_type: "Gifts & Sets",
              created_at: "2018-11-28T10:42:45+05:30",
              handle: "smudge-me-not-liquid-lipstick-minis-set",
              updated_at: "2020-12-09T00:40:38+05:30",
              published_at: "2019-09-19T13:23:46+05:30",
              template_suffix: "",
              published_scope: "web",
              tags:
                "10 HR, 12 HR, 24 HR, Affordable, Alcohol Free, amp-hide, Anniversary, Bestseller, Birthday, Bold, Budget, Budget Friendly, Classy, College, Combo, Cost Effective, Cruelty Free, Date-proof, Deals, Dermatologically Tested, Dry matte, Easy Application, Economical, Fadeproof, Gift, Gifting, Gifts for her, Girlfriend, Glides Effortlessly, Gluten Free, High Coverage, Intensely Pigmented, Kissproof, Lips, Lipstick, Lipstick Combo, Liquid Lip, Liquid Lipstick, Long Lasting, Longwear, Love, Low Cost, Low Priced, Marriage, Matte, Matte lipstick, Mineral Oil Free, Mini, Miniature, Mom, Money Saving, Mother, NO-OOS, Nominal, Non budge, One-coat wonder, Opaque, Pigmented, Present, Presents for Her, Reasonable, Save Money, Savings, Single Swipe, Sister, Small, Smudge Me Not, Smudgeproof, st_bestseller:1, st_gifting filter: Trending Gifts Minis Set, st_hot:, st_popularityscore: 9070, Steal Deal, sugar_type_2, Superstay, Transferproof, Travel Friendly, Ultra-matte, Valentine, Valentine's Day, Value for Money, Vegan, Vegetarian, Vitamin E, Water Resistant, Waterproof, Wedding, Wife, Worth The Money, Worthy, Zero featherings",
              admin_graphql_api_id: "gid://shopify/Product/2350278148179",
              variants: [
                {
                  id: 21206334177363,
                  product_id: 2350278148179,
                  title: "Default Title",
                  price: "799.00",
                  sku: "8904320700652",
                  position: 1,
                  inventory_policy: "deny",
                  compare_at_price: "999.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "Default Title",
                  option2: null,
                  option3: null,
                  created_at: "2018-11-28T10:42:46+05:30",
                  updated_at: "2020-12-09T00:40:36+05:30",
                  taxable: true,
                  barcode: "8904320700652",
                  grams: 0,
                  image_id: null,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 21708234489939,
                  inventory_quantity: 328,
                  old_inventory_quantity: 328,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/21206334177363",
                },
              ],
              options: [
                {
                  id: 3250281578579,
                  product_id: 2350278148179,
                  name: "Title",
                  position: 1,
                  values: ["Default Title"],
                },
              ],
              images: [
                {
                  id: 13730777464915,
                  product_id: 2350278148179,
                  position: 1,
                  created_at: "2019-12-24T08:59:41+05:30",
                  updated_at: "2020-09-04T19:29:42+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-6976266829907.jpg?v=1599227982",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13730777464915",
                },
                {
                  id: 13731705782355,
                  product_id: 2350278148179,
                  position: 2,
                  created_at: "2019-12-24T13:13:50+05:30",
                  updated_at: "2020-09-04T19:29:42+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-6976266797139.jpg?v=1599227982",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13731705782355",
                },
                {
                  id: 13731705192531,
                  product_id: 2350278148179,
                  position: 3,
                  created_at: "2019-12-24T13:13:33+05:30",
                  updated_at: "2020-09-04T19:29:42+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-6976266862675.jpg?v=1599227982",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13731705192531",
                },
                {
                  id: 13730778611795,
                  product_id: 2350278148179,
                  position: 4,
                  created_at: "2019-12-24T08:59:58+05:30",
                  updated_at: "2020-09-04T19:29:42+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-7014181175379.png?v=1599227982",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13730778611795",
                },
                {
                  id: 13730776612947,
                  product_id: 2350278148179,
                  position: 5,
                  created_at: "2019-12-24T08:59:26+05:30",
                  updated_at: "2020-09-04T19:29:42+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-7450445316179.jpg?v=1599227982",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13730776612947",
                },
                {
                  id: 14972614410323,
                  product_id: 2350278148179,
                  position: 6,
                  created_at: "2020-09-04T19:09:44+05:30",
                  updated_at: "2020-09-04T19:29:42+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-14972586197075.jpg?v=1599227982",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14972614410323",
                },
                {
                  id: 13731699195987,
                  product_id: 2350278148179,
                  position: 7,
                  created_at: "2019-12-24T13:11:54+05:30",
                  updated_at: "2020-09-04T19:29:42+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-7450536673363.jpg?v=1599227982",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13731699195987",
                },
                {
                  id: 14972735619155,
                  product_id: 2350278148179,
                  position: 8,
                  created_at: "2020-09-04T19:29:08+05:30",
                  updated_at: "2020-09-04T19:29:42+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-14972587180115.jpg?v=1599227982",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14972735619155",
                },
                {
                  id: 13731693854803,
                  product_id: 2350278148179,
                  position: 9,
                  created_at: "2019-12-24T13:11:34+05:30",
                  updated_at: "2020-09-04T19:29:42+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-7450446168147.jpg?v=1599227982",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13731693854803",
                },
                {
                  id: 14972617621587,
                  product_id: 2350278148179,
                  position: 10,
                  created_at: "2020-09-04T19:10:16+05:30",
                  updated_at: "2020-09-04T19:29:42+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-14972588359763.jpg?v=1599227982",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14972617621587",
                },
                {
                  id: 13731692314707,
                  product_id: 2350278148179,
                  position: 11,
                  created_at: "2019-12-24T13:11:20+05:30",
                  updated_at: "2020-09-04T19:29:42+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-7450446200915.jpg?v=1599227982",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13731692314707",
                },
                {
                  id: 14972738404435,
                  product_id: 2350278148179,
                  position: 12,
                  created_at: "2020-09-04T19:29:41+05:30",
                  updated_at: "2020-09-04T19:29:42+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-14972590555219.jpg?v=1599227982",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14972738404435",
                },
                {
                  id: 13730784247891,
                  product_id: 2350278148179,
                  position: 13,
                  created_at: "2019-12-24T09:01:54+05:30",
                  updated_at: "2020-09-04T19:29:42+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-7450446233683.jpg?v=1599227982",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13730784247891",
                },
                {
                  id: 14972610445395,
                  product_id: 2350278148179,
                  position: 14,
                  created_at: "2020-09-04T19:09:10+05:30",
                  updated_at: "2020-09-04T19:29:42+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-14972594487379.jpg?v=1599227982",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14972610445395",
                },
                {
                  id: 13730783297619,
                  product_id: 2350278148179,
                  position: 15,
                  created_at: "2019-12-24T09:01:38+05:30",
                  updated_at: "2020-09-04T19:29:42+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-7450446266451.jpg?v=1599227982",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13730783297619",
                },
                {
                  id: 14972643246163,
                  product_id: 2350278148179,
                  position: 16,
                  created_at: "2020-09-04T19:14:18+05:30",
                  updated_at: "2020-09-04T19:29:42+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-14972598943827.jpg?v=1599227982",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14972643246163",
                },
                {
                  id: 13730782642259,
                  product_id: 2350278148179,
                  position: 17,
                  created_at: "2019-12-24T09:01:22+05:30",
                  updated_at: "2020-09-04T19:29:42+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-7450535231571.jpg?v=1599227982",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13730782642259",
                },
                {
                  id: 13730792734803,
                  product_id: 2350278148179,
                  position: 18,
                  created_at: "2019-12-24T09:03:56+05:30",
                  updated_at: "2020-09-04T19:29:42+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-7450446889043.jpg?v=1599227982",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13730792734803",
                },
                {
                  id: 14972713730131,
                  product_id: 2350278148179,
                  position: 19,
                  created_at: "2020-09-04T19:25:10+05:30",
                  updated_at: "2020-09-04T19:29:42+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-14972601499731.jpg?v=1599227982",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14972713730131",
                },
                {
                  id: 13730790408275,
                  product_id: 2350278148179,
                  position: 20,
                  created_at: "2019-12-24T09:03:24+05:30",
                  updated_at: "2020-09-04T19:29:42+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-7450446987347.jpg?v=1599227982",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13730790408275",
                },
                {
                  id: 14972625453139,
                  product_id: 2350278148179,
                  position: 21,
                  created_at: "2020-09-04T19:11:45+05:30",
                  updated_at: "2020-09-04T19:29:42+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-14972603498579.jpg?v=1599227982",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14972625453139",
                },
                {
                  id: 13730791424083,
                  product_id: 2350278148179,
                  position: 22,
                  created_at: "2019-12-24T09:03:39+05:30",
                  updated_at: "2020-09-04T19:29:42+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-7450446954579.jpg?v=1599227982",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13730791424083",
                },
                {
                  id: 14972622078035,
                  product_id: 2350278148179,
                  position: 23,
                  created_at: "2020-09-04T19:11:11+05:30",
                  updated_at: "2020-09-04T19:29:42+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-14972605956179.jpg?v=1599227982",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14972622078035",
                },
                {
                  id: 13739036147795,
                  product_id: 2350278148179,
                  position: 24,
                  created_at: "2019-12-26T06:42:54+05:30",
                  updated_at: "2020-09-04T19:29:42+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-7450445545555.png?v=1599227982",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13739036147795",
                },
                {
                  id: 14972719333459,
                  product_id: 2350278148179,
                  position: 25,
                  created_at: "2020-09-04T19:26:15+05:30",
                  updated_at: "2020-09-04T19:29:42+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-14972609232979.jpg?v=1599227982",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14972719333459",
                },
                {
                  id: 13739035590739,
                  product_id: 2350278148179,
                  position: 26,
                  created_at: "2019-12-26T06:42:37+05:30",
                  updated_at: "2020-09-04T19:29:42+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-7450445414483.png?v=1599227982",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13739035590739",
                },
                {
                  id: 14972716646483,
                  product_id: 2350278148179,
                  position: 27,
                  created_at: "2020-09-04T19:25:44+05:30",
                  updated_at: "2020-09-04T19:29:42+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-14972611264595.jpg?v=1599227982",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14972716646483",
                },
                {
                  id: 13739035099219,
                  product_id: 2350278148179,
                  position: 28,
                  created_at: "2019-12-26T06:42:21+05:30",
                  updated_at: "2020-09-04T19:29:42+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-7450445873235.png?v=1599227982",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13739035099219",
                },
                {
                  id: 14972703834195,
                  product_id: 2350278148179,
                  position: 29,
                  created_at: "2020-09-04T19:23:44+05:30",
                  updated_at: "2020-09-04T19:29:42+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-14972613820499.jpg?v=1599227982",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14972703834195",
                },
                {
                  id: 13739029987411,
                  product_id: 2350278148179,
                  position: 30,
                  created_at: "2019-12-26T06:40:50+05:30",
                  updated_at: "2020-09-04T19:29:42+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-7450445906003.png?v=1599227982",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13739029987411",
                },
                {
                  id: 14972699574355,
                  product_id: 2350278148179,
                  position: 31,
                  created_at: "2020-09-04T19:23:11+05:30",
                  updated_at: "2020-09-04T19:29:42+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-14972640821331.jpg?v=1599227982",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14972699574355",
                },
                {
                  id: 14972637118547,
                  product_id: 2350278148179,
                  position: 32,
                  created_at: "2020-09-04T19:13:12+05:30",
                  updated_at: "2020-09-04T19:29:42+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-14972618047571.png?v=1599227982",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14972637118547",
                },
                {
                  id: 14972651208787,
                  product_id: 2350278148179,
                  position: 33,
                  created_at: "2020-09-04T19:15:49+05:30",
                  updated_at: "2020-09-04T19:29:42+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-14972618965075.jpg?v=1599227982",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14972651208787",
                },
                {
                  id: 14972647374931,
                  product_id: 2350278148179,
                  position: 34,
                  created_at: "2020-09-04T19:15:16+05:30",
                  updated_at: "2020-09-04T19:29:42+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-14972619522131.jpg?v=1599227982",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14972647374931",
                },
                {
                  id: 14972685287507,
                  product_id: 2350278148179,
                  position: 35,
                  created_at: "2020-09-04T19:21:11+05:30",
                  updated_at: "2020-09-04T19:29:42+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-14972635873363.jpg?v=1599227982",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14972685287507",
                },
                {
                  id: 14972671885395,
                  product_id: 2350278148179,
                  position: 36,
                  created_at: "2020-09-04T19:19:12+05:30",
                  updated_at: "2020-09-04T19:29:42+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-14972636954707.jpg?v=1599227982",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14972671885395",
                },
                {
                  id: 14972675457107,
                  product_id: 2350278148179,
                  position: 37,
                  created_at: "2020-09-04T19:19:44+05:30",
                  updated_at: "2020-09-04T19:29:42+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-14972637446227.jpg?v=1599227982",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14972675457107",
                },
                {
                  id: 14972660285523,
                  product_id: 2350278148179,
                  position: 38,
                  created_at: "2020-09-04T19:17:13+05:30",
                  updated_at: "2020-09-04T19:29:42+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-14972638167123.jpg?v=1599227982",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14972660285523",
                },
                {
                  id: 14972689252435,
                  product_id: 2350278148179,
                  position: 39,
                  created_at: "2020-09-04T19:21:44+05:30",
                  updated_at: "2020-09-04T19:29:42+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-14972639346771.jpg?v=1599227982",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14972689252435",
                },
                {
                  id: 14972692463699,
                  product_id: 2350278148179,
                  position: 40,
                  created_at: "2020-09-04T19:22:17+05:30",
                  updated_at: "2020-09-04T19:29:42+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-14972640460883.png?v=1599227982",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14972692463699",
                },
                {
                  id: 14972662874195,
                  product_id: 2350278148179,
                  position: 41,
                  created_at: "2020-09-04T19:17:45+05:30",
                  updated_at: "2020-09-04T19:29:42+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-14972641247315.jpg?v=1599227982",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14972662874195",
                },
                {
                  id: 14972665987155,
                  product_id: 2350278148179,
                  position: 42,
                  created_at: "2020-09-04T19:18:19+05:30",
                  updated_at: "2020-09-04T19:29:42+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-14972641542227.jpg?v=1599227982",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14972665987155",
                },
              ],
              image: {
                id: 13730777464915,
                product_id: 2350278148179,
                position: 1,
                created_at: "2019-12-24T08:59:41+05:30",
                updated_at: "2020-09-04T19:29:42+05:30",
                alt: "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                width: 1025,
                height: 1400,
                src:
                  "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-6976266829907.jpg?v=1599227982",
                variant_ids: [],
                admin_graphql_api_id:
                  "gid://shopify/ProductImage/13730777464915",
              },
            },
            mediaText: null,
            redirectUrl: null,
          },
          {
            id: 123,
            sectionId: 15,
            webViewTitle: null,
            contentType: 3,
            mediaUrl: null,
            product_json: {
              id: 125449863193,
              title: "Contour De Force Face Palette",
              body_html:
                '<p>Achieve jaw-dropping, #nofilter beauty looks that\'ll set your Insta-circle on fire with the <strong>SUGAR Contour De Force Face Palette</strong>. Crafted for the makeup maven who’s slaying goals on the go, this sleek all-in-one face palette houses a power-packed trio of bronzer, highlighter and blush – exactly what you’d love to stash in your clutch to create any look in a flash. Just mix, layer and blend these uber-creamy, lightweight powders for a great payoff that lasts all day, any day. See the magic unfold in the built-in mirror that’s super handy for on-the-spot application and prepare to be stunned with how easily this face palette takes your makeup game to a whole new level!</p>\n<p><strong>Available In: </strong></p>\n<p><strong>01 SUBTLE SUMMIT</strong></p>\n<p>This subtle-themed face palette contains one soft, cool shade each of highlighter, Mini Blushbronzer and blush, carefully picked to create a moderate look. It is all you need when you wish to keep things simple yet fabulous.  <br>(L to R): Taupe Topper (Cool-toned Brown Bronzer), Champagne Champion (Champagne Gold Highlighter) and Peach Peak (Soft Peach Pink Blush)</p>\n<p><strong>02 VIVID VICTORY</strong></p>\n<p>This vibrant-themed face palette contains one bold shade each of highlighter, bronzer and blush that sums up beautifully for a dramatic look. If your plans demand you to be vibrant and glamorous, this is what you want. <br>(L to R): Woody Wonder (Warm Brown Bronzer), Gold Glory (Golden Bronze Highlighter) and Pink Pinnacle (Deep Rose Blush)</p>\n<p><strong>How To Apply</strong></p>\n<ul>\n<li>Moisturise your face and apply primer. Apply foundation all over face and neck, then blend. Set the foundation with the SUGAR Dream Cover SPF15 Mattifying Compact or your existing compact.</li>\n<li>For an easy, every day contoured look, apply the bronzer under your cheekbones to sculpt and define. Use short brush strokes from the top of the cheekbone down towards the chin</li>\n<li>For a brilliant flush of colour use a blush brush and apply blush to the apples of the cheek.  Sweep the excess across the hairline, bridge of the nose and chin</li>\n<li>Apply pressed highlighter anywhere you want intense glow like the top of your cheekbones, over or under browbones, down the bridge of the nose, and even the inner corner of the eyes</li>\n</ul>\n<p><strong>Benefits:</strong> </p>\n<p>Each of the 3 powders in the <strong>SUGAR Contour De Force Face Palette</strong> has excellent shading, blends effortlessly without loss of pay-off and lasts for hours without flaking or caking. This product is dermatologically tested &amp; approved and 100% safe for your skin.</p>\n<p><strong></strong><strong>Additional Details: </strong>The <strong>SUGAR Contour De Force Face Palette</strong> comes in two striking options viz., <a href="https://in.sugarcosmetics.com/products/contour-de-force-face-palette-01-subtle-summit" target="_blank" rel="noopener noreferrer"><strong>01 Subtle Summit</strong></a> and <strong><a href="https://in.sugarcosmetics.com/products/contour-de-force-face-palette-02-vivid-victory" target="_blank" rel="noopener noreferrer">02 Vivid Victory</a></strong>. Each of the powders is also available as an individual mini. This product is free from parabens, D5, mineral oil and nano-ingredients.</p>\n<p><strong>List Of Ingredients: <br> </strong><u>Bronzer:</u> Talc, Mica, Dimethicone, Octyldodecyl Stearoyl Stearate, Isostearyl Neopentanoate, Zinc Stearate, Polybutene, Caprylyl Glycol, Phenoxyethanol, Hexylene Glycol, Isopropyl Isostearate, Tocopherol, Lecithin, Ascorbyl Palmitate, Glyceryl Stearate, Glyceryl Oleate, Citric Acid, May Contain: Iron Oxides, Red 7 Lake.<br><br> <u>Highlighter:</u> Talc, Mica, Isostearyl Neopentanoate, Zinc Stearate, Dimethicone, Diisostearyl Malate, Octyldodecyl Stearoyl Stearate, Polybutene, Isopropyl Isostearate, Caprylyl Glycol, Phenoxyethanol, Hexylene Glycol, Tocopherol, Lecithin,Ascorbyl Palmitate, Glyceryl Stearate, Glyceryl Oleate, Citric Acid, Tin Oxide, May Contain: Titanium Dioxide, Iron Oxides.</p>\n<p><u>Blush:</u> Talc, Mica, Caprylic/Capric Triglyceride, Dimethiconol Stearate, Magnesium Myristate, Dimethicone, Trimethylsiloxysilicate, Caprylyl Glycol, Phenoxyethanol, Hexylene Glycol, Aluminum Hydroxide, May Contain: Iron Oxides, Red 7 Lake, Yellow 5 Lake, Ultramarines, Blue 1 Lake.</p>\n<p><strong>MRP</strong><span>: 799 (incl. all taxes)</span><br><br><span></span><strong>Formulated</strong><span> in Italy</span><br><br><span></span><strong>Company Name</strong><span>: Regi India Cosmetics Pvt Ltd.</span><br><br><span></span><strong>Company Address</strong><span>: C-08, Sara Ind Estate, Vill-Rampur, Selaqui, Dehradun 248197, Uttarakhand.</span></p>\n<p> </p>',
              vendor: "SUGAR Cosmetics",
              product_type: "Blushes & Bronzers",
              created_at: "2017-11-24T16:14:10+05:30",
              handle: "contour-de-force-face-palette",
              updated_at: "2020-12-09T00:26:31+05:30",
              published_at: "2020-08-20T21:25:35+05:30",
              template_suffix: "",
              published_scope: "web",
              tags:
                "amp-hide, st_formulation: Pressed Powder, st_type: Compact, sugar_type_1",
              admin_graphql_api_id: "gid://shopify/Product/125449863193",
              variants: [
                {
                  id: 1137389174809,
                  product_id: 125449863193,
                  title: "01 Subtle Summit",
                  price: "799.00",
                  sku: "8906090492249",
                  position: 1,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "01 Subtle Summit",
                  option2: null,
                  option3: null,
                  created_at: "2017-11-24T16:14:10+05:30",
                  updated_at: "2020-12-09T00:26:31+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13727998574675,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 1107030474777,
                  inventory_quantity: 1328,
                  old_inventory_quantity: 1328,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/1137389174809",
                },
                {
                  id: 1137389207577,
                  product_id: 125449863193,
                  title: "02 Vivid Victory",
                  price: "799.00",
                  sku: "8906090492256",
                  position: 2,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "02 Vivid Victory",
                  option2: null,
                  option3: null,
                  created_at: "2017-11-24T16:14:10+05:30",
                  updated_at: "2020-12-08T23:56:35+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13736222556243,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 1107030507545,
                  inventory_quantity: 2509,
                  old_inventory_quantity: 2509,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/1137389207577",
                },
              ],
              options: [
                {
                  id: 164969873433,
                  product_id: 125449863193,
                  name: "Shades",
                  position: 1,
                  values: ["01 Subtle Summit", "02 Vivid Victory"],
                },
              ],
              images: [
                {
                  id: 13727998574675,
                  product_id: 125449863193,
                  position: 1,
                  created_at: "2019-12-23T13:45:23+05:30",
                  updated_at: "2019-12-25T13:41:19+05:30",
                  alt:
                    "SUGAR Cosmetics Contour De Force Face Palette 01 Subtle Summit",
                  width: 336,
                  height: 446,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-contour-de-force-face-palette-01-subtle-summit-627479150617.jpg?v=1577261479",
                  variant_ids: [1137389174809],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13727998574675",
                },
                {
                  id: 13736222556243,
                  product_id: 125449863193,
                  position: 2,
                  created_at: "2019-12-25T13:41:17+05:30",
                  updated_at: "2019-12-25T13:41:19+05:30",
                  alt:
                    "SUGAR Cosmetics Contour De Force Face Palette 02 Vivid Victory",
                  width: 600,
                  height: 900,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-contour-de-force-face-palette-02-vivid-victory-627479216153.jpg?v=1577261479",
                  variant_ids: [1137389207577],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13736222556243",
                },
              ],
              image: {
                id: 13727998574675,
                product_id: 125449863193,
                position: 1,
                created_at: "2019-12-23T13:45:23+05:30",
                updated_at: "2019-12-25T13:41:19+05:30",
                alt:
                  "SUGAR Cosmetics Contour De Force Face Palette 01 Subtle Summit",
                width: 336,
                height: 446,
                src:
                  "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-contour-de-force-face-palette-01-subtle-summit-627479150617.jpg?v=1577261479",
                variant_ids: [1137389174809],
                admin_graphql_api_id:
                  "gid://shopify/ProductImage/13727998574675",
              },
            },
            mediaText: null,
            redirectUrl: null,
          },
          {
            id: 126,
            sectionId: 15,
            webViewTitle: null,
            contentType: 3,
            mediaUrl: null,
            product_json: {
              id: 4375975395411,
              title: "Kohl Of Honour Intense Kajal - 01 Black Out (Black)",
              body_html:
                '<p><span style="font-weight: 400;">Ready to elevate your eye game to many notches above the ordinary? Step up to the world of extreme-wear, waterproof kajals with the SUGAR Kohl Of Honour Intense Kajal. Smudge and transfer-resistant, this breakthrough formula glides on smoothly for flawless, precise application and stays put for 12 straight hours. Ergonomically designed in an easy-to-use twist-up form, this wonder product delivers pigment-rich, high-impact colour that will fill your eyes with all the definition and drama you’ve ever known!</span></p>\n<p><span style="font-weight: 400;"><strong>01 Black Out (Black)</strong> – Every look intensified!</span></p>\n<p><span style="font-weight: 400;"><strong>Additional Details</strong>: This product does not require an additional sharpener.</span></p>\n<p><span style="font-weight: 400;"><strong>Net Weight</strong>: 0.25 gm.</span></p>\n<p><span style="font-weight: 400;"><strong>SUGAR Kohl Of Honour Intense Kajal Ingredients</strong>: Cyclopentasiloxane, Hydrogenated Microcrystalline Wax, Iron Oxides, Polybutene, Synthetic Bees Wax, Trimethylsiloxysilicate, Black 2, Paraffin, Talc, Carnauba Wax, Triethoxycaprylylsilane, Tocopherol, Sunflower Seed Oil &amp; BHT.</span></p>\n<p><span style="font-weight: 400;"><span><strong>Maximum Retail Price</strong>: Rs. </span><span>24</span><span>9 (incl. all taxes)</span></span></p>\n<p><span style="font-weight: 400;"><strong>Country of Origin</strong><span>: India</span></span></p>\n<p><span style="font-weight: 400;"><strong>Company Name</strong><span>:</span><span> Viva Cosmetics</span></span></p>\n<p><span style="font-weight: 400;"><strong>Company Address</strong><span>:</span><span> A/19 G4 Happy Valley, Nr Tikujiniwadi, Manpada, Thane W - 400610</span></span></p>\n<h5>How to apply</h5>\n<p><span style="font-weight: 400;">Swivel up and glide on SUGAR Kohl Of Honour Intense Kajal, using short strokes. Build the desired shape on your lash line for a more controlled and smoother application. This best kajal can be applied to your upper/lower lash line and also on the waterline - you can even blend over the entire eyelid for a smokey eye makeup effect. Re-cap securely after use. This formula is designed to be used alone or over eyeshadow.</span></p>\n<p><span style="font-weight: 400;"><strong>Best paired with</strong>:</span></p>\n<p><span style="font-weight: 400;">Go in with an </span><strong><a href="https://in.sugarcosmetics.com/collections/blend-the-rules-eyeshadow-palette/products/blend-the-rules-eyeshadow-palette-02-warrior-smokey">eyeshadow palette </a></strong><span style="font-weight: 400;">and create a simple, smokey look. Add loads of </span><strong><a href="https://in.sugarcosmetics.com/products/lash-mob-limitless-mascara-01-black-with-a-bang">lengthening mascara </a></strong><span style="font-weight: 400;">for long lashes. Finish with a </span><strong><a href="https://in.sugarcosmetics.com/products/face-fwd-highlighter-stick">pop of highlighter</a></strong><span style="font-weight: 400;">, pink blush to the cheeks, and a </span><strong><a href="https://in.sugarcosmetics.com/collections/nudes-lipsticks/products/matte-as-hell-crayon-lipstick-25-lily-aldrin-mauve-pink">nude lipstick</a></strong><span style="font-weight: 400;">.</span></p>\n<h5>Benefits</h5>\n<p><span style="font-weight: 400;">SUGAR Kohl of Honour Intense Kajal is powered by:<br>- Ultra-creamy texture (Glides on smoothly)<br>- 100% opacity (Super rich colour pay off)<br>- 12-hour stay power (No touch ups needed)</span></p>\n<h5>Commonly asked questions</h5>\n<p>Q. How long will it last on my eyes?<br>A. Up to 12 hours.<br><br>Q. Is it waterproof?<br>A. SUGAR Kohl Of Honour Intense Kajal is waterproof, smudgeproof, and even crackproof.<br> <br>Q. Is the product paraben free?<br>A. Yes.<br> <br>Q. How many shades does it come in?<br>A. Currently, just one.<br> <br>Q. Does it come with a sharpener?<br>A. The product doesn\'t need a sharpener. It comes in a twist-up packaging.</p>\n<p> </p>',
              vendor: "SUGAR Cosmetics",
              product_type: "Kajal",
              created_at: "2020-01-01T12:29:56+05:30",
              handle: "kohl-of-honour-intense-kajal-01-black-out-black",
              updated_at: "2020-12-09T00:31:21+05:30",
              published_at: "2020-01-08T13:31:56+05:30",
              template_suffix: "",
              published_scope: "web",
              tags:
                "12 Hour Wear, 249, Affordable, Black, Blister Pack, Bold Eyes, Bold Makeup, Budge Proof, Budget Friendly, College, Colossal, Creamy, Creamy Texture, Dark Black, Easy to Apply, Easy to Use, Economical, Essentials, Everyday, Eye Makeup, Eyeconic, Eyes, Festive, Glides Easily, Intense Pigmentation, Jet Black, Kajal, Kohl, Long Lasting, Made In India, Office, One Swipe Application, Onyx, Opaque, Pigmented, Pocket Friendly, Precise Application, Smooth Application, Smudge Proof, Soft, st_bestseller:8, st_Color: Black, st_feature: Black Kajal, st_feature: Crème, st_feature: Smudge Free, st_feature: Waterproof, st_finish: Creme, st_finish: Matte, st_formulation: Kajal, st_popularityscore: 9035, st_popularityscore: 9063, st_type: Kajal and Kohl, sugar_type_0, Transfer Proof, Travel Friendly, Twist up Kajal, Under 250, Water Proof, Water Resistant, Work",
              admin_graphql_api_id: "gid://shopify/Product/4375975395411",
              variants: [
                {
                  id: 31321217728595,
                  product_id: 4375975395411,
                  title: "Default Title",
                  price: "249.00",
                  sku: "8904320705886",
                  position: 1,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "Default Title",
                  option2: null,
                  option3: null,
                  created_at: "2020-01-01T12:29:56+05:30",
                  updated_at: "2020-12-09T00:31:21+05:30",
                  taxable: false,
                  barcode: "",
                  grams: 0,
                  image_id: null,
                  weight: 0.25,
                  weight_unit: "g",
                  inventory_item_id: 32885943500883,
                  inventory_quantity: 8292,
                  old_inventory_quantity: 8292,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/31321217728595",
                },
              ],
              options: [
                {
                  id: 5680575053907,
                  product_id: 4375975395411,
                  name: "Title",
                  position: 1,
                  values: ["Default Title"],
                },
              ],
              images: [
                {
                  id: 14935906025555,
                  product_id: 4375975395411,
                  position: 1,
                  created_at: "2020-08-31T13:00:27+05:30",
                  updated_at: "2020-08-31T13:00:27+05:30",
                  alt:
                    "SUGAR Cosmetics Kohl Of Honour Intense Kajal - 01 Black Out (Black)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-kohl-of-honour-intense-kajal-01-black-out-black-14935858970707.jpg?v=1598859027",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14935906025555",
                },
                {
                  id: 14935885119571,
                  product_id: 4375975395411,
                  position: 2,
                  created_at: "2020-08-31T12:52:53+05:30",
                  updated_at: "2020-08-31T13:00:27+05:30",
                  alt:
                    "SUGAR Cosmetics Kohl Of Honour Intense Kajal - 01 Black Out (Black)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-kohl-of-honour-intense-kajal-01-black-out-black-14935858937939.jpg?v=1598859027",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14935885119571",
                },
                {
                  id: 14935895507027,
                  product_id: 4375975395411,
                  position: 3,
                  created_at: "2020-08-31T12:57:47+05:30",
                  updated_at: "2020-08-31T13:00:27+05:30",
                  alt:
                    "SUGAR Cosmetics Kohl Of Honour Intense Kajal - 01 Black Out (Black)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-kohl-of-honour-intense-kajal-01-black-out-black-14935858839635.jpg?v=1598859027",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14935895507027",
                },
                {
                  id: 14935894261843,
                  product_id: 4375975395411,
                  position: 4,
                  created_at: "2020-08-31T12:57:12+05:30",
                  updated_at: "2020-08-31T13:00:27+05:30",
                  alt:
                    "SUGAR Cosmetics Kohl Of Honour Intense Kajal - 01 Black Out (Black)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-kohl-of-honour-intense-kajal-01-black-out-black-14935859003475.jpg?v=1598859027",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14935894261843",
                },
                {
                  id: 14935891607635,
                  product_id: 4375975395411,
                  position: 5,
                  created_at: "2020-08-31T12:56:23+05:30",
                  updated_at: "2020-08-31T13:00:27+05:30",
                  alt:
                    "SUGAR Cosmetics Kohl Of Honour Intense Kajal - 01 Black Out (Black)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-kohl-of-honour-intense-kajal-01-black-out-black-14935858905171.jpg?v=1598859027",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14935891607635",
                },
                {
                  id: 14935901470803,
                  product_id: 4375975395411,
                  position: 6,
                  created_at: "2020-08-31T12:59:22+05:30",
                  updated_at: "2020-08-31T13:00:27+05:30",
                  alt:
                    "SUGAR Cosmetics Kohl Of Honour Intense Kajal - 01 Black Out (Black)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-kohl-of-honour-intense-kajal-01-black-out-black-14935858806867.jpg?v=1598859027",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14935901470803",
                },
                {
                  id: 14935904321619,
                  product_id: 4375975395411,
                  position: 7,
                  created_at: "2020-08-31T12:59:55+05:30",
                  updated_at: "2020-08-31T13:00:27+05:30",
                  alt:
                    "SUGAR Cosmetics Kohl Of Honour Intense Kajal - 01 Black Out (Black)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-kohl-of-honour-intense-kajal-01-black-out-black-14935858872403.jpg?v=1598859027",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14935904321619",
                },
              ],
              image: {
                id: 14935906025555,
                product_id: 4375975395411,
                position: 1,
                created_at: "2020-08-31T13:00:27+05:30",
                updated_at: "2020-08-31T13:00:27+05:30",
                alt:
                  "SUGAR Cosmetics Kohl Of Honour Intense Kajal - 01 Black Out (Black)",
                width: 1025,
                height: 1400,
                src:
                  "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-kohl-of-honour-intense-kajal-01-black-out-black-14935858970707.jpg?v=1598859027",
                variant_ids: [],
                admin_graphql_api_id:
                  "gid://shopify/ProductImage/14935906025555",
              },
            },
            mediaText: null,
            redirectUrl: null,
          },
          {
            id: 127,
            sectionId: 15,
            webViewTitle: null,
            contentType: 3,
            mediaUrl: null,
            product_json: {
              id: 4352967049299,
              title: "Aquaholic Priming Moisturizer",
              body_html:
                '<p>It soothes skin from irritation &amp; dryness. Glides on easily, locks down moisture to keep the skin refreshed. It is ultra-lightweight, non-greasy and refreshing.<br><br>A perfect mix of makeup &amp; skincare, this is an absolute must-have.</p>\n<p><strong>How to Apply: </strong>Apply a pea-sized amount of this moisturizer and gently massage into your face by using gentle upward and outward strokes. It is buildable, so you can achieve desired effect. Use alone as a moisturizer or as a prep step before applying makeup. This 2 in 1 product acts as a primer and a moisturizer &amp; can be used every day.<br><br><strong>Benefits: SUGAR Aquaholic Priming Moisturizer</strong> is enriched with Moringa Seed extracts that help protect the skin from the effect of harmful pollutants. Hyaluronic acid lends a breathable film to the skin keeping it moist &amp; smooth. It is 60% water, starts out as a cream &amp; bursts into water molecules leaving a 100% matte-finish.<br><br><strong>Additional Details: SUGAR Aquaholic range </strong>comprises of 4 hydrating and soothing products viz., <strong>Aquaholic Priming Moisturizer, <a href="https://in.sugarcosmetics.com/products/aquaholic-water-boost-mask" target="_blank" rel="noopener noreferrer">Aquaholic Water Boost Mask</a>, <a href="https://in.sugarcosmetics.com/products/aquaholic-overnight-water-mask" target="_blank" rel="noopener noreferrer">Aquaholic Overnight Water Mask</a>, <a href="https://in.sugarcosmetics.com/products/aquaholic-hydrating-stick" target="_blank" rel="noopener noreferrer">Aquaholic Hydrating Stick</a>.<br><br></strong>Net Volume: 30 ml.<br><br><strong>List of Ingredients: </strong> Water, Cyclopentasiloxane, Dimethicone, Mica, Butylene Glycol, Cyclohexasiloxane, Dimethicone Crosspolymer, Glycerin, Trimethylsiloxysilicate, Propanediol, Polypropylsilsesquioxane, Isododecane, Parfum , Sodium Chloride, Dimethicone/PEG-10/15 Crosspolymer, 1,2-Hexanediol, PEG-9 Polydimethylsiloxyethyl Dimethicone, Phenoxyethanol, Dimethicone/Vinyl Dimethicone Crosspolymer, Sodium Hyaluronate, Caprylhydroxamic Acid, Disodium EDTA, Dipropylene Glycol, Tocopherol</p>\n<p><strong>MRP</strong><span>: Rs. 499 (incl. all taxes)</span><br><br><strong>Country of Origin</strong><span>: </span><span mce-data-marked="1">PRC</span><br><br><strong>Company Name</strong><span>:</span><span> SUGAR Cosmetics LLC</span><br><br><strong>Company Address</strong><span>:</span><span> 8 The Green Suite A, Dover, DE 19901, USA</span></p>',
              vendor: "SUGAR Cosmetics",
              product_type: "Moisturizer",
              created_at: "2019-11-20T15:07:57+05:30",
              handle: "aquaholic-priming-moisturizer",
              updated_at: "2020-12-09T00:26:01+05:30",
              published_at: "2019-11-20T16:20:08+05:30",
              template_suffix: "",
              published_scope: "web",
              tags:
                "2 in 1, 699, Alcohol free, Cream, Cruelty free, Essentials, Fresh, Gluten free, Hyaluronic Acid, Hydrate, Hydrating, Lightweight, Makeup, Matte, Matte Finish, Moisturise, Moisturiser, Moisturizer, Moringa Seed, Non Greasy, Non Sticky, Nourish, Paraben free, Prime, Primer, Priming Moisturizer, Refreshing, Retains Moisture, Skin, Skincare, Soft, st_bestseller:13, st_concern: Hydration, st_popularityscore: 9055, st_range: Aquaholic, sugar_type_0, Sulphate free, Summers, Supple, Under 700, Vegan, Water, Water Base",
              admin_graphql_api_id: "gid://shopify/Product/4352967049299",
              variants: [
                {
                  id: 31194979729491,
                  product_id: 4352967049299,
                  title: "Default Title",
                  price: "499.00",
                  sku: "8904320701246",
                  position: 1,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "Default Title",
                  option2: null,
                  option3: null,
                  created_at: "2019-11-20T15:07:57+05:30",
                  updated_at: "2020-12-09T00:26:01+05:30",
                  taxable: false,
                  barcode: "",
                  grams: 30,
                  image_id: null,
                  weight: 30,
                  weight_unit: "g",
                  inventory_item_id: 32716419563603,
                  inventory_quantity: 3865,
                  old_inventory_quantity: 3865,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/31194979729491",
                },
              ],
              options: [
                {
                  id: 5651258605651,
                  product_id: 4352967049299,
                  name: "Title",
                  position: 1,
                  values: ["Default Title"],
                },
              ],
              images: [
                {
                  id: 13736578809939,
                  product_id: 4352967049299,
                  position: 1,
                  created_at: "2019-12-25T15:24:37+05:30",
                  updated_at: "2020-04-22T12:58:25+05:30",
                  alt: "SUGAR Cosmetics Aquaholic Priming Moisturizer",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-aquaholic-priming-moisturizer-13548826886227.jpg?v=1587540505",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13736578809939",
                },
                {
                  id: 14280392573011,
                  product_id: 4352967049299,
                  position: 2,
                  created_at: "2020-04-22T12:58:25+05:30",
                  updated_at: "2020-04-22T12:58:25+05:30",
                  alt: "SUGAR Cosmetics Aquaholic Priming Moisturizer",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-aquaholic-priming-moisturizer-14280379498579.jpg?v=1587540505",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14280392573011",
                },
                {
                  id: 13736579629139,
                  product_id: 4352967049299,
                  position: 3,
                  created_at: "2019-12-25T15:24:55+05:30",
                  updated_at: "2020-04-22T12:58:25+05:30",
                  alt: "SUGAR Cosmetics Aquaholic Priming Moisturizer",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-aquaholic-priming-moisturizer-13548826918995.jpg?v=1587540505",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13736579629139",
                },
                {
                  id: 13736582643795,
                  product_id: 4352967049299,
                  position: 4,
                  created_at: "2019-12-25T15:26:20+05:30",
                  updated_at: "2020-04-22T12:58:25+05:30",
                  alt: "SUGAR Cosmetics Aquaholic Priming Moisturizer",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-aquaholic-priming-moisturizer-13548905332819.jpg?v=1587540505",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13736582643795",
                },
              ],
              image: {
                id: 13736578809939,
                product_id: 4352967049299,
                position: 1,
                created_at: "2019-12-25T15:24:37+05:30",
                updated_at: "2020-04-22T12:58:25+05:30",
                alt: "SUGAR Cosmetics Aquaholic Priming Moisturizer",
                width: 1025,
                height: 1400,
                src:
                  "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-aquaholic-priming-moisturizer-13548826886227.jpg?v=1587540505",
                variant_ids: [],
                admin_graphql_api_id:
                  "gid://shopify/ProductImage/13736578809939",
              },
            },
            mediaText: null,
            redirectUrl: null,
          },
          {
            id: 128,
            sectionId: 15,
            webViewTitle: null,
            contentType: 3,
            mediaUrl: null,
            product_json: {
              id: 4352974979155,
              title: "Aquaholic Hydrating Stick",
              body_html:
                '<p>It provides instant <strong>hydration</strong>, nourishes skin and provides skin protection. Soothes puffy eyes and keeps you looking refreshed all day long.</p>\n<p>You just have to dive in!</p>\n<p><strong>How to Apply</strong>: Gently use the SUGAR Aquaholic Hydrating Stick on areas to relieve them from dehydration symptoms and massage until absorbed. Apply under eyes to remove puffiness.</p>\n<p><strong>Benefits</strong>: <strong>SUGAR Aquaholic Hydrating Stick</strong> hydrates the skin and instantly moisturizes leaving it soft, supple and healthy-looking. It aids in relieving puffiness around eyes, gives a cooling sensation and is travel-friendly.</p>\n<p><strong>Additional Details</strong>: <strong>SUGAR Aquaholic</strong> range comprises of 4 hydrating and soothing products viz., <strong>Aquaholic Priming Moisturizer</strong>, <strong>Aquaholic Water Boost Mask</strong>, <strong>Aquaholic Overnight Water Mask</strong>, <strong>Aquaholic Hydrating Stick</strong>.</p>\n<p><strong>Net Weight</strong>: 32g</p>\n<p><strong>List of Ingredients</strong>:  AQUA, GLYCERIN, SODIUM STEARATE, BUTYLENE GLYCOL, BIS-PEG-18 METHYL ETHER DIMETHYL SILANE, CAMELLIA SINENSIS LEAF EXTRACT, ALOE BARBADENSIS LEAF JUICE, PHENOXYETHANOL, MENTHOXYPROPANEDIOL, HAMAMELIS VIRGINIANA (WITCH HAZEL) EXTRACT, MENTHOL, SODIUM HYALURONATE.<br></p>\n<p><strong>MRP</strong><span>: Rs. 899 (incl. all taxes)</span><br><br><strong>Country of Origin</strong><span>: </span><span mce-data-marked="1">PRC</span><br><br><strong>Company Name</strong><span>:</span><span> SUGAR Cosmetics LLC</span><br><br><strong>Company Address</strong><span>:</span><span> 8 The Green Suite A, Dover, DE 19901, USA</span></p>',
              vendor: "SUGAR Cosmetics",
              product_type: "Hydrating Stick",
              created_at: "2019-11-20T16:10:58+05:30",
              handle: "aquaholic-hydrating-stick",
              updated_at: "2021-01-13T09:55:50+05:30",
              published_at: "2019-11-20T16:20:05+05:30",
              template_suffix: "",
              published_scope: "web",
              tags:
                "999, Aqua, Aquaholic, Blue, Cooling, Cruelty Free, Easy to use, Elastic, Essentials, Face, Fresh, Healthy Skin, Hydrated Skin, Hydrating, Hydrating Stick, Hydration, Massage, Mess Free, Moisturising, Moisturizing, Non Sticky, Nourishes, Paraben Free, Reduce Puffiness, Refreshing, Retains Moisture, Skin, Skin Care, Skin Protection, Soft, Soothing, st_ concern: Oil control, st_ concern: Sun protection, st_concern: Hydration, st_range: Aquaholic, Stick, sugar_type_0, Summers, Supple, Travel Friendly, Under 1000, Water, Water Base",
              admin_graphql_api_id: "gid://shopify/Product/4352974979155",
              variants: [
                {
                  id: 31195033239635,
                  product_id: 4352974979155,
                  title: "Default Title",
                  price: "899.00",
                  sku: "8904320702113",
                  position: 1,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "Default Title",
                  option2: null,
                  option3: null,
                  created_at: "2019-11-20T16:10:58+05:30",
                  updated_at: "2021-01-13T09:55:50+05:30",
                  taxable: false,
                  barcode: "",
                  grams: 32,
                  image_id: null,
                  weight: 32,
                  weight_unit: "g",
                  inventory_item_id: 32716517933139,
                  inventory_quantity: 8441,
                  old_inventory_quantity: 8441,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/31195033239635",
                },
              ],
              options: [
                {
                  id: 5651270762579,
                  product_id: 4352974979155,
                  name: "Title",
                  position: 1,
                  values: ["Default Title"],
                },
              ],
              images: [
                {
                  id: 13736575074387,
                  product_id: 4352974979155,
                  position: 1,
                  created_at: "2019-12-25T15:22:36+05:30",
                  updated_at: "2020-04-22T12:39:13+05:30",
                  alt: "SUGAR Cosmetics Aquaholic Hydrating Stick",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-aquaholic-hydrating-stick-13548839731283.jpg?v=1587539353",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13736575074387",
                },
                {
                  id: 14280328609875,
                  product_id: 4352974979155,
                  position: 2,
                  created_at: "2020-04-22T12:39:12+05:30",
                  updated_at: "2020-04-23T11:24:33+05:30",
                  alt: "SUGAR Cosmetics Aquaholic Hydrating Stick",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-aquaholic-hydrating-stick-14280318353491.jpg?v=1587621273",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14280328609875",
                },
                {
                  id: 13736575598675,
                  product_id: 4352974979155,
                  position: 3,
                  created_at: "2019-12-25T15:22:53+05:30",
                  updated_at: "2020-04-23T11:24:33+05:30",
                  alt: "SUGAR Cosmetics Aquaholic Hydrating Stick",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-aquaholic-hydrating-stick-13548839796819.jpg?v=1587621273",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13736575598675",
                },
                {
                  id: 13736577925203,
                  product_id: 4352974979155,
                  position: 4,
                  created_at: "2019-12-25T15:24:22+05:30",
                  updated_at: "2020-04-23T11:24:33+05:30",
                  alt: "SUGAR Cosmetics Aquaholic Hydrating Stick",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-aquaholic-hydrating-stick-13548905103443.jpg?v=1587621273",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13736577925203",
                },
              ],
              image: {
                id: 13736575074387,
                product_id: 4352974979155,
                position: 1,
                created_at: "2019-12-25T15:22:36+05:30",
                updated_at: "2020-04-22T12:39:13+05:30",
                alt: "SUGAR Cosmetics Aquaholic Hydrating Stick",
                width: 1025,
                height: 1400,
                src:
                  "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-aquaholic-hydrating-stick-13548839731283.jpg?v=1587539353",
                variant_ids: [],
                admin_graphql_api_id:
                  "gid://shopify/ProductImage/13736575074387",
              },
            },
            mediaText: null,
            redirectUrl: null,
          },
          {
            id: 130,
            sectionId: 15,
            webViewTitle: null,
            contentType: 3,
            mediaUrl: null,
            product_json: {
              id: 4313455722579,
              title: "All Set To Go Banana Powder",
              body_html:
                "Go ahead and set things right as<span> </span><strong>SUGAR All Set To Go Banana Powder</strong><span> </span>makes its way to the gorgeous makeup clan. A flattering setting powder that diminishes the appearance of fine lines, absorbs oil and colour corrects discolouration, its yellow/golden tone suits all skin tones. This multi-purpose powder gives you a brighter, flawless mattified skin minus the pores and flashbacks. Used by beginners to dust off excess oil from the face and by makeup artists for baking technique &amp; contouring, this product is a step towards advanced makeup. Making your makeup last longer, it comes with no added fragrances. \n<div><br></div>\n<div>\n<strong>How to Apply:<span> </span></strong>Dab the product under eyes or over your face using a puff or a powder brush. To bake makeup, dab<span> </span><strong>SUGAR All Set To Go Banana Powder</strong><span> </span>and let it set for 5 minutes. This lets your base set properly without any creases. Dust the powder properly to avoid patches. <br><br>\n</div>\n<div>\n<strong>Benefits</strong>:<span> </span><strong>SUGAR All Set To Go Banana Powder<span> </span></strong>colour corrects and reduces shine by absorbing oil. Giving a luminous highlight to the skin, it sets makeup for a longer wear and reduces the look of wrinkles &amp; fine lines. <br><br>\n</div>\n<div>\n<strong>Additional Details: SUGAR All Set To Go Banana Powder<span> </span></strong>has a yellow/golden tint. It is also used for contouring and gives the face a more chiseled look. It is paraben-free, cruelty-free, vegan and suits all skin tones. Comes with a puff applicator for smooth application.<br><br>\n</div>\n<div>Net Weight: 7g<br><br>\n</div>\n<div>\n<strong>List of Ingredients:<span> </span></strong>Talc, Nylon-12, Boron Nitride, Bismuth Oxychloride, Pentaerythrityl Tetraisostearate, Diisostearyl Malate, Silicone, Tocopheryl Acetate, Phenoxyethanol, Iron Oxide Yellow (CI 77492), Titanium Dioxide(CI 77891)<br><br>\n</div>\n<div></div>\n<div>\n<strong>MRP</strong>: 599 (incl. all taxes)<br><br><strong>Country of Origin</strong>: PRC<br><br><strong>Company Name</strong>: SUGAR Cosmetics LLC.<br><br><strong>Company Address</strong>: 8 The Green Suite A, Dover, DE 19901, USA.</div>",
              vendor: "SUGAR Cosmetics",
              product_type: "Powder",
              created_at: "2019-10-24T17:03:30+05:30",
              handle: "all-set-to-go-banana-powder",
              updated_at: "2021-01-09T16:56:11+05:30",
              published_at: "2019-11-22T10:38:06+05:30",
              template_suffix: "",
              published_scope: "web",
              tags:
                "599, Alcohol free, All skin tones, Baking, Blendable, Blur pores, Carmine free, Colour correction, Contouring, Cruelty free, Dailywear, Easy application, Extremly Light, Face, Fixes discolouration, Handy, High Coverage, Intense pigment, Loose powder, Luminous, Luminous finish, Make up, Matte, Mattifying Powder, Melt proof makeup, Minimise lines, Mirror, NEW, Oil free look, Paraben free, Partywear, Powder, Puff, Radiant glow, Seamless, Set, Sets makeup, Setting, Setting powder, Shine free look, st_ concern: Brightening, st_bestseller:8, st_concern: Under eye, st_feature: Translucent Powder, st_finish: Natural, st_formulation: Loose Powder, st_popularityscore: 9007, st_type: Powders, sugar_type_0, Suphate free, Sweatproof, Under 600, Vegan, Water resistant, Yellow golden undertone, Yellow macaroon",
              admin_graphql_api_id: "gid://shopify/Product/4313455722579",
              variants: [
                {
                  id: 30988231573587,
                  product_id: 4313455722579,
                  title: "Default Title",
                  price: "599.00",
                  sku: "8904320703165",
                  position: 1,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "Default Title",
                  option2: null,
                  option3: null,
                  created_at: "2019-10-24T17:03:31+05:30",
                  updated_at: "2021-01-09T16:56:11+05:30",
                  taxable: false,
                  barcode: "",
                  grams: 7,
                  image_id: null,
                  weight: 7,
                  weight_unit: "g",
                  inventory_item_id: 32446241996883,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/30988231573587",
                },
              ],
              options: [
                {
                  id: 5605223202899,
                  product_id: 4313455722579,
                  name: "Title",
                  position: 1,
                  values: ["Default Title"],
                },
              ],
              images: [
                {
                  id: 13727901155411,
                  product_id: 4313455722579,
                  position: 1,
                  created_at: "2019-12-23T12:31:54+05:30",
                  updated_at: "2020-05-11T22:33:00+05:30",
                  alt: "SUGAR Cosmetics All Set To Go Banana Powder",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-all-set-to-go-banana-powder-13289662218323.jpg?v=1589216580",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13727901155411",
                },
                {
                  id: 14376589885523,
                  product_id: 4313455722579,
                  position: 2,
                  created_at: "2020-05-11T22:33:00+05:30",
                  updated_at: "2020-05-11T22:33:00+05:30",
                  alt: "SUGAR Cosmetics All Set To Go Banana Powder",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-all-set-to-go-banana-powder-14376570912851.jpg?v=1589216580",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14376589885523",
                },
                {
                  id: 13727899910227,
                  product_id: 4313455722579,
                  position: 3,
                  created_at: "2019-12-23T12:31:37+05:30",
                  updated_at: "2020-05-11T22:33:00+05:30",
                  alt: "SUGAR Cosmetics All Set To Go Banana Powder",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-all-set-to-go-banana-powder-13289662152787.jpg?v=1589216580",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13727899910227",
                },
                {
                  id: 13727898959955,
                  product_id: 4313455722579,
                  position: 4,
                  created_at: "2019-12-23T12:31:20+05:30",
                  updated_at: "2020-05-11T22:33:00+05:30",
                  alt: "SUGAR Cosmetics All Set To Go Banana Powder",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-all-set-to-go-banana-powder-13289662185555.jpg?v=1589216580",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13727898959955",
                },
                {
                  id: 13736567865427,
                  product_id: 4313455722579,
                  position: 5,
                  created_at: "2019-12-25T15:20:41+05:30",
                  updated_at: "2020-05-11T22:33:00+05:30",
                  alt: "SUGAR Cosmetics All Set To Go Banana Powder",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-all-set-to-go-banana-powder-13289662251091.jpg?v=1589216580",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13736567865427",
                },
                {
                  id: 13736568488019,
                  product_id: 4313455722579,
                  position: 6,
                  created_at: "2019-12-25T15:20:54+05:30",
                  updated_at: "2020-05-11T22:33:00+05:30",
                  alt: "SUGAR Cosmetics All Set To Go Banana Powder",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-all-set-to-go-banana-powder-13289662283859.jpg?v=1589216580",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13736568488019",
                },
              ],
              image: {
                id: 13727901155411,
                product_id: 4313455722579,
                position: 1,
                created_at: "2019-12-23T12:31:54+05:30",
                updated_at: "2020-05-11T22:33:00+05:30",
                alt: "SUGAR Cosmetics All Set To Go Banana Powder",
                width: 1025,
                height: 1400,
                src:
                  "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-all-set-to-go-banana-powder-13289662218323.jpg?v=1589216580",
                variant_ids: [],
                admin_graphql_api_id:
                  "gid://shopify/ProductImage/13727901155411",
              },
            },
            mediaText: null,
            redirectUrl: null,
          },
          {
            id: 131,
            sectionId: 15,
            webViewTitle: null,
            contentType: 3,
            mediaUrl: null,
            product_json: {
              id: 1563643019347,
              title: "Base Of Glory Pore Minimizing Primer",
              body_html:
                'Ladies, you have got yourself a winner. The much-awaited and the most radiant superstar, <strong>The Base Of Glory Pore Minimizing Primer </strong>is here! This satiny-smooth, luxurious wonder is a lifesaver for the girl on-the-go! Wear it alone or under makeup, its weightless texture and no-clog no-block formula mattifies on your skin perfectly, minimizes shine and keeps excess oil at bay and that’s not all. A little goes a long way with this skin-perfector, as a pea-sized amount creates the definition of a flawless base whilst blurring out wrinkles and fine lines. Making your skin the smoothest canvas for all the beauty to follow, this primer is surely a fan-favourite!<br><br><strong>How To Apply:</strong> Wash your face and apply moisturizer. Squeeze a pea-sized amount of primer onto your fingertips and spread evenly on your face. Blend it properly and make sure you don’t forget the skin around eyes. Use it as a base for other makeup products.\n<p><br><strong>Benefits:</strong> Base Of Glory Pore Minimizing Primer’s satin-smooth formula mattifies and creates smooth, flawless skin. It minimizes shine, prevents oil from disturbing your makeup and makes the foundation last longer. With a no-block no-clog formula, this skin-perfector efficiently evens out skin and blurs out the pores without clogging them.<br><br><strong>Additional Details: </strong>The Base Of Glory Pore Minimizing Primer is free from parabens, oil and mineral oil. This product is dermatologically tested &amp; approved and 100% safe for your skin.<br><br><strong>Net Volume</strong>: 30ml<br><br><strong>List Of Ingredient: </strong>Isoddecane, Retinyl Palmitate, Tocopheryl Acetate, Cyclopentasiloxane and Dimethicone/Vinyl Dimethicone Crosspolymer.</p>\n<p><strong>MRP</strong><span>: Rs. 799 (incl. all taxes)</span><br><br><strong>Country of Origin</strong><span>: </span><span mce-data-marked="1">India</span><br><br><strong>Company Name</strong><span>:</span><span> Cosmic Nutracos Solutions Pvt Ltd</span><br><br><strong>Company Address</strong><span>:</span><span> Cosmic Nutracos Solutions Pvt. Ltd., Plot 12, Bhatolikalan, PO Baddi District Solan - 173205</span></p>',
              vendor: "SUGAR Cosmetics",
              product_type: "Primer",
              created_at: "2018-11-06T15:42:36+05:30",
              handle: "base-of-glory-pore-minimizing-primer",
              updated_at: "2021-01-13T12:55:52+05:30",
              published_at: "2019-05-03T14:30:05+05:30",
              template_suffix: "",
              published_scope: "web",
              tags:
                "Base, Base coat for Face, Before Make Up, Before Makeup, Crease Free, Cruelty Free, Dermatologically Tested, Essentials, Everyday Wear, Face, Face Primer, Festive Wear, Flawless Skin, For All Skin Types, Gel Based, Halal, Hydrating Finish, Light Weight, Locks Moisture, Long Lasting, Make Up Longevity, Matte Finish, Mineral Oil Free, Minimize Pores, Minimize Shine, Non Cakey, Office Wear, Oil Control, Paraben Free, Prep, Prevents Crease, Primer, Safe For Skin, Satin Smooth Formula, Skin Perfector, Smooth Application, Smooth Base, st_bestseller:10, st_concern: Blurring, st_feature: Primer, st_formulation: Cream, st_popularityscore: 9009, st_type: Primer, sugar_type_0, Sulphate Free, Travel Friendly, Under 800, Vegan Free, Vegetarian, Wedding Wear, Weightless Texture, White",
              admin_graphql_api_id: "gid://shopify/Product/1563643019347",
              variants: [
                {
                  id: 15385362825299,
                  product_id: 1563643019347,
                  title: "Default Title",
                  price: "799.00",
                  sku: "8906090497428",
                  position: 1,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "Default Title",
                  option2: null,
                  option3: null,
                  created_at: "2018-11-06T15:42:36+05:30",
                  updated_at: "2021-01-13T12:55:52+05:30",
                  taxable: false,
                  barcode: "",
                  grams: 0,
                  image_id: null,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 15511096393811,
                  inventory_quantity: 4848,
                  old_inventory_quantity: 4848,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/15385362825299",
                },
              ],
              options: [
                {
                  id: 2131417301075,
                  product_id: 1563643019347,
                  name: "Title",
                  position: 1,
                  values: ["Default Title"],
                },
              ],
              images: [
                {
                  id: 13727920783443,
                  product_id: 1563643019347,
                  position: 1,
                  created_at: "2019-12-23T12:49:56+05:30",
                  updated_at: "2020-04-22T13:04:30+05:30",
                  alt: "SUGAR Cosmetics Base Of Glory Pore Minimizing Primer",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-base-of-glory-pore-minimizing-primer-12774767624275.jpg?v=1587540870",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13727920783443",
                },
                {
                  id: 14280415576147,
                  product_id: 1563643019347,
                  position: 2,
                  created_at: "2020-04-22T13:04:29+05:30",
                  updated_at: "2020-04-22T13:04:30+05:30",
                  alt: "SUGAR Cosmetics Base Of Glory Pore Minimizing Primer",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-base-of-glory-pore-minimizing-primer-14280393588819.jpg?v=1587540870",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14280415576147",
                },
                {
                  id: 13727919997011,
                  product_id: 1563643019347,
                  position: 3,
                  created_at: "2019-12-23T12:49:39+05:30",
                  updated_at: "2020-04-22T13:04:30+05:30",
                  alt: "SUGAR Cosmetics Base Of Glory Pore Minimizing Primer",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-base-of-glory-pore-minimizing-primer-12774767657043.jpg?v=1587540870",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13727919997011",
                },
                {
                  id: 13727919800403,
                  product_id: 1563643019347,
                  position: 4,
                  created_at: "2019-12-23T12:49:22+05:30",
                  updated_at: "2020-04-22T13:04:30+05:30",
                  alt: "SUGAR Cosmetics Base Of Glory Pore Minimizing Primer",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-base-of-glory-pore-minimizing-primer-12774767853651.jpg?v=1587540870",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13727919800403",
                },
                {
                  id: 13736609185875,
                  product_id: 1563643019347,
                  position: 5,
                  created_at: "2019-12-25T15:36:54+05:30",
                  updated_at: "2020-04-22T13:04:30+05:30",
                  alt: "SUGAR Cosmetics Base Of Glory Pore Minimizing Primer",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-base-of-glory-pore-minimizing-primer-12774767984723.jpg?v=1587540870",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13736609185875",
                },
              ],
              image: {
                id: 13727920783443,
                product_id: 1563643019347,
                position: 1,
                created_at: "2019-12-23T12:49:56+05:30",
                updated_at: "2020-04-22T13:04:30+05:30",
                alt: "SUGAR Cosmetics Base Of Glory Pore Minimizing Primer",
                width: 1025,
                height: 1400,
                src:
                  "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-base-of-glory-pore-minimizing-primer-12774767624275.jpg?v=1587540870",
                variant_ids: [],
                admin_graphql_api_id:
                  "gid://shopify/ProductImage/13727920783443",
              },
            },
            mediaText: null,
            redirectUrl: null,
          },
          {
            id: 132,
            sectionId: 15,
            webViewTitle: null,
            contentType: 3,
            mediaUrl: null,
            product_json: {
              id: 4410900250707,
              title: "Wingman Waterproof Microliner - 01 I'll Be Black (Black)",
              body_html:
                '<p><span><span style="font-weight: 400;">A highly pigmented and waterproof eyeliner, SUGAR Wingman Waterproof Microliner delivers long-lasting 12-hour wear. It\'s smudgeproof, transferproof and settles to a semi-matte finish. The micro felt-tip aids in smooth and precise application, perfect for all your </span><strong><a href="https://in.sugarcosmetics.com/collections/all-eyeliner-collection/products/eye-told-you-so-smudgeproof-eyeliner">winged eyeliner needs</a></strong><span style="font-weight: 400;">!</span></span></p>\n<p><span><span style="font-weight: 400;"><strong>01 I\'ll Be Black (Black)</strong> - Perfect for anytime, any wear!</span></span></p>\n<p><span style="font-weight: 400;"><strong>Additional Details</strong>: Shake the eyeliner pen before using, so the formula saturates the felt tip. For best results, wipe the tip with a damp tissue before applying. Use warm water for easy removal.</span></p>\n<p><span style="font-weight: 400;"><strong>Net Volume</strong>: 1.6 ml</span></p>\n<p><span style="font-weight: 400;"><strong>SUGAR Wingman Waterproof Microliner Ingredients</strong>:  Water (Aqua), Phenoxyethanol, Sodium Laureth-12 Sulfate, Sodium Dehydroacetate, Acrylates/Dimethylaminoethyl Methacrylate Copolymer, Peg-60 Hydrogenated Castor Oil, 1,2-hexanediol, Sodium Lignosulfonate, Simethicone, Polysorbate 65, Potassium Sorbate, C11-15 Pareth-7, Caprylyl Glycol, Acrylates/Ethylhexyl Acrylate Copolymer, Black 2 Ci 77266 (Nano), Butylene Glycol, Sorbic Acid, Glyceryl Stearate, Methylcellulose, Sulphuric Acid</span></p>\n<p><span style="font-weight: 400;">Maximum Retail Price: Rs. 499 (incl. all taxes)</span></p>\n<p><span style="font-weight: 400;">Country of Origin: Italy</span></p>\n<p><span style="font-weight: 400;">Company Name: SUGAR Cosmetics LLC</span></p>\n<p><span style="font-weight: 400;">Company Address: 8 The Green Suite A, Dover, DE 19901, USA</span></p>\n<h5>How to apply</h5>\n<p><span style="font-weight: 400;">For a subtle defining line, draw the tip from the inner to the outer eye corner, sticking as close to the lash line as possible. Slant the tip of SUGAR Wingman Waterproof Microliner and add more pressure to create a bolder line or continue to layer until you achieve desired definition. </span></p>\n<p><span style="font-weight: 400;"><strong>Best paired with</strong>:</span></p>\n<p><span style="font-weight: 400;">Go for an exquisite <strong><a href="https://in.sugarcosmetics.com/collections/blend-the-rules-eyeshadow-palette/products/blend-the-rules-eyeshadow-palette-03-fantasy-mauve">eyeshadow palette</a></strong> and finish off with a <strong><a href="https://in.sugarcosmetics.com/products/uptown-curl-lengthening-mascara-01-black-beauty">curling mascara </a></strong>or a lengthening mascara. Finish with a <strong><a href="https://in.sugarcosmetics.com/collections/lips/products/plush-crush-creme-crayon">lip colour </a></strong>of your choice.</span></p>\n<h5>Benefits</h5>\n<p><span style="font-weight: 400;">SUGAR WIngman Waterproof Microliner is powered by:</span></p>\n<p><span style="font-weight: 400;">Waterproof formula (Doesn’t budge in humid conditions)</span></p>\n<p><span style="font-weight: 400;">Micro felt tip (For great precision)</span></p>\n<p><span style="font-weight: 400;">Intense pigment (For one-stroke wonder)</span></p>\n<h5><strong>Commonly asked questions</strong></h5>\n<strong> </strong>\n<p>Q. What’s the finish like of SUGAR Wingman Waterproof Microliner:<br>A. You will get a semi-matte finish.<br> <br>Q. How many shades does it come?<br>A. Currently, there’s just one shade - 01 I\'ll Be Black (Black)<br><br>Q. I’m new to eyeliners, will it be easy to apply?<br>A. Absolutely! The micro tip eyeliner pen helps with easy and precise application. For a handy guide to apply eyeliners if you’re new to it, read more. <br><br>Q. What’s the best way to remove the product?<br>A. You can remove it easily with warm water.<br><br>Q. What is the country of origin?<br>A. The product is made in Italy.</p>\n<strong></strong>',
              vendor: "SUGAR Cosmetics",
              product_type: "Eyeliner",
              created_at: "2020-03-03T10:34:36+05:30",
              handle: "wingman-waterproof-microliner-01-ill-be-black-black",
              updated_at: "2021-01-13T13:00:31+05:30",
              published_at: "2020-03-03T10:28:02+05:30",
              template_suffix: "",
              published_scope: "web",
              tags:
                "10 HR, 12 HR, 499, 6M PAO, 9 to 5, Affordable, Air tight, Alcohol Free, Black, Bold application, Budget friendly, College, Complete control, Crackproof, Cruelty Free, Deep colour, Easy to remove, Economic, Everyday, Eye, Eye Makeup, Eye pen, Eye shaping contours, Eyeliner, Fadeproof, Felt tip, Film forming polymers, Fine tip, Flexible film, Gluten Free, High quality carbon, High staying power, Highly pigmented, Ink, Intense, Intense pigment, Italy, Jet black formula, Lash line, Long lasting, Longwear, Marker eyeliner, matte eyeliner, Micro felt tip, Micro precision tip, Office, Paraben Free, Party, Pen eyeliner, Pen Liner, Pocket friendly, Precise application, Precise Wing, Quick drying, Retro Black Fluidline Pen, Rub off resistant, Semi matte finish, Sketch pen liner, Sleek Application, Slim, Slim marker, Smooth application, st_bestseller:11, st_color: Black, st_feature: Eyeliner, st_feature: Microliner, st_finish: Matte, st_formulation: Pen, st_popularityscore: 9010, st_type: Eyeliner, sugar_type_0, Transfer, Travel friendly, Truest Black pigment, Under 500, Value for money, Vegan, Vegetarian, Water resistant, Water resistant liquid eyeliner, Waterproof, Weightless, Wing",
              admin_graphql_api_id: "gid://shopify/Product/4410900250707",
              variants: [
                {
                  id: 31578636517459,
                  product_id: 4410900250707,
                  title: "Default Title",
                  price: "499.00",
                  sku: "8904320704360",
                  position: 1,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "Default Title",
                  option2: null,
                  option3: null,
                  created_at: "2020-03-03T10:34:36+05:30",
                  updated_at: "2021-01-13T13:00:31+05:30",
                  taxable: false,
                  barcode: "",
                  grams: 2,
                  image_id: null,
                  weight: 1.6,
                  weight_unit: "g",
                  inventory_item_id: 33236948353107,
                  inventory_quantity: 4351,
                  old_inventory_quantity: 4351,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/31578636517459",
                },
              ],
              options: [
                {
                  id: 5728268681299,
                  product_id: 4410900250707,
                  name: "Title",
                  position: 1,
                  values: ["Default Title"],
                },
              ],
              images: [
                {
                  id: 14233690013779,
                  product_id: 4410900250707,
                  position: 1,
                  created_at: "2020-04-10T11:13:14+05:30",
                  updated_at: "2020-04-10T11:13:15+05:30",
                  alt:
                    "SUGAR Cosmetics Wingman Waterproof Microliner - 01 I'll Be Black (Black)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-wingman-waterproof-microliner-01-i-ll-be-black-black-14233683034195.jpg?v=1586497395",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14233690013779",
                },
                {
                  id: 14164747976787,
                  product_id: 4410900250707,
                  position: 2,
                  created_at: "2020-03-13T11:55:11+05:30",
                  updated_at: "2020-04-10T11:13:15+05:30",
                  alt:
                    "SUGAR Cosmetics Wingman Waterproof Microliner - 01 I'll Be Black (Black)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-wingman-waterproof-microliner-01-i-ll-be-black-black-14164742930515.jpg?v=1586497395",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14164747976787",
                },
                {
                  id: 14141835477075,
                  product_id: 4410900250707,
                  position: 3,
                  created_at: "2020-03-04T06:48:59+05:30",
                  updated_at: "2020-04-10T11:13:15+05:30",
                  alt:
                    "SUGAR Cosmetics Wingman Waterproof Microliner - 01 I'll Be Black (Black)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-wingman-waterproof-microliner-01-i-ll-be-black-black-14138808107091.jpg?v=1586497395",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14141835477075",
                },
                {
                  id: 14141837181011,
                  product_id: 4410900250707,
                  position: 4,
                  created_at: "2020-03-04T06:50:06+05:30",
                  updated_at: "2020-04-10T11:13:15+05:30",
                  alt:
                    "SUGAR Cosmetics Wingman Waterproof Microliner - 01 I'll Be Black (Black)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-wingman-waterproof-microliner-01-i-ll-be-black-black-14138808139859.jpg?v=1586497395",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14141837181011",
                },
                {
                  id: 14141836263507,
                  product_id: 4410900250707,
                  position: 5,
                  created_at: "2020-03-04T06:49:31+05:30",
                  updated_at: "2020-04-10T11:13:15+05:30",
                  alt:
                    "SUGAR Cosmetics Wingman Waterproof Microliner - 01 I'll Be Black (Black)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-wingman-waterproof-microliner-01-i-ll-be-black-black-14138808172627.jpg?v=1586497395",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14141836263507",
                },
              ],
              image: {
                id: 14233690013779,
                product_id: 4410900250707,
                position: 1,
                created_at: "2020-04-10T11:13:14+05:30",
                updated_at: "2020-04-10T11:13:15+05:30",
                alt:
                  "SUGAR Cosmetics Wingman Waterproof Microliner - 01 I'll Be Black (Black)",
                width: 1025,
                height: 1400,
                src:
                  "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-wingman-waterproof-microliner-01-i-ll-be-black-black-14233683034195.jpg?v=1586497395",
                variant_ids: [],
                admin_graphql_api_id:
                  "gid://shopify/ProductImage/14233690013779",
              },
            },
            mediaText: null,
            redirectUrl: null,
          },
          {
            id: 133,
            sectionId: 15,
            webViewTitle: null,
            contentType: 3,
            mediaUrl: null,
            product_json: {
              id: 1361079238739,
              title: "All Set To Go Translucent Powder",
              body_html:
                '<div class="col-md-12 video" style="text-align: left;">The fastest route to looking picture-perfect around the clock - say hello to the all-new <strong>SUGAR All Set To Go Translucent Powder!</strong> Crafted to lock in the base and mask the shine, this lightweight mattifying powder keeps you looking stunningly fresh throughout the day. The translucent powder settles on to the skin effortlessly to blur out imperfections while simultaneously enhancing your complexion. Just pat it on, dust it off and stay flawless for the next 8+ hours!</div>\n<p><span><br></span> <span></span> <span><span><strong>How to use</strong>: Make sure the puff is evenly saturated by massaging the powder into it and tapping any excess off. Next, keep the puff folded in half, and press and roll it into skin—this ensures that you’re not moving your foundation or concealer out of place. Wait for a few seconds and dust off with a brush. Avoid rubbing it against your skin and pay special attention to oily areas of the face.<br></span></span><span><strong><br></strong><strong>Benefits:</strong><span> </span>Extend the lifespan of your base with <strong>SUGAR</strong> <strong>All Set To Go Translucent Powder</strong>. Its shine-control formula guarantees 8+ hours of flawless perfection. The invisible particles of this translucent powder act as filling agents for pores and fine lines, giving a smooth &amp; natural finish. <br><br></span> <span><strong>Additional Details</strong>: Paraben free, Oil-free, Mineral oil-free, D5 free, Nano -ingredients free, Cruelty-free.</span></p>\n<p><strong>Net Weight:</strong> 7gm.</p>\n<p><span><strong>Ingredients</strong>: Talc, Aluminum Starch Octenylsuccinate, Polymethyl Methacrylate, Caprylyl Glycol, Phenoxyethanol, Hexylen Glycol.</span></p>\n<p><span><strong>MRP</strong>: 599 (incl. all taxes)<br><br><strong>Formulated</strong> in Italy.<br><br><strong>Company Name</strong>: Regi India Cosmetics Pvt Ltd.<br><br><strong>Company Address</strong>: C-08, Sara Ind Estate, Vill-Rampur, Selaqui, Dehradun 248197, Uttarakhand.</span></p>',
              vendor: "SUGAR Cosmetics",
              product_type: "Powder",
              created_at: "2018-07-19T16:46:59+05:30",
              handle: "all-set-to-go-translucent-powder",
              updated_at: "2021-01-13T13:25:35+05:30",
              published_at: "2018-07-19T16:45:14+05:30",
              template_suffix: "",
              published_scope: "web",
              tags:
                "599, Alcohol free, All skin tones, Baking, Blendable, Blur pores, Carmine free, Colour correction, Contouring, Cruelty free, Dailywear, Easy application, Extremly Light, Face, Fixes discolouration, Handy, High Coverage, Intense pigment, Loose powder, Luminous, Luminous finish, Make up, Matte, Mattifying Powder, Melt proof makeup, Minimise lines, Mirror, Oil free look, Paraben free, Partywear, Powder, Puff, Radiant glow, Seamless, Set, Sets makeup, Setting, Setting powder, Shine free look, st_bestseller:12, st_concern: Oil control, st_feature: Translucent Powder, st_finish: Natural, st_formulation: Loose Powder, st_popularityscore: 9011, st_type: Powders, sugar_type_0, Suphate free, Sweatproof, Translucent, Under 600, Vegan, Water resistant, White",
              admin_graphql_api_id: "gid://shopify/Product/1361079238739",
              variants: [
                {
                  id: 12666920271955,
                  product_id: 1361079238739,
                  title: "Default Title",
                  price: "599.00",
                  sku: "8906090495004",
                  position: 1,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "Default Title",
                  option2: null,
                  option3: null,
                  created_at: "2018-08-02T10:13:39+05:30",
                  updated_at: "2021-01-13T13:25:35+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 7,
                  image_id: null,
                  weight: 7,
                  weight_unit: "g",
                  inventory_item_id: 12722877661267,
                  inventory_quantity: 2456,
                  old_inventory_quantity: 2456,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/12666920271955",
                },
              ],
              options: [
                {
                  id: 1827417227347,
                  product_id: 1361079238739,
                  name: "Title",
                  position: 1,
                  values: ["Default Title"],
                },
              ],
              images: [
                {
                  id: 13736574386259,
                  product_id: 1361079238739,
                  position: 1,
                  created_at: "2019-12-25T15:22:21+05:30",
                  updated_at: "2020-05-11T22:33:34+05:30",
                  alt: "SUGAR Cosmetics All Set To Go Translucent Powder",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-all-set-to-go-translucent-powder-12785059758163.jpg?v=1589216614",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13736574386259",
                },
                {
                  id: 14376592113747,
                  product_id: 1361079238739,
                  position: 2,
                  created_at: "2020-05-11T22:33:33+05:30",
                  updated_at: "2020-05-11T22:33:34+05:30",
                  alt: "SUGAR Cosmetics All Set To Go Translucent Powder",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-all-set-to-go-translucent-powder-14376568324179.jpg?v=1589216614",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14376592113747",
                },
                {
                  id: 13727901909075,
                  product_id: 1361079238739,
                  position: 3,
                  created_at: "2019-12-23T12:33:51+05:30",
                  updated_at: "2020-05-11T22:33:34+05:30",
                  alt: "SUGAR Cosmetics All Set To Go Translucent Powder",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-all-set-to-go-translucent-powder-12785059790931.jpg?v=1589216614",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13727901909075",
                },
                {
                  id: 13727901778003,
                  product_id: 1361079238739,
                  position: 4,
                  created_at: "2019-12-23T12:33:32+05:30",
                  updated_at: "2020-05-11T22:33:34+05:30",
                  alt: "SUGAR Cosmetics All Set To Go Translucent Powder",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-all-set-to-go-translucent-powder-12785059922003.jpg?v=1589216614",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13727901778003",
                },
                {
                  id: 13727901679699,
                  product_id: 1361079238739,
                  position: 5,
                  created_at: "2019-12-23T12:33:16+05:30",
                  updated_at: "2020-05-11T22:33:34+05:30",
                  alt: "SUGAR Cosmetics All Set To Go Translucent Powder",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-all-set-to-go-translucent-powder-12785059954771.jpg?v=1589216614",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13727901679699",
                },
              ],
              image: {
                id: 13736574386259,
                product_id: 1361079238739,
                position: 1,
                created_at: "2019-12-25T15:22:21+05:30",
                updated_at: "2020-05-11T22:33:34+05:30",
                alt: "SUGAR Cosmetics All Set To Go Translucent Powder",
                width: 1025,
                height: 1400,
                src:
                  "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-all-set-to-go-translucent-powder-12785059758163.jpg?v=1589216614",
                variant_ids: [],
                admin_graphql_api_id:
                  "gid://shopify/ProductImage/13736574386259",
              },
            },
            mediaText: null,
            redirectUrl: null,
          },
        ],
      },
      {
        id: 5,
        title: "THIS OR THAT",
        sequence: 60,
        layoutType: 4,
        isTitleEnabled: 1,
        backgroundImg: null,
        backgroundColor: "#000000",
        text: null,
        contentType: 1,
        contentData: [
          {
            id: 108,
            sectionId: 5,
            webViewTitle: null,
            contentType: 1,
            mediaUrl:
              "https://d32baadbbpueqt.cloudfront.net/dashboard/1610005076split_banner_1.jpg",
            product_json: null,
            mediaText: null,
            redirectUrl:
              "https://in.sugarcosmetics.com/collections/bold-lipsticks",
          },
          {
            id: 109,
            sectionId: 5,
            webViewTitle: null,
            contentType: 1,
            mediaUrl:
              "https://d32baadbbpueqt.cloudfront.net/dashboard/1610005091split_banner_2-.jpg",
            product_json: null,
            mediaText: null,
            redirectUrl:
              "https://in.sugarcosmetics.com/collections/nudes-lipsticks",
          },
        ],
      },
      {
        id: 6,
        title: "SUPER SAVERS",
        sequence: 70,
        layoutType: 7,
        isTitleEnabled: 1,
        backgroundImg:
          "https://d32baadbbpueqt.cloudfront.net/dashboard/160129613001_4.jpg",
        backgroundColor: "#fcfcfc",
        text: "Grab these products for up to 50% off, till stocks last!",
        contentType: 3,
        contentData: [
          {
            id: 192,
            sectionId: 6,
            webViewTitle: null,
            contentType: 3,
            mediaUrl: null,
            product_json: {
              id: 3517527654483,
              title: "Drop The Base Serum Foundation",
              body_html:
                'Been wanting a lightweight foundation that doesn\'t weigh your skin down and freshens your complexion? It\'s time to grab <strong>SUGAR Drop The Base Serum Foundation</strong> right away! This product is magic for so many reasons - With a feather-weight fluid and velvety texture that lends a flawless medium to high coverage, this foundation is a skin-perfecting luxury itself. It delivers a natural finish and the look and feel of beautiful skin. The result is a noticeably brighter and youthful appearance. \n<p><br>Ladies, grab this magic potion and revel in the luxury now.</p>\n<ul>\n<li>\n<strong><a href="https://in.sugarcosmetics.com/products/drop-the-base-serum-foundation-10-latte" target="_blank" rel="noopener noreferrer">10 Latte</a></strong> is a light shade with warm undertone.<br><br>\n</li>\n<li>\n<strong><a href="https://in.sugarcosmetics.com/products/drop-the-base-serum-foundation-15-cappuccino" target="_blank" rel="noopener noreferrer">15 Cappuccino</a></strong> is a light shade with cool undertone.<br><br>\n</li>\n<li>\n<strong><a href="https://in.sugarcosmetics.com/products/drop-the-base-serum-foundation-17-raf" target="_blank" rel="noopener noreferrer">17 Raf</a></strong> is a light shade with golden undertone.<br><br>\n</li>\n<li>\n<strong><a href="https://in.sugarcosmetics.com/products/drop-the-base-serum-foundation-27-vienna" target="_blank" rel="noopener noreferrer">27 Vienna</a></strong> is a light medium shade with warm undertone.<br><br>\n</li>\n<li>\n<strong><a href="https://in.sugarcosmetics.com/products/drop-the-base-serum-foundation-32-cortado" target="_blank" rel="noopener noreferrer">32 Cortado</a></strong> is a medium shade with golden undertone.<br><br>\n</li>\n<li>\n<strong><a href="https://in.sugarcosmetics.com/products/drop-the-base-serum-foundation-37-freddo" target="_blank" rel="noopener noreferrer">37 Freddo</a></strong> is a medium beige shade with golden peach undertone.<br><br>\n</li>\n<li>\n<strong><a href="https://in.sugarcosmetics.com/products/drop-the-base-serum-foundation-42-glace" target="_blank" rel="noopener noreferrer">42 Glace</a></strong> is a medium beige shade with golden undertone.<br><br>\n</li>\n<li>\n<strong><a href="https://in.sugarcosmetics.com/products/drop-the-base-serum-foundation-50-mocha" target="_blank" rel="noopener noreferrer">50 Mocha</a></strong> is a medium tan shade with golden undertone.<br><br>\n</li>\n<li>\n<strong><a href="https://in.sugarcosmetics.com/products/drop-the-base-serum-foundation-52-corretto" target="_blank" rel="noopener noreferrer">52 Corretto</a></strong> is a tan shade with yellow undertone.<br><br>\n</li>\n<li>\n<strong><a href="https://in.sugarcosmetics.com/products/drop-the-base-serum-foundation-60-lungo" target="_blank" rel="noopener noreferrer">60 Lungo</a></strong> is a medium deep shade with golden undertone.<br><br>\n</li>\n</ul>\n<p><strong>How to Apply: </strong>Take a dollop onto your palm. With the applicator, apply small dots over your face and neck. Using your fingertips, beauty sponge or a foundation brush, blend the product well into the skin to get a refreshed look.<br> <br> <strong>Benefits:</strong> <strong>SUGAR Drop The Base Serum Foundation </strong>contains an<strong> </strong>ultra-thin skin-perfecting formula that provides seamless adjustable coverage and a natural finish. A powerful marine-based algae extract helps reactivate the metabolism of aging cells and gives you a youthful appearance.<br> <br> <strong>Additional Details:</strong> Find your skin’s perfect partner from 10 shades of the <strong>SUGAR Drop The Base Serum Foundation</strong>, viz. <strong><a href="https://in.sugarcosmetics.com/products/drop-the-base-serum-foundation-10-latte" target="_blank" rel="noopener noreferrer">10 Latte <span>(Light, Warm Undertone)</span></a>, <a href="https://in.sugarcosmetics.com/products/drop-the-base-serum-foundation-15-cappuccino" target="_blank" rel="noopener noreferrer">15 Cappuccino <span>(Light, Cool Undertone)</span></a>, <a href="https://in.sugarcosmetics.com/products/drop-the-base-serum-foundation-17-raf" target="_blank" rel="noopener noreferrer">17 Raf <span>(Light, Golden Undertone)</span></a>, <a href="https://in.sugarcosmetics.com/products/drop-the-base-serum-foundation-27-vienna" target="_blank" rel="noopener noreferrer">27 Vienna <span>(Light Medium, Warm Undertone)</span></a>, <a href="https://in.sugarcosmetics.com/products/drop-the-base-serum-foundation-32-cortado" target="_blank" rel="noopener noreferrer">32 Cortado <span>(Medium, Golden Undertone)</span></a>, <a href="https://in.sugarcosmetics.com/products/drop-the-base-serum-foundation-37-freddo" target="_blank" rel="noopener noreferrer">37 Freddo <span>(Medium Beige, Golden Peach Undertone)</span></a>, <a href="https://in.sugarcosmetics.com/products/drop-the-base-serum-foundation-42-glace" target="_blank" rel="noopener noreferrer">42 Glace <span>(Medium Beige, Golden Undertone)</span></a>, <a href="https://in.sugarcosmetics.com/products/drop-the-base-serum-foundation-50-mocha" target="_blank" rel="noopener noreferrer">50 Mocha <span>(Medium Tan, Golden Undertone)</span></a>, <a href="https://in.sugarcosmetics.com/products/drop-the-base-serum-foundation-52-corretto" target="_blank" rel="noopener noreferrer">52 Corretto <span>(Tan, Yellow Undertone)</span></a>, <a href="https://in.sugarcosmetics.com/products/drop-the-base-serum-foundation-60-lungo" target="_blank" rel="noopener noreferrer">60 Lungo <span>(Medium Deep, Golden Undertone)</span></a>. </strong>This product is dermatologically tested &amp; approved and 100% safe for your skin.<br> <br> <strong>List Of Ingredients: </strong>Dimethicone, Cyclohexasiloxane, Isododecane, Vinyl Dimethicone, EthylHexyl Methoxycinnamate, Phenyl Trimethicone, Alaria Esculentia, Disteardimonium Hectorite, Isohexadecane, Peg-10 Dimethicone, Zinc Oxide, Polybutene, Talc, Propylene Carbonate, Cyclopentasiloxane, Parfum, Hydrogen Dimethicone, CI 77891, CI 77491, CI 77492, CI 77499<br><br> <strong>Net Volume: </strong>20ml<br><br><strong>MRP</strong>: 899 (incl. all taxes)<br><br><strong>Formulated</strong> in Italy<br><br><strong>Company Name</strong>: Regi India Cosmetics Pvt Ltd.<br><br><strong>Company Address</strong>: C-08, Sara Ind Estate, Vill-Rampur, Selaqui, Dehradun 248197, Uttarakhand.<br></p>',
              vendor: "SUGAR Cosmetics",
              product_type: "Foundations & Concealers",
              created_at: "2019-04-19T16:04:18+05:30",
              handle: "drop-the-base-serum-foundation",
              updated_at: "2021-01-13T13:05:11+05:30",
              published_at: "2019-04-30T14:32:02+05:30",
              template_suffix: "",
              published_scope: "web",
              tags: "amp-hide, sugar_type_1",
              admin_graphql_api_id: "gid://shopify/Product/3517527654483",
              variants: [
                {
                  id: 27847255916627,
                  product_id: 3517527654483,
                  title: "10 Latte (Light, Warm Undertone)",
                  price: "450.00",
                  sku: "8904320700003",
                  position: 1,
                  inventory_policy: "deny",
                  compare_at_price: "899.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "10 Latte (Light, Warm Undertone)",
                  option2: null,
                  option3: null,
                  created_at: "2019-04-19T16:04:19+05:30",
                  updated_at: "2020-12-15T15:56:45+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13736644935763,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 28874577444947,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/27847255916627",
                },
                {
                  id: 27863591223379,
                  product_id: 3517527654483,
                  title: "15 Cappuccino (Light, Cool Undertone)",
                  price: "450.00",
                  sku: "8904320700010",
                  position: 2,
                  inventory_policy: "deny",
                  compare_at_price: "899.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "15 Cappuccino (Light, Cool Undertone)",
                  option2: null,
                  option3: null,
                  created_at: "2019-04-29T18:43:59+05:30",
                  updated_at: "2021-01-09T13:59:11+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13736644968531,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 28892058484819,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/27863591223379",
                },
                {
                  id: 27863591256147,
                  product_id: 3517527654483,
                  title: "17 Raf (Light, Golden Undertone)",
                  price: "450.00",
                  sku: "8904320700027",
                  position: 3,
                  inventory_policy: "deny",
                  compare_at_price: "899.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "17 Raf (Light, Golden Undertone)",
                  option2: null,
                  option3: null,
                  created_at: "2019-04-29T18:43:59+05:30",
                  updated_at: "2020-12-15T14:27:46+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13728404078675,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 28892058517587,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/27863591256147",
                },
                {
                  id: 27863591288915,
                  product_id: 3517527654483,
                  title: "27 Vienna (Light Medium, Warm Undertone)",
                  price: "450.00",
                  sku: "8904320700034",
                  position: 4,
                  inventory_policy: "deny",
                  compare_at_price: "899.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "27 Vienna (Light Medium, Warm Undertone)",
                  option2: null,
                  option3: null,
                  created_at: "2019-04-29T18:43:59+05:30",
                  updated_at: "2020-12-15T14:27:30+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13728403488851,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 28892058550355,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/27863591288915",
                },
                {
                  id: 27863591321683,
                  product_id: 3517527654483,
                  title: "32 Cortado (Medium, Golden Undertone)",
                  price: "450.00",
                  sku: "8904320700041",
                  position: 5,
                  inventory_policy: "deny",
                  compare_at_price: "899.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "32 Cortado (Medium, Golden Undertone)",
                  option2: null,
                  option3: null,
                  created_at: "2019-04-29T18:43:59+05:30",
                  updated_at: "2021-01-07T11:59:41+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13728403456083,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 28892058583123,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/27863591321683",
                },
                {
                  id: 27863591354451,
                  product_id: 3517527654483,
                  title: "37 Freddo (Medium Beige, Golden Peach Undertone)",
                  price: "450.00",
                  sku: "8904320700058",
                  position: 6,
                  inventory_policy: "deny",
                  compare_at_price: "899.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "37 Freddo (Medium Beige, Golden Peach Undertone)",
                  option2: null,
                  option3: null,
                  created_at: "2019-04-29T18:43:59+05:30",
                  updated_at: "2021-01-09T14:39:41+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13736645066835,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 28892058615891,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/27863591354451",
                },
                {
                  id: 27863591387219,
                  product_id: 3517527654483,
                  title: "42 Glace (Medium Beige, Golden Undertone)",
                  price: "450.00",
                  sku: "8904320700065",
                  position: 7,
                  inventory_policy: "deny",
                  compare_at_price: "899.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "42 Glace (Medium Beige, Golden Undertone)",
                  option2: null,
                  option3: null,
                  created_at: "2019-04-29T18:43:59+05:30",
                  updated_at: "2021-01-10T20:28:56+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13736645886035,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 28892058648659,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/27863591387219",
                },
                {
                  id: 27863591452755,
                  product_id: 3517527654483,
                  title: "50 Mocha (Medium Tan, Golden Undertone)",
                  price: "450.00",
                  sku: "8904320700089",
                  position: 8,
                  inventory_policy: "deny",
                  compare_at_price: "899.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "50 Mocha (Medium Tan, Golden Undertone)",
                  option2: null,
                  option3: null,
                  created_at: "2019-04-29T18:43:59+05:30",
                  updated_at: "2021-01-13T08:26:05+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13736645918803,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 28892058714195,
                  inventory_quantity: 3396,
                  old_inventory_quantity: 3396,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/27863591452755",
                },
                {
                  id: 27863591419987,
                  product_id: 3517527654483,
                  title: "52 Corretto (Tan, Yellow Undertone)",
                  price: "450.00",
                  sku: "8904320700072",
                  position: 9,
                  inventory_policy: "deny",
                  compare_at_price: "899.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "52 Corretto (Tan, Yellow Undertone)",
                  option2: null,
                  option3: null,
                  created_at: "2019-04-29T18:43:59+05:30",
                  updated_at: "2021-01-13T12:56:36+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13728409419859,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 28892058681427,
                  inventory_quantity: 142,
                  old_inventory_quantity: 142,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/27863591419987",
                },
                {
                  id: 27863591485523,
                  product_id: 3517527654483,
                  title: "60 Lungo (Medium Deep, Golden Undertone)",
                  price: "450.00",
                  sku: "8904320700102",
                  position: 10,
                  inventory_policy: "deny",
                  compare_at_price: "899.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "60 Lungo (Medium Deep, Golden Undertone)",
                  option2: null,
                  option3: null,
                  created_at: "2019-04-29T18:43:59+05:30",
                  updated_at: "2021-01-12T20:56:35+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13728408469587,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 28892058746963,
                  inventory_quantity: 5863,
                  old_inventory_quantity: 5863,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/27863591485523",
                },
              ],
              options: [
                {
                  id: 4600787304531,
                  product_id: 3517527654483,
                  name: "Colour",
                  position: 1,
                  values: [
                    "10 Latte (Light, Warm Undertone)",
                    "15 Cappuccino (Light, Cool Undertone)",
                    "17 Raf (Light, Golden Undertone)",
                    "27 Vienna (Light Medium, Warm Undertone)",
                    "32 Cortado (Medium, Golden Undertone)",
                    "37 Freddo (Medium Beige, Golden Peach Undertone)",
                    "42 Glace (Medium Beige, Golden Undertone)",
                    "50 Mocha (Medium Tan, Golden Undertone)",
                    "52 Corretto (Tan, Yellow Undertone)",
                    "60 Lungo (Medium Deep, Golden Undertone)",
                  ],
                },
              ],
              images: [
                {
                  id: 13736642216019,
                  product_id: 3517527654483,
                  position: 1,
                  created_at: "2019-12-25T16:00:56+05:30",
                  updated_at: "2019-12-25T16:04:35+05:30",
                  alt: "SUGAR Cosmetics Drop The Base Serum Foundation",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-drop-the-base-serum-foundation-10959991767123.jpg?v=1577270075",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13736642216019",
                },
                {
                  id: 13736644935763,
                  product_id: 3517527654483,
                  position: 2,
                  created_at: "2019-12-25T16:02:17+05:30",
                  updated_at: "2019-12-25T16:04:35+05:30",
                  alt:
                    "SUGAR Cosmetics Drop The Base Serum Foundation 10 Latte (Light, Warm Undertone)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-drop-the-base-serum-foundation-10-latte-light-warm-undertone-10965244805203.jpg?v=1577270075",
                  variant_ids: [27847255916627],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13736644935763",
                },
                {
                  id: 13736644968531,
                  product_id: 3517527654483,
                  position: 3,
                  created_at: "2019-12-25T16:02:34+05:30",
                  updated_at: "2019-12-25T16:04:35+05:30",
                  alt:
                    "SUGAR Cosmetics Drop The Base Serum Foundation 15 Cappuccino (Light, Cool Undertone)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-drop-the-base-serum-foundation-15-cappuccino-light-cool-undertone-10965245001811.jpg?v=1577270075",
                  variant_ids: [27863591223379],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13736644968531",
                },
                {
                  id: 13728404078675,
                  product_id: 3517527654483,
                  position: 4,
                  created_at: "2019-12-23T15:37:53+05:30",
                  updated_at: "2019-12-25T16:04:35+05:30",
                  alt:
                    "SUGAR Cosmetics Drop The Base Serum Foundation 17 Raf (Light, Golden Undertone)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-drop-the-base-serum-foundation-17-raf-light-golden-undertone-10965245034579.jpg?v=1577270075",
                  variant_ids: [27863591256147],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13728404078675",
                },
                {
                  id: 13728403488851,
                  product_id: 3517527654483,
                  position: 5,
                  created_at: "2019-12-23T15:37:36+05:30",
                  updated_at: "2019-12-25T16:04:35+05:30",
                  alt:
                    "SUGAR Cosmetics Drop The Base Serum Foundation 27 Vienna (Light Medium, Warm Undertone)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-drop-the-base-serum-foundation-27-vienna-light-medium-warm-undertone-10965245100115.jpg?v=1577270075",
                  variant_ids: [27863591288915],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13728403488851",
                },
                {
                  id: 13728403456083,
                  product_id: 3517527654483,
                  position: 6,
                  created_at: "2019-12-23T15:37:20+05:30",
                  updated_at: "2019-12-25T16:04:35+05:30",
                  alt:
                    "SUGAR Cosmetics Drop The Base Serum Foundation 32 Cortado (Medium, Golden Undertone)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-drop-the-base-serum-foundation-32-cortado-medium-golden-undertone-10965245132883.jpg?v=1577270075",
                  variant_ids: [27863591321683],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13728403456083",
                },
                {
                  id: 13736645066835,
                  product_id: 3517527654483,
                  position: 7,
                  created_at: "2019-12-25T16:02:50+05:30",
                  updated_at: "2019-12-25T16:04:35+05:30",
                  alt:
                    "SUGAR Cosmetics Drop The Base Serum Foundation 37 Freddo (Medium Beige, Golden Peach Undertone)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-drop-the-base-serum-foundation-37-freddo-medium-beige-golden-peach-undertone-10965245296723.jpg?v=1577270075",
                  variant_ids: [27863591354451],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13736645066835",
                },
                {
                  id: 13736645886035,
                  product_id: 3517527654483,
                  position: 8,
                  created_at: "2019-12-25T16:04:17+05:30",
                  updated_at: "2019-12-25T16:04:35+05:30",
                  alt:
                    "SUGAR Cosmetics Drop The Base Serum Foundation 42 Glace (Medium Beige, Golden Undertone)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-drop-the-base-serum-foundation-42-glace-medium-beige-golden-undertone-10965248278611.jpg?v=1577270075",
                  variant_ids: [27863591387219],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13736645886035",
                },
                {
                  id: 13736645918803,
                  product_id: 3517527654483,
                  position: 9,
                  created_at: "2019-12-25T16:04:34+05:30",
                  updated_at: "2019-12-25T16:04:35+05:30",
                  alt:
                    "SUGAR Cosmetics Drop The Base Serum Foundation 50 Mocha (Medium Tan, Golden Undertone)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-drop-the-base-serum-foundation-50-mocha-medium-tan-golden-undertone-10965250244691.jpg?v=1577270075",
                  variant_ids: [27863591452755],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13736645918803",
                },
                {
                  id: 13728409419859,
                  product_id: 3517527654483,
                  position: 10,
                  created_at: "2019-12-23T15:39:55+05:30",
                  updated_at: "2019-12-25T16:04:35+05:30",
                  alt:
                    "SUGAR Cosmetics Drop The Base Serum Foundation 52 Corretto (Tan, Yellow Undertone)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-drop-the-base-serum-foundation-52-corretto-tan-yellow-undertone-10965250474067.jpg?v=1577270075",
                  variant_ids: [27863591419987],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13728409419859",
                },
                {
                  id: 13728408469587,
                  product_id: 3517527654483,
                  position: 11,
                  created_at: "2019-12-23T15:39:37+05:30",
                  updated_at: "2019-12-25T16:04:35+05:30",
                  alt:
                    "SUGAR Cosmetics Drop The Base Serum Foundation 60 Lungo (Medium Deep, Golden Undertone)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-drop-the-base-serum-foundation-60-lungo-medium-deep-golden-undertone-10965250539603.jpg?v=1577270075",
                  variant_ids: [27863591485523],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13728408469587",
                },
              ],
              image: {
                id: 13736642216019,
                product_id: 3517527654483,
                position: 1,
                created_at: "2019-12-25T16:00:56+05:30",
                updated_at: "2019-12-25T16:04:35+05:30",
                alt: "SUGAR Cosmetics Drop The Base Serum Foundation",
                width: 1025,
                height: 1400,
                src:
                  "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-drop-the-base-serum-foundation-10959991767123.jpg?v=1577270075",
                variant_ids: [],
                admin_graphql_api_id:
                  "gid://shopify/ProductImage/13736642216019",
              },
            },
            mediaText: null,
            redirectUrl:
              "https://in.sugarcosmetics.com/products/drop-the-base-serum-foundation",
          },
          {
            id: 185,
            sectionId: 6,
            webViewTitle: null,
            contentType: 3,
            mediaUrl: null,
            product_json: {
              id: 4011767238,
              title:
                "Matte As Hell Crayon Lipstick - 04 Holly Golightly (Nude)",
              body_html:
                '<p><span data-sheets-value=\'{"1":2,"2":"· 04 Holly Golightly (Nude) - Goes well with elaborate eye make-up, and can also be used every day for a natural look."}\' data-sheets-userformat=\'{"2":14849,"3":{"1":0},"12":0,"14":[null,2,3355443],"15":"Symbol","16":12}\' data-sheets-hyperlinkruns=\'[null,5,"https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-04-holly-golightly-nude?variant=12071232134"]{"1":20}\'>If excellent coverage and high colour pay-off is what gets you going, you will absolutely love our <strong>SUGAR</strong> <strong>Matte As Hell Crayon Lipstick</strong> that is available in 36 gorgeous shades to suit your taste &amp; your ever-changing mood! Super long-lasting and highly pigmented so you don’t have to worry about constant re-application, this must-have crayon lipstick has a soft texture with a silky matte finish.<br><br> <strong>04 Holly Golightly (Nude)</strong> - Goes well with elaborate eye make-up, and can also be used every day for a natural look.</span></p>\n<p><strong>Bonus:</strong><span> </span><span>Each </span><span>Crayon Lipstick Shade</span><span> comes with its specially designed high quality sharpener that is carefully crafted to help you make the most of your purchase. </span><span>Read more details </span><span>about the </span><strong><a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-sharpener" target="_blank" rel="noopener noreferrer">Crayon Lipstick Sharpener</a>.</strong><br></p>\n<p><span>Size: 2.8 gm.</span></p>\n<p><span><strong>Additional Details:</strong> The <strong>SUGAR Matte As Hell Crayon Lipstick</strong> is available in 36 Attractive shades viz., <strong><a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-01-scarlett-ohara-red" target="_blank" rel="noopener noreferrer">01 Scarlett O’ Hara</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-02-mary-poppins-fuchsia" target="_blank" rel="noopener noreferrer">02 Mary Poppins</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-03-poison-ivy-wine" target="_blank" rel="noopener noreferrer">03 Poison Ivy</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-04-holly-golightly-nude" target="_blank" rel="noopener noreferrer">04 Holly Golightly</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-05-rose-dawson-rose-pink" target="_blank" rel="noopener noreferrer">05 Rose Dawson</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-06-coraline-jones-orange-coral" target="_blank" rel="noopener noreferrer">06 Coraline Jones</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-07-viola-mauve-nude" target="_blank" rel="noopener noreferrer">07 Viola</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-08-jacky-brown-reddish-brown" target="_blank" rel="noopener noreferrer">08 Jackie Brown</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-09-princess-peach-peach" target="_blank" rel="noopener noreferrer">09 Princess Peach</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-10-cherry-darling-cherry-red" target="_blank" rel="noopener noreferrer">10 Cherry Darling</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-11-elle-woods-brown-nude" target="_blank" rel="noopener noreferrer">11 Elle Woods</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-baby-houseman-deep-pink" target="_blank" rel="noopener noreferrer">12 Baby Houseman</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-murphy-brown-chocolate-burgundy" target="_blank" rel="noopener noreferrer">13 Murphy Brown</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-violet-crawley-smokey-violet" target="_blank" rel="noopener noreferrer">14 Violet Crawley</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-15-stephanie-plum-plum-mauve" target="_blank" rel="noopener noreferrer">15 Stephanie Plum</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-16-claire-underwood-burnt-orange" target="_blank" rel="noopener noreferrer">16 Claire Underwood</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-17-brandy-harrington-rusty-reddish-pink" target="_blank" rel="noopener noreferrer">17 Brandy Harrington</a>, </strong><strong><a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-18-rosalind-nude-rose" target="_blank" rel="noopener noreferrer">18 Rosalind</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-19-emma-woodhouse-earthy-brown" target="_blank" rel="noopener noreferrer">19 Emma Woodhouse</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-20-buffy-summers-mid-tone-warm-nude" target="_blank" rel="noopener noreferrer">20 Buffy Summers</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-21-rose-tyler-light-fuchsia-deep-rose" target="_blank" rel="noopener noreferrer">21 Rose Tyler</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-22-donna-pinciotti-magenta-pink" target="_blank" rel="noopener noreferrer">22 Donna Pinciotti</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-23-jessica-day-dusty-coral" target="_blank" rel="noopener noreferrer">23 Jessica Day</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-24-rachel-berry-deep-berry" target="_blank" rel="noopener noreferrer">24 Rachel Berry,</a> <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-25-lily-aldrin-mauve-pink" target="_blank" rel="noopener noreferrer">25 Lily Aldrin,</a> <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-26-vianne-rocher" target="_blank" rel="noopener noreferrer">26 Vianne Rocher</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-27-sunny-randall" target="_blank" rel="noopener noreferrer">27 Sunny Randall</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-28-honey-rider" target="_blank" rel="noopener noreferrer">28 Honey Rider</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-29-molly-brown" target="_blank" rel="noopener noreferrer">29 Molly Brown</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-30-lillian-rose" target="_blank" rel="noopener noreferrer">30 Lillian Rose</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-31-poppy-adams" target="_blank" rel="noopener noreferrer">31 Poppy Adams</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-32-miss-rosa" target="_blank" rel="noopener noreferrer">32 Miss Rosa</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-33-pepper-anderson" target="_blank" rel="noopener noreferrer">33 Pepper Anderson</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-34-cherry-ames" target="_blank" rel="noopener noreferrer">34 Cherry Ames</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-35-claire-redfield" target="_blank" rel="noopener noreferrer">35 Claire Redfield </a></strong>and<strong> <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-36-veronica-mars" target="_blank" rel="noopener noreferrer">36 Veronica Mars</a>. </strong></span></p>\n<p><strong><b>SUGAR Matte As Hell Crayon Lipstick Ingredients</b>:<span> </span></strong>Dimethicone, Diisostearyl Malate, Synthetic Wax, Distarch Phosphate, Polybutene, Dimethicone/Vinyl Dimethicone Crosspolymer, Caprylyl Methicone, Dimethylimidazolidinone Rice Starch, Glyceryl Caprylate, Pentaerythrityl Tetra-Di-T-Butyl Hydroxyhydrocinnamate, Silica, Isoceteth - 10 May Contain: C.I. Nos. 45410, 15850, 19140.</p>\n<p><strong>MRP</strong>: Rs. 899 (incl. all taxes)<br><br><strong>Country of Origin</strong>: Germany<br><br><strong>Company Name</strong>: Schwan Cosmetics Germany Gmbh &amp; Co.Kg<br><br><strong>Company Address</strong>: Schwanweg 1, 90562 Heroldsberg - (Germany)<br><br></p>\n<h5>How to apply</h5>\n<p><span>Exfoliate your lips and apply SUGAR Matte As Hell Crayon Lipstick just the way you would apply any other lipstick. Blot and reapply. </span><span>Sharpen your </span><a href="https://in.sugarcosmetics.com/collections/matte-lipstick-collection" target="_blank" rel="noopener noreferrer"><strong>best matte lipstick</strong></a><span> when it starts to dull for best effect</span><span>.<br><br><strong>Best paired with</strong>:<br></span></p>\n<p><span>Partner this <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick" target="_blank" rel="noopener noreferrer"><strong>best matte crayon lipstick</strong></a> with the best <strong><a href="https://in.sugarcosmetics.com/products/stroke-of-genius-heavy-duty-kohl" target="_blank" rel="noopener noreferrer">kajal pencil</a></strong> to your eyes. Next, use an <strong><a href="https://in.sugarcosmetics.com/products/bling-leader-illuminating-moisturizer" target="_blank" rel="noopener noreferrer">illuminating moisturizer</a></strong><a href="https://in.sugarcosmetics.com/products/bling-leader-illuminating-moisturizer"> </a>to get that perfect glow. Finish your perfect makeup with some <a href="https://in.sugarcosmetics.com/collections/the-sugar-contour-de-force-mini-blush-collection" target="_blank" rel="noopener noreferrer"><strong>blusher</strong></a> to the apple of your cheeks.</span></p>\n<h5>Benefits</h5>\n<p><strong><a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick" target="_blank" rel="noopener noreferrer">SUGAR Matte As Hell Crayon Lipstick</a></strong><span> is powered by:</span></p>\n<p><span>- Highly pigmented formula </span><span>(For maximum coverage)</span></p>\n<p>- Smooth application (Glides easily)</p>\n<p><span>- <a href="https://in.sugarcosmetics.com/products/nothing-else-matter-longwear-lipstick" target="_blank" rel="noopener noreferrer"><strong>Long-lasting matte</strong></a></span><span> wear (Stays for hours)</span></p>\n<p><span>- Paraben-free </span><span>ingredients</span><span> </span></p>\n<p>- A cruelty free formula</p>\n<h5>Commonly asked questions</h5>\n<p>Q. What’s the finish like?<br>A. This crayon lipstick has a gorgeous, silky matte finish.<br><br>Q. What’s the net weight?<br>A. 2.8g.<br><br>Q. Is it vegan?<br>A. Yes, the SUGAR Matte As Hell Crayon Lipstick is vegan, and is also paraben-free.<br><br>Q. Do we need to sharpen it?<br>A. Yes. It comes with an original, high quality lipstick sharpener.<br><br>Q. Any nude shades available in the range?<br>A.<span> </span><span>For a great nude look, </span><span>try 04 Holly Golightly and 18 Rosalind. </span><span>Read more about sporting </span><strong><a href="https://blog.sugarcosmetics.com/nude-makeup-for-indian-skin-tones/">nude makeup for Indian skin tones</a></strong><span> here</span><span>.</span></p>',
              vendor: "SUGAR Cosmetics",
              product_type: "Crayon Lipstick",
              created_at: "2015-12-07T17:40:15+05:30",
              handle: "matte-as-hell-crayon-lipstick-04-holly-golightly-nude",
              updated_at: "2020-12-31T14:42:40+05:30",
              published_at: "2015-12-07T17:37:00+05:30",
              template_suffix: "",
              published_scope: "web",
              tags:
                "899, 9 to 5, Alcohol Free, All Skin tones, Best selling colours, Bestseller, Bold, Chubby Lipstick, Chubby Pencil, crayon, Crayon Lipstick, Creamy consistency, Cruelty Free, Everyday wear, Festive, Full coverage, Gluten Free, High colour pay off, High coverage, Highly pigmented, Intense colour, Intensely Pigmented, Lasting colour, Light weight, lip, Lip Crayon, Lips, lipstick, Long lasting, Longlasting, Longwear, Made In Germany, Matte, Matte Crayon, Mineral Oil Free, Must have, Nude, Office wear, Opaque, Paraben Free, Paraffin free, Peach Pink, Rich coverage, Scarlet, Silky Matte, Smudgeproof, Soft Texture, st_ color: Nudes, st_color: Pinks and Purples, st_feature: Crayon, st_feature: Long Lasting, st_feature: Smudge Proof, st_feature: Waterproof, st_finish: Matte, st_formulation: Crayon, st_type: Lipstick, sugar_type_0, Sulphate Free, Super Long lasting, Travel friendly, Ultimate comfort, Under 1000, Vegan, Vegetarian, Velvet, Water Resistant, Wax based formulation, Zero drying, Zero sticky",
              admin_graphql_api_id: "gid://shopify/Product/4011767238",
              variants: [
                {
                  id: 12071232134,
                  product_id: 4011767238,
                  title: "Default Title",
                  price: "539.00",
                  sku: "8904320703585",
                  position: 1,
                  inventory_policy: "deny",
                  compare_at_price: "899.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "Default Title",
                  option2: null,
                  option3: null,
                  created_at: "2015-12-07T17:40:15+05:30",
                  updated_at: "2020-12-31T14:42:40+05:30",
                  taxable: false,
                  barcode: "",
                  grams: 3,
                  image_id: null,
                  weight: 2.8,
                  weight_unit: "g",
                  inventory_item_id: 5815314822,
                  inventory_quantity: 1716,
                  old_inventory_quantity: 1716,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/12071232134",
                },
              ],
              options: [
                {
                  id: 4863382150,
                  product_id: 4011767238,
                  name: "Title",
                  position: 1,
                  values: ["Default Title"],
                },
              ],
              images: [
                {
                  id: 15051208097875,
                  product_id: 4011767238,
                  position: 1,
                  created_at: "2020-09-15T14:43:05+05:30",
                  updated_at: "2020-09-15T14:43:06+05:30",
                  alt:
                    "SUGAR Cosmetics Matte As Hell Crayon Lipstick - 04 Holly Golightly (Nude)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-matte-as-hell-crayon-lipstick-04-holly-golightly-nude-15051198693459.jpg?v=1600161186",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15051208097875",
                },
                {
                  id: 14271246860371,
                  product_id: 4011767238,
                  position: 2,
                  created_at: "2020-04-20T20:03:03+05:30",
                  updated_at: "2020-09-15T14:43:06+05:30",
                  alt:
                    "SUGAR Cosmetics Matte As Hell Crayon Lipstick - 04 Holly Golightly (Nude)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-matte-as-hell-crayon-lipstick-04-holly-golightly-nude-14271176441939.jpg?v=1600161186",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14271246860371",
                },
                {
                  id: 14376937783379,
                  product_id: 4011767238,
                  position: 3,
                  created_at: "2020-05-11T23:45:00+05:30",
                  updated_at: "2020-09-15T14:43:06+05:30",
                  alt:
                    "SUGAR Cosmetics Matte As Hell Crayon Lipstick - 04 Holly Golightly (Nude)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-matte-as-hell-crayon-lipstick-04-holly-golightly-nude-14376903180371.jpg?v=1600161186",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14376937783379",
                },
                {
                  id: 13729169571923,
                  product_id: 4011767238,
                  position: 4,
                  created_at: "2019-12-23T23:37:50+05:30",
                  updated_at: "2020-09-15T14:43:06+05:30",
                  alt:
                    "SUGAR Cosmetics Matte As Hell Crayon Lipstick - 04 Holly Golightly (Nude)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-matte-as-hell-crayon-lipstick-04-holly-golightly-nude-12775240925267.jpg?v=1600161186",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13729169571923",
                },
                {
                  id: 13729168425043,
                  product_id: 4011767238,
                  position: 5,
                  created_at: "2019-12-23T23:37:33+05:30",
                  updated_at: "2020-09-15T14:43:06+05:30",
                  alt:
                    "SUGAR Cosmetics Matte As Hell Crayon Lipstick - 04 Holly Golightly (Nude)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-matte-as-hell-crayon-lipstick-04-holly-golightly-nude-12775240892499.jpg?v=1600161186",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13729168425043",
                },
                {
                  id: 13729167343699,
                  product_id: 4011767238,
                  position: 6,
                  created_at: "2019-12-23T23:37:17+05:30",
                  updated_at: "2020-09-15T14:43:06+05:30",
                  alt:
                    "SUGAR Cosmetics Matte As Hell Crayon Lipstick - 04 Holly Golightly (Nude)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-matte-as-hell-crayon-lipstick-04-holly-golightly-nude-12775241023571.jpg?v=1600161186",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13729167343699",
                },
                {
                  id: 13737159262291,
                  product_id: 4011767238,
                  position: 7,
                  created_at: "2019-12-25T19:48:35+05:30",
                  updated_at: "2020-09-15T14:43:06+05:30",
                  alt:
                    "SUGAR Cosmetics Matte As Hell Crayon Lipstick - 04 Holly Golightly (Nude)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-matte-as-hell-crayon-lipstick-04-holly-golightly-nude-12793545949267.jpg?v=1600161186",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13737159262291",
                },
                {
                  id: 13737159950419,
                  product_id: 4011767238,
                  position: 8,
                  created_at: "2019-12-25T19:48:51+05:30",
                  updated_at: "2020-09-15T14:43:06+05:30",
                  alt:
                    "SUGAR Cosmetics Matte As Hell Crayon Lipstick - 04 Holly Golightly (Nude)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-matte-as-hell-crayon-lipstick-04-holly-golightly-nude-12775241056339.jpg?v=1600161186",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13737159950419",
                },
              ],
              image: {
                id: 15051208097875,
                product_id: 4011767238,
                position: 1,
                created_at: "2020-09-15T14:43:05+05:30",
                updated_at: "2020-09-15T14:43:06+05:30",
                alt:
                  "SUGAR Cosmetics Matte As Hell Crayon Lipstick - 04 Holly Golightly (Nude)",
                width: 1025,
                height: 1400,
                src:
                  "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-matte-as-hell-crayon-lipstick-04-holly-golightly-nude-15051198693459.jpg?v=1600161186",
                variant_ids: [],
                admin_graphql_api_id:
                  "gid://shopify/ProductImage/15051208097875",
              },
            },
            mediaText: null,
            redirectUrl:
              "https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-04-holly-golightly-nude",
          },
          {
            id: 181,
            sectionId: 6,
            webViewTitle: null,
            contentType: 3,
            mediaUrl: null,
            product_json: {
              id: 10730303756,
              title:
                "Matte As Hell Crayon Lipstick - 09 Princess Peach (Peach)",
              body_html:
                '<p><span data-sheets-value="{&quot;1&quot;:2,&quot;2&quot;:&quot;· 09 Princess Peach (Peach) - For the \'My lips but definitely better\' look.&quot;}" data-sheets-userformat=\'{"2":31233,"3":{"1":0},"12":0,"14":[null,2,3355443],"15":"Symbol","16":12,"17":1}\' data-sheets-hyperlinkruns=\'[null,5,"https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-09-princess-peach-peach"]{"1":19}\'>If excellent coverage and high colour pay-off is what gets you going, you will absolutely love our <strong>SUGAR Matte As Hell Crayon Lipstick</strong> that is available in 36 gorgeous shades to suit your taste &amp; your ever-changing mood! Super long-lasting and highly pigmented so you don’t have to worry about constant re-application, this must-have crayon lipstick has a soft texture with a silky matte finish.<strong><br><br>09 Princess Peach (Peach)</strong> - For the \'My lips but definitely better\' look.</span></p>\n<p><strong>Bonus:</strong><span> </span><span>Each </span><span>Crayon Lipstick Shade</span><span> comes with its specially designed high quality sharpener that is carefully crafted to help you make the most of your purchase. </span><span>Read more details </span><span>about the </span><strong><a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-sharpener" target="_blank" rel="noopener noreferrer">Crayon Lipstick Sharpener</a>.</strong><br></p>\n<p><span>Size: 2.8 gm.</span></p>\n<p><span><strong>Additional Details:</strong> The <strong>SUGAR Matte As Hell Crayon Lipstick</strong> is available in 36 Attractive shades viz., <strong><a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-01-scarlett-ohara-red" target="_blank" rel="noopener noreferrer">01 Scarlett O’ Hara</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-02-mary-poppins-fuchsia" target="_blank" rel="noopener noreferrer">02 Mary Poppins</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-03-poison-ivy-wine" target="_blank" rel="noopener noreferrer">03 Poison Ivy</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-04-holly-golightly-nude" target="_blank" rel="noopener noreferrer">04 Holly Golightly</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-05-rose-dawson-rose-pink" target="_blank" rel="noopener noreferrer">05 Rose Dawson</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-06-coraline-jones-orange-coral" target="_blank" rel="noopener noreferrer">06 Coraline Jones</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-07-viola-mauve-nude" target="_blank" rel="noopener noreferrer">07 Viola</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-08-jacky-brown-reddish-brown" target="_blank" rel="noopener noreferrer">08 Jackie Brown</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-09-princess-peach-peach" target="_blank" rel="noopener noreferrer">09 Princess Peach</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-10-cherry-darling-cherry-red" target="_blank" rel="noopener noreferrer">10 Cherry Darling</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-11-elle-woods-brown-nude" target="_blank" rel="noopener noreferrer">11 Elle Woods</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-baby-houseman-deep-pink" target="_blank" rel="noopener noreferrer">12 Baby Houseman</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-murphy-brown-chocolate-burgundy" target="_blank" rel="noopener noreferrer">13 Murphy Brown</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-violet-crawley-smokey-violet" target="_blank" rel="noopener noreferrer">14 Violet Crawley</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-15-stephanie-plum-plum-mauve" target="_blank" rel="noopener noreferrer">15 Stephanie Plum</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-16-claire-underwood-burnt-orange" target="_blank" rel="noopener noreferrer">16 Claire Underwood</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-17-brandy-harrington-rusty-reddish-pink" target="_blank" rel="noopener noreferrer">17 Brandy Harrington</a>, </strong><strong><a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-18-rosalind-nude-rose" target="_blank" rel="noopener noreferrer">18 Rosalind</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-19-emma-woodhouse-earthy-brown" target="_blank" rel="noopener noreferrer">19 Emma Woodhouse</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-20-buffy-summers-mid-tone-warm-nude" target="_blank" rel="noopener noreferrer">20 Buffy Summers</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-21-rose-tyler-light-fuchsia-deep-rose" target="_blank" rel="noopener noreferrer">21 Rose Tyler</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-22-donna-pinciotti-magenta-pink" target="_blank" rel="noopener noreferrer">22 Donna Pinciotti</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-23-jessica-day-dusty-coral" target="_blank" rel="noopener noreferrer">23 Jessica Day</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-24-rachel-berry-deep-berry" target="_blank" rel="noopener noreferrer">24 Rachel Berry,</a> <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-25-lily-aldrin-mauve-pink" target="_blank" rel="noopener noreferrer">25 Lily Aldrin,</a> <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-26-vianne-rocher" target="_blank" rel="noopener noreferrer">26 Vianne Rocher</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-27-sunny-randall" target="_blank" rel="noopener noreferrer">27 Sunny Randall</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-28-honey-rider" target="_blank" rel="noopener noreferrer">28 Honey Rider</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-29-molly-brown" target="_blank" rel="noopener noreferrer">29 Molly Brown</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-30-lillian-rose" target="_blank" rel="noopener noreferrer">30 Lillian Rose</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-31-poppy-adams" target="_blank" rel="noopener noreferrer">31 Poppy Adams</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-32-miss-rosa" target="_blank" rel="noopener noreferrer">32 Miss Rosa</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-33-pepper-anderson" target="_blank" rel="noopener noreferrer">33 Pepper Anderson</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-34-cherry-ames" target="_blank" rel="noopener noreferrer">34 Cherry Ames</a>, <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-35-claire-redfield" target="_blank" rel="noopener noreferrer">35 Claire Redfield </a></strong>and<strong> <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-36-veronica-mars" target="_blank" rel="noopener noreferrer">36 Veronica Mars</a>. </strong></span></p>\n<p><strong><b>SUGAR Matte As Hell Crayon Lipstick Ingredients</b>:<span> </span></strong>Dimethicone, Diisostearyl Malate, Synthetic Wax, Distarch Phosphate, Polybutene, Dimethicone/Vinyl Dimethicone Crosspolymer, Caprylyl Methicone, Dimethylimidazolidinone Rice Starch, Glyceryl Caprylate, Pentaerythrityl Tetra-Di-T-Butyl Hydroxyhydrocinnamate, Silica, Isoceteth - 10 May Contain: C.I. Nos. 45410, 15850, 19140.</p>\n<p><strong>MRP</strong>: Rs. 899 (incl. all taxes)<br><br><strong>Country of Origin</strong>: Germany<br><br><strong>Company Name</strong>: Schwan Cosmetics Germany Gmbh &amp; Co.Kg<br><br><strong>Company Address</strong>: Schwanweg 1, 90562 Heroldsberg - (Germany)<br><br></p>\n<h5>How to apply</h5>\n<p><span>Exfoliate your lips and apply SUGAR Matte As Hell Crayon Lipstick just the way you would apply any other lipstick. Blot and reapply. </span><span>Sharpen your </span><a href="https://in.sugarcosmetics.com/collections/matte-lipstick-collection" target="_blank" rel="noopener noreferrer"><strong>best matte lipstick</strong></a><span> when it starts to dull for best effect</span><span>.<br><br><strong>Best paired with</strong>:<br></span></p>\n<p><span>Partner this <a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick" target="_blank" rel="noopener noreferrer"><strong>best matte crayon lipstick</strong></a> with the best <strong><a href="https://in.sugarcosmetics.com/products/stroke-of-genius-heavy-duty-kohl" target="_blank" rel="noopener noreferrer">kajal pencil</a></strong> to your eyes. Next, use an <strong><a href="https://in.sugarcosmetics.com/products/bling-leader-illuminating-moisturizer" target="_blank" rel="noopener noreferrer">illuminating moisturizer</a></strong><a href="https://in.sugarcosmetics.com/products/bling-leader-illuminating-moisturizer"> </a>to get that perfect glow. Finish your perfect makeup with some <a href="https://in.sugarcosmetics.com/collections/the-sugar-contour-de-force-mini-blush-collection" target="_blank" rel="noopener noreferrer"><strong>blusher</strong></a> to the apple of your cheeks.</span></p>\n<h5>Benefits</h5>\n<p><strong><a href="https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick" target="_blank" rel="noopener noreferrer">SUGAR Matte As Hell Crayon Lipstick</a></strong><span> is powered by:</span></p>\n<p><span>- Highly pigmented formula </span><span>(For maximum coverage)</span></p>\n<p>- Smooth application (Glides easily)</p>\n<p><span>- <a href="https://in.sugarcosmetics.com/products/nothing-else-matter-longwear-lipstick" target="_blank" rel="noopener noreferrer"><strong>Long-lasting matte</strong></a></span><span> wear (Stays for hours)</span></p>\n<p><span>- Paraben-free </span><span>ingredients</span><span> </span></p>\n<p>- A cruelty free formula</p>\n<h5>Commonly asked questions</h5>\n<p>Q. What’s the finish like?<br>A. This crayon lipstick has a gorgeous, silky matte finish.<br><br>Q. What’s the net weight?<br>A. 2.8g.<br><br>Q. Is it vegan?<br>A. Yes, the SUGAR Matte As Hell Crayon Lipstick is vegan, and is also paraben-free.<br><br>Q. Do we need to sharpen it?<br>A. Yes. It comes with an original, high quality lipstick sharpener.<br><br>Q. Any nude shades available in the range?<br>A.<span> </span><span>For a great nude look, </span><span>try 04 Holly Golightly and 18 Rosalind. </span><span>Read more about sporting </span><strong><a href="https://blog.sugarcosmetics.com/nude-makeup-for-indian-skin-tones/">nude makeup for Indian skin tones</a></strong><span> here</span><span>.</span></p>',
              vendor: "SUGAR Cosmetics",
              product_type: "Crayon Lipstick",
              created_at: "2017-05-17T09:55:05+05:30",
              handle: "matte-as-hell-crayon-lipstick-09-princess-peach-peach",
              updated_at: "2020-12-31T14:42:56+05:30",
              published_at: "2017-05-18T10:36:00+05:30",
              template_suffix: "",
              published_scope: "web",
              tags:
                "899, 9 to 5, Alcohol Free, All Skin tones, Best selling colours, Bestseller, Bold, Chubby Lipstick, Chubby Pencil, crayon, Crayon Lipstick, Creamy consistency, Cruelty Free, Everyday wear, Festive, Full coverage, Gluten Free, High colour pay off, High coverage, Highly pigmented, Intense colour, Intensely Pigmented, Lasting colour, Light weight, Lip Crayon, lips, lipstick, Long lasting, Longlasting, Longwear, Made In Germany, Matte, Matte Crayon, Mineral Oil Free, Must have, Nude, Office wear, Opaque, Paraben Free, Paraffin free, Peach, Peach Nude, Princess Peach, Rich coverage, Scarlet, Silky Matte, Smudgeproof, Soft Texture, st_color: Nudes, st_Color: Orange, st_Color: Peach, st_Color: Red, st_feature: Crayon, st_feature: Long Lasting, st_feature: Smudge Proof, st_feature: Waterproof, st_finish: Matte, st_formulation: Crayon, st_type: Lipstick, sugar_type_0, Sulphate Free, Super Long lasting, Travel friendly, Ultimate comfort, Under 1000, Vegan, Vegetarian, Velvet, Water Resistant, Wax based formulation, Zero drying, Zero sticky",
              admin_graphql_api_id: "gid://shopify/Product/10730303756",
              variants: [
                {
                  id: 39831232332,
                  product_id: 10730303756,
                  title: "Default Title",
                  price: "539.00",
                  sku: "8904320703639",
                  position: 1,
                  inventory_policy: "deny",
                  compare_at_price: "899.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "Default Title",
                  option2: null,
                  option3: null,
                  created_at: "2017-05-17T09:55:05+05:30",
                  updated_at: "2020-12-31T14:42:56+05:30",
                  taxable: false,
                  barcode: "",
                  grams: 3,
                  image_id: null,
                  weight: 2.8,
                  weight_unit: "g",
                  inventory_item_id: 28269391052,
                  inventory_quantity: 3284,
                  old_inventory_quantity: 3284,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/39831232332",
                },
              ],
              options: [
                {
                  id: 13043725900,
                  product_id: 10730303756,
                  name: "Title",
                  position: 1,
                  values: ["Default Title"],
                },
              ],
              images: [
                {
                  id: 15051230314579,
                  product_id: 10730303756,
                  position: 1,
                  created_at: "2020-09-15T14:47:41+05:30",
                  updated_at: "2020-09-15T14:47:42+05:30",
                  alt:
                    "SUGAR Cosmetics Matte As Hell Crayon Lipstick - 09 Princess Peach (Peach)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-matte-as-hell-crayon-lipstick-09-princess-peach-peach-15051211833427.jpg?v=1600161462",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15051230314579",
                },
                {
                  id: 14271261016147,
                  product_id: 10730303756,
                  position: 2,
                  created_at: "2020-04-20T20:13:01+05:30",
                  updated_at: "2020-09-15T14:47:42+05:30",
                  alt:
                    "SUGAR Cosmetics Matte As Hell Crayon Lipstick - 09 Princess Peach (Peach)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-matte-as-hell-crayon-lipstick-09-princess-peach-peach-14271177457747.jpg?v=1600161462",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14271261016147",
                },
                {
                  id: 14376978743379,
                  product_id: 10730303756,
                  position: 3,
                  created_at: "2020-05-11T23:56:09+05:30",
                  updated_at: "2020-09-15T14:47:42+05:30",
                  alt:
                    "SUGAR Cosmetics Matte As Hell Crayon Lipstick - 09 Princess Peach (Peach)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-matte-as-hell-crayon-lipstick-09-princess-peach-peach-14376904360019.jpg?v=1600161462",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14376978743379",
                },
                {
                  id: 13737184067667,
                  product_id: 10730303756,
                  position: 4,
                  created_at: "2019-12-25T19:58:34+05:30",
                  updated_at: "2020-09-15T14:47:42+05:30",
                  alt:
                    "SUGAR Cosmetics Matte As Hell Crayon Lipstick - 09 Princess Peach (Peach)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-matte-as-hell-crayon-lipstick-09-princess-peach-peach-12775251804243.jpg?v=1600161462",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13737184067667",
                },
                {
                  id: 13737187147859,
                  product_id: 10730303756,
                  position: 5,
                  created_at: "2019-12-25T20:00:21+05:30",
                  updated_at: "2020-09-15T14:47:42+05:30",
                  alt:
                    "SUGAR Cosmetics Matte As Hell Crayon Lipstick - 09 Princess Peach (Peach)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-matte-as-hell-crayon-lipstick-09-princess-peach-peach-13737184559187.jpg?v=1600161462",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13737187147859",
                },
                {
                  id: 13737187835987,
                  product_id: 10730303756,
                  position: 6,
                  created_at: "2019-12-25T20:00:36+05:30",
                  updated_at: "2020-09-15T14:47:42+05:30",
                  alt:
                    "SUGAR Cosmetics Matte As Hell Crayon Lipstick - 09 Princess Peach (Peach)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-matte-as-hell-crayon-lipstick-09-princess-peach-peach-12775251869779.jpg?v=1600161462",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13737187835987",
                },
                {
                  id: 13729211351123,
                  product_id: 10730303756,
                  position: 7,
                  created_at: "2019-12-23T23:49:54+05:30",
                  updated_at: "2020-09-15T14:47:42+05:30",
                  alt:
                    "SUGAR Cosmetics Matte As Hell Crayon Lipstick - 09 Princess Peach (Peach)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-matte-as-hell-crayon-lipstick-09-princess-peach-peach-12793549783123.jpg?v=1600161462",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13729211351123",
                },
                {
                  id: 13729210531923,
                  product_id: 10730303756,
                  position: 8,
                  created_at: "2019-12-23T23:49:39+05:30",
                  updated_at: "2020-09-15T14:47:42+05:30",
                  alt:
                    "SUGAR Cosmetics Matte As Hell Crayon Lipstick - 09 Princess Peach (Peach)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-matte-as-hell-crayon-lipstick-09-princess-peach-peach-12775252033619.jpg?v=1600161462",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13729210531923",
                },
              ],
              image: {
                id: 15051230314579,
                product_id: 10730303756,
                position: 1,
                created_at: "2020-09-15T14:47:41+05:30",
                updated_at: "2020-09-15T14:47:42+05:30",
                alt:
                  "SUGAR Cosmetics Matte As Hell Crayon Lipstick - 09 Princess Peach (Peach)",
                width: 1025,
                height: 1400,
                src:
                  "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-matte-as-hell-crayon-lipstick-09-princess-peach-peach-15051211833427.jpg?v=1600161462",
                variant_ids: [],
                admin_graphql_api_id:
                  "gid://shopify/ProductImage/15051230314579",
              },
            },
            mediaText: null,
            redirectUrl:
              "https://in.sugarcosmetics.com/products/matte-as-hell-crayon-lipstick-09-princess-peach-peach",
          },
          {
            id: 74,
            sectionId: 6,
            webViewTitle: null,
            contentType: 3,
            mediaUrl: null,
            product_json: {
              id: 4395185209427,
              title: "Plush Crush Crème Crayon",
              body_html:
                '<p><span style="font-weight: 400;">A highly-pigmented, retractable crème lipstick with a long-lasting, water-resistant wear. SUGAR Plush Crush Crème Crayon comes enriched with Vitamin E, Collagen, Hyaluronic Acid and Pomegranate Fruit Extract for nourishment and hydration.</span><br></p>\n<ul>\n<li>\n<p><strong><a href="https://in.sugarcosmetics.com/products/plush-crush-creme-crayon-01-bourbon-bree-peach-brown" target="_blank" rel="noopener noreferrer">01 Bourbon Bree: Peach Brown</a> - </strong>Sass and class, wrapped in one!<br><br></p>\n</li>\n<li>\n<p><strong><a href="https://in.sugarcosmetics.com/products/plush-crush-creme-crayon-02-blush-babe-coral-peach" target="_blank" rel="noopener noreferrer">02 Blush Babe: Coral Peach</a> - </strong>Brings about the romance in the air.<br><br></p>\n</li>\n<li>\n<p><strong><a href="https://in.sugarcosmetics.com/products/plush-crush-creme-crayon-03-violet-vixen-warm-purple-with-hints-of-pink" target="_blank" rel="noopener noreferrer">03 Violet Vixen: Warm Purple with hints of pink</a> - </strong>Makes you look ravishing in its truest sense.<br><br></p>\n</li>\n<li>\n<p><strong><a href="https://in.sugarcosmetics.com/products/plush-crush-creme-crayon-04-red-rebel-blue-toned-red" target="_blank" rel="noopener noreferrer">04 Red Rebel: Blue toned Red</a> - </strong>Ensures every gaze is fixed on you.<br><br></p>\n</li>\n<li>\n<p><strong><a href="https://in.sugarcosmetics.com/products/plush-crush-creme-crayon-05-mauve-mama-warm-rose-mauve" target="_blank" rel="noopener noreferrer">05 Mauve Mama: Warm rose mauve</a> - </strong>Flirts like a bee, with an ambitious feel.<br><br></p>\n</li>\n<li>\n<p><strong><a href="https://in.sugarcosmetics.com/products/plush-crush-creme-crayon-06-grape-goddess-deep-mauve-with-hints-of-purple" target="_blank" rel="noopener noreferrer">06 Grape Goddess: Deep Mauve with hints of purple</a> - </strong>Classic and elegant, all at once.<br><br></p>\n</li>\n<li>\n<p><strong><a href="https://in.sugarcosmetics.com/products/plush-crush-creme-crayon-07-caramel-crush-caramel-nude" target="_blank" rel="noopener noreferrer">07 Caramel Crush: Caramel Nude</a> - </strong>A natural shade that brings out the goddess within, naturally.<br><br></p>\n</li>\n<li>\n<p><strong><a href="https://in.sugarcosmetics.com/products/plush-crush-creme-crayon-08-pink-princess-pink-with-a-blue-undertone" target="_blank" rel="noopener noreferrer">08 Pink Princess: Pink with a blue undertone</a> - </strong>Glams you up in no time<b>!</b><br><br></p>\n</li>\n<li>\n<p><strong><a href="https://in.sugarcosmetics.com/products/plush-crush-creme-crayon-09-peach-pixie-peach-rose" target="_blank" rel="noopener noreferrer">09 Peach Pixie: Peach Rose</a> - </strong>Can seem docile, but has a fierce heart.<br><br></p>\n</li>\n<li>\n<strong><a href="https://in.sugarcosmetics.com/products/plush-crush-creme-crayon-10-berry-belle-berry-red" target="_blank" rel="noopener noreferrer">10 Berry Belle: Berry Red</a> - </strong>Want to be the belle of the ball? Go with this one.</li>\n</ul>\n<br>\n<p><strong>Additional details</strong>: Plush Crush Crème Crayon is dermatologically tested and approved.<br> <br><strong>Net Weight</strong>: 1.8g<br> <br><strong>Plush Crush Crème Crayon Ingredients</strong>: DIISOSTEARYL MALATE, HYDROGENATED POLYISOBUTENE, HYDROGENATED STYRENE/METHYL STYRENE/INDENE COPOLYMER, POLYETHYLENE, RED 28 LAKE CI 45410, CAPRYLIC/CAPRIC TRIGLYCERIDE, SYNTHETIC WAX, YELLOW 5 LAKE CI 19140, RED 7 LAKE CI 15850, STEARALKONIUM HECTORITE, HYDROGENATED STYRENE/ISOPRENE COPOLYMER, COPERNICIA CERIFERA (CARNAUBA) WAX [CERA CARNAUBA], PROPYLENE CARBONATE, PENTAERYTHRITYL TETRA-DI-t-BUTYL, HYDROXYHYDROCINNAMATE, DICALCIUM PHOSPHATE, MICA<br> <br>Maximum Retail Price: Rs. 699 (incl. all taxes)<br> <br>Country of Origin: PRC<br> <br>Company Name: SUGAR Cosmetics LLC<br> <br>Company Address: 8 The Green Suite A, Dover, DE 19901, USA</p>\n<h5>How to apply</h5>\n<p>Gently twist and apply the SUGAR Plush Crush Crème Crayon on clean, exfoliated lips. The retractable, highly pigmented crayon ensures precise and proper application and coverage. Mix it with a matte lip colour for a slight sheen<br><br><strong>Best paired with</strong>:<br>Before you don your favourite SUGAR Plush Crush Crème Crayon shade, prep your lips with <a href="https://in.sugarcosmetics.com/products/seal-the-show-lip-primer" target="_blank" rel="noopener noreferrer"><strong>lip primer</strong></a> that will ensure the colour will stay in place longer. Complete your look with a beautiful <a href="https://in.sugarcosmetics.com/products/wingman-waterproof-microliner-01-ill-be-black-black" target="_blank" rel="noopener noreferrer"><strong>winged eyeliner</strong></a>, some mascara, and some <a href="https://in.sugarcosmetics.com/collections/blend-the-rules-eyeshadow-palette" target="_blank" rel="noopener noreferrer"><strong>eyeshadow makeup</strong></a> to compliment your lips. Finally, apply a touch of shine to the high points of your face with the <a href="https://in.sugarcosmetics.com/products/glow-and-behold-jelly-highlighter" target="_blank" rel="noopener noreferrer"><strong>best highlighter</strong></a>, to look picture-perfect!<br></p>\n<h5>Benefits</h5>\n<p>SUGAR Plush Crush Crème Crayon Powered by:<br> <br>- Vitamin E<br>  (Hydrates and nourishes)<br>  Read why Vitamin E nourished products are a must in your makeup kit, <a href="https://blog.sugarcosmetics.com/5-reasons-why-vitamin-e-enriched-products-are-a-must-have-in-your-makeup-kit/">here</a>. <br> <br>- Pomegranate extract<br>  (Restores lip colour)<br> <br>- Collagen<br>  (Smoothens wrinkles &amp; fine lines)<br> <br>- A cruelty-free formula</p>\n<h5>Commonly asked questions</h5>\n<p>Q. What’s the net weight?<br>A. The net weight of SUGAR Plush Crush Crème Crayon is 1.8g.<br> <br>Q. Does this lipstick bleed?<br>A. No. It is completely bleed-proof.<br> <br>Q. Is it water-resistant?<br>A. Yes, it’s water-resistant and highly pigmented.<br> <br>Q. How many shades are available in the range?<br>A. SUGAR Plush Crush Crème Crayon is available in 10 shades.<br> <br>Q. Can you suggest a shade in the mauve or magenta spectrum?<br>A. Yes, you can go for 05 Mauve Mama and 06 Grape Goddess.</p>',
              vendor: "SUGAR Cosmetics",
              product_type: "Creme Lipstick",
              created_at: "2020-01-31T13:06:10+05:30",
              handle: "plush-crush-creme-crayon",
              updated_at: "2021-01-13T13:00:26+05:30",
              published_at: "2020-02-04T12:26:37+05:30",
              template_suffix: "",
              published_scope: "web",
              tags:
                "9 to 5, Alcohol Free, All Skin tones, Bestseller, Bold, Creamy colour, Creamy consistency, Creamy lipstick, Creme Lipstick, Cruelty Free, Everyday wear, Full coverage, Gluten Free, High colour pay off, High coverage, Highly pigmented, Intense colour, Intensely Pigmented, Lasting colour, Light weight, Lips, Lipstick, Long lasting, Longwear, Matte, Mineral Oil Free, Must have, Opaque, Paraben Free, Paraffin free, Rich coverage, Silky matte, Smudgeproof, Soft texture, st_finish: Bullet, st_formulation: Crème, st_type: Lipstick, sugar_type_1, Sulphate Free, Super Long lasting, Travel friendly, Twist up, Ultimate comfort, Under 1000, Under 800, Vegan, Vegetarian, Water Resistant, Wax based formulation, Zero drying, Zero sticky",
              admin_graphql_api_id: "gid://shopify/Product/4395185209427",
              variants: [
                {
                  id: 31470708260947,
                  product_id: 4395185209427,
                  title: "01 Bourbon Bree (Peach Brown)",
                  price: "419.00",
                  sku: "8904320705329",
                  position: 1,
                  inventory_policy: "deny",
                  compare_at_price: "699.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "01 Bourbon Bree (Peach Brown)",
                  option2: null,
                  option3: null,
                  created_at: "2020-01-31T13:06:10+05:30",
                  updated_at: "2021-01-13T12:32:35+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13974635872339,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 33083158462547,
                  inventory_quantity: 5145,
                  old_inventory_quantity: 5145,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/31470708260947",
                },
                {
                  id: 31470708293715,
                  product_id: 4395185209427,
                  title: "02 Blush Babe (Coral Peach)",
                  price: "419.00",
                  sku: "8904320705336",
                  position: 2,
                  inventory_policy: "deny",
                  compare_at_price: "699.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "02 Blush Babe (Coral Peach)",
                  option2: null,
                  option3: null,
                  created_at: "2020-01-31T13:06:10+05:30",
                  updated_at: "2021-01-13T03:04:00+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13974631776339,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 33083158495315,
                  inventory_quantity: 5062,
                  old_inventory_quantity: 5062,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/31470708293715",
                },
                {
                  id: 31470708326483,
                  product_id: 4395185209427,
                  title: "03 Violet Vixen (Warm Purple with hints of pink)",
                  price: "419.00",
                  sku: "8904320705343",
                  position: 3,
                  inventory_policy: "deny",
                  compare_at_price: "699.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "03 Violet Vixen (Warm Purple with hints of pink)",
                  option2: null,
                  option3: null,
                  created_at: "2020-01-31T13:06:10+05:30",
                  updated_at: "2021-01-12T16:37:41+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13974633906259,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 33083158528083,
                  inventory_quantity: 5591,
                  old_inventory_quantity: 5591,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/31470708326483",
                },
                {
                  id: 31470708359251,
                  product_id: 4395185209427,
                  title: "04 Red Rebel (Blue toned Red)",
                  price: "419.00",
                  sku: "8904320705350",
                  position: 4,
                  inventory_policy: "deny",
                  compare_at_price: "699.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "04 Red Rebel (Blue toned Red)",
                  option2: null,
                  option3: null,
                  created_at: "2020-01-31T13:06:10+05:30",
                  updated_at: "2021-01-13T10:29:41+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13974666149971,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 33083158560851,
                  inventory_quantity: 4968,
                  old_inventory_quantity: 4968,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/31470708359251",
                },
                {
                  id: 31470708392019,
                  product_id: 4395185209427,
                  title: "05 Mauve Mama (Warm rose mauve)",
                  price: "419.00",
                  sku: "8904320705367",
                  position: 5,
                  inventory_policy: "deny",
                  compare_at_price: "699.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "05 Mauve Mama (Warm rose mauve)",
                  option2: null,
                  option3: null,
                  created_at: "2020-01-31T13:06:11+05:30",
                  updated_at: "2021-01-12T21:00:26+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13974661988435,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 33083158593619,
                  inventory_quantity: 5001,
                  old_inventory_quantity: 5001,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/31470708392019",
                },
                {
                  id: 31470708457555,
                  product_id: 4395185209427,
                  title: "06 Grape Goddess (Deep Mauve with hints of purple)",
                  price: "419.00",
                  sku: "8904320705374",
                  position: 6,
                  inventory_policy: "deny",
                  compare_at_price: "699.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "06 Grape Goddess (Deep Mauve with hints of purple)",
                  option2: null,
                  option3: null,
                  created_at: "2020-01-31T13:06:11+05:30",
                  updated_at: "2021-01-13T01:00:06+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13974652584019,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 33083158626387,
                  inventory_quantity: 4924,
                  old_inventory_quantity: 4924,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/31470708457555",
                },
                {
                  id: 31470708523091,
                  product_id: 4395185209427,
                  title: "07 Caramel Crush (Caramel Nude)",
                  price: "419.00",
                  sku: "8904320705381",
                  position: 7,
                  inventory_policy: "deny",
                  compare_at_price: "699.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "07 Caramel Crush (Caramel Nude)",
                  option2: null,
                  option3: null,
                  created_at: "2020-01-31T13:06:11+05:30",
                  updated_at: "2021-01-13T12:32:40+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13974655959123,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 33083158659155,
                  inventory_quantity: 3738,
                  old_inventory_quantity: 3738,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/31470708523091",
                },
                {
                  id: 31470708588627,
                  product_id: 4395185209427,
                  title: "08 Pink Princess (Pink with a blue undertone)",
                  price: "419.00",
                  sku: "8904320705398",
                  position: 8,
                  inventory_policy: "deny",
                  compare_at_price: "699.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "08 Pink Princess (Pink with a blue undertone)",
                  option2: null,
                  option3: null,
                  created_at: "2020-01-31T13:06:11+05:30",
                  updated_at: "2021-01-13T00:02:21+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13974658187347,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 33083158691923,
                  inventory_quantity: 5095,
                  old_inventory_quantity: 5095,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/31470708588627",
                },
                {
                  id: 31470708654163,
                  product_id: 4395185209427,
                  title: "09 Peach Pixie (Peach Rose)",
                  price: "419.00",
                  sku: "8904320705404",
                  position: 9,
                  inventory_policy: "deny",
                  compare_at_price: "699.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "09 Peach Pixie (Peach Rose)",
                  option2: null,
                  option3: null,
                  created_at: "2020-01-31T13:06:11+05:30",
                  updated_at: "2021-01-13T13:00:26+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13974646358099,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 33083158724691,
                  inventory_quantity: 4032,
                  old_inventory_quantity: 4032,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/31470708654163",
                },
                {
                  id: 31470708719699,
                  product_id: 4395185209427,
                  title: "10 Berry Belle (Berry Red)",
                  price: "419.00",
                  sku: "8904320705411",
                  position: 10,
                  inventory_policy: "deny",
                  compare_at_price: "699.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "10 Berry Belle (Berry Red)",
                  option2: null,
                  option3: null,
                  created_at: "2020-01-31T13:06:11+05:30",
                  updated_at: "2021-01-13T12:32:40+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13974640820307,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 33083158757459,
                  inventory_quantity: 5212,
                  old_inventory_quantity: 5212,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/31470708719699",
                },
              ],
              options: [
                {
                  id: 5706487595091,
                  product_id: 4395185209427,
                  name: "Colour",
                  position: 1,
                  values: [
                    "01 Bourbon Bree (Peach Brown)",
                    "02 Blush Babe (Coral Peach)",
                    "03 Violet Vixen (Warm Purple with hints of pink)",
                    "04 Red Rebel (Blue toned Red)",
                    "05 Mauve Mama (Warm rose mauve)",
                    "06 Grape Goddess (Deep Mauve with hints of purple)",
                    "07 Caramel Crush (Caramel Nude)",
                    "08 Pink Princess (Pink with a blue undertone)",
                    "09 Peach Pixie (Peach Rose)",
                    "10 Berry Belle (Berry Red)",
                  ],
                },
              ],
              images: [
                {
                  id: 13974635872339,
                  product_id: 4395185209427,
                  position: 1,
                  created_at: "2020-01-31T13:10:05+05:30",
                  updated_at: "2020-01-31T13:15:31+05:30",
                  alt: "SUGAR Cosmetics Plush Crush Creme Crayon",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-plush-crush-creme-crayon-13974621519955.jpg?v=1580456731",
                  variant_ids: [31470708260947],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13974635872339",
                },
                {
                  id: 13974631776339,
                  product_id: 4395185209427,
                  position: 2,
                  created_at: "2020-01-31T13:08:58+05:30",
                  updated_at: "2020-01-31T13:15:31+05:30",
                  alt: "SUGAR Cosmetics Plush Crush Creme Crayon",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-plush-crush-creme-crayon-13974622830675.jpg?v=1580456731",
                  variant_ids: [31470708293715],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13974631776339",
                },
                {
                  id: 13974633906259,
                  product_id: 4395185209427,
                  position: 3,
                  created_at: "2020-01-31T13:09:30+05:30",
                  updated_at: "2020-01-31T13:15:31+05:30",
                  alt: "SUGAR Cosmetics Plush Crush Creme Crayon",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-plush-crush-creme-crayon-13974624043091.jpg?v=1580456731",
                  variant_ids: [31470708326483],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13974633906259",
                },
                {
                  id: 13974666149971,
                  product_id: 4395185209427,
                  position: 4,
                  created_at: "2020-01-31T13:15:31+05:30",
                  updated_at: "2020-01-31T13:15:31+05:30",
                  alt: "SUGAR Cosmetics Plush Crush Creme Crayon",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-plush-crush-creme-crayon-13974625386579.jpg?v=1580456731",
                  variant_ids: [31470708359251],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13974666149971",
                },
                {
                  id: 13974661988435,
                  product_id: 4395185209427,
                  position: 5,
                  created_at: "2020-01-31T13:14:58+05:30",
                  updated_at: "2020-01-31T13:15:31+05:30",
                  alt: "SUGAR Cosmetics Plush Crush Creme Crayon",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-plush-crush-creme-crayon-13974626631763.jpg?v=1580456731",
                  variant_ids: [31470708392019],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13974661988435",
                },
                {
                  id: 13974652584019,
                  product_id: 4395185209427,
                  position: 6,
                  created_at: "2020-01-31T13:12:55+05:30",
                  updated_at: "2020-01-31T13:15:31+05:30",
                  alt: "SUGAR Cosmetics Plush Crush Creme Crayon",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-plush-crush-creme-crayon-13974627418195.jpg?v=1580456731",
                  variant_ids: [31470708457555],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13974652584019",
                },
                {
                  id: 13974655959123,
                  product_id: 4395185209427,
                  position: 7,
                  created_at: "2020-01-31T13:13:28+05:30",
                  updated_at: "2020-01-31T13:15:31+05:30",
                  alt: "SUGAR Cosmetics Plush Crush Creme Crayon",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-plush-crush-creme-crayon-13974628335699.jpg?v=1580456731",
                  variant_ids: [31470708523091],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13974655959123",
                },
                {
                  id: 13974658187347,
                  product_id: 4395185209427,
                  position: 8,
                  created_at: "2020-01-31T13:14:02+05:30",
                  updated_at: "2020-01-31T13:15:31+05:30",
                  alt: "SUGAR Cosmetics Plush Crush Creme Crayon",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-plush-crush-creme-crayon-13974629384275.jpg?v=1580456731",
                  variant_ids: [31470708588627],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13974658187347",
                },
                {
                  id: 13974646358099,
                  product_id: 4395185209427,
                  position: 9,
                  created_at: "2020-01-31T13:11:36+05:30",
                  updated_at: "2020-01-31T13:15:31+05:30",
                  alt: "SUGAR Cosmetics Plush Crush Creme Crayon",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-plush-crush-creme-crayon-13974630826067.jpg?v=1580456731",
                  variant_ids: [31470708654163],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13974646358099",
                },
                {
                  id: 13974640820307,
                  product_id: 4395185209427,
                  position: 10,
                  created_at: "2020-01-31T13:11:01+05:30",
                  updated_at: "2020-01-31T13:15:31+05:30",
                  alt: "SUGAR Cosmetics Plush Crush Creme Crayon",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-plush-crush-creme-crayon-13974632038483.jpg?v=1580456731",
                  variant_ids: [31470708719699],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13974640820307",
                },
              ],
              image: {
                id: 13974635872339,
                product_id: 4395185209427,
                position: 1,
                created_at: "2020-01-31T13:10:05+05:30",
                updated_at: "2020-01-31T13:15:31+05:30",
                alt: "SUGAR Cosmetics Plush Crush Creme Crayon",
                width: 1025,
                height: 1400,
                src:
                  "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-plush-crush-creme-crayon-13974621519955.jpg?v=1580456731",
                variant_ids: [31470708260947],
                admin_graphql_api_id:
                  "gid://shopify/ProductImage/13974635872339",
              },
            },
            mediaText: null,
            redirectUrl:
              "https://in.sugarcosmetics.com/products/plush-crush-creme-crayon",
          },
          {
            id: 161,
            sectionId: 6,
            webViewTitle: null,
            contentType: 3,
            mediaUrl: null,
            product_json: {
              id: 1477082677331,
              title: "Born To Wing Gel Eyeliner",
              body_html:
                'Make way, ladies - the saviour for all your winged eye woes, is here! With its silky smooth formula and a rich matte finish, the <strong>Born To Wing Gel Eyeliner</strong>, is the queen of ultra-luxe. Achieve absolute symmetry in everything from the chic-yet-casual look to the sultry cat-eye with this beauty and that’s not all. Smudge-proof and water-resistant, your eyes will now sport the boldest of tug-free flicks for 12 straight hours. Grab one today for those effortless ‘Oh! So dramatic’ looks that will stay with you through thick and thin.<br><br>\n<p>Comes with a built-in extendable brush that helps you ace those perfect luscious strokes on the go. Available in 5 signature shades, this eyeliner is the Gel of the New Generation.</p>\n<ul>\n<li>Cast an enchanting spell everywhere you go with <a href="https://in.sugarcosmetics.com/products/born-to-wing-gel-eyeliner-01-blackmagic-woman" target="_blank" rel="noopener noreferrer"><strong>01</strong> <strong>BLACKMAGIC WOMAN</strong></a>. With the charred black drama on your lids, you are one hell of a sorceress.<br><br>\n</li>\n<li>Slay that badass Old West vibe with the<a href="https://in.sugarcosmetics.com/products/born-to-wing-gel-eyeliner-02-roadhouse-blues" target="_blank" rel="noopener noreferrer"><strong> 02 ROADHOUSE BLUES</strong></a>. If you are done with the mundane and adventure is what stirs your soul, put some Roadhouse Blues on and glide to glory!<br><br>\n</li>\n<li>\n<span>Grace the world with the utmost poise and fineness of </span><strong><a href="https://in.sugarcosmetics.com/products/born-to-wing-gel-eyeliner-03-brown-sugar-walnut-brown" target="_blank" rel="noopener noreferrer">03 BROWN SUGAR</a>.</strong><span> A timeless walnut brown classic, it is as elegant as it gets.<br><br></span>\n</li>\n<li><span>Cheat your way into a state of trance with <a href="https://in.sugarcosmetics.com/products/born-to-wing-gel-eyeliner-04-purple-haze-grape-purple" target="_blank" rel="noopener noreferrer"><strong>04 PURPLE HAZE</strong></a> - intense grape purple hue to bedazzle your eyes.<br><br></span></li>\n<li><span>Stand out of the crowd with fierce blended perfection of <a href="https://in.sugarcosmetics.com/products/born-to-wing-gel-eyeliner-05-green-eyes-dark-green-deep-green" target="_blank" rel="noopener noreferrer"><strong>05</strong> <strong>GREEN EYES</strong></a> – a dramatic deep green for that perfect feline flick!</span></li>\n</ul>\n<p><strong><u>How to Apply:</u></strong> Dip the built-in brush into the gel eyeliner pot. Dab it gently on the edge to remove excess. Trace a line from the inner corner, extend it towards the outer corner of the eye and rock that super-rad wing!</p>\n<p><strong><u>Benefits: </u></strong>With rich, creamy pigment and a bulletproof wear <strong>SUGAR Born To Wing Gel Eyeliner</strong> glides across skin smoothly to create superb, smudge-proof strokes with saturated colour. Its excellent waterproof formula has legit staying powers<strong><u>. </u></strong></p>\n<p><strong><u>Additional Details: </u></strong>The richly pigmented Born To Wing Gel Eyeliner is dermatologically and ophthalmologically tested and approved. It is available in 5 fabulous shades, <strong><a href="https://in.sugarcosmetics.com/products/born-to-wing-gel-eyeliner-01-blackmagic-woman" target="_blank" rel="noopener noreferrer">01 Blackmagic Woman</a>, <a href="https://in.sugarcosmetics.com/products/born-to-wing-gel-eyeliner-02-roadhouse-blues" target="_blank" rel="noopener noreferrer">02 Roadhouse Blues</a>,<span> </span><a href="https://in.sugarcosmetics.com/products/born-to-wing-gel-eyeliner-03-brown-sugar-walnut-brown" target="_blank" rel="noopener noreferrer">03 Brown Sugar</a><span>, <a href="https://in.sugarcosmetics.com/products/born-to-wing-gel-eyeliner-04-purple-haze-grape-purple" target="_blank" rel="noopener noreferrer">04 Purple Haze</a> </span></strong>and<strong><span> </span><a href="https://in.sugarcosmetics.com/products/born-to-wing-gel-eyeliner-05-green-eyes-dark-green-deep-green" target="_blank" rel="noopener noreferrer">05 Green Eyes.</a></strong></p>\n<p><strong></strong><strong>Net Weight</strong>: 2.3 gm</p>\n<p><strong><u>List of ingredients: </u></strong>Cyclopentasiloxane, Trimethylsiloxysilicate, Paraffin, Isododecane, Ethylhexyl Palmitate, Mica, Sorbitan Stearate, Microcrystalline Wax, Silica Dimethyl Silylate, Phenoxyethanol, Tin Oxide.  May Contain: C.I Nos. 77499, 77266, 77891, 42090, 15850.</p>\n<p><strong>MRP</strong><span>: Rs. 599 (incl. all taxes)</span><br><br><strong>Country of Origin</strong><span>: </span><span mce-data-marked="1">Taiwan</span><br><br><strong>Company Name</strong><span>:</span><span> Beaunion Colours Co. Ltd.</span><br><br><strong>Company Address</strong><span>:</span><span> 2F , No. 163, Liufen Rd. Wai Pu Dist., Taichung City 43857</span></p>',
              vendor: "SUGAR Cosmetics",
              product_type: "Eyeliner",
              created_at: "2018-10-08T13:46:00+05:30",
              handle: "born-to-wing-gel-eyeliner",
              updated_at: "2021-01-13T12:35:07+05:30",
              published_at: "2018-10-09T09:32:51+05:30",
              template_suffix: "",
              published_scope: "web",
              tags:
                "10 HR, 12 HR, 599, 9 to 5, Alcohol Free, amp-hide, Bold, Budgeproof, Built in Brush, Built in extendable brush, Cat Eye, Creamy Pigment, Cruelty Free, Dermatologically Tested, Easy application, Easy Wing, Every day wear, Eye, Eye Liner, Eyeliner, Gel, Gel Eyeliner, Gluten Free, High Coverage, Intense, Long Lasting, Long wear, Made in Taiwan, Matte, Mineral Oil Free, No Fade, Ophthalmologically tested, Paraben Free, Pot Eyeliner, Precise Application, Rich matte finish, Saturated colour, Silky smooth formula, Smooth Application, Smudgeproof, st_finish: Matte, st_formulation: Gel, st_type: Eyeliner, sugar_type_1, Sulphate Free, Travel friendly, Under 600, Vegan, Vegetarian, Water resistant, Waterproof, Winged Eyeliner, With Brush",
              admin_graphql_api_id: "gid://shopify/Product/1477082677331",
              variants: [
                {
                  id: 13545549430867,
                  product_id: 1477082677331,
                  title: "01 Blackmagic Woman (Black)",
                  price: "599.00",
                  sku: "8906090495660",
                  position: 1,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "01 Blackmagic Woman (Black)",
                  option2: null,
                  option3: null,
                  created_at: "2018-10-08T13:46:00+05:30",
                  updated_at: "2021-01-13T12:26:01+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13728200065107,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 13644020088915,
                  inventory_quantity: 3756,
                  old_inventory_quantity: 3756,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/13545549430867",
                },
                {
                  id: 13545549463635,
                  product_id: 1477082677331,
                  title: "02 Roadhouse Blues (Denim Blue)",
                  price: "420.00",
                  sku: "8906090495677",
                  position: 2,
                  inventory_policy: "deny",
                  compare_at_price: "599.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "02 Roadhouse Blues (Denim Blue)",
                  option2: null,
                  option3: null,
                  created_at: "2018-10-08T13:46:00+05:30",
                  updated_at: "2021-01-13T12:26:07+05:30",
                  taxable: false,
                  barcode: "",
                  grams: 0,
                  image_id: 13728198393939,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 13644020121683,
                  inventory_quantity: 512,
                  old_inventory_quantity: 512,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/13545549463635",
                },
                {
                  id: 29509221941331,
                  product_id: 1477082677331,
                  title: "03 Brown Sugar (Walnut Brown)",
                  price: "420.00",
                  sku: "8904320702793",
                  position: 3,
                  inventory_policy: "deny",
                  compare_at_price: "599.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "03 Brown Sugar (Walnut Brown)",
                  option2: null,
                  option3: null,
                  created_at: "2019-09-12T11:37:38+05:30",
                  updated_at: "2021-01-13T12:26:07+05:30",
                  taxable: false,
                  barcode: "",
                  grams: 0,
                  image_id: 13728192397395,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 30688691716179,
                  inventory_quantity: 2822,
                  old_inventory_quantity: 2822,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/29509221941331",
                },
                {
                  id: 29509222072403,
                  product_id: 1477082677331,
                  title: "04 Purple Haze (Grape Purple)",
                  price: "420.00",
                  sku: "8904320702809",
                  position: 4,
                  inventory_policy: "deny",
                  compare_at_price: "599.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "04 Purple Haze (Grape Purple)",
                  option2: null,
                  option3: null,
                  created_at: "2019-09-12T11:37:57+05:30",
                  updated_at: "2021-01-13T12:33:01+05:30",
                  taxable: false,
                  barcode: "",
                  grams: 0,
                  image_id: 13728144392275,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 30688691847251,
                  inventory_quantity: 4977,
                  old_inventory_quantity: 4977,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/29509222072403",
                },
                {
                  id: 29509222334547,
                  product_id: 1477082677331,
                  title: "05 Green Eyes (Dark Green/Deep Green)",
                  price: "420.00",
                  sku: "8904320702816",
                  position: 5,
                  inventory_policy: "deny",
                  compare_at_price: "599.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "05 Green Eyes (Dark Green/Deep Green)",
                  option2: null,
                  option3: null,
                  created_at: "2019-09-12T11:38:15+05:30",
                  updated_at: "2021-01-13T12:33:01+05:30",
                  taxable: false,
                  barcode: "",
                  grams: 0,
                  image_id: 13728145277011,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 30688693420115,
                  inventory_quantity: 3800,
                  old_inventory_quantity: 3800,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/29509222334547",
                },
              ],
              options: [
                {
                  id: 1974471688275,
                  product_id: 1477082677331,
                  name: "Colour",
                  position: 1,
                  values: [
                    "01 Blackmagic Woman (Black)",
                    "02 Roadhouse Blues (Denim Blue)",
                    "03 Brown Sugar (Walnut Brown)",
                    "04 Purple Haze (Grape Purple)",
                    "05 Green Eyes (Dark Green/Deep Green)",
                  ],
                },
              ],
              images: [
                {
                  id: 13728200065107,
                  product_id: 1477082677331,
                  position: 1,
                  created_at: "2019-12-23T14:35:39+05:30",
                  updated_at: "2019-12-23T14:35:41+05:30",
                  alt:
                    "SUGAR Cosmetics Born To Wing Gel Eyeliner 01 Blackmagic Woman",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-born-to-wing-gel-eyeliner-01-blackmagic-woman-13271970283603.jpg?v=1577091941",
                  variant_ids: [13545549430867],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13728200065107",
                },
                {
                  id: 13728198393939,
                  product_id: 1477082677331,
                  position: 2,
                  created_at: "2019-12-23T14:35:22+05:30",
                  updated_at: "2019-12-23T14:35:41+05:30",
                  alt:
                    "SUGAR Cosmetics Born To Wing Gel Eyeliner 02 Roadhouse Blues",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-born-to-wing-gel-eyeliner-02-roadhouse-blues-13271973691475.jpg?v=1577091941",
                  variant_ids: [13545549463635],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13728198393939",
                },
                {
                  id: 13728192397395,
                  product_id: 1477082677331,
                  position: 3,
                  created_at: "2019-12-23T14:33:56+05:30",
                  updated_at: "2019-12-23T14:35:41+05:30",
                  alt:
                    "SUGAR Cosmetics Born To Wing Gel Eyeliner 03 Brown Sugar (Walnut Brown)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-born-to-wing-gel-eyeliner-03-brown-sugar-walnut-brown-13271845797971.jpg?v=1577091941",
                  variant_ids: [29509221941331],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13728192397395",
                },
                {
                  id: 13728145277011,
                  product_id: 1477082677331,
                  position: 4,
                  created_at: "2019-12-23T14:23:53+05:30",
                  updated_at: "2019-12-23T14:35:41+05:30",
                  alt:
                    "SUGAR Cosmetics Born To Wing Gel Eyeliner 05 Green Eyes (Dark Green/Deep Green)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-born-to-wing-gel-eyeliner-05-green-eyes-dark-green-deep-green-13271846518867.jpg?v=1577091941",
                  variant_ids: [29509222334547],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13728145277011",
                },
                {
                  id: 13728144392275,
                  product_id: 1477082677331,
                  position: 5,
                  created_at: "2019-12-23T14:23:36+05:30",
                  updated_at: "2019-12-23T14:35:41+05:30",
                  alt:
                    "SUGAR Cosmetics Born To Wing Gel Eyeliner 04 Purple Haze (Grape Purple)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-born-to-wing-gel-eyeliner-04-purple-haze-grape-purple-13271848222803.jpg?v=1577091941",
                  variant_ids: [29509222072403],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13728144392275",
                },
              ],
              image: {
                id: 13728200065107,
                product_id: 1477082677331,
                position: 1,
                created_at: "2019-12-23T14:35:39+05:30",
                updated_at: "2019-12-23T14:35:41+05:30",
                alt:
                  "SUGAR Cosmetics Born To Wing Gel Eyeliner 01 Blackmagic Woman",
                width: 1025,
                height: 1400,
                src:
                  "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-born-to-wing-gel-eyeliner-01-blackmagic-woman-13271970283603.jpg?v=1577091941",
                variant_ids: [13545549430867],
                admin_graphql_api_id:
                  "gid://shopify/ProductImage/13728200065107",
              },
            },
            mediaText: null,
            redirectUrl:
              "https://in.sugarcosmetics.com/products/born-to-wing-gel-eyeliner",
          },
          {
            id: 144,
            sectionId: 6,
            webViewTitle: null,
            contentType: 3,
            mediaUrl: null,
            product_json: {
              id: 4776716038,
              title: "It's A-pout Time! Vivid Lipstick",
              body_html:
                '<span style="font-weight: 400;">If you’ve been crushing over classic matte lips – enriched with micronized pigments and silica microbeads – you’ll totally adore </span><b>SUGAR It\'s A-Pout Time! Vivid Lipstick</b><span style="font-weight: 400;">. Creamy and moisturising, this lipstick combines the look of a </span><strong><a href="https://in.sugarcosmetics.com/products/sugar-mettle-matte-lipstick">classic matte </a></strong><span style="font-weight: 400;">lip, with the benefit of incredible comfort.</span><br><br>\n<ul>\n<li>\n<a href="https://in.sugarcosmetics.com/products/its-a-pout-time-vivid-lipstick-01-the-big-bang-berry-wine"><strong>01</strong></a><strong><a href="https://in.sugarcosmetics.com/products/its-a-pout-time-vivid-lipstick-01-the-big-bang-berry-wine"> The Big Bang Berry (Wine)</a></strong><span style="line-height: 1.4;"> <span style="font-weight: 400;">A shade that spells out power and charisma.</span>.<br><br></span>\n</li>\n<li>\n<strong><a href="https://in.sugarcosmetics.com/products/its-a-pout-time-vivid-lipstick-02-breaking-bare-rose-pink?variant=15621115462">02</a><a href="https://in.sugarcosmetics.com/products/its-a-pout-time-vivid-lipstick-02-breaking-bare-rose-pink?variant=15621115462"> Breaking Bare</a><a href="https://in.sugarcosmetics.com/products/its-a-pout-time-vivid-lipstick-02-breaking-bare-rose-pink?variant=15621115462"> (Mauve Pink)</a></strong><strong style="line-height: 1.4;"> <span style="font-weight: 400;">Feminine and charming, we bet you’ll carry off this shade like you two were soul mates.</span></strong><span style="line-height: 1.4;"><br><br></span>\n</li>\n<li>\n<a href="https://in.sugarcosmetics.com/products/its-a-pout-time-vivid-lipstick-03-mad-magenta-magenta?variant=15621143942"><strong>03</strong></a><strong><a href="https://in.sugarcosmetics.com/products/its-a-pout-time-vivid-lipstick-03-mad-magenta-magenta?variant=15621143942"> Mad Magenta (Magenta)</a></strong><span style="line-height: 1.4;"> <span style="font-weight: 400;">Bold and unapologetic, this is the perfect shade for someone as unique as you.</span><br><br></span>\n</li>\n<li><span style="line-height: 1.4;"><a href="https://in.sugarcosmetics.com/products/its-a-pout-time-vivid-lipstick-05-that-70s-red-red"><strong>05</strong></a><strong><a href="https://in.sugarcosmetics.com/products/its-a-pout-time-vivid-lipstick-05-that-70s-red-red"> That ’70s Red (Red)</a></strong><span> <span style="font-weight: 400;">A lip colour shade for the femme fatale in you.</span><br><br></span></span></li>\n<li><span style="line-height: 1.4;"><span><a href="https://in.sugarcosmetics.com/products/its-a-pout-time-vivid-lipstick-06-peachy-little-liars-nude-pink?variant=22917148678"><strong>06</strong></a><strong><a href="https://in.sugarcosmetics.com/products/its-a-pout-time-vivid-lipstick-06-peachy-little-liars-nude-pink?variant=22917148678"> Peachy Little Liars (Pink)</a></strong> <span style="font-weight: 400;">A charming mix of everything fun and flirty!</span><br><br></span></span></li>\n<li><span style="line-height: 1.4;"><span><strong><a href="https://in.sugarcosmetics.com/products/its-a-pout-time-vivid-lipstick-07-twilight-rose-rose-pink"><strong>07</strong></a><a href="https://in.sugarcosmetics.com/products/its-a-pout-time-vivid-lipstick-07-twilight-rose-rose-pink"> <b>The Twilight Rose</b></a><a href="https://in.sugarcosmetics.com/products/its-a-pout-time-vivid-lipstick-07-twilight-rose-rose-pink"><b> (Rose Pink)</b></a></strong> This go-to shade lends itself perfectly to your rose-tinted runway of life.<br><br></span></span></li>\n<li>\n<strong><a href="https://in.sugarcosmetics.com/products/its-a-pout-time-vivid-lipstick-08-brownton-abbey-peach-brown">08</a><a href="https://in.sugarcosmetics.com/products/its-a-pout-time-vivid-lipstick-08-brownton-abbey-peach-brown"> Brownton Abbey</a><a href="https://in.sugarcosmetics.com/products/its-a-pout-time-vivid-lipstick-08-brownton-abbey-peach-brown"> (Peach Brown)</a></strong> <span style="font-weight: 400;">This subtle shade is made for the woman who knows what she wants and gets it, with some elegance and a whole lot of class!</span><br><br>\n</li>\n<li>\n<strong><a href="https://in.sugarcosmetics.com/products/its-a-pout-time-vivid-lipstick-09-better-call-salmon-peach-pink">09</a><a href="https://in.sugarcosmetics.com/products/its-a-pout-time-vivid-lipstick-09-better-call-salmon-peach-pink"> Better Call Salmon</a><a href="https://in.sugarcosmetics.com/products/its-a-pout-time-vivid-lipstick-09-better-call-salmon-peach-pink"> (Peach Pink)</a></strong> <span style="font-weight: 400;">A matte lip colour shade for a vivacious diva.</span><br><br>\n</li>\n<li>\n<strong><a href="https://in.sugarcosmetics.com/products/its-a-pout-time-vivid-lipstick-10-true-oxblood-burgundy-red"><strong>10</strong></a><a href="https://in.sugarcosmetics.com/products/its-a-pout-time-vivid-lipstick-10-true-oxblood-burgundy-red"> <b>True Oxblood</b></a><a href="https://in.sugarcosmetics.com/products/its-a-pout-time-vivid-lipstick-10-true-oxblood-burgundy-red"><b> (Burgundy Red)</b></a> <span style="font-weight: 400;">A beautiful matte lip colour shade for the girl boss you are.</span></strong><br><br>\n</li>\n<li>\n<strong><a href="https://in.sugarcosmetics.com/products/its-a-pout-time-vivid-lipstick-11-six-feet-umber-mocha-brown">11</a><a href="https://in.sugarcosmetics.com/products/its-a-pout-time-vivid-lipstick-11-six-feet-umber-mocha-brown"> Six Feet Umber</a><a href="https://in.sugarcosmetics.com/products/its-a-pout-time-vivid-lipstick-11-six-feet-umber-mocha-brown"> (Mocha Brown)</a></strong> <span style="font-weight: 400;">Striving for perfection comes naturally to you, this colour is for you!</span>\n</li>\n</ul>\n<p><span style="font-weight: 400;"></span><b>Net Weight:</b><span style="font-weight: 400;"> 3.5 gm.</span></p>\n<p><b>SUGAR It’s A-Pout Time! Vivid Lipstick Ingredients: </b><span style="font-weight: 400;">Pentaerythrityl Tetraisostearate, Silica, Polyethylene,Octyldodecyl Stearoyl Stearate, C12-15 Alkyl Benzoate, Octyldodecanol, Synthetic Beeswax, Isocetyl Stearate, Vp/Eicosene Copolymer, Euphorbia Cerifera (Candelilla) Wax,Flavor, Diethylhexyl Syringylidenemalonate, Caprylic/Capric Triglyceride, Mica, Talc, Tin Oxide, Titanium Dioxide, Iron Oxides, Bismuth Oxychloride, Red 6, Yellow 5 Lake, Red 7 Lake, Blue 1 Lake, Red 28 Lake.</span></p>\n<p><span style="font-weight: 400;">Maximum Retail Price: Rs 699 - Rs 599 (incl. all taxes)<br> <br>Formulated in Italy<br> <br>Company Name: Regi India Cosmetics Pvt Ltd.<br> <br>Company Address: C-08, Sara Ind Estate, Vill-Rampur, Selaqui, Dehradun 248197, Uttarakhand.</span></p>\n<p> </p>\n<h5>How to apply</h5>\n<p><span style="font-weight: 400;">Start applying SUGAR </span><b>It’s A-Pout Time! Vivid Lipstick</b><span style="font-weight: 400;"> from the center of your upper lip moving outward towards the corners. Blot your lips with a tissue and reapply the </span><span style="font-weight: 400;">matte lipstick.</span></p>\n<p><span style="font-weight: 400;">Best paired with:</span></p>\n<p><span style="font-weight: 400;">This highly pigmented matte lipstick can be enhanced with a creamy</span><strong><a href="https://in.sugarcosmetics.com/products/lipping-on-the-edge-lip-liner"> lip liner</a></strong><span style="font-weight: 400;"> .</span></p>\n<p><span style="font-weight: 400;">Complement SUGAR <b>It’s A-Pout Time! Vivid Lipstick</b>’s matte goodness with a<strong><a href="https://in.sugarcosmetics.com/products/eye-warned-you-so-double-matte-eyeliner"> double matte eyeliner</a></strong> and the<strong><a href="https://in.sugarcosmetics.com/collections/blend-the-rules-eyeshadow-palette"> best eyeshadow makeup</a></strong>! Finish off your look with a hint of<strong><a href="https://in.sugarcosmetics.com/collections/the-sugar-contour-de-force-mini-highlighter-collection"> highlighter makeup</a></strong> on your cheeks and the inner corners of your eyes.</span></p>\n<p> </p>\n<h5>Benefits</h5>\n<p>SUGAR It’s A-Pout Time! Vivid Lipstick is powered by:<br><br>- Creamy texture<br>(Spreads evenly on lips)<br> <br>- Rich pigment<br>(Stays for long)<br> <br>- Hydrating formula<br>(Moisturizes lips)<br> <br>- Cruelty-free formula</p>\n<h5>Commonly asked questions</h5>\n<p>Q. What’s the net weight?<br>A. 3.5g<br> <br>Q. What’s the finish like?<br>A. It’s a very classy matte finish.<br> <br>Q. How many shades are available in the range?<br>A. SUGAR It’s A-Pout Time! Vivid Lipstick is available in 10 shades.<br> <br>Q. Is it paraben free?<br>A. Yes. It’s paraben free and cruelty free.<br> <br>Q. Any shade in the wine spectrum?<br>A. Yes. We’d recommend 01 The Big Bang Berry.</p>',
              vendor: "SUGAR Cosmetics",
              product_type: "Vivid Lipstick",
              created_at: "2016-02-18T15:06:46+05:30",
              handle: "its-a-pout-time-vivid-lipstick",
              updated_at: "2020-12-31T14:30:26+05:30",
              published_at: "2016-02-22T12:25:00+05:30",
              template_suffix: "",
              published_scope: "web",
              tags:
                "'70s Red, amp-hide, Breaking Bare, Coral, Coraline In The City, Lips, lipstick, Mad Magenta, Matte, Mauve Pink, Peachy Little Liars, st_finish: Matte, st_formulation: Bullet, st_type: Lipstick, sugar_type_1, The Big Bang Berry, Vivid, Wine",
              admin_graphql_api_id: "gid://shopify/Product/4776716038",
              variants: [
                {
                  id: 15620361798,
                  product_id: 4776716038,
                  title: "01 The Big Bang Berry (Wine)",
                  price: "419.00",
                  sku: "8906090490061",
                  position: 1,
                  inventory_policy: "deny",
                  compare_at_price: "699.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "01 The Big Bang Berry (Wine)",
                  option2: null,
                  option3: null,
                  created_at: "2016-02-18T15:06:46+05:30",
                  updated_at: "2020-12-31T14:30:20+05:30",
                  taxable: false,
                  barcode: "",
                  grams: 4,
                  image_id: 13736528805971,
                  weight: 4,
                  weight_unit: "g",
                  inventory_item_id: 7047306950,
                  inventory_quantity: 509,
                  old_inventory_quantity: 509,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/15620361798",
                },
                {
                  id: 15620707974,
                  product_id: 4776716038,
                  title: "02 Breaking Bare (Mauve Pink)",
                  price: "359.00",
                  sku: "8906090490078",
                  position: 2,
                  inventory_policy: "deny",
                  compare_at_price: "599.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "02 Breaking Bare (Mauve Pink)",
                  option2: null,
                  option3: null,
                  created_at: "2016-02-18T15:14:08+05:30",
                  updated_at: "2020-12-31T13:30:50+05:30",
                  taxable: false,
                  barcode: "",
                  grams: 4,
                  image_id: 13736530051155,
                  weight: 4,
                  weight_unit: "g",
                  inventory_item_id: 3106364614,
                  inventory_quantity: 617,
                  old_inventory_quantity: 617,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/15620707974",
                },
                {
                  id: 15620708038,
                  product_id: 4776716038,
                  title: "03 Mad Magenta (Magenta)",
                  price: "359.00",
                  sku: "8906090490085",
                  position: 3,
                  inventory_policy: "deny",
                  compare_at_price: "599.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "03 Mad Magenta (Magenta)",
                  option2: null,
                  option3: null,
                  created_at: "2016-02-18T15:14:08+05:30",
                  updated_at: "2020-10-23T15:24:29+05:30",
                  taxable: false,
                  barcode: "",
                  grams: 4,
                  image_id: 13736534474835,
                  weight: 4,
                  weight_unit: "g",
                  inventory_item_id: 3052684934,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/15620708038",
                },
                {
                  id: 13550424490067,
                  product_id: 4776716038,
                  title: "05 That ’70s Red (Red)",
                  price: "359.00",
                  sku: "8906090490108",
                  position: 4,
                  inventory_policy: "deny",
                  compare_at_price: "599.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "05 That ’70s Red (Red)",
                  option2: null,
                  option3: null,
                  created_at: "2018-10-10T13:37:03+05:30",
                  updated_at: "2020-12-01T12:27:41+05:30",
                  taxable: false,
                  barcode: "",
                  grams: 4,
                  image_id: 13728807256147,
                  weight: 4,
                  weight_unit: "g",
                  inventory_item_id: 13648992370771,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/13550424490067",
                },
                {
                  id: 22916683974,
                  product_id: 4776716038,
                  title: "06 Peachy Little Liars (Nude Pink)",
                  price: "359.00",
                  sku: "8906090490115",
                  position: 5,
                  inventory_policy: "deny",
                  compare_at_price: "599.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "06 Peachy Little Liars (Nude Pink)",
                  option2: null,
                  option3: null,
                  created_at: "2016-06-23T17:46:52+05:30",
                  updated_at: "2020-12-31T14:30:26+05:30",
                  taxable: false,
                  barcode: "",
                  grams: 4,
                  image_id: 13728805617747,
                  weight: 0.004,
                  weight_unit: "kg",
                  inventory_item_id: 12510633606,
                  inventory_quantity: 237,
                  old_inventory_quantity: 237,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/22916683974",
                },
                {
                  id: 41698985996,
                  product_id: 4776716038,
                  title: "07 Twilight Rose (Rose Pink)",
                  price: "359.00",
                  sku: "8906090491563",
                  position: 6,
                  inventory_policy: "deny",
                  compare_at_price: "599.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "07 Twilight Rose (Rose Pink)",
                  option2: null,
                  option3: null,
                  created_at: "2017-06-22T10:02:40+05:30",
                  updated_at: "2020-12-31T14:30:26+05:30",
                  taxable: false,
                  barcode: "",
                  grams: 4,
                  image_id: 13728804438099,
                  weight: 4,
                  weight_unit: "g",
                  inventory_item_id: 30077169292,
                  inventory_quantity: 271,
                  old_inventory_quantity: 271,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/41698985996",
                },
                {
                  id: 41699026188,
                  product_id: 4776716038,
                  title: "08 Brownton Abbey (Peach Brown)",
                  price: "359.00",
                  sku: "8906090491570",
                  position: 7,
                  inventory_policy: "deny",
                  compare_at_price: "599.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "08 Brownton Abbey (Peach Brown)",
                  option2: null,
                  option3: null,
                  created_at: "2017-06-22T10:03:20+05:30",
                  updated_at: "2020-12-07T14:30:11+05:30",
                  taxable: false,
                  barcode: "",
                  grams: 4,
                  image_id: 13736535425107,
                  weight: 4,
                  weight_unit: "g",
                  inventory_item_id: 30077212300,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/41699026188",
                },
                {
                  id: 41699085132,
                  product_id: 4776716038,
                  title: "09 Better Call Salmon (Peach Pink)",
                  price: "359.00",
                  sku: "8906090491587",
                  position: 8,
                  inventory_policy: "deny",
                  compare_at_price: "599.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "09 Better Call Salmon (Peach Pink)",
                  option2: null,
                  option3: null,
                  created_at: "2017-06-22T10:03:59+05:30",
                  updated_at: "2020-12-15T16:28:17+05:30",
                  taxable: false,
                  barcode: "",
                  grams: 4,
                  image_id: 13736542928979,
                  weight: 4,
                  weight_unit: "g",
                  inventory_item_id: 30077270924,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/41699085132",
                },
                {
                  id: 41699126284,
                  product_id: 4776716038,
                  title: "10 True Oxblood (Burgundy Red)",
                  price: "359.00",
                  sku: "8906090491594",
                  position: 9,
                  inventory_policy: "deny",
                  compare_at_price: "599.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "10 True Oxblood (Burgundy Red)",
                  option2: null,
                  option3: null,
                  created_at: "2017-06-22T10:04:37+05:30",
                  updated_at: "2020-12-03T09:57:56+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 4,
                  image_id: 13736543780947,
                  weight: 4,
                  weight_unit: "g",
                  inventory_item_id: 30077310924,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/41699126284",
                },
                {
                  id: 41699172620,
                  product_id: 4776716038,
                  title: "11 Six Feet Umber (Mocha Brown)",
                  price: "359.00",
                  sku: "8906090491600",
                  position: 10,
                  inventory_policy: "deny",
                  compare_at_price: "599.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "11 Six Feet Umber (Mocha Brown)",
                  option2: null,
                  option3: null,
                  created_at: "2017-06-22T10:05:15+05:30",
                  updated_at: "2020-11-06T21:32:07+05:30",
                  taxable: false,
                  barcode: "",
                  grams: 4,
                  image_id: 13736547942483,
                  weight: 4,
                  weight_unit: "g",
                  inventory_item_id: 30077357132,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/41699172620",
                },
              ],
              options: [
                {
                  id: 5875677894,
                  product_id: 4776716038,
                  name: "Colour",
                  position: 1,
                  values: [
                    "01 The Big Bang Berry (Wine)",
                    "02 Breaking Bare (Mauve Pink)",
                    "03 Mad Magenta (Magenta)",
                    "05 That ’70s Red (Red)",
                    "06 Peachy Little Liars (Nude Pink)",
                    "07 Twilight Rose (Rose Pink)",
                    "08 Brownton Abbey (Peach Brown)",
                    "09 Better Call Salmon (Peach Pink)",
                    "10 True Oxblood (Burgundy Red)",
                    "11 Six Feet Umber (Mocha Brown)",
                  ],
                },
              ],
              images: [
                {
                  id: 13736528805971,
                  product_id: 4776716038,
                  position: 1,
                  created_at: "2019-12-25T15:08:38+05:30",
                  updated_at: "2019-12-25T15:14:25+05:30",
                  alt:
                    "SUGAR Cosmetics It's A-pout Time! Vivid Lipstick 01 The Big Bang Berry (Wine)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-it-s-a-pout-time-vivid-lipstick-01-the-big-bang-berry-wine-13204815937619.jpg?v=1577267065",
                  variant_ids: [15620361798],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13736528805971",
                },
                {
                  id: 13736530051155,
                  product_id: 4776716038,
                  position: 2,
                  created_at: "2019-12-25T15:08:55+05:30",
                  updated_at: "2019-12-25T15:14:25+05:30",
                  alt:
                    "SUGAR Cosmetics It's A-pout Time! Vivid Lipstick 02 Breaking Bare (Mauve Pink)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-it-s-a-pout-time-vivid-lipstick-02-breaking-bare-mauve-pink-13204816101459.jpg?v=1577267065",
                  variant_ids: [15620707974],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13736530051155",
                },
                {
                  id: 13736534474835,
                  product_id: 4776716038,
                  position: 3,
                  created_at: "2019-12-25T15:10:26+05:30",
                  updated_at: "2019-12-25T15:14:25+05:30",
                  alt:
                    "SUGAR Cosmetics It's A-pout Time! Vivid Lipstick 03 Mad Magenta (Magenta)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-it-s-a-pout-time-vivid-lipstick-03-mad-magenta-magenta-13204816298067.jpg?v=1577267065",
                  variant_ids: [15620708038],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13736534474835",
                },
                {
                  id: 13728807256147,
                  product_id: 4776716038,
                  position: 4,
                  created_at: "2019-12-23T18:52:00+05:30",
                  updated_at: "2019-12-25T15:14:25+05:30",
                  alt:
                    "SUGAR Cosmetics It's A-pout Time! Vivid Lipstick 05 That ’70s Red (Red)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-it-s-a-pout-time-vivid-lipstick-05-that-70s-red-red-13204816560211.jpg?v=1577267065",
                  variant_ids: [13550424490067],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13728807256147",
                },
                {
                  id: 13728805617747,
                  product_id: 4776716038,
                  position: 5,
                  created_at: "2019-12-23T18:51:43+05:30",
                  updated_at: "2019-12-25T15:14:25+05:30",
                  alt:
                    "SUGAR Cosmetics It's A-pout Time! Vivid Lipstick 06 Peachy Little Liars (Nude Pink)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-it-s-a-pout-time-vivid-lipstick-06-peachy-little-liars-nude-pink-13204816789587.jpg?v=1577267065",
                  variant_ids: [22916683974],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13728805617747",
                },
                {
                  id: 13728804438099,
                  product_id: 4776716038,
                  position: 6,
                  created_at: "2019-12-23T18:51:26+05:30",
                  updated_at: "2019-12-25T15:14:25+05:30",
                  alt:
                    "SUGAR Cosmetics It's A-pout Time! Vivid Lipstick 07 Twilight Rose (Rose Pink)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-it-s-a-pout-time-vivid-lipstick-07-twilight-rose-rose-pink-13204817018963.jpg?v=1577267065",
                  variant_ids: [41698985996],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13728804438099",
                },
                {
                  id: 13736535425107,
                  product_id: 4776716038,
                  position: 7,
                  created_at: "2019-12-25T15:11:00+05:30",
                  updated_at: "2019-12-25T15:14:25+05:30",
                  alt:
                    "SUGAR Cosmetics It's A-pout Time! Vivid Lipstick 08 Brownton Abbey (Peach Brown)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-it-s-a-pout-time-vivid-lipstick-08-brownton-abbey-peach-brown-13204817215571.jpg?v=1577267065",
                  variant_ids: [41699026188],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13736535425107",
                },
                {
                  id: 13736542928979,
                  product_id: 4776716038,
                  position: 8,
                  created_at: "2019-12-25T15:12:20+05:30",
                  updated_at: "2019-12-25T15:14:25+05:30",
                  alt:
                    "SUGAR Cosmetics It's A-pout Time! Vivid Lipstick 09 Better Call Salmon (Peach Pink)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-it-s-a-pout-time-vivid-lipstick-09-better-call-salmon-peach-pink-13204817412179.jpg?v=1577267065",
                  variant_ids: [41699085132],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13736542928979",
                },
                {
                  id: 13736543780947,
                  product_id: 4776716038,
                  position: 9,
                  created_at: "2019-12-25T15:12:35+05:30",
                  updated_at: "2019-12-25T15:14:25+05:30",
                  alt:
                    "SUGAR Cosmetics It's A-pout Time! Vivid Lipstick 10 True Oxblood (Burgundy Red)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-it-s-a-pout-time-vivid-lipstick-10-true-oxblood-burgundy-red-13204817608787.jpg?v=1577267065",
                  variant_ids: [41699126284],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13736543780947",
                },
                {
                  id: 13736547942483,
                  product_id: 4776716038,
                  position: 10,
                  created_at: "2019-12-25T15:14:24+05:30",
                  updated_at: "2019-12-25T15:14:25+05:30",
                  alt:
                    "SUGAR Cosmetics It's A-pout Time! Vivid Lipstick 11 Six Feet Umber (Mocha Brown)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-it-s-a-pout-time-vivid-lipstick-11-six-feet-umber-mocha-brown-13204817969235.jpg?v=1577267065",
                  variant_ids: [41699172620],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13736547942483",
                },
              ],
              image: {
                id: 13736528805971,
                product_id: 4776716038,
                position: 1,
                created_at: "2019-12-25T15:08:38+05:30",
                updated_at: "2019-12-25T15:14:25+05:30",
                alt:
                  "SUGAR Cosmetics It's A-pout Time! Vivid Lipstick 01 The Big Bang Berry (Wine)",
                width: 1025,
                height: 1400,
                src:
                  "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-it-s-a-pout-time-vivid-lipstick-01-the-big-bang-berry-wine-13204815937619.jpg?v=1577267065",
                variant_ids: [15620361798],
                admin_graphql_api_id:
                  "gid://shopify/ProductImage/13736528805971",
              },
            },
            mediaText: null,
            redirectUrl:
              "https://in.sugarcosmetics.com/products/its-a-pout-time-vivid-lipstick",
          },
          {
            id: 159,
            sectionId: 6,
            webViewTitle: null,
            contentType: 3,
            mediaUrl: null,
            product_json: {
              id: 6117057990,
              title: "Stroke Of Genius Heavy-Duty Kohl",
              body_html:
                'Eye makeup can be tricky but a stroke of genius can put you at ease. Add to that intense colour and soft creamy texture and you know you\'ve been hooked for life. We\'re on about <strong>SUGAR Stroke of Genius Heavy-Duty Kohl</strong> - a waterproof Kohl pencil that can add pretty to your eyes in a single swipe. Its wax-based formula spreads evenly and lasts for up to 8 hours. Not just that, it comes with a free sharpener to ensure your eye makeup is always on point.\n<ul>\n<li>\n<p>For all you feisty bold women who never go out of style because you go<span> </span><a href="https://in.sugarcosmetics.com/products/stroke-of-genius-heavy-duty-kohl-01-back-to-black"><strong>Back To Black</strong>!</a> Entice the world with your power when you don this<span> </span>pitch black<span> </span>temptress.</p>\n</li>\n<li>\n<span>For all you strong sassy women who don’t wait for the storm to pass, but dance in the </span><a href="https://in.sugarcosmetics.com/products/stroke-of-genius-heavy-duty-kohl-02-purple-rain"><strong>Purple Rain</strong></a><span>. Captivate the world with your guts when you swipe this purple seductress.<br><br></span>\n</li>\n<li>\n<p>For all you spirited women with LIT attitude, whose hearts revel in the<span> </span><strong><a href="https://in.sugarcosmetics.com/products/stroke-of-genius-heavy-duty-kohl-03-green-light">Green Light</a>.<span> </span></strong>Allure the cosmos with your passion when you sport this green goddess.</p>\n</li>\n<li>\n<p>For the once in a lifetime kind of women who own the ground they walk on with<span> </span><strong><a href="https://in.sugarcosmetics.com/products/stroke-of-genius-heavy-duty-kohl-04-blue-suede-shoes">Blue Suede Shoes</a>.<span> </span></strong>Amaze the Universe with your confidence as you slay this royal blue bombshell.</p>\n</li>\n<li>\n<p>For all you alluring women who have everyone under their spell, and in your eyes, have the sins of<span> </span><a href="https://in.sugarcosmetics.com/products/stroke-of-genius-heavy-duty-kohl-05-black-magic"><strong>Black Magic</strong></a>. Bewitch the whole world with vigour as you rock this black enchantress with silver glitter.</p>\n</li>\n<li>\n<span>For all you passionate women out there who aim for the </span><a href="https://in.sugarcosmetics.com/products/stroke-of-genius-heavy-duty-kohl-06-blue-skies-dark-blue-navy-blue" target="_blank" rel="noopener noreferrer"><strong>Blue Skies</strong></a><span>. Stun them with style as you swipe this navy blue wonder on.</span><br><br>\n</li>\n<li>For all you charming women who walk with the captivating grace of <strong><a href="https://in.sugarcosmetics.com/products/stroke-of-genius-heavy-duty-kohl-07-peacock-peacock-green-velvet-green" target="_blank" rel="noopener noreferrer">Peacock</a>. </strong>Dazzle everyone with this fascinating peacock green.<br><br>\n</li>\n<li>\n<span>For the vivacious woman who is the quintessential </span><a href="https://in.sugarcosmetics.com/products/stroke-of-genius-heavy-duty-kohl-08-brown-eyed-girl-walnut-brown" target="_blank" rel="noopener noreferrer"><strong>Brown Eyed Girl</strong></a><span>.</span><strong><span> </span></strong><span>Mesmerize them all with this gorgeous walnut brown.</span>\n</li>\n</ul>\n<p><strong>Bonus:</strong> Every Stroke of Genius Heavy-Duty Kohl comes with its specially designed high quality sharpener that is carefully crafted to help you make the most of your purchase. For more details about the <a href="https://in.sugarcosmetics.com/products/stroke-of-genius-heavy-duty-kohl-sharpener"><strong>Stroke of Genius Heavy-Duty Kohl Sharpener</strong></a>.</p>\n<p><strong>How to apply:</strong> Gently rim your lower and upper lash lines with this creamy Kohl. Open your eyes wide for precise application.</p>\n<p><strong>Benefits:</strong> This high-performance waterproof Kohl provides maximum colour in a single stroke and lasts for up to 8 hours. Its creamy texture makes application super easy and helps to create varied eye makeup looks in a matter of minutes.</p>\n<p><strong>Additional details:</strong><span> The </span><strong>SUGAR Stroke of Genius Heavy-Duty Kohl</strong><span> is currently available in the 8 shades </span><a href="https://in.sugarcosmetics.com/products/stroke-of-genius-heavy-duty-kohl-01-back-to-black" target="_blank" rel="noopener noreferrer"><strong>01 Back to Black</strong></a><span>, </span><a href="https://in.sugarcosmetics.com/products/stroke-of-genius-heavy-duty-kohl-02-purple-rain" target="_blank" rel="noopener noreferrer"><strong>02 Purple Rain</strong></a><strong>,<span> </span></strong><a href="https://in.sugarcosmetics.com/products/stroke-of-genius-heavy-duty-kohl-03-green-light" target="_blank" rel="noopener noreferrer"><strong>03 Green Light</strong></a><strong>,<span> </span></strong><a href="https://in.sugarcosmetics.com/products/stroke-of-genius-heavy-duty-kohl-04-blue-suede-shoes" target="_blank" rel="noopener noreferrer"><strong>04 Blue Suede Shoes</strong></a><a href="https://in.sugarcosmetics.com/products/stroke-of-genius-heavy-duty-kohl-05-black-magic" target="_blank" rel="noopener noreferrer"><strong>,</strong></a><strong><span> </span></strong><a href="https://in.sugarcosmetics.com/products/stroke-of-genius-heavy-duty-kohl-05-black-magic" target="_blank" rel="noopener noreferrer"><strong>05 Black Magic</strong></a><strong>,<span> </span><a href="https://in.sugarcosmetics.com/products/stroke-of-genius-heavy-duty-kohl-06-blue-skies-dark-blue-navy-blue" target="_blank" rel="noopener noreferrer">06 Blue Skies</a>,<span> </span><a href="https://in.sugarcosmetics.com/products/stroke-of-genius-heavy-duty-kohl-07-peacock-peacock-green-velvet-green" target="_blank" rel="noopener noreferrer">07 <span>Peacock</span></a>,<span> </span><a href="https://in.sugarcosmetics.com/products/stroke-of-genius-heavy-duty-kohl-08-brown-eyed-girl-walnut-brown" target="_blank" rel="noopener noreferrer">08 Brown Eyed Girl</a>.<span> </span></strong><span>It is free of mineral oil, paraffin, preservatives and volatile cyclomethicones (D4, D5, D6).</span><br><br>Size: <strong>1.2 gm</strong></p>\n<p><strong>List of Ingredients:</strong> Iron Oxides, Black 2, Mica, Ferric Ferrocyanide, Dimethicone, Synthetic Wax, Silica, Stearyl Dimethicone, Caprylic/Capric Triglyceride, Trimethylsiloxysilicate/Dimethiconol Crosspolymer, Octadecene, Dimethicone/Vinyl Dimethicone Crosspolymer, Pentaerythrityl Tetra-Di-T-Butyl Hydroxyhydrocinnamate, Isoceteth-10.</p>\n<p><strong>MRP</strong><span>: Rs. 499 (incl. all taxes)</span><br><br><strong>Country of Origin</strong><span>: Germany</span><br><br><strong>Company Name</strong><span>: Schwan Cosmetics Germany Gmbh &amp; Co.Kg</span><br><br><strong>Company Address</strong><span>: Schwanweg 1, 90562 Heroldsberg - (Germany)</span></p>',
              vendor: "SUGAR Cosmetics",
              product_type: "Kohl",
              created_at: "2016-04-29T15:27:45+05:30",
              handle: "stroke-of-genius-heavy-duty-kohl",
              updated_at: "2021-01-13T12:59:12+05:30",
              published_at: "2018-03-29T17:41:17+05:30",
              template_suffix: "",
              published_scope: "web",
              tags:
                "12 Hour Wear, 499, Affordable, amp-hide, Black, Blister Pack, Bold Eyes, Bold Makeup, Budge Proof, Budget Friendly, Buttery Soft, College, Colossal, Creamy, Creamy Texture, Cruelty Free, Easy to Apply, Easy to Use, Economic, Essentials, Everyday, Eye Makeup, Eyeconic, Eyes, Festive, Glides Easily, Intense Pigmentation, Kajal, Kohl, Light Weight, Long Lasting, Made in Germany, Made In India, Mineral Oil Free, Office, One Stroke Application, One Swipe Application, Opaque, Pabaren Free, Pigmented, Pocket Friendly, Precise Application, Sensitive Eyes, Smokey Eye makeup, Smooth Application, Smudge Proof, Soft, sugar_type_1, Transfer Proof, Travel Friendly, Twist up Kajal, Under 500, Water Proof, Water Resistant, Waterline, Wax, Work",
              admin_graphql_api_id: "gid://shopify/Product/6117057990",
              variants: [
                {
                  id: 19364046470,
                  product_id: 6117057990,
                  title: "01 Back to Black",
                  price: "499.00",
                  sku: "8904320705596",
                  position: 1,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "01 Back to Black",
                  option2: null,
                  option3: null,
                  created_at: "2016-04-29T15:27:45+05:30",
                  updated_at: "2021-01-13T12:30:46+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 1,
                  image_id: 13731297296467,
                  weight: 1,
                  weight_unit: "g",
                  inventory_item_id: 7240768326,
                  inventory_quantity: 5119,
                  old_inventory_quantity: 5119,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/19364046470",
                },
                {
                  id: 13602580889683,
                  product_id: 6117057990,
                  title: "02 Purple Rain (Purple)",
                  price: "299.00",
                  sku: "8904320705602",
                  position: 2,
                  inventory_policy: "deny",
                  compare_at_price: "499.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "02 Purple Rain (Purple)",
                  option2: null,
                  option3: null,
                  created_at: "2018-10-30T11:56:36+05:30",
                  updated_at: "2020-12-22T11:33:47+05:30",
                  taxable: false,
                  barcode: "",
                  grams: 1,
                  image_id: 13731363848275,
                  weight: 1,
                  weight_unit: "g",
                  inventory_item_id: 13719226056787,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/13602580889683",
                },
                {
                  id: 13602581282899,
                  product_id: 6117057990,
                  title: "03 Green Light (Green)",
                  price: "499.00",
                  sku: "8904320705619",
                  position: 3,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "03 Green Light (Green)",
                  option2: null,
                  option3: null,
                  created_at: "2018-10-30T11:57:09+05:30",
                  updated_at: "2021-01-12T15:33:10+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 1,
                  image_id: 13731361390675,
                  weight: 1,
                  weight_unit: "g",
                  inventory_item_id: 13719227236435,
                  inventory_quantity: 1573,
                  old_inventory_quantity: 1573,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/13602581282899",
                },
                {
                  id: 13602581479507,
                  product_id: 6117057990,
                  title: "04 Blue Suede Shoes (Royal Blue)",
                  price: "499.00",
                  sku: "8904320705626",
                  position: 4,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "04 Blue Suede Shoes (Royal Blue)",
                  option2: null,
                  option3: null,
                  created_at: "2018-10-30T11:57:36+05:30",
                  updated_at: "2021-01-13T11:30:25+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 1,
                  image_id: 13731355557971,
                  weight: 1,
                  weight_unit: "g",
                  inventory_item_id: 13719227957331,
                  inventory_quantity: 267,
                  old_inventory_quantity: 267,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/13602581479507",
                },
                {
                  id: 13602581839955,
                  product_id: 6117057990,
                  title: "05 Black Magic (Black With Silver Glitter)",
                  price: "499.00",
                  sku: "8904320705633",
                  position: 5,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "05 Black Magic (Black With Silver Glitter)",
                  option2: null,
                  option3: null,
                  created_at: "2018-10-30T11:58:15+05:30",
                  updated_at: "2021-01-13T04:26:25+05:30",
                  taxable: false,
                  barcode: "",
                  grams: 1,
                  image_id: 13731302113363,
                  weight: 1,
                  weight_unit: "g",
                  inventory_item_id: 13719228317779,
                  inventory_quantity: 5524,
                  old_inventory_quantity: 5524,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/13602581839955",
                },
                {
                  id: 29484696502355,
                  product_id: 6117057990,
                  title: "06 Blue Skies (Dark Blue/Navy Blue)",
                  price: "499.00",
                  sku: "8904320705640",
                  position: 6,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "06 Blue Skies (Dark Blue/Navy Blue)",
                  option2: null,
                  option3: null,
                  created_at: "2019-08-29T12:03:17+05:30",
                  updated_at: "2021-01-13T12:59:12+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 1,
                  image_id: 13731301654611,
                  weight: 1,
                  weight_unit: "g",
                  inventory_item_id: 30651818213459,
                  inventory_quantity: 32,
                  old_inventory_quantity: 32,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/29484696502355",
                },
                {
                  id: 29484698271827,
                  product_id: 6117057990,
                  title: "07 Peacock (Peacock Green/Velvet Green)",
                  price: "499.00",
                  sku: "8904320705657",
                  position: 7,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "07 Peacock (Peacock Green/Velvet Green)",
                  option2: null,
                  option3: null,
                  created_at: "2019-08-29T12:03:55+05:30",
                  updated_at: "2021-01-12T19:39:45+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 1,
                  image_id: 13731301621843,
                  weight: 1,
                  weight_unit: "g",
                  inventory_item_id: 30651821293651,
                  inventory_quantity: 89,
                  old_inventory_quantity: 89,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/29484698271827",
                },
                {
                  id: 29484700336211,
                  product_id: 6117057990,
                  title: "08 Brown Eyed Girl (Walnut Brown)",
                  price: "499.00",
                  sku: "8904320705664",
                  position: 8,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "08 Brown Eyed Girl (Walnut Brown)",
                  option2: null,
                  option3: null,
                  created_at: "2019-08-29T12:04:28+05:30",
                  updated_at: "2021-01-13T10:28:30+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 1,
                  image_id: 13731354837075,
                  weight: 1,
                  weight_unit: "g",
                  inventory_item_id: 30651823358035,
                  inventory_quantity: 61,
                  old_inventory_quantity: 61,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/29484700336211",
                },
              ],
              options: [
                {
                  id: 7353745030,
                  product_id: 6117057990,
                  name: "Colour",
                  position: 1,
                  values: [
                    "01 Back to Black",
                    "02 Purple Rain (Purple)",
                    "03 Green Light (Green)",
                    "04 Blue Suede Shoes (Royal Blue)",
                    "05 Black Magic (Black With Silver Glitter)",
                    "06 Blue Skies (Dark Blue/Navy Blue)",
                    "07 Peacock (Peacock Green/Velvet Green)",
                    "08 Brown Eyed Girl (Walnut Brown)",
                  ],
                },
              ],
              images: [
                {
                  id: 13731297296467,
                  product_id: 6117057990,
                  position: 1,
                  created_at: "2019-12-24T11:13:23+05:30",
                  updated_at: "2019-12-24T11:35:43+05:30",
                  alt:
                    "SUGAR Cosmetics Stroke Of Genius Heavy-Duty Kohl 01 Back to Black",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-stroke-of-genius-heavy-duty-kohl-01-back-to-black-13278410866771.jpg?v=1577167543",
                  variant_ids: [19364046470],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13731297296467",
                },
                {
                  id: 13731363848275,
                  product_id: 6117057990,
                  position: 2,
                  created_at: "2019-12-24T11:35:42+05:30",
                  updated_at: "2019-12-24T11:35:43+05:30",
                  alt:
                    "SUGAR Cosmetics Stroke Of Genius Heavy-Duty Kohl 02 Purple Rain (Purple)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-stroke-of-genius-heavy-duty-kohl-02-purple-rain-purple-13278411685971.jpg?v=1577167543",
                  variant_ids: [13602580889683],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13731363848275",
                },
                {
                  id: 13731361390675,
                  product_id: 6117057990,
                  position: 3,
                  created_at: "2019-12-24T11:35:27+05:30",
                  updated_at: "2019-12-24T11:35:43+05:30",
                  alt:
                    "SUGAR Cosmetics Stroke Of Genius Heavy-Duty Kohl 03 Green Light (Green)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-stroke-of-genius-heavy-duty-kohl-03-green-light-green-13278411817043.jpg?v=1577167543",
                  variant_ids: [13602581282899],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13731361390675",
                },
                {
                  id: 13731355557971,
                  product_id: 6117057990,
                  position: 4,
                  created_at: "2019-12-24T11:33:56+05:30",
                  updated_at: "2019-12-24T11:35:43+05:30",
                  alt:
                    "SUGAR Cosmetics Stroke Of Genius Heavy-Duty Kohl 04 Blue Suede Shoes (Royal Blue)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-stroke-of-genius-heavy-duty-kohl-04-blue-suede-shoes-royal-blue-13278413127763.jpg?v=1577167543",
                  variant_ids: [13602581479507],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13731355557971",
                },
                {
                  id: 13731302113363,
                  product_id: 6117057990,
                  position: 5,
                  created_at: "2019-12-24T11:15:50+05:30",
                  updated_at: "2019-12-24T11:35:43+05:30",
                  alt:
                    "SUGAR Cosmetics Stroke Of Genius Heavy-Duty Kohl 05 Black Magic (Black With Silver Glitter)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-stroke-of-genius-heavy-duty-kohl-05-black-magic-black-with-silver-glitter-13278414372947.jpg?v=1577167543",
                  variant_ids: [13602581839955],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13731302113363",
                },
                {
                  id: 13731301654611,
                  product_id: 6117057990,
                  position: 6,
                  created_at: "2019-12-24T11:15:34+05:30",
                  updated_at: "2019-12-24T11:35:43+05:30",
                  alt:
                    "SUGAR Cosmetics Stroke Of Genius Heavy-Duty Kohl 06 Blue Skies (Dark Blue/Navy Blue)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-stroke-of-genius-heavy-duty-kohl-06-blue-skies-dark-blue-navy-blue-13278416896083.jpg?v=1577167543",
                  variant_ids: [29484696502355],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13731301654611",
                },
                {
                  id: 13731301621843,
                  product_id: 6117057990,
                  position: 7,
                  created_at: "2019-12-24T11:15:21+05:30",
                  updated_at: "2019-12-24T11:35:43+05:30",
                  alt:
                    "SUGAR Cosmetics Stroke Of Genius Heavy-Duty Kohl 07 Peacock (Peacock Green/Velvet Green)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-stroke-of-genius-heavy-duty-kohl-07-peacock-peacock-green-velvet-green-13278417059923.jpg?v=1577167543",
                  variant_ids: [29484698271827],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13731301621843",
                },
                {
                  id: 13731354837075,
                  product_id: 6117057990,
                  position: 8,
                  created_at: "2019-12-24T11:33:39+05:30",
                  updated_at: "2019-12-24T11:35:43+05:30",
                  alt:
                    "SUGAR Cosmetics Stroke Of Genius Heavy-Duty Kohl 08 Brown Eyed Girl (Walnut Brown)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-stroke-of-genius-heavy-duty-kohl-08-brown-eyed-girl-walnut-brown-13278417420371.jpg?v=1577167543",
                  variant_ids: [29484700336211],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13731354837075",
                },
              ],
              image: {
                id: 13731297296467,
                product_id: 6117057990,
                position: 1,
                created_at: "2019-12-24T11:13:23+05:30",
                updated_at: "2019-12-24T11:35:43+05:30",
                alt:
                  "SUGAR Cosmetics Stroke Of Genius Heavy-Duty Kohl 01 Back to Black",
                width: 1025,
                height: 1400,
                src:
                  "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-stroke-of-genius-heavy-duty-kohl-01-back-to-black-13278410866771.jpg?v=1577167543",
                variant_ids: [19364046470],
                admin_graphql_api_id:
                  "gid://shopify/ProductImage/13731297296467",
              },
            },
            mediaText: null,
            redirectUrl:
              "https://in.sugarcosmetics.com/products/stroke-of-genius-heavy-duty-kohl",
          },
          {
            id: 160,
            sectionId: 6,
            webViewTitle: null,
            contentType: 3,
            mediaUrl: null,
            product_json: {
              id: 164594221081,
              title: "Suede Secret Matte Lipcolour",
              body_html:
                'Plunge head first into high-pigmented matte fantasy with <strong>SUGAR Suede Secret Matte Lipcolour!</strong> With a formula so lightweight that you\'ll forget you\'re wearing it and a finish so plush that it never feels dry even when it dries – your search for the ‘holy grail’ liquid lipstick is pretty much done.<br><br>\n<p>Work this #MadeInItaly beauty to take matte to the max with a punch of opaque colour in every swipe. Dries quickly to a smooth matte finish without leaving your lips parched – this is your secret weapon to hack a luxurious pout! The <strong>SUGAR Suede Secret Matte Lipcolour</strong> boasts of a 12-hour stay and comes in a spectrum of highly-pigmented shades that complement a slew of complexions - make it your goal to own every one of them!</p>\n<ul>\n<li>The lovechild of your fave hues, <a href="https://in.sugarcosmetics.com/products/suede-secret-matte-lipcolour-01-muslin-mauve-mauve-nude" target="_blank" rel="noopener noreferrer"><strong>Muslin Mauve</strong></a> is a dramatic MLBB shade that punches up the natural pink of your lips with its mauve nude tint.<br><br>\n</li>\n<li>Become the life and soul of any party with <a href="https://in.sugarcosmetics.com/products/suede-secret-matte-lipcolour-02-plush-pink-deep-rose-pink" target="_blank" rel="noopener noreferrer"><strong>Plush Pink</strong></a>, a deep rose pink shade that will bring your pout to the forefront.</li>\n</ul>\n<ul>\n<li>Channel your inner diva with <a href="https://in.sugarcosmetics.com/products/suede-secret-matte-lipcolour-03-velvet-violet-dark-raspberry" target="_blank" rel="noopener noreferrer"><strong>Velvet Violet</strong></a>. This pop of dark raspberry announces your arrival even before you say “Hello!”<br><br>\n</li>\n<li>Reach for <a href="https://in.sugarcosmetics.com/products/suede-secret-matte-lipcolour-04-crepe-coral-peachy-coral" target="_blank" rel="noopener noreferrer"><strong>Crêpe Coral</strong></a> when your mood calls for some playful twinning of peach and coral. A single swipe of this shade will wake up any look.<br><br>\n</li>\n<li>Swipe on <a href="https://in.sugarcosmetics.com/products/suede-secret-matte-lip-colour-05-poplin-plum-blackened-plum" target="_blank" rel="noopener noreferrer"><strong>Poplin Plum</strong></a> for all the good-girl-gone-bad vibes. Let the world see your confidence with each wear of this blackened plum shade.<br><br>\n</li>\n<li>Get ready to rething brown with <strong><a href="https://in.sugarcosmetics.com/products/suede-secret-matte-lipcolour-06-taffeta-terracotta-terracotta-brown">Taffeta Terracotta</a></strong> an edgy shade that blends red with mocha to give you the perfect earthy brown.<br><br>\n</li>\n<li>Not too pink and not quite grey, the dreamer in you will find the taupe rose of <a href="https://in.sugarcosmetics.com/products/suede-secret-matte-lipcolour-07-tweed-taupe-taupe-rose" target="_blank" rel="noopener noreferrer"><strong>Tweed Taupe</strong></a> just enough to always help you shoot for the stars.<br><br>\n</li>\n<li>Elevate your everyday look with <a href="https://in.sugarcosmetics.com/products/suede-secret-matte-lipcolour-08-brocade-burgundy-burgundy" target="_blank" rel="noopener noreferrer"><strong>Brocade Burgundy</strong></a>. Not for the faint-hearted, this burgundy shade will inject some major drama to your look.<br><br>\n</li>\n<li>Raise the bar and add a dash of sophistication with <a href="https://in.sugarcosmetics.com/products/suede-secret-matte-lipcolour-09-felt-fawn-beige-nude" target="_blank" rel="noopener noreferrer"><strong>Felt Fawn</strong></a> - a grey-toned beige shade for a gorgeous nude lip so you can play up other parts of your look.<br><br>\n</li>\n<li>Up the glam factor in your everyday look with the unmistakable <a href="https://in.sugarcosmetics.com/products/suede-secret-matte-lipcolour-10-satin-scarlet-red" target="_blank" rel="noopener noreferrer"><strong>Satin Scarlet</strong></a> – a classic ‘oomph’ red shade that will be the ‘cherry on top’ for every OOTD of yours.<br><br>\n</li>\n<li>Celebrate the marriage of two iconic hues of pink and red with <a href="https://in.sugarcosmetics.com/products/suede-secret-matte-lipcolour-11-rayon-rose-brick-rose-reddish-pink"><strong>Rayon Rose</strong></a> - a brick rose fantasy that is every girl\'s dream!</li>\n</ul>\n<ul>\n<li>An ode to the gentle shades of a petal, <a href="https://in.sugarcosmetics.com/products/suede-secret-matte-lipcolour-12-linen-lilac-mauve"><strong>Linen Lilac</strong></a> with its subtle mauve hues is the quickest way to cast a spell on everyone in sight!<br><br>\n</li>\n<li>Max out your love for nudes with the inimitable <a href="https://in.sugarcosmetics.com/products/suede-secret-matte-lipcolour-13-nylon-nude-nude-pink"><strong>Nylon Nude</strong></a> - a delicate shade of pink that you\'ll have to fight hard to keep a secret.<br><br>\n</li>\n<li>Pep it up with <a href="https://in.sugarcosmetics.com/products/suede-secret-matte-lipcolour-14-tartan-tangerine-orange-with-hints-of-red"><strong>Tartan Tangerine!</strong></a> Packed with positive vibes, this orange hue with hints of red is the perfect pick for a day under the sun!<br><br>\n</li>\n<li>The perfect fix for all the times "you just want attention, you don\'t want (his) heart" - yes, this tone of <a href="https://in.sugarcosmetics.com/products/suede-secret-matte-lipcolour-15-fleece-fuchsia-deep-fuchsia"><strong>Fleece Fuchsia</strong></a> is that legendary!<br><br>\n</li>\n<li>Get intoxicated with the irresistible <a href="https://in.sugarcosmetics.com/products/suede-secret-matte-lipcolour-16-silk-sangria-magenta-purple"><strong>Silk Sangria</strong></a> - a bold blend of purple and magenta that will drive all conversations back to your lips.<br><br>\n</li>\n<li>Drench your lips in a heady punch of plum and mauve with <a href="https://in.sugarcosmetics.com/products/suede-secret-matte-lipcolour-17-georgette-grape-plum-mauve"><strong>Georgette Grape!</strong></a> Be warned though, this one sparks compliments and envy in equal measure.<br><br>\n</li>\n<li>Strut about in your boldest best with <a href="https://in.sugarcosmetics.com/products/suede-secret-matte-lipcolour-18-batiste-berry-berry"><strong>Batiste Berry</strong></a> - a blast of deep plum that will paint your parade with all the glory of this gallant shade.<br><br>\n</li>\n<li>Treat your lips to the merry <a href="https://in.sugarcosmetics.com/products/suede-secret-matte-lipcolour-19-terry-tomato-bright-red-with-hints-of-orange"><strong>Terry Tomato!</strong></a> Pretty &amp; bright, this red hue with hints of orange will give a new twist to your usual \'red lips\' look.<br><br>\n</li>\n<li>Surrender to your love for cocoa with <a href="https://in.sugarcosmetics.com/products/suede-secret-matte-lipcolour-20-chino-chocolate-deep-brown"><strong>Chino Chocolate</strong></a> - a deep matte brown shade that will play equally hard whether at work or at your next party!</li>\n</ul>\n<ul></ul>\n<p><strong>How to Apply:</strong> Gently remove the excess from the applicator first. Use its curved tip to apply the liquid lipstick like a liner. Then swipe the lipstick on to fill in your lips.</p>\n<p><strong>Benefits:</strong> <strong>SUGAR Suede Secret Matte Lipcolour</strong> applies wet then dries down to an ultra-matte yet ultra-comfortable finish that doesn\'t budge.</p>\n<h5>Additional Details</h5>\n<strong>SUGAR Suede Secret Matte Lipcolour</strong> is available in 20 sensational shades to choose from, viz. <strong><a href="https://in.sugarcosmetics.com/products/suede-secret-matte-lipcolour-01-muslin-mauve-mauve-nude" target="_blank" rel="noopener noreferrer">01 Muslin Mauve</a></strong>, <strong><a href="https://in.sugarcosmetics.com/products/suede-secret-matte-lipcolour-02-plush-pink-deep-rose-pink" target="_blank" rel="noopener noreferrer">02 Plush Pink</a></strong>, <strong><a href="https://in.sugarcosmetics.com/products/suede-secret-matte-lipcolour-03-velvet-violet-dark-raspberry" target="_blank" rel="noopener noreferrer">03 Velvet Violet</a></strong>, <strong><a href="https://in.sugarcosmetics.com/products/suede-secret-matte-lipcolour-04-crepe-coral-peachy-coral" target="_blank" rel="noopener noreferrer">04 Crêpe Coral</a></strong>, <strong><a href="https://in.sugarcosmetics.com/products/suede-secret-matte-lip-colour-05-poplin-plum-blackened-plum" target="_blank" rel="noopener noreferrer">05 Poplin Plum</a></strong>, <strong><a href="https://in.sugarcosmetics.com/products/suede-secret-matte-lipcolour-06-taffeta-terracotta-terracotta-brown" target="_blank" rel="noopener noreferrer">06 Taffeta Terracotta</a></strong>, <strong><a href="https://in.sugarcosmetics.com/products/suede-secret-matte-lipcolour-07-tweed-taupe-taupe-rose" target="_blank" rel="noopener noreferrer">07 Tweed Taupe</a></strong>, <strong><a href="https://in.sugarcosmetics.com/products/suede-secret-matte-lipcolour-08-brocade-burgundy-burgundy" target="_blank" rel="noopener noreferrer">08 Brocade Burgundy</a></strong>, <a href="https://in.sugarcosmetics.com/products/suede-secret-matte-lipcolour-09-felt-fawn-beige-nude" target="_blank" rel="noopener noreferrer" style="font-weight: bold;">09 Felt Fawn</a>, <strong><a href="https://in.sugarcosmetics.com/products/suede-secret-matte-lipcolour-10-satin-scarlet-red" target="_blank" rel="noopener noreferrer">10 Satin Scarlet</a></strong>,<strong> <a href="https://in.sugarcosmetics.com/products/suede-secret-matte-lipcolour-11-rayon-rose-brick-rose-reddish-pink" target="_blank" rel="noopener noreferrer">11 Rayon Rose</a></strong>,<strong> <a href="https://in.sugarcosmetics.com/products/suede-secret-matte-lipcolour-12-linen-lilac-mauve" target="_blank" rel="noopener noreferrer">12 Linen Lilac</a></strong>,<strong> <a href="https://in.sugarcosmetics.com/products/suede-secret-matte-lipcolour-13-nylon-nude-nude-pink" target="_blank" rel="noopener noreferrer">13 Nylon Nude</a></strong>,<strong> <a href="https://in.sugarcosmetics.com/products/suede-secret-matte-lipcolour-14-tartan-tangerine-orange-with-hints-of-red" target="_blank" rel="noopener noreferrer">14 Tartan Tangerine!</a></strong>,<strong> <a href="https://in.sugarcosmetics.com/products/suede-secret-matte-lipcolour-15-fleece-fuchsia-deep-fuchsia" target="_blank" rel="noopener noreferrer">15 Fleece Fuchsia</a></strong>,<b> </b><a href="https://in.sugarcosmetics.com/products/suede-secret-matte-lipcolour-16-silk-sangria-magenta-purple" style="font-weight: bold;" target="_blank" rel="noopener noreferrer">16 Silk Sangria</a>,<strong> <a href="https://in.sugarcosmetics.com/products/suede-secret-matte-lipcolour-17-georgette-grape-plum-mauve" target="_blank" rel="noopener noreferrer">17 Georgette Grape</a>, <a href="https://in.sugarcosmetics.com/products/suede-secret-matte-lipcolour-18-batiste-berry-berry" target="_blank" rel="noopener noreferrer">18 Batiste Berry</a>, <a href="https://in.sugarcosmetics.com/products/suede-secret-matte-lipcolour-19-terry-tomato-bright-red-with-hints-of-orange" target="_blank" rel="noopener noreferrer">19 Terry Tomato</a> </strong>and <a href="https://in.sugarcosmetics.com/products/suede-secret-matte-lipcolour-20-chino-chocolate-deep-brown" target="_blank" rel="noopener noreferrer"><strong>20 Chino Chocolate</strong></a>. This product is dermatologically tested &amp; approved and 100% safe for your skin.<br>Made in Italy.<br><strong>Net Volume:</strong> 6 ml.<br>\n<h5>List of Ingredients</h5>\n<br><strong>List of Ingredients: </strong>Cyclopentasiloxane, Petrolatum, Isododecane, Polymethylsilsesquioxane, Silica Dimethyl Silylate, Mica, VP/Eicosene Copolymer, Paraffin, Simmondsia Chinensis Seed Oil [Simmondsia Chinensis (Jojoba) Seed Oil], Butyrospermum Parkii Butter [Butyrospermum Parkii (Shea) Butter ], Ethylhexyl Palmitate, Tribehenin, Sorbitan Isostearate, Palmitoyl Tripeptide, Phenoxyethanol, Tropolone, CI 15850 (Red 7 Lake), CI 15850 (Red 6).\n<p><span><strong>MRP</strong>: 799 (incl. all taxes)</span></p>\n<p><span><strong>Country of Origin</strong>: Italy</span></p>\n<p><strong>Company Name</strong>: SUGAR Cosmetics LLC.</p>\n<p><span><strong>Company Address</strong>: 8 The Green Suite A, Dover, DE 19901, USA.</span></p>\n<p><strong><em>IMP: While products listed under our Clearance section are perfectly safe for usage, they may be within 12 months of the expiry of the recommended shelf life. You are requested to factor in the same while planning your purchase. Thank you for shopping with us!</em></strong></p>',
              vendor: "SUGAR Cosmetics",
              product_type: "Liquid Lipstick",
              created_at: "2018-01-11T09:31:06+05:30",
              handle: "suede-secret-matte-lipcolour",
              updated_at: "2020-12-31T15:00:06+05:30",
              published_at: "2018-01-11T09:23:42+05:30",
              template_suffix: "",
              published_scope: "web",
              tags:
                "amp-hide, Lipcolour, Lipstick, Offer, st_finish: Matte, st_formulation: Liquid, st_type: Lipstick, sugar_type_1",
              admin_graphql_api_id: "gid://shopify/Product/164594221081",
              variants: [
                {
                  id: 1323054891033,
                  product_id: 164594221081,
                  title: "01 Muslin Mauve (Mauve Nude)",
                  price: "400.00",
                  sku: "8906090492508",
                  position: 1,
                  inventory_policy: "deny",
                  compare_at_price: "799.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "01 Muslin Mauve (Mauve Nude)",
                  option2: null,
                  option3: null,
                  created_at: "2018-01-11T09:31:07+05:30",
                  updated_at: "2020-11-17T14:42:30+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13738927095891,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 1296097345561,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/1323054891033",
                },
                {
                  id: 1323054923801,
                  product_id: 164594221081,
                  title: "02 Plush Pink (Deep Rose Pink)",
                  price: "400.00",
                  sku: "8906090492515",
                  position: 2,
                  inventory_policy: "deny",
                  compare_at_price: "799.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "02 Plush Pink (Deep Rose Pink)",
                  option2: null,
                  option3: null,
                  created_at: "2018-01-11T09:31:07+05:30",
                  updated_at: "2020-12-23T22:35:10+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13738926440531,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 1296097378329,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/1323054923801",
                },
                {
                  id: 1323054956569,
                  product_id: 164594221081,
                  title: "03 Velvet Violet (Dark Raspberry)",
                  price: "400.00",
                  sku: "8906090492522",
                  position: 3,
                  inventory_policy: "deny",
                  compare_at_price: "799.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "03 Velvet Violet (Dark Raspberry)",
                  option2: null,
                  option3: null,
                  created_at: "2018-01-11T09:31:07+05:30",
                  updated_at: "2020-12-25T17:41:15+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13738922311763,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 1296097411097,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/1323054956569",
                },
                {
                  id: 1323054989337,
                  product_id: 164594221081,
                  title: "04 Crêpe Coral (Peachy Coral)",
                  price: "400.00",
                  sku: "8906090492539",
                  position: 4,
                  inventory_policy: "deny",
                  compare_at_price: "799.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "04 Crêpe Coral (Peachy Coral)",
                  option2: null,
                  option3: null,
                  created_at: "2018-01-11T09:31:07+05:30",
                  updated_at: "2020-12-21T13:51:31+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13738921885779,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 1296097443865,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/1323054989337",
                },
                {
                  id: 1323055022105,
                  product_id: 164594221081,
                  title: "05 Poplin Plum (Blackened Plum)",
                  price: "400.00",
                  sku: "8906090492546",
                  position: 5,
                  inventory_policy: "deny",
                  compare_at_price: "799.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "05 Poplin Plum (Blackened Plum)",
                  option2: null,
                  option3: null,
                  created_at: "2018-01-11T09:31:07+05:30",
                  updated_at: "2020-12-31T14:30:10+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13738921197651,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 1296097476633,
                  inventory_quantity: 205,
                  old_inventory_quantity: 205,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/1323055022105",
                },
                {
                  id: 1323055054873,
                  product_id: 164594221081,
                  title: "06 Taffeta Terracotta (Terracotta Brown)",
                  price: "400.00",
                  sku: "8906090492553",
                  position: 6,
                  inventory_policy: "deny",
                  compare_at_price: "799.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "06 Taffeta Terracotta (Terracotta Brown)",
                  option2: null,
                  option3: null,
                  created_at: "2018-01-11T09:31:07+05:30",
                  updated_at: "2020-12-31T14:30:10+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13738915790931,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 1296097509401,
                  inventory_quantity: 21,
                  old_inventory_quantity: 21,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/1323055054873",
                },
                {
                  id: 1323055087641,
                  product_id: 164594221081,
                  title: "07 Tweed Taupe (Taupe Rose)",
                  price: "400.00",
                  sku: "8906090492560",
                  position: 7,
                  inventory_policy: "deny",
                  compare_at_price: "799.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "07 Tweed Taupe (Taupe Rose)",
                  option2: null,
                  option3: null,
                  created_at: "2018-01-11T09:31:07+05:30",
                  updated_at: "2020-12-07T13:48:20+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13738915201107,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 1296097542169,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/1323055087641",
                },
                {
                  id: 1323055120409,
                  product_id: 164594221081,
                  title: "08 Brocade Burgundy (Burgundy)",
                  price: "400.00",
                  sku: "8906090492577",
                  position: 8,
                  inventory_policy: "deny",
                  compare_at_price: "799.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "08 Brocade Burgundy (Burgundy)",
                  option2: null,
                  option3: null,
                  created_at: "2018-01-11T09:31:07+05:30",
                  updated_at: "2020-12-11T15:56:55+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13738914840659,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 1296097574937,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/1323055120409",
                },
                {
                  id: 1323055153177,
                  product_id: 164594221081,
                  title: "09 Felt Fawn (Beige Nude)",
                  price: "400.00",
                  sku: "8906090492584",
                  position: 9,
                  inventory_policy: "deny",
                  compare_at_price: "799.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "09 Felt Fawn (Beige Nude)",
                  option2: null,
                  option3: null,
                  created_at: "2018-01-11T09:31:07+05:30",
                  updated_at: "2020-11-06T12:48:11+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13738911465555,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 1296097607705,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/1323055153177",
                },
                {
                  id: 1323055185945,
                  product_id: 164594221081,
                  title: "10 Satin Scarlet (Red)",
                  price: "400.00",
                  sku: "8906090492591",
                  position: 10,
                  inventory_policy: "deny",
                  compare_at_price: "799.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "10 Satin Scarlet (Red)",
                  option2: null,
                  option3: null,
                  created_at: "2018-01-11T09:31:07+05:30",
                  updated_at: "2020-11-17T18:45:47+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13738910842963,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 1296097640473,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/1323055185945",
                },
                {
                  id: 12860856860755,
                  product_id: 164594221081,
                  title: "11 Rayon Rose (Brick Rose / Reddish Pink)",
                  price: "400.00",
                  sku: "8906090495448",
                  position: 11,
                  inventory_policy: "deny",
                  compare_at_price: "799.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "11 Rayon Rose (Brick Rose / Reddish Pink)",
                  option2: null,
                  option3: null,
                  created_at: "2018-08-30T13:44:55+05:30",
                  updated_at: "2020-12-15T17:01:45+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13738910351443,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 12917367046227,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/12860856860755",
                },
                {
                  id: 12860865740883,
                  product_id: 164594221081,
                  title: "12 Linen Lilac (Mauve)",
                  price: "400.00",
                  sku: "8906090495455",
                  position: 12,
                  inventory_policy: "deny",
                  compare_at_price: "799.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "12 Linen Lilac (Mauve)",
                  option2: null,
                  option3: null,
                  created_at: "2018-08-30T13:45:55+05:30",
                  updated_at: "2020-12-31T14:30:10+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13738907140179,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 12917375926355,
                  inventory_quantity: 68,
                  old_inventory_quantity: 68,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/12860865740883",
                },
                {
                  id: 12860875505747,
                  product_id: 164594221081,
                  title: "13 Nylon Nude (Nude Pink)",
                  price: "400.00",
                  sku: "8906090495462",
                  position: 13,
                  inventory_policy: "deny",
                  compare_at_price: "799.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "13 Nylon Nude (Nude Pink)",
                  option2: null,
                  option3: null,
                  created_at: "2018-08-30T13:47:00+05:30",
                  updated_at: "2020-12-15T11:08:20+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13738905567315,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 12917385691219,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/12860875505747",
                },
                {
                  id: 12860889137235,
                  product_id: 164594221081,
                  title: "14 Tartan Tangerine (Orange With Hints Of Red)",
                  price: "400.00",
                  sku: "8906090495479",
                  position: 14,
                  inventory_policy: "deny",
                  compare_at_price: "799.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "14 Tartan Tangerine (Orange With Hints Of Red)",
                  option2: null,
                  option3: null,
                  created_at: "2018-08-30T13:48:28+05:30",
                  updated_at: "2020-12-31T14:30:11+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13738903961683,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 12917399322707,
                  inventory_quantity: 720,
                  old_inventory_quantity: 720,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/12860889137235",
                },
                {
                  id: 12860972400723,
                  product_id: 164594221081,
                  title: "15 Fleece Fuchsia (Deep Fuchsia)",
                  price: "400.00",
                  sku: "8906090495486",
                  position: 15,
                  inventory_policy: "deny",
                  compare_at_price: "799.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "15 Fleece Fuchsia (Deep Fuchsia)",
                  option2: null,
                  option3: null,
                  created_at: "2018-08-30T13:53:50+05:30",
                  updated_at: "2020-12-01T12:40:15+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13738900717651,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 12917482684499,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/12860972400723",
                },
                {
                  id: 12861017325651,
                  product_id: 164594221081,
                  title: "16 Silk Sangria (Magenta Purple)",
                  price: "400.00",
                  sku: "8906090495493",
                  position: 16,
                  inventory_policy: "deny",
                  compare_at_price: "799.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "16 Silk Sangria (Magenta Purple)",
                  option2: null,
                  option3: null,
                  created_at: "2018-08-30T13:56:14+05:30",
                  updated_at: "2020-12-23T17:37:22+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13738900095059,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 12917526888531,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/12861017325651",
                },
                {
                  id: 12861045833811,
                  product_id: 164594221081,
                  title: "17 Georgette Grape (Plum Mauve)",
                  price: "400.00",
                  sku: "8906090495509",
                  position: 17,
                  inventory_policy: "deny",
                  compare_at_price: "799.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "17 Georgette Grape (Plum Mauve)",
                  option2: null,
                  option3: null,
                  created_at: "2018-08-30T13:57:44+05:30",
                  updated_at: "2020-12-31T13:48:25+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13738899406931,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 12917556019283,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/12861045833811",
                },
                {
                  id: 12861068804179,
                  product_id: 164594221081,
                  title: "18 Batiste Berry (Berry)",
                  price: "400.00",
                  sku: "8906090496803",
                  position: 18,
                  inventory_policy: "deny",
                  compare_at_price: "799.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "18 Batiste Berry (Berry)",
                  option2: null,
                  option3: null,
                  created_at: "2018-08-30T13:59:13+05:30",
                  updated_at: "2020-12-23T20:52:20+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13738895671379,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 12917578989651,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/12861068804179",
                },
                {
                  id: 12861104947283,
                  product_id: 164594221081,
                  title: "19 Terry Tomato (Bright Red with hints of orange)",
                  price: "400.00",
                  sku: "8906090496810",
                  position: 19,
                  inventory_policy: "deny",
                  compare_at_price: "799.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "19 Terry Tomato (Bright Red with hints of orange)",
                  option2: null,
                  option3: null,
                  created_at: "2018-08-30T14:01:56+05:30",
                  updated_at: "2020-12-21T14:48:50+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13738895081555,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 12917615329363,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/12861104947283",
                },
                {
                  id: 12861140729939,
                  product_id: 164594221081,
                  title: "20 Chino Chocolate (Deep Brown)",
                  price: "400.00",
                  sku: "8906090496827",
                  position: 20,
                  inventory_policy: "deny",
                  compare_at_price: "799.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "20 Chino Chocolate (Deep Brown)",
                  option2: null,
                  option3: null,
                  created_at: "2018-08-30T14:04:54+05:30",
                  updated_at: "2020-12-04T17:17:42+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13738894622803,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 12917653864531,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/12861140729939",
                },
              ],
              options: [
                {
                  id: 208996040729,
                  product_id: 164594221081,
                  name: "Colour",
                  position: 1,
                  values: [
                    "01 Muslin Mauve (Mauve Nude)",
                    "02 Plush Pink (Deep Rose Pink)",
                    "03 Velvet Violet (Dark Raspberry)",
                    "04 Crêpe Coral (Peachy Coral)",
                    "05 Poplin Plum (Blackened Plum)",
                    "06 Taffeta Terracotta (Terracotta Brown)",
                    "07 Tweed Taupe (Taupe Rose)",
                    "08 Brocade Burgundy (Burgundy)",
                    "09 Felt Fawn (Beige Nude)",
                    "10 Satin Scarlet (Red)",
                    "11 Rayon Rose (Brick Rose / Reddish Pink)",
                    "12 Linen Lilac (Mauve)",
                    "13 Nylon Nude (Nude Pink)",
                    "14 Tartan Tangerine (Orange With Hints Of Red)",
                    "15 Fleece Fuchsia (Deep Fuchsia)",
                    "16 Silk Sangria (Magenta Purple)",
                    "17 Georgette Grape (Plum Mauve)",
                    "18 Batiste Berry (Berry)",
                    "19 Terry Tomato (Bright Red with hints of orange)",
                    "20 Chino Chocolate (Deep Brown)",
                  ],
                },
              ],
              images: [
                {
                  id: 13732321919059,
                  product_id: 164594221081,
                  position: 1,
                  created_at: "2019-12-24T16:18:21+05:30",
                  updated_at: "2019-12-26T05:54:36+05:30",
                  alt: "SUGAR Cosmetics Suede Secret Matte Lipcolour",
                  width: 439,
                  height: 600,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-suede-secret-matte-lipcolour-7757166739539.jpg?v=1577319876",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13732321919059",
                },
                {
                  id: 13738927095891,
                  product_id: 164594221081,
                  position: 2,
                  created_at: "2019-12-26T05:54:35+05:30",
                  updated_at: "2019-12-26T05:54:36+05:30",
                  alt:
                    "SUGAR Cosmetics Suede Secret Matte Lipcolour 01 Muslin Mauve (Mauve Nude)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-suede-secret-matte-lipcolour-01-muslin-mauve-mauve-nude-10728632614995.png?v=1577319876",
                  variant_ids: [1323054891033],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13738927095891",
                },
                {
                  id: 13738926440531,
                  product_id: 164594221081,
                  position: 3,
                  created_at: "2019-12-26T05:54:19+05:30",
                  updated_at: "2019-12-26T05:54:36+05:30",
                  alt:
                    "SUGAR Cosmetics Suede Secret Matte Lipcolour 02 Plush Pink (Deep Rose Pink)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-suede-secret-matte-lipcolour-02-plush-pink-deep-rose-pink-10728632647763.png?v=1577319876",
                  variant_ids: [1323054923801],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13738926440531",
                },
                {
                  id: 13738922311763,
                  product_id: 164594221081,
                  position: 4,
                  created_at: "2019-12-26T05:52:54+05:30",
                  updated_at: "2019-12-26T05:54:36+05:30",
                  alt:
                    "SUGAR Cosmetics Suede Secret Matte Lipcolour 03 Velvet Violet (Dark Raspberry)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-suede-secret-matte-lipcolour-03-velvet-violet-dark-raspberry-10728632680531.png?v=1577319876",
                  variant_ids: [1323054956569],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13738922311763",
                },
                {
                  id: 13738921885779,
                  product_id: 164594221081,
                  position: 5,
                  created_at: "2019-12-26T05:52:38+05:30",
                  updated_at: "2019-12-26T05:54:36+05:30",
                  alt:
                    "SUGAR Cosmetics Suede Secret Matte Lipcolour 04 Crêpe Coral (Peachy Coral)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-suede-secret-matte-lipcolour-04-crepe-coral-peachy-coral-10728632713299.png?v=1577319876",
                  variant_ids: [1323054989337],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13738921885779",
                },
                {
                  id: 13738921197651,
                  product_id: 164594221081,
                  position: 6,
                  created_at: "2019-12-26T05:52:20+05:30",
                  updated_at: "2019-12-26T05:54:36+05:30",
                  alt:
                    "SUGAR Cosmetics Suede Secret Matte Lipcolour 05 Poplin Plum (Blackened Plum)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-suede-secret-matte-lipcolour-05-poplin-plum-blackened-plum-10728632746067.png?v=1577319876",
                  variant_ids: [1323055022105],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13738921197651",
                },
                {
                  id: 13738915790931,
                  product_id: 164594221081,
                  position: 7,
                  created_at: "2019-12-26T05:50:56+05:30",
                  updated_at: "2019-12-26T05:54:36+05:30",
                  alt:
                    "SUGAR Cosmetics Suede Secret Matte Lipcolour 06 Taffeta Terracotta (Terracotta Brown)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-suede-secret-matte-lipcolour-06-taffeta-terracotta-terracotta-brown-10728633040979.png?v=1577319876",
                  variant_ids: [1323055054873],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13738915790931",
                },
                {
                  id: 13738915201107,
                  product_id: 164594221081,
                  position: 8,
                  created_at: "2019-12-26T05:50:39+05:30",
                  updated_at: "2019-12-26T05:54:36+05:30",
                  alt:
                    "SUGAR Cosmetics Suede Secret Matte Lipcolour 07 Tweed Taupe (Taupe Rose)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-suede-secret-matte-lipcolour-07-tweed-taupe-taupe-rose-10728633204819.png?v=1577319876",
                  variant_ids: [1323055087641],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13738915201107",
                },
                {
                  id: 13738914840659,
                  product_id: 164594221081,
                  position: 9,
                  created_at: "2019-12-26T05:50:23+05:30",
                  updated_at: "2019-12-26T05:54:36+05:30",
                  alt:
                    "SUGAR Cosmetics Suede Secret Matte Lipcolour 08 Brocade Burgundy (Burgundy)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-suede-secret-matte-lipcolour-08-brocade-burgundy-burgundy-10728633237587.png?v=1577319876",
                  variant_ids: [1323055120409],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13738914840659",
                },
                {
                  id: 13738911465555,
                  product_id: 164594221081,
                  position: 10,
                  created_at: "2019-12-26T05:48:54+05:30",
                  updated_at: "2019-12-26T05:54:36+05:30",
                  alt:
                    "SUGAR Cosmetics Suede Secret Matte Lipcolour 09 Felt Fawn (Beige Nude)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-suede-secret-matte-lipcolour-09-felt-fawn-beige-nude-10728633434195.png?v=1577319876",
                  variant_ids: [1323055153177],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13738911465555",
                },
                {
                  id: 13738910842963,
                  product_id: 164594221081,
                  position: 11,
                  created_at: "2019-12-26T05:48:37+05:30",
                  updated_at: "2019-12-26T05:54:36+05:30",
                  alt:
                    "SUGAR Cosmetics Suede Secret Matte Lipcolour 10 Satin Scarlet (Red)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-suede-secret-matte-lipcolour-10-satin-scarlet-red-10728633466963.png?v=1577319876",
                  variant_ids: [1323055185945],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13738910842963",
                },
                {
                  id: 13738910351443,
                  product_id: 164594221081,
                  position: 12,
                  created_at: "2019-12-26T05:48:21+05:30",
                  updated_at: "2019-12-26T05:54:36+05:30",
                  alt:
                    "SUGAR Cosmetics Suede Secret Matte Lipcolour 11 Rayon Rose (Brick Rose / Reddish Pink)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-suede-secret-matte-lipcolour-11-rayon-rose-brick-rose-reddish-pink-10728633499731.png?v=1577319876",
                  variant_ids: [12860856860755],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13738910351443",
                },
                {
                  id: 13738907140179,
                  product_id: 164594221081,
                  position: 13,
                  created_at: "2019-12-26T05:46:54+05:30",
                  updated_at: "2019-12-26T05:54:36+05:30",
                  alt:
                    "SUGAR Cosmetics Suede Secret Matte Lipcolour 12 Linen Lilac (Mauve)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-suede-secret-matte-lipcolour-12-linen-lilac-mauve-10728633630803.png?v=1577319876",
                  variant_ids: [12860865740883],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13738907140179",
                },
                {
                  id: 13738905567315,
                  product_id: 164594221081,
                  position: 14,
                  created_at: "2019-12-26T05:46:39+05:30",
                  updated_at: "2019-12-26T05:54:36+05:30",
                  alt:
                    "SUGAR Cosmetics Suede Secret Matte Lipcolour 13 Nylon Nude (Nude Pink)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-suede-secret-matte-lipcolour-13-nylon-nude-nude-pink-10728633663571.png?v=1577319876",
                  variant_ids: [12860875505747],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13738905567315",
                },
                {
                  id: 13738903961683,
                  product_id: 164594221081,
                  position: 15,
                  created_at: "2019-12-26T05:46:22+05:30",
                  updated_at: "2019-12-26T05:54:36+05:30",
                  alt:
                    "SUGAR Cosmetics Suede Secret Matte Lipcolour 14 Tartan Tangerine (Orange With Hints Of Red)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-suede-secret-matte-lipcolour-14-tartan-tangerine-orange-with-hints-of-red-10728633696339.png?v=1577319876",
                  variant_ids: [12860889137235],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13738903961683",
                },
                {
                  id: 13738900717651,
                  product_id: 164594221081,
                  position: 16,
                  created_at: "2019-12-26T05:44:54+05:30",
                  updated_at: "2019-12-26T05:54:36+05:30",
                  alt:
                    "SUGAR Cosmetics Suede Secret Matte Lipcolour 15 Fleece Fuchsia (Deep Fuchsia)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-suede-secret-matte-lipcolour-15-fleece-fuchsia-deep-fuchsia-10728633827411.png?v=1577319876",
                  variant_ids: [12860972400723],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13738900717651",
                },
                {
                  id: 13738900095059,
                  product_id: 164594221081,
                  position: 17,
                  created_at: "2019-12-26T05:44:35+05:30",
                  updated_at: "2019-12-26T05:54:36+05:30",
                  alt:
                    "SUGAR Cosmetics Suede Secret Matte Lipcolour 16 Silk Sangria (Magenta Purple)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-suede-secret-matte-lipcolour-16-silk-sangria-magenta-purple-10728634024019.png?v=1577319876",
                  variant_ids: [12861017325651],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13738900095059",
                },
                {
                  id: 13738899406931,
                  product_id: 164594221081,
                  position: 18,
                  created_at: "2019-12-26T05:44:20+05:30",
                  updated_at: "2019-12-26T05:54:36+05:30",
                  alt:
                    "SUGAR Cosmetics Suede Secret Matte Lipcolour 17 Georgette Grape (Plum Mauve)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-suede-secret-matte-lipcolour-17-georgette-grape-plum-mauve-10728634646611.png?v=1577319876",
                  variant_ids: [12861045833811],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13738899406931",
                },
                {
                  id: 13738895671379,
                  product_id: 164594221081,
                  position: 19,
                  created_at: "2019-12-26T05:42:52+05:30",
                  updated_at: "2019-12-26T05:54:36+05:30",
                  alt:
                    "SUGAR Cosmetics Suede Secret Matte Lipcolour 18 Batiste Berry (Berry)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-suede-secret-matte-lipcolour-18-batiste-berry-berry-10728634679379.png?v=1577319876",
                  variant_ids: [12861068804179],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13738895671379",
                },
                {
                  id: 13738895081555,
                  product_id: 164594221081,
                  position: 20,
                  created_at: "2019-12-26T05:42:35+05:30",
                  updated_at: "2019-12-26T05:54:36+05:30",
                  alt:
                    "SUGAR Cosmetics Suede Secret Matte Lipcolour 19 Terry Tomato (Bright Red with hints of orange)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-suede-secret-matte-lipcolour-19-terry-tomato-bright-red-with-hints-of-orange-10728634810451.png?v=1577319876",
                  variant_ids: [12861104947283],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13738895081555",
                },
                {
                  id: 13738894622803,
                  product_id: 164594221081,
                  position: 21,
                  created_at: "2019-12-26T05:42:20+05:30",
                  updated_at: "2019-12-26T05:54:36+05:30",
                  alt:
                    "SUGAR Cosmetics Suede Secret Matte Lipcolour 20 Chino Chocolate (Deep Brown)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-suede-secret-matte-lipcolour-20-chino-chocolate-deep-brown-10728634875987.png?v=1577319876",
                  variant_ids: [12861140729939],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13738894622803",
                },
              ],
              image: {
                id: 13732321919059,
                product_id: 164594221081,
                position: 1,
                created_at: "2019-12-24T16:18:21+05:30",
                updated_at: "2019-12-26T05:54:36+05:30",
                alt: "SUGAR Cosmetics Suede Secret Matte Lipcolour",
                width: 439,
                height: 600,
                src:
                  "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-suede-secret-matte-lipcolour-7757166739539.jpg?v=1577319876",
                variant_ids: [],
                admin_graphql_api_id:
                  "gid://shopify/ProductImage/13732321919059",
              },
            },
            mediaText: null,
            redirectUrl:
              "https://in.sugarcosmetics.com/products/suede-secret-matte-lipcolour",
          },
          {
            id: 162,
            sectionId: 6,
            webViewTitle: null,
            contentType: 3,
            mediaUrl: null,
            product_json: {
              id: 3538482528339,
              title: "Eye Dared You So! Metallic Eyeliner",
              body_html:
                '<div class="col-md-12 video" style="text-align: left;">Ditch the usual and own the glamorous - let there be no monotony cos\' <strong>SUGAR Eye Dared You So! Metallic Eyeliner</strong> is here! Now is your time to cross the line and step into the world of glamour and glitz. Line your lids with this long-lasting, waterproof and smudge-proof metallic eyeliner. Comes in six bold &amp; shimmering shades whose liquid formula is quick drying and gives a rich matte finish. The brush applicator ensures for a smooth application of the product so that you\'re ready to wing it, anytime, any day!</div>\n<p> </p>\n<ul>\n<li>Give them a reason to swoon with the metallic coppery bronze richness of <strong><a href="https://in.sugarcosmetics.com/products/eye-dared-you-so-metallic-eyeliner-01-bronze-horseman-metallic-coppery-bronze" target="_blank" rel="noopener noreferrer">Bronze Horseman</a>.<br><br></strong>\n</li>\n<li>Make the saying \'Too glam to give a damn\' true with the metallic purple lavishness of <strong><a href="https://in.sugarcosmetics.com/products/eye-dared-you-so-metallic-eyeliner-02-purple-pirate-metallic-purple" target="_blank" rel="noopener noreferrer">Purple Pirate</a>.<br><br></strong>\n</li>\n<li>Go make the world your oyster with the metallic darkened copper drama of <strong><a href="https://in.sugarcosmetics.com/products/eye-dared-you-so-metallic-eyeliner-03-copper-gauntlet-metallic-darkened-copper" target="_blank" rel="noopener noreferrer">Copper Gauntlet</a>.<br><br></strong>\n</li>\n<li>Jump spark your most arresting assets with the metallic royal blue dazzle of <strong><a href="https://in.sugarcosmetics.com/products/eye-dared-you-so-metallic-eyeliner-04-blue-moon-metallic-royal-blue" target="_blank" rel="noopener noreferrer">Blue Moon</a>.<br><br></strong>\n</li>\n<li>Ace being both a beauty and a savage with the metallic teal green oomph of <strong><a href="https://in.sugarcosmetics.com/products/eye-dared-you-so-metallic-eyeliner-05-green-mile-metallic-teal-green" target="_blank" rel="noopener noreferrer">Green Mile</a>.<br><br></strong>\n</li>\n<li>Let the universe know you\'re a force to be reckoned with when you don black with silver microglitter of <strong><a href="https://in.sugarcosmetics.com/products/eye-dared-you-so-metallic-eyeliner-06-black-mirror-black-with-silver-micro-glitter" target="_blank" rel="noopener noreferrer">Black Mirror</a>. </strong>\n</li>\n</ul>\n<p><br> <strong>How To Apply: </strong>Shake the product well before use. Sweep the brush applicator across your lash line for a precise liner look. For a more intense look, re-apply and slay the day!</p>\n<p><strong>Benefits: SUGAR Eye Dared You So! Metallic Eyeliner </strong>gives a rich matte-metallic finish that\'s long-lasting and waterproof. It is cruelty-free and paraben free.<br> <br> <strong>Additional Details: SUGAR Eye Dared You So! Metallic Eyeliner </strong>is available in 6 bold shades viz. <a href="https://in.sugarcosmetics.com/products/eye-dared-you-so-metallic-eyeliner-01-bronze-horseman-metallic-coppery-bronze" target="_blank" rel="noopener noreferrer"><strong>01 Bronze Horseman (Metallic Coppery Bronze)</strong></a>, <a href="https://in.sugarcosmetics.com/products/eye-dared-you-so-metallic-eyeliner-02-purple-pirate-metallic-purple" target="_blank" rel="noopener noreferrer"><strong>02 Purple Pirate (Metallic Purple)</strong></a>, <a href="https://in.sugarcosmetics.com/products/eye-dared-you-so-metallic-eyeliner-03-copper-gauntlet-metallic-darkened-copper" target="_blank" rel="noopener noreferrer"><strong>03 Copper Gauntlet (Metallic Darkened Copper)</strong></a>, <a href="https://in.sugarcosmetics.com/products/eye-dared-you-so-metallic-eyeliner-04-blue-moon-metallic-royal-blue" target="_blank" rel="noopener noreferrer"><strong>04 Blue Moon (Metallic Royal Blue)</strong></a>, <a href="https://in.sugarcosmetics.com/products/eye-dared-you-so-metallic-eyeliner-05-green-mile-metallic-teal-green" target="_blank" rel="noopener noreferrer"><strong>05 Green Mile (Metallic Teal Green)</strong></a>, <a href="https://in.sugarcosmetics.com/products/eye-dared-you-so-metallic-eyeliner-06-black-mirror-black-with-silver-micro-glitter" target="_blank" rel="noopener noreferrer"><strong>06 Black Mirror (Black with Silver micro glitter)</strong></a>. This product is dermatologically tested &amp; approved and 100% safe for your skin.<br><br> <strong>Net Volume: </strong>2 ml. <br> <br> <strong>List of Ingredients: Water, Styrene/Acrylates Copolymer, Alcohol, Synthetic Fluorphlogopite, Mica, Laureth-21, Glycerin, PEG-40, Hydrogenated Castor Oil, Caprylyl Glycol, Phenoxyethanol, Carbomer, Sodium Dehydroacetate, Triethanolamine, Hexylene glycol, Calcium Aluminium Borosilicate,  Tin Oxide, Laureth-21, CI 77891, CI 74160, CI 77266, CI 77288, CI 77499.</strong></p>\n<p><strong>MRP</strong><span>: Rs. 699 (incl. all taxes)</span><br><br><strong>Country of Origin</strong><span>: </span><span mce-data-marked="1">Taiwan</span><br><br><strong>Company Name</strong><span>:</span><span> Beaunion Colours Co. Ltd.</span><br><br><strong>Company Address</strong><span>:</span><span> 2F , No. 163, Liufen Rd. Wai Pu Dist., Taichung City 43857</span></p>',
              vendor: "SUGAR Cosmetics",
              product_type: "Eyeliner",
              created_at: "2019-06-06T11:31:39+05:30",
              handle: "eye-dared-you-so-metallic-eyeliner",
              updated_at: "2020-12-31T14:29:21+05:30",
              published_at: "2019-06-07T16:21:50+05:30",
              template_suffix: "",
              published_scope: "web",
              tags:
                "100% safe for skin, 12 HR, 699, 9 to 5, Alcohol Free, amp-hide, Bold colours, Bride, Brush applicator, Budgeproof, Creamy, Cruelty Free, Dermatologically Tested, Eye, Eye Liner, Eyeliner, Festive, Glitter, Glossy, Gluten Free, High Coverage, Intense finsih, Long Lasting, Long wear, Made In Taiwan, Metallic eyeliner, Metallic finish, Metallic Shades, Microglitter, Ophthalmologically tested, Paraben Free, Precise application, Quick drying, Rich Matte, Rich matte finish, Shimmering shades, Smooth application, Smudge Free, Smudgeproof, st_finish: Metallic, st_formulation: Liquid, st_type: Eyeliner, sugar_type_1, Sulphate Free, Travel friendly, Under 700, Vegan, Vegetarian, Waterproof, Wedding, Wing",
              admin_graphql_api_id: "gid://shopify/Product/3538482528339",
              variants: [
                {
                  id: 27921122590803,
                  product_id: 3538482528339,
                  title: "01 Bronze Horseman (Metallic Coppery Bronze)",
                  price: "349.00",
                  sku: "8904320701109",
                  position: 1,
                  inventory_policy: "deny",
                  compare_at_price: "699.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "01 Bronze Horseman (Metallic Coppery Bronze)",
                  option2: null,
                  option3: null,
                  created_at: "2019-06-06T11:31:40+05:30",
                  updated_at: "2020-12-31T12:15:11+05:30",
                  taxable: false,
                  barcode: "",
                  grams: 4,
                  image_id: 13728768131155,
                  weight: 4,
                  weight_unit: "g",
                  inventory_item_id: 28989041639507,
                  inventory_quantity: 3163,
                  old_inventory_quantity: 3163,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/27921122590803",
                },
                {
                  id: 27921124786259,
                  product_id: 3538482528339,
                  title: "02 Purple Pirate (Metallic Purple)",
                  price: "349.00",
                  sku: "8904320701116",
                  position: 2,
                  inventory_policy: "deny",
                  compare_at_price: "699.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "02 Purple Pirate (Metallic Purple)",
                  option2: null,
                  option3: null,
                  created_at: "2019-06-06T11:35:46+05:30",
                  updated_at: "2020-12-31T14:29:21+05:30",
                  taxable: false,
                  barcode: "",
                  grams: 4,
                  image_id: 13728766525523,
                  weight: 4,
                  weight_unit: "g",
                  inventory_item_id: 28989048258643,
                  inventory_quantity: 3873,
                  old_inventory_quantity: 3873,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/27921124786259",
                },
                {
                  id: 27921124819027,
                  product_id: 3538482528339,
                  title: "03 Copper Gauntlet (Metallic Darkened Copper)",
                  price: "349.00",
                  sku: "8904320701123",
                  position: 3,
                  inventory_policy: "deny",
                  compare_at_price: "699.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "03 Copper Gauntlet (Metallic Darkened Copper)",
                  option2: null,
                  option3: null,
                  created_at: "2019-06-06T11:35:47+05:30",
                  updated_at: "2020-12-31T11:30:06+05:30",
                  taxable: false,
                  barcode: "",
                  grams: 4,
                  image_id: 13728765018195,
                  weight: 4,
                  weight_unit: "g",
                  inventory_item_id: 28989048488019,
                  inventory_quantity: 2542,
                  old_inventory_quantity: 2542,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/27921124819027",
                },
                {
                  id: 27921124851795,
                  product_id: 3538482528339,
                  title: "04 Blue Moon (Metallic Royal Blue)",
                  price: "699.00",
                  sku: "8904320701130",
                  position: 4,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "04 Blue Moon (Metallic Royal Blue)",
                  option2: null,
                  option3: null,
                  created_at: "2019-06-06T11:35:47+05:30",
                  updated_at: "2020-12-31T11:30:11+05:30",
                  taxable: false,
                  barcode: "",
                  grams: 4,
                  image_id: 13736409628755,
                  weight: 4,
                  weight_unit: "g",
                  inventory_item_id: 28989048684627,
                  inventory_quantity: 1123,
                  old_inventory_quantity: 1123,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/27921124851795",
                },
                {
                  id: 27921124884563,
                  product_id: 3538482528339,
                  title: "05 Green Mile (Metallic Teal Green)",
                  price: "349.00",
                  sku: "8904320701147",
                  position: 5,
                  inventory_policy: "deny",
                  compare_at_price: "699.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "05 Green Mile (Metallic Teal Green)",
                  option2: null,
                  option3: null,
                  created_at: "2019-06-06T11:35:47+05:30",
                  updated_at: "2020-12-31T14:29:21+05:30",
                  taxable: false,
                  barcode: "",
                  grams: 4,
                  image_id: 13736410415187,
                  weight: 4,
                  weight_unit: "g",
                  inventory_item_id: 28989048881235,
                  inventory_quantity: 2529,
                  old_inventory_quantity: 2529,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/27921124884563",
                },
                {
                  id: 27921124917331,
                  product_id: 3538482528339,
                  title: "06 Black Mirror (Black with Silver micro glitter)",
                  price: "699.00",
                  sku: "8904320701154",
                  position: 6,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "06 Black Mirror (Black with Silver micro glitter)",
                  option2: null,
                  option3: null,
                  created_at: "2019-06-06T11:35:47+05:30",
                  updated_at: "2020-12-31T11:30:11+05:30",
                  taxable: false,
                  barcode: "",
                  grams: 4,
                  image_id: 13736416673875,
                  weight: 4,
                  weight_unit: "g",
                  inventory_item_id: 28989049077843,
                  inventory_quantity: 1623,
                  old_inventory_quantity: 1623,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/27921124917331",
                },
              ],
              options: [
                {
                  id: 4629505343571,
                  product_id: 3538482528339,
                  name: "Colour",
                  position: 1,
                  values: [
                    "01 Bronze Horseman (Metallic Coppery Bronze)",
                    "02 Purple Pirate (Metallic Purple)",
                    "03 Copper Gauntlet (Metallic Darkened Copper)",
                    "04 Blue Moon (Metallic Royal Blue)",
                    "05 Green Mile (Metallic Teal Green)",
                    "06 Black Mirror (Black with Silver micro glitter)",
                  ],
                },
              ],
              images: [
                {
                  id: 13728768131155,
                  product_id: 3538482528339,
                  position: 1,
                  created_at: "2019-12-23T18:11:55+05:30",
                  updated_at: "2019-12-25T14:32:23+05:30",
                  alt:
                    "SUGAR Cosmetics Eye Dared You So! Metallic Eyeliner 01 Bronze Horseman (Metallic Coppery Bronze)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-eye-dared-you-so-metallic-eyeliner-01-bronze-horseman-metallic-coppery-bronze-13272118165587.jpg?v=1577264543",
                  variant_ids: [27921122590803],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13728768131155",
                },
                {
                  id: 13728766525523,
                  product_id: 3538482528339,
                  position: 2,
                  created_at: "2019-12-23T18:11:38+05:30",
                  updated_at: "2019-12-25T14:32:23+05:30",
                  alt:
                    "SUGAR Cosmetics Eye Dared You So! Metallic Eyeliner 02 Purple Pirate (Metallic Purple)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-eye-dared-you-so-metallic-eyeliner-02-purple-pirate-metallic-purple-13272121737299.jpg?v=1577264543",
                  variant_ids: [27921124786259],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13728766525523",
                },
                {
                  id: 13728765018195,
                  product_id: 3538482528339,
                  position: 3,
                  created_at: "2019-12-23T18:11:23+05:30",
                  updated_at: "2019-12-25T14:32:23+05:30",
                  alt:
                    "SUGAR Cosmetics Eye Dared You So! Metallic Eyeliner 03 Copper Gauntlet (Metallic Darkened Copper)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-eye-dared-you-so-metallic-eyeliner-03-copper-gauntlet-metallic-darkened-copper-13272123932755.jpg?v=1577264543",
                  variant_ids: [27921124819027],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13728765018195",
                },
                {
                  id: 13736409628755,
                  product_id: 3538482528339,
                  position: 4,
                  created_at: "2019-12-25T14:30:41+05:30",
                  updated_at: "2019-12-25T14:32:23+05:30",
                  alt:
                    "SUGAR Cosmetics Eye Dared You So! Metallic Eyeliner 04 Blue Moon (Metallic Royal Blue)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-eye-dared-you-so-metallic-eyeliner-04-blue-moon-metallic-royal-blue-13272126980179.jpg?v=1577264543",
                  variant_ids: [27921124851795],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13736409628755",
                },
                {
                  id: 13736410415187,
                  product_id: 3538482528339,
                  position: 5,
                  created_at: "2019-12-25T14:30:56+05:30",
                  updated_at: "2019-12-25T14:32:23+05:30",
                  alt:
                    "SUGAR Cosmetics Eye Dared You So! Metallic Eyeliner 05 Green Mile (Metallic Teal Green)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-eye-dared-you-so-metallic-eyeliner-05-green-mile-metallic-teal-green-13272128913491.jpg?v=1577264543",
                  variant_ids: [27921124884563],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13736410415187",
                },
                {
                  id: 13736416673875,
                  product_id: 3538482528339,
                  position: 6,
                  created_at: "2019-12-25T14:32:22+05:30",
                  updated_at: "2019-12-25T14:32:23+05:30",
                  alt:
                    "SUGAR Cosmetics Eye Dared You So! Metallic Eyeliner 06 Black Mirror (Black with Silver micro glitter)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-eye-dared-you-so-metallic-eyeliner-06-black-mirror-black-with-silver-micro-glitter-13272133664851.jpg?v=1577264543",
                  variant_ids: [27921124917331],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13736416673875",
                },
              ],
              image: {
                id: 13728768131155,
                product_id: 3538482528339,
                position: 1,
                created_at: "2019-12-23T18:11:55+05:30",
                updated_at: "2019-12-25T14:32:23+05:30",
                alt:
                  "SUGAR Cosmetics Eye Dared You So! Metallic Eyeliner 01 Bronze Horseman (Metallic Coppery Bronze)",
                width: 1025,
                height: 1400,
                src:
                  "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-eye-dared-you-so-metallic-eyeliner-01-bronze-horseman-metallic-coppery-bronze-13272118165587.jpg?v=1577264543",
                variant_ids: [27921122590803],
                admin_graphql_api_id:
                  "gid://shopify/ProductImage/13728768131155",
              },
            },
            mediaText: null,
            redirectUrl:
              "https://in.sugarcosmetics.com/products/eye-dared-you-so-metallic-eyeliner",
          },
          {
            id: 163,
            sectionId: 6,
            webViewTitle: null,
            contentType: 3,
            mediaUrl: null,
            product_json: {
              id: 2383381692499,
              title: "Click Me Up Velvet Lipstick",
              body_html:
                'Give in to your alter ego and live the deepest of your lip fantasies with the <strong>SUGAR Click Me Up Velvet Lipstick</strong>. <br>From the land of premium products, we bring you the luxury itself. There\'ll be no more compromises cos’ now you can have it all – with just a single click! A single stroke of this lipstick creates sultry bold colour that lasts for 10+ hours. Choose from 10 richly pigmented, addictive shades that leave your lips with a matte yet soft velvety finish. From colours that look phenomenal on fair skin to colours that make the deep skin tone look tremendous, be ready to create your own bespoke look. Featuring a super-soft bullet that embraces your lips, this gorgeous lipstick enables you to precisely line that pretty pout of yours. Get the right amount of lipstick in one single click, cos’ there’s no going back *wink*<br><br>Lust over the first-ever in the nation, the <strong>SUGAR Click Me Up Velvet Lipstick</strong>. <br>You gotta get addicted to these!<br><br>\n<ul>\n<li>Living life with dollops of spice? Get slayin\' with the heady mix of peach and rose that\'s <a href="https://in.sugarcosmetics.com/products/click-me-up-velvet-lipstick-01-spicy-salmon-peach-rose"><strong>Spicy Salmon!</strong></a><br><br>\n</li>\n<li>Get the raunch factor soaring to the zenith, with a dose of nude rose pink in <a href="https://in.sugarcosmetics.com/products/click-me-up-velvet-lipstick-02-raunchy-rose-nude-rose-pink"><strong>Raunchy Rose</strong></a>.<br><br>\n</li>\n<li>Make all hearts dance for you when you walk with the sensual grace of the rusty nude shade in <a href="https://in.sugarcosmetics.com/products/click-me-up-velvet-lipstick-03-foxy-fawn-burnt-red-brown-nude-rusty-nude"><strong>Foxy Fawn</strong></a>.<br><br>\n</li>\n<li>A little provoking by the plum rose hue in <a href="https://in.sugarcosmetics.com/products/click-me-up-velvet-lipstick-04-provocative-pink-plum-rose"><strong>Provocative Pink</strong></a> makes them spill what’s hidden in their hearts.<br><br>\n</li>\n<li>What’s causing them to be pulled towards you, girl? It has to be the deep fuchsia pink of <a href="https://in.sugarcosmetics.com/products/click-me-up-velvet-lipstick-05-magnetic-magenta-deep-fuchsia-pink"><strong>Magnetic Magenta</strong></a>.<br><br>\n</li>\n<li>Get those lips to match that fantastic form! All you need is the deep berry mauve concoction of <a href="https://in.sugarcosmetics.com/products/click-me-up-velvet-lipstick-06-berry-bootylicious-deep-berry-mauve"><strong>Berry Bootylicious</strong></a>.<br><br>\n</li>\n<li>Drench them lips in the wine colour of <a href="https://in.sugarcosmetics.com/products/click-me-up-velvet-lipstick-07-risque-raspberry-wine-sangria"><strong>Risque Raspberry</strong></a>, to make them do all the talking.<br><br>\n</li>\n<li>Strike when the iron’s hot with the rich shade of brown toned burnt orange in <a href="https://in.sugarcosmetics.com/products/click-me-up-velvet-lipstick-08-smoking-sienna-brown-toned-burnt-orange"><strong>Smoking Sienna</strong></a>.<br><br>\n</li>\n<li>Make their souls fly with that alluring hue of bright orange with hints of red and that super terrific pout. Get <a href="https://in.sugarcosmetics.com/products/click-me-up-velvet-lipstick-09-tantalising-tangerine-bright-orange-with-hints-of-red"><strong>Tantalising Tangerine!</strong></a><br><br>\n</li>\n<li>\n<span>Get their heartbeats racing when you don the bright red superpowers of </span><a href="https://in.sugarcosmetics.com/products/click-me-up-velvet-lipstick-10-racy-ruby-bright-red"><strong>Racy Ruby</strong></a>.</li>\n</ul>\n<p><strong>How to Apply</strong>: Exfoliate your lips. Click once and the lip colour instantly appears. The ergonomic tip with the non-retractable click technology precisely outlines the contour &amp; fills the lips. Apply to the lips with the pointed edge of the applicator facing upwards. <br><br><strong>Benefits</strong>: <strong>SUGAR Click Me Up Velvet Lipstick</strong> has a paraben-free and cruelty-free formula that lends a light and premium finish to your lips. With a slight rosy fragrance, one single swipe of colour gives rich pigmentation.<br><br><strong>Additional Details</strong>: <strong>SUGAR Click Me Up Velvet Lipstick</strong> is available in 10 addictive shades viz., <a href="https://in.sugarcosmetics.com/products/click-me-up-velvet-lipstick-01-spicy-salmon-peach-rose" target="_blank" rel="noopener noreferrer"><strong>01 Spicy Salmon</strong></a><strong>, <a href="https://in.sugarcosmetics.com/products/click-me-up-velvet-lipstick-02-raunchy-rose-nude-rose-pink" target="_blank" rel="noopener noreferrer">02 Raunchy Rose</a>, <a href="https://in.sugarcosmetics.com/products/click-me-up-velvet-lipstick-03-foxy-fawn-burnt-red-brown-nude-rusty-nude" target="_blank" rel="noopener noreferrer">03 Foxy Fawn</a>, <a href="https://in.sugarcosmetics.com/products/click-me-up-velvet-lipstick-04-provocative-pink-plum-rose" target="_blank" rel="noopener noreferrer">04 Provocative Pink</a>, <a href="https://in.sugarcosmetics.com/products/click-me-up-velvet-lipstick-05-magnetic-magenta-deep-fuchsia-pink" target="_blank" rel="noopener noreferrer">05 Magnetic Magenta</a>, <a href="https://in.sugarcosmetics.com/products/click-me-up-velvet-lipstick-06-berry-bootylicious-deep-berry-mauve" target="_blank" rel="noopener noreferrer">06 Berry Bootylicious</a>, <a href="https://in.sugarcosmetics.com/products/click-me-up-velvet-lipstick-07-risque-raspberry-wine-sangria" target="_blank" rel="noopener noreferrer">07 Risque Raspberry</a>, <a href="https://in.sugarcosmetics.com/products/click-me-up-velvet-lipstick-08-smoking-sienna-brown-toned-burnt-orange" target="_blank" rel="noopener noreferrer">08 Smoking Sienna</a>, <a href="https://in.sugarcosmetics.com/products/click-me-up-velvet-lipstick-09-tantalising-tangerine-bright-orange-with-hints-of-red" target="_blank" rel="noopener noreferrer">09 Tantalising Tangerine</a></strong> and <a href="https://in.sugarcosmetics.com/products/click-me-up-velvet-lipstick-10-racy-ruby-bright-red" target="_blank" rel="noopener noreferrer"><strong>10 Racy Ruby</strong></a>.<br><br><strong>Net Weight</strong>: 2g<br><br><strong>List Of Ingredients</strong>: C12-15 Alkyl Ethylhexanoate, Kaolin, Silica, Methyl Methacrylate Crosspolymer, Phenyl Trimethicone, Ceresin, Triethylhexanoin, Dimethicone, Polyglyceryl-2 Triisostearate, Microcrystalline Wax, Euphorbia Cerifera (Candelilla) Wax, Titanium Dioxide (CI 77891), Polybutene, Iron Oxide Red(CI 77491), Dimethicone Crosspolymer, Red 6 (CI 15850), Tocopheryl Acetate, VP/Hexadecene Copolymer, Iron Oxide Black(CI 77499), Red 7 (CI 15850), Yellow 6 (CI 15985), Aluminum Hydroxide, Fragrance, BHT.</p>\n<p><strong>MRP</strong><span>: Rs. 699 (incl. all taxes)</span><br><br><strong>Country of Origin</strong><span>: </span><span>Korea</span><br><br><strong>Company Name</strong><span>:</span><span> SUGAR Cosmetics LLC</span><br><br><strong>Company Address</strong><span>:</span><span> 8 The Green Suite A, Dover, DE 19901, USA</span></p>\n<p><strong><em>IMP: While products listed under our Clearance section are perfectly safe for usage, they may be within 12 months of the expiry of the recommended shelf life. You are requested to factor in the same while planning your purchase. Thank you for shopping with us!</em></strong></p>',
              vendor: "SUGAR Cosmetics",
              product_type: "Velvet Lipstick",
              created_at: "2019-01-04T12:43:12+05:30",
              handle: "click-me-up-velvet-lipstick",
              updated_at: "2020-12-23T20:29:31+05:30",
              published_at: "2019-01-10T16:49:20+05:30",
              template_suffix: "",
              published_scope: "web",
              tags:
                "amp-hide, lipstick, Offer, st_finish: Matte, st_formulation: Bullet, st_type: Lipstick, sugar_type_1",
              admin_graphql_api_id: "gid://shopify/Product/2383381692499",
              variants: [
                {
                  id: 21304816140371,
                  product_id: 2383381692499,
                  title: "01 Spicy Salmon (Peach Rose)",
                  price: "350.00",
                  sku: "8906090497664",
                  position: 1,
                  inventory_policy: "deny",
                  compare_at_price: "699.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "01 Spicy Salmon (Peach Rose)",
                  option2: null,
                  option3: null,
                  created_at: "2019-01-04T12:43:13+05:30",
                  updated_at: "2020-12-15T10:57:45+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13728301973587,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 21832059420755,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/21304816140371",
                },
                {
                  id: 21304816173139,
                  product_id: 2383381692499,
                  title: "02 Raunchy Rose (Nude Rose Pink)",
                  price: "350.00",
                  sku: "8906090497671",
                  position: 2,
                  inventory_policy: "deny",
                  compare_at_price: "699.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "02 Raunchy Rose (Nude Rose Pink)",
                  option2: null,
                  option3: null,
                  created_at: "2019-01-04T12:43:13+05:30",
                  updated_at: "2020-12-23T20:29:12+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13728301088851,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 21832059486291,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/21304816173139",
                },
                {
                  id: 21304816205907,
                  product_id: 2383381692499,
                  title: "03 Foxy Fawn (Burnt Red Brown Nude/Rusty Nude)",
                  price: "350.00",
                  sku: "8906090497688",
                  position: 3,
                  inventory_policy: "deny",
                  compare_at_price: "699.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "03 Foxy Fawn (Burnt Red Brown Nude/Rusty Nude)",
                  option2: null,
                  option3: null,
                  created_at: "2019-01-04T12:43:13+05:30",
                  updated_at: "2020-12-15T10:57:51+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13728300499027,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 21832059551827,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/21304816205907",
                },
                {
                  id: 21304816238675,
                  product_id: 2383381692499,
                  title: "04 Provocative Pink (Plum Rose)",
                  price: "350.00",
                  sku: "8906090497695",
                  position: 4,
                  inventory_policy: "deny",
                  compare_at_price: "699.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "04 Provocative Pink (Plum Rose)",
                  option2: null,
                  option3: null,
                  created_at: "2019-01-04T12:43:13+05:30",
                  updated_at: "2020-10-23T14:43:49+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13735788675155,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 21832059617363,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/21304816238675",
                },
                {
                  id: 21304816271443,
                  product_id: 2383381692499,
                  title: "05 Magnetic Magenta (Deep Fuchsia Pink)",
                  price: "350.00",
                  sku: "8906090497701",
                  position: 5,
                  inventory_policy: "deny",
                  compare_at_price: "699.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "05 Magnetic Magenta (Deep Fuchsia Pink)",
                  option2: null,
                  option3: null,
                  created_at: "2019-01-04T12:43:13+05:30",
                  updated_at: "2020-11-10T18:58:23+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13735789428819,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 21832059682899,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/21304816271443",
                },
                {
                  id: 21304816304211,
                  product_id: 2383381692499,
                  title: "06 Berry Bootylicious (Deep Berry Mauve)",
                  price: "350.00",
                  sku: "8906090497718",
                  position: 6,
                  inventory_policy: "deny",
                  compare_at_price: "699.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "06 Berry Bootylicious (Deep Berry Mauve)",
                  option2: null,
                  option3: null,
                  created_at: "2019-01-04T12:43:13+05:30",
                  updated_at: "2020-10-17T18:28:36+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13735794704467,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 21832059715667,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/21304816304211",
                },
                {
                  id: 21304816336979,
                  product_id: 2383381692499,
                  title: "07 Risque Raspberry (Wine/Sangria)",
                  price: "350.00",
                  sku: "8906090497725",
                  position: 7,
                  inventory_policy: "deny",
                  compare_at_price: "699.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "07 Risque Raspberry (Wine/Sangria)",
                  option2: null,
                  option3: null,
                  created_at: "2019-01-04T12:43:13+05:30",
                  updated_at: "2020-11-06T17:19:05+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13728307609683,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 21832059781203,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/21304816336979",
                },
                {
                  id: 21304816369747,
                  product_id: 2383381692499,
                  title: "08 Smoking Sienna (Brown Toned Burnt Orange)",
                  price: "350.00",
                  sku: "8906090497732",
                  position: 8,
                  inventory_policy: "deny",
                  compare_at_price: "699.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "08 Smoking Sienna (Brown Toned Burnt Orange)",
                  option2: null,
                  option3: null,
                  created_at: "2019-01-04T12:43:13+05:30",
                  updated_at: "2020-11-06T12:28:45+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13728306757715,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 21832059846739,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/21304816369747",
                },
                {
                  id: 21304816402515,
                  product_id: 2383381692499,
                  title:
                    "09 Tantalising Tangerine (Bright Orange with Hints of Red)",
                  price: "350.00",
                  sku: "8906090497749",
                  position: 9,
                  inventory_policy: "deny",
                  compare_at_price: "699.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1:
                    "09 Tantalising Tangerine (Bright Orange with Hints of Red)",
                  option2: null,
                  option3: null,
                  created_at: "2019-01-04T12:43:13+05:30",
                  updated_at: "2020-11-09T14:08:08+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13728306004051,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 21832059912275,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/21304816402515",
                },
                {
                  id: 21304816435283,
                  product_id: 2383381692499,
                  title: "10 Racy Ruby (Bright Red)",
                  price: "350.00",
                  sku: "8906090497756",
                  position: 10,
                  inventory_policy: "deny",
                  compare_at_price: "699.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "10 Racy Ruby (Bright Red)",
                  option2: null,
                  option3: null,
                  created_at: "2019-01-04T12:43:13+05:30",
                  updated_at: "2020-12-23T20:29:31+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13735798833235,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 21832059977811,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/21304816435283",
                },
              ],
              options: [
                {
                  id: 3288800591955,
                  product_id: 2383381692499,
                  name: "Colour",
                  position: 1,
                  values: [
                    "01 Spicy Salmon (Peach Rose)",
                    "02 Raunchy Rose (Nude Rose Pink)",
                    "03 Foxy Fawn (Burnt Red Brown Nude/Rusty Nude)",
                    "04 Provocative Pink (Plum Rose)",
                    "05 Magnetic Magenta (Deep Fuchsia Pink)",
                    "06 Berry Bootylicious (Deep Berry Mauve)",
                    "07 Risque Raspberry (Wine/Sangria)",
                    "08 Smoking Sienna (Brown Toned Burnt Orange)",
                    "09 Tantalising Tangerine (Bright Orange with Hints of Red)",
                    "10 Racy Ruby (Bright Red)",
                  ],
                },
              ],
              images: [
                {
                  id: 13728301973587,
                  product_id: 2383381692499,
                  position: 1,
                  created_at: "2019-12-23T15:05:53+05:30",
                  updated_at: "2019-12-25T11:16:21+05:30",
                  alt:
                    "SUGAR Cosmetics Click Me Up Velvet Lipstick 01 Spicy Salmon (Peach Rose)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-click-me-up-velvet-lipstick-01-spicy-salmon-peach-rose-13200621994067.jpg?v=1577252781",
                  variant_ids: [21304816140371],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13728301973587",
                },
                {
                  id: 13728301088851,
                  product_id: 2383381692499,
                  position: 2,
                  created_at: "2019-12-23T15:05:36+05:30",
                  updated_at: "2019-12-25T11:16:21+05:30",
                  alt:
                    "SUGAR Cosmetics Click Me Up Velvet Lipstick 02 Raunchy Rose (Nude Rose Pink)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-click-me-up-velvet-lipstick-02-raunchy-rose-nude-rose-pink-13200626516051.jpg?v=1577252781",
                  variant_ids: [21304816173139],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13728301088851",
                },
                {
                  id: 13728300499027,
                  product_id: 2383381692499,
                  position: 3,
                  created_at: "2019-12-23T15:05:22+05:30",
                  updated_at: "2019-12-25T11:16:21+05:30",
                  alt:
                    "SUGAR Cosmetics Click Me Up Velvet Lipstick 03 Foxy Fawn (Burnt Red Brown Nude/Rusty Nude)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-click-me-up-velvet-lipstick-03-foxy-fawn-burnt-red-brown-nude-rusty-nude-13200627105875.jpg?v=1577252781",
                  variant_ids: [21304816205907],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13728300499027",
                },
                {
                  id: 13735788675155,
                  product_id: 2383381692499,
                  position: 4,
                  created_at: "2019-12-25T11:12:40+05:30",
                  updated_at: "2019-12-25T11:16:21+05:30",
                  alt:
                    "SUGAR Cosmetics Click Me Up Velvet Lipstick 04 Provocative Pink (Plum Rose)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-click-me-up-velvet-lipstick-04-provocative-pink-plum-rose-13200628613203.jpg?v=1577252781",
                  variant_ids: [21304816238675],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13735788675155",
                },
                {
                  id: 13735789428819,
                  product_id: 2383381692499,
                  position: 5,
                  created_at: "2019-12-25T11:12:56+05:30",
                  updated_at: "2019-12-25T11:16:21+05:30",
                  alt:
                    "SUGAR Cosmetics Click Me Up Velvet Lipstick 05 Magnetic Magenta (Deep Fuchsia Pink)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-click-me-up-velvet-lipstick-05-magnetic-magenta-deep-fuchsia-pink-13200629497939.jpg?v=1577252781",
                  variant_ids: [21304816271443],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13735789428819",
                },
                {
                  id: 13735794704467,
                  product_id: 2383381692499,
                  position: 6,
                  created_at: "2019-12-25T11:14:56+05:30",
                  updated_at: "2019-12-25T11:16:21+05:30",
                  alt:
                    "SUGAR Cosmetics Click Me Up Velvet Lipstick 06 Berry Bootylicious (Deep Berry Mauve)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-click-me-up-velvet-lipstick-06-berry-bootylicious-deep-berry-mauve-13200630284371.jpg?v=1577252781",
                  variant_ids: [21304816304211],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13735794704467",
                },
                {
                  id: 13728307609683,
                  product_id: 2383381692499,
                  position: 7,
                  created_at: "2019-12-23T15:07:54+05:30",
                  updated_at: "2019-12-25T11:16:21+05:30",
                  alt:
                    "SUGAR Cosmetics Click Me Up Velvet Lipstick 07 Risque Raspberry (Wine/Sangria)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-click-me-up-velvet-lipstick-07-risque-raspberry-wine-sangria-13200631038035.jpg?v=1577252781",
                  variant_ids: [21304816336979],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13728307609683",
                },
                {
                  id: 13728306757715,
                  product_id: 2383381692499,
                  position: 8,
                  created_at: "2019-12-23T15:07:36+05:30",
                  updated_at: "2019-12-25T11:16:21+05:30",
                  alt:
                    "SUGAR Cosmetics Click Me Up Velvet Lipstick 08 Smoking Sienna (Brown Toned Burnt Orange)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-click-me-up-velvet-lipstick-08-smoking-sienna-brown-toned-burnt-orange-13200631529555.jpg?v=1577252781",
                  variant_ids: [21304816369747],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13728306757715",
                },
                {
                  id: 13728306004051,
                  product_id: 2383381692499,
                  position: 9,
                  created_at: "2019-12-23T15:07:22+05:30",
                  updated_at: "2019-12-25T11:16:21+05:30",
                  alt:
                    "SUGAR Cosmetics Click Me Up Velvet Lipstick 09 Tantalising Tangerine (Bright Orange with Hints of Red)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-click-me-up-velvet-lipstick-09-tantalising-tangerine-bright-orange-with-hints-of-red-13200631758931.jpg?v=1577252781",
                  variant_ids: [21304816402515],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13728306004051",
                },
                {
                  id: 13735798833235,
                  product_id: 2383381692499,
                  position: 10,
                  created_at: "2019-12-25T11:16:20+05:30",
                  updated_at: "2019-12-25T11:16:21+05:30",
                  alt:
                    "SUGAR Cosmetics Click Me Up Velvet Lipstick 10 Racy Ruby (Bright Red)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-click-me-up-velvet-lipstick-10-racy-ruby-bright-red-13200632086611.jpg?v=1577252781",
                  variant_ids: [21304816435283],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13735798833235",
                },
              ],
              image: {
                id: 13728301973587,
                product_id: 2383381692499,
                position: 1,
                created_at: "2019-12-23T15:05:53+05:30",
                updated_at: "2019-12-25T11:16:21+05:30",
                alt:
                  "SUGAR Cosmetics Click Me Up Velvet Lipstick 01 Spicy Salmon (Peach Rose)",
                width: 1025,
                height: 1400,
                src:
                  "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-click-me-up-velvet-lipstick-01-spicy-salmon-peach-rose-13200621994067.jpg?v=1577252781",
                variant_ids: [21304816140371],
                admin_graphql_api_id:
                  "gid://shopify/ProductImage/13728301973587",
              },
            },
            mediaText: null,
            redirectUrl:
              "https://in.sugarcosmetics.com/products/click-me-up-velvet-lipstick",
          },
        ],
      },
      {
        id: 14,
        title: "BEST OF THE BEST",
        sequence: 73,
        layoutType: 4,
        isTitleEnabled: 1,
        backgroundImg: null,
        backgroundColor: "#000000",
        text: null,
        contentType: 1,
        contentData: [
          {
            id: 117,
            sectionId: 14,
            webViewTitle: null,
            contentType: 1,
            mediaUrl:
              "https://d32baadbbpueqt.cloudfront.net/dashboard/1610016023BEST-OF-BEST-Pink-600x300.jpg",
            product_json: null,
            mediaText: null,
            redirectUrl:
              "https://in.sugarcosmetics.com/collections/pink-and-purple-lipsticks",
          },
          {
            id: 118,
            sectionId: 14,
            webViewTitle: null,
            contentType: 1,
            mediaUrl:
              "https://d32baadbbpueqt.cloudfront.net/dashboard/1610016043BEST-OF-BEST-Red-600x300.jpg",
            product_json: null,
            mediaText: null,
            redirectUrl:
              "https://in.sugarcosmetics.com/collections/red-and-orange-lipsticks",
          },
          {
            id: 119,
            sectionId: 14,
            webViewTitle: null,
            contentType: 1,
            mediaUrl:
              "https://d32baadbbpueqt.cloudfront.net/dashboard/1610016078Bold-Browns-Bold-Browns-600x300.jpg",
            product_json: null,
            mediaText: null,
            redirectUrl:
              "https://in.sugarcosmetics.com/collections/brown-lipsticks",
          },
        ],
      },
      {
        id: 12,
        title: "FRESH FACE LOOKS",
        sequence: 76,
        layoutType: 2,
        isTitleEnabled: 1,
        backgroundImg: null,
        backgroundColor: "#d4f1f7",
        text: null,
        contentType: 1,
        contentData: [
          {
            id: 111,
            sectionId: 12,
            webViewTitle: null,
            contentType: 1,
            mediaUrl:
              "https://d32baadbbpueqt.cloudfront.net/dashboard/1610027555Get-the-look-600x500-01_(1).jpg",
            product_json: null,
            mediaText: "Candy Cane Babe",
            redirectUrl:
              "https://in.sugarcosmetics.com/collections/cherry-blossom-dew",
          },
          {
            id: 112,
            sectionId: 12,
            webViewTitle: null,
            contentType: 1,
            mediaUrl:
              "https://d32baadbbpueqt.cloudfront.net/dashboard/1610027581Get-the-look-600x500-02_(1).jpg",
            product_json: null,
            mediaText: "starry goals",
            redirectUrl:
              "https://in.sugarcosmetics.com/collections/call-it-coral",
          },
          {
            id: 113,
            sectionId: 12,
            webViewTitle: null,
            contentType: 1,
            mediaUrl:
              "https://d32baadbbpueqt.cloudfront.net/dashboard/1610027592Get-the-look-600x500x03_(1).jpg",
            product_json: null,
            mediaText: "Classic Holiday Glam",
            redirectUrl:
              "https://in.sugarcosmetics.com/collections/pink-it-up-glow",
          },
        ],
      },
      {
        id: 16,
        title: "SKINCARE BASICS",
        sequence: 87,
        layoutType: 3,
        isTitleEnabled: 1,
        backgroundImg: null,
        backgroundColor: "#000000",
        text: null,
        contentType: 3,
        contentData: [
          {
            id: 147,
            sectionId: 16,
            webViewTitle: null,
            contentType: 3,
            mediaUrl: null,
            product_json: {
              id: 4352974979155,
              title: "Aquaholic Hydrating Stick",
              body_html:
                '<p>It provides instant <strong>hydration</strong>, nourishes skin and provides skin protection. Soothes puffy eyes and keeps you looking refreshed all day long.</p>\n<p>You just have to dive in!</p>\n<p><strong>How to Apply</strong>: Gently use the SUGAR Aquaholic Hydrating Stick on areas to relieve them from dehydration symptoms and massage until absorbed. Apply under eyes to remove puffiness.</p>\n<p><strong>Benefits</strong>: <strong>SUGAR Aquaholic Hydrating Stick</strong> hydrates the skin and instantly moisturizes leaving it soft, supple and healthy-looking. It aids in relieving puffiness around eyes, gives a cooling sensation and is travel-friendly.</p>\n<p><strong>Additional Details</strong>: <strong>SUGAR Aquaholic</strong> range comprises of 4 hydrating and soothing products viz., <strong>Aquaholic Priming Moisturizer</strong>, <strong>Aquaholic Water Boost Mask</strong>, <strong>Aquaholic Overnight Water Mask</strong>, <strong>Aquaholic Hydrating Stick</strong>.</p>\n<p><strong>Net Weight</strong>: 32g</p>\n<p><strong>List of Ingredients</strong>:  AQUA, GLYCERIN, SODIUM STEARATE, BUTYLENE GLYCOL, BIS-PEG-18 METHYL ETHER DIMETHYL SILANE, CAMELLIA SINENSIS LEAF EXTRACT, ALOE BARBADENSIS LEAF JUICE, PHENOXYETHANOL, MENTHOXYPROPANEDIOL, HAMAMELIS VIRGINIANA (WITCH HAZEL) EXTRACT, MENTHOL, SODIUM HYALURONATE.<br></p>\n<p><strong>MRP</strong><span>: Rs. 899 (incl. all taxes)</span><br><br><strong>Country of Origin</strong><span>: </span><span mce-data-marked="1">PRC</span><br><br><strong>Company Name</strong><span>:</span><span> SUGAR Cosmetics LLC</span><br><br><strong>Company Address</strong><span>:</span><span> 8 The Green Suite A, Dover, DE 19901, USA</span></p>',
              vendor: "SUGAR Cosmetics",
              product_type: "Hydrating Stick",
              created_at: "2019-11-20T16:10:58+05:30",
              handle: "aquaholic-hydrating-stick",
              updated_at: "2020-12-31T13:26:25+05:30",
              published_at: "2019-11-20T16:20:05+05:30",
              template_suffix: "",
              published_scope: "web",
              tags:
                "999, Aqua, Aquaholic, Blue, Cooling, Cruelty Free, Easy to use, Elastic, Essentials, Face, Fresh, Healthy Skin, Hydrated Skin, Hydrating, Hydrating Stick, Hydration, Massage, Mess Free, Moisturising, Moisturizing, Non Sticky, Nourishes, Paraben Free, Reduce Puffiness, Refreshing, Retains Moisture, Skin, Skin Care, Skin Protection, Soft, Soothing, st_ concern: Oil control, st_ concern: Sun protection, st_concern: Hydration, st_range: Aquaholic, Stick, sugar_type_0, Summers, Supple, Travel Friendly, Under 1000, Water, Water Base",
              admin_graphql_api_id: "gid://shopify/Product/4352974979155",
              variants: [
                {
                  id: 31195033239635,
                  product_id: 4352974979155,
                  title: "Default Title",
                  price: "899.00",
                  sku: "8904320702113",
                  position: 1,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "Default Title",
                  option2: null,
                  option3: null,
                  created_at: "2019-11-20T16:10:58+05:30",
                  updated_at: "2020-12-31T13:26:25+05:30",
                  taxable: false,
                  barcode: "",
                  grams: 32,
                  image_id: null,
                  weight: 32,
                  weight_unit: "g",
                  inventory_item_id: 32716517933139,
                  inventory_quantity: 9073,
                  old_inventory_quantity: 9073,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/31195033239635",
                },
              ],
              options: [
                {
                  id: 5651270762579,
                  product_id: 4352974979155,
                  name: "Title",
                  position: 1,
                  values: ["Default Title"],
                },
              ],
              images: [
                {
                  id: 13736575074387,
                  product_id: 4352974979155,
                  position: 1,
                  created_at: "2019-12-25T15:22:36+05:30",
                  updated_at: "2020-04-22T12:39:13+05:30",
                  alt: "SUGAR Cosmetics Aquaholic Hydrating Stick",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-aquaholic-hydrating-stick-13548839731283.jpg?v=1587539353",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13736575074387",
                },
                {
                  id: 14280328609875,
                  product_id: 4352974979155,
                  position: 2,
                  created_at: "2020-04-22T12:39:12+05:30",
                  updated_at: "2020-04-23T11:24:33+05:30",
                  alt: "SUGAR Cosmetics Aquaholic Hydrating Stick",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-aquaholic-hydrating-stick-14280318353491.jpg?v=1587621273",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14280328609875",
                },
                {
                  id: 13736575598675,
                  product_id: 4352974979155,
                  position: 3,
                  created_at: "2019-12-25T15:22:53+05:30",
                  updated_at: "2020-04-23T11:24:33+05:30",
                  alt: "SUGAR Cosmetics Aquaholic Hydrating Stick",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-aquaholic-hydrating-stick-13548839796819.jpg?v=1587621273",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13736575598675",
                },
                {
                  id: 13736577925203,
                  product_id: 4352974979155,
                  position: 4,
                  created_at: "2019-12-25T15:24:22+05:30",
                  updated_at: "2020-04-23T11:24:33+05:30",
                  alt: "SUGAR Cosmetics Aquaholic Hydrating Stick",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-aquaholic-hydrating-stick-13548905103443.jpg?v=1587621273",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13736577925203",
                },
              ],
              image: {
                id: 13736575074387,
                product_id: 4352974979155,
                position: 1,
                created_at: "2019-12-25T15:22:36+05:30",
                updated_at: "2020-04-22T12:39:13+05:30",
                alt: "SUGAR Cosmetics Aquaholic Hydrating Stick",
                width: 1025,
                height: 1400,
                src:
                  "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-aquaholic-hydrating-stick-13548839731283.jpg?v=1587539353",
                variant_ids: [],
                admin_graphql_api_id:
                  "gid://shopify/ProductImage/13736575074387",
              },
            },
            mediaText: null,
            redirectUrl: null,
          },
          {
            id: 148,
            sectionId: 16,
            webViewTitle: null,
            contentType: 3,
            mediaUrl: null,
            product_json: {
              id: 4352967049299,
              title: "Aquaholic Priming Moisturizer",
              body_html:
                '<p>It soothes skin from irritation &amp; dryness. Glides on easily, locks down moisture to keep the skin refreshed. It is ultra-lightweight, non-greasy and refreshing.<br><br>A perfect mix of makeup &amp; skincare, this is an absolute must-have.</p>\n<p><strong>How to Apply: </strong>Apply a pea-sized amount of this moisturizer and gently massage into your face by using gentle upward and outward strokes. It is buildable, so you can achieve desired effect. Use alone as a moisturizer or as a prep step before applying makeup. This 2 in 1 product acts as a primer and a moisturizer &amp; can be used every day.<br><br><strong>Benefits: SUGAR Aquaholic Priming Moisturizer</strong> is enriched with Moringa Seed extracts that help protect the skin from the effect of harmful pollutants. Hyaluronic acid lends a breathable film to the skin keeping it moist &amp; smooth. It is 60% water, starts out as a cream &amp; bursts into water molecules leaving a 100% matte-finish.<br><br><strong>Additional Details: SUGAR Aquaholic range </strong>comprises of 4 hydrating and soothing products viz., <strong>Aquaholic Priming Moisturizer, <a href="https://in.sugarcosmetics.com/products/aquaholic-water-boost-mask" target="_blank" rel="noopener noreferrer">Aquaholic Water Boost Mask</a>, <a href="https://in.sugarcosmetics.com/products/aquaholic-overnight-water-mask" target="_blank" rel="noopener noreferrer">Aquaholic Overnight Water Mask</a>, <a href="https://in.sugarcosmetics.com/products/aquaholic-hydrating-stick" target="_blank" rel="noopener noreferrer">Aquaholic Hydrating Stick</a>.<br><br></strong>Net Volume: 30 ml.<br><br><strong>List of Ingredients: </strong> Water, Cyclopentasiloxane, Dimethicone, Mica, Butylene Glycol, Cyclohexasiloxane, Dimethicone Crosspolymer, Glycerin, Trimethylsiloxysilicate, Propanediol, Polypropylsilsesquioxane, Isododecane, Parfum , Sodium Chloride, Dimethicone/PEG-10/15 Crosspolymer, 1,2-Hexanediol, PEG-9 Polydimethylsiloxyethyl Dimethicone, Phenoxyethanol, Dimethicone/Vinyl Dimethicone Crosspolymer, Sodium Hyaluronate, Caprylhydroxamic Acid, Disodium EDTA, Dipropylene Glycol, Tocopherol</p>\n<p><strong>MRP</strong><span>: Rs. 499 (incl. all taxes)</span><br><br><strong>Country of Origin</strong><span>: </span><span mce-data-marked="1">PRC</span><br><br><strong>Company Name</strong><span>:</span><span> SUGAR Cosmetics LLC</span><br><br><strong>Company Address</strong><span>:</span><span> 8 The Green Suite A, Dover, DE 19901, USA</span></p>',
              vendor: "SUGAR Cosmetics",
              product_type: "Moisturizer",
              created_at: "2019-11-20T15:07:57+05:30",
              handle: "aquaholic-priming-moisturizer",
              updated_at: "2020-12-31T14:26:25+05:30",
              published_at: "2019-11-20T16:20:08+05:30",
              template_suffix: "",
              published_scope: "web",
              tags:
                "2 in 1, 699, Alcohol free, Cream, Cruelty free, Essentials, Fresh, Gluten free, Hyaluronic Acid, Hydrate, Hydrating, Lightweight, Makeup, Matte, Matte Finish, Moisturise, Moisturiser, Moisturizer, Moringa Seed, Non Greasy, Non Sticky, Nourish, Paraben free, Prime, Primer, Priming Moisturizer, Refreshing, Retains Moisture, Skin, Skincare, Soft, st_bestseller:3, st_concern: Hydration, st_popularityscore: 9002, st_range: Aquaholic, sugar_type_0, Sulphate free, Summers, Supple, Under 700, Vegan, Water, Water Base",
              admin_graphql_api_id: "gid://shopify/Product/4352967049299",
              variants: [
                {
                  id: 31194979729491,
                  product_id: 4352967049299,
                  title: "Default Title",
                  price: "499.00",
                  sku: "8904320701246",
                  position: 1,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "Default Title",
                  option2: null,
                  option3: null,
                  created_at: "2019-11-20T15:07:57+05:30",
                  updated_at: "2020-12-31T14:26:25+05:30",
                  taxable: false,
                  barcode: "",
                  grams: 30,
                  image_id: null,
                  weight: 30,
                  weight_unit: "g",
                  inventory_item_id: 32716419563603,
                  inventory_quantity: 2743,
                  old_inventory_quantity: 2743,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/31194979729491",
                },
              ],
              options: [
                {
                  id: 5651258605651,
                  product_id: 4352967049299,
                  name: "Title",
                  position: 1,
                  values: ["Default Title"],
                },
              ],
              images: [
                {
                  id: 13736578809939,
                  product_id: 4352967049299,
                  position: 1,
                  created_at: "2019-12-25T15:24:37+05:30",
                  updated_at: "2020-04-22T12:58:25+05:30",
                  alt: "SUGAR Cosmetics Aquaholic Priming Moisturizer",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-aquaholic-priming-moisturizer-13548826886227.jpg?v=1587540505",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13736578809939",
                },
                {
                  id: 14280392573011,
                  product_id: 4352967049299,
                  position: 2,
                  created_at: "2020-04-22T12:58:25+05:30",
                  updated_at: "2020-04-22T12:58:25+05:30",
                  alt: "SUGAR Cosmetics Aquaholic Priming Moisturizer",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-aquaholic-priming-moisturizer-14280379498579.jpg?v=1587540505",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14280392573011",
                },
                {
                  id: 13736579629139,
                  product_id: 4352967049299,
                  position: 3,
                  created_at: "2019-12-25T15:24:55+05:30",
                  updated_at: "2020-04-22T12:58:25+05:30",
                  alt: "SUGAR Cosmetics Aquaholic Priming Moisturizer",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-aquaholic-priming-moisturizer-13548826918995.jpg?v=1587540505",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13736579629139",
                },
                {
                  id: 13736582643795,
                  product_id: 4352967049299,
                  position: 4,
                  created_at: "2019-12-25T15:26:20+05:30",
                  updated_at: "2020-04-22T12:58:25+05:30",
                  alt: "SUGAR Cosmetics Aquaholic Priming Moisturizer",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-aquaholic-priming-moisturizer-13548905332819.jpg?v=1587540505",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13736582643795",
                },
              ],
              image: {
                id: 13736578809939,
                product_id: 4352967049299,
                position: 1,
                created_at: "2019-12-25T15:24:37+05:30",
                updated_at: "2020-04-22T12:58:25+05:30",
                alt: "SUGAR Cosmetics Aquaholic Priming Moisturizer",
                width: 1025,
                height: 1400,
                src:
                  "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-aquaholic-priming-moisturizer-13548826886227.jpg?v=1587540505",
                variant_ids: [],
                admin_graphql_api_id:
                  "gid://shopify/ProductImage/13736578809939",
              },
            },
            mediaText: null,
            redirectUrl: null,
          },
          {
            id: 149,
            sectionId: 16,
            webViewTitle: null,
            contentType: 3,
            mediaUrl: null,
            product_json: {
              id: 4345622954067,
              title: "Power Clay Peel Off Mask",
              body_html:
                '<div class="col-md-12 video" style="text-align: left;">It\'s about time you pick your play as <strong>SUGAR Power Clay Peel Off Mask</strong> makes its entry into the sensational SUGAR tribe. </div>\n<p>This mask delivers cleansing, oil-absorbing and skin-toning perks<strong>.</strong><strong> </strong>Packed with mineral-rich Kaolin Clay that acts as a purifier and exfoliator to yield smoother skin, this easy-to-peel off mask gives your skin a healthy glow in short time.<br> <br> <strong>Benefits: SUGAR Power Clay Peel Off Mask</strong> contains activated carbon that absorbs excess sebum, deep cleans the skin plus removes blackheads. Kaolin Clay also rejuvenates the cells, improves blood circulation and removes toxins from skin. It is vegan, suits all skin types and can be used by men and women.<br> <br> <strong>How to Use: </strong>Apply an even layer to clean, dry face. Leave on for 15-20 minutes until dry. Gently peel off mask from outer edges. Rinse with warm water. Use twice or thrice a week for a naturally glowing skin. Avoid contact with eyes.<br> <br> <strong>Net Volume</strong>: 50 ml<br> <br> <strong>List of Ingredients: </strong>Charcoal Powder, Silicic acid, Lithium magnesium sodium salt, Cyclopentasiloxane, Cyclotetrasiloxane, Phenoxyethanol, Bis-PEG/PPG-20/5 PEG/PPG-20/5 Dimethicone, Caprylic/Capric Triglyceride, Methoxy PEG/PPG- 25/4 Dimethicone, Pentylene Glycol, Chlorophenesin, Acrylates/C10-30, Alkyl Acrylate Crosspolymer, Ethylhexylglycerin, Glyceryl Caprylate, Xanthan Gum, Dipotassium Glycerrhizate, Hectorite, Disodium EDTA, Fragrance.</p>\n<p><strong>MRP</strong>: Rs. 499 (incl. all taxes)<br><br><strong>Country of Origin</strong>: Taiwan<br><br><strong>Company Name</strong>: SUGAR Cosmetics LLC<br><br><strong>Company Address</strong>: 8 The Green Suite A, Dover, DE 19901, USA.<br></p>',
              vendor: "SUGAR Cosmetics",
              product_type: "Face Mask",
              created_at: "2019-11-11T18:52:42+05:30",
              handle: "power-clay-peel-of-mask",
              updated_at: "2020-12-31T00:31:06+05:30",
              published_at: "2019-11-12T15:38:07+05:30",
              template_suffix: "",
              published_scope: "web",
              tags:
                "15 mins, 20 mins, 599, Absorbs Oil, Activated carbon, Alcohol free, All skin types, Calms Skin, Clay Mask, Cleansing, Deep Cleaning, Essentials, Exfoliates, Face, Face mask, Glow, Healthy Skin, Improves blood circulation, Kaolin Clay, Mask, Men, Paraben free, Peel Off Mask, Purifies, Removes Blackhead, Removes toxin, Sebum, Skin, Skin Care, Skin conditioner, Skin Smoothening, Skin toning, Soften Skin, Soothe Damaged Skin, st_ concern: Acne, st_ concern: Oil control, st_range: Power Clay, sugar_type_0, Sulphate free, Taiwan, Travel Friendly, Under 1000, Under 600, Vegan, Women",
              admin_graphql_api_id: "gid://shopify/Product/4345622954067",
              variants: [
                {
                  id: 31135843713107,
                  product_id: 4345622954067,
                  title: "Default Title",
                  price: "499.00",
                  sku: "8904320702045",
                  position: 1,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "Default Title",
                  option2: null,
                  option3: null,
                  created_at: "2019-11-11T18:52:42+05:30",
                  updated_at: "2020-12-31T00:31:06+05:30",
                  taxable: false,
                  barcode: "",
                  grams: 50,
                  image_id: null,
                  weight: 50,
                  weight_unit: "g",
                  inventory_item_id: 32648603631699,
                  inventory_quantity: 313,
                  old_inventory_quantity: 313,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/31135843713107",
                },
              ],
              options: [
                {
                  id: 5641717612627,
                  product_id: 4345622954067,
                  name: "Title",
                  position: 1,
                  values: ["Default Title"],
                },
              ],
              images: [
                {
                  id: 13732137533523,
                  product_id: 4345622954067,
                  position: 1,
                  created_at: "2019-12-24T15:26:35+05:30",
                  updated_at: "2020-04-08T14:03:27+05:30",
                  alt: null,
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-power-clay-peel-off-mask-13470537646163.jpg?v=1586334807",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13732137533523",
                },
                {
                  id: 14227221872723,
                  product_id: 4345622954067,
                  position: 2,
                  created_at: "2020-04-08T14:03:27+05:30",
                  updated_at: "2020-04-08T14:03:27+05:30",
                  alt: "SUGAR Cosmetics Power Clay Peel Off Mask",
                  width: 540,
                  height: 738,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-power-clay-peel-off-mask-14227218169939.jpg?v=1586334807",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14227221872723",
                },
                {
                  id: 13730192523347,
                  product_id: 4345622954067,
                  position: 3,
                  created_at: "2019-12-24T06:25:51+05:30",
                  updated_at: "2020-04-08T14:03:27+05:30",
                  alt: "SUGAR Cosmetics Power Clay Peel Off Mask",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-power-clay-peel-off-mask-13470290346067.jpg?v=1586334807",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13730192523347",
                },
                {
                  id: 13730192031827,
                  product_id: 4345622954067,
                  position: 4,
                  created_at: "2019-12-24T06:25:34+05:30",
                  updated_at: "2020-04-08T14:03:27+05:30",
                  alt: "SUGAR Cosmetics Power Clay Peel Off Mask",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-power-clay-peel-off-mask-13470290378835.jpg?v=1586334807",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13730192031827",
                },
              ],
              image: {
                id: 13732137533523,
                product_id: 4345622954067,
                position: 1,
                created_at: "2019-12-24T15:26:35+05:30",
                updated_at: "2020-04-08T14:03:27+05:30",
                alt: null,
                width: 1025,
                height: 1400,
                src:
                  "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-power-clay-peel-off-mask-13470537646163.jpg?v=1586334807",
                variant_ids: [],
                admin_graphql_api_id:
                  "gid://shopify/ProductImage/13732137533523",
              },
            },
            mediaText: null,
            redirectUrl: null,
          },
          {
            id: 150,
            sectionId: 16,
            webViewTitle: null,
            contentType: 3,
            mediaUrl: null,
            product_json: {
              id: 3939315941459,
              title: "Charcoal Patrol Face Mask",
              body_html:
                "It’s time to put your hands together for none other than the luxurious, the lavish, the grand SUGAR Sheet Mask. Packed individually with enriched formulas that help detoxify, cleanse, hydrate, vitalize &amp; brighten your skin; this serum-soaked mask is the quick fix you need at any downtime. Be it a work night, weekend, before stepping out or when you are watching TV, reading a book or even when you’re doing your hair, this is a great way to enjoy the ultimate at-home spa experience.\n<p><br><span><strong>Benefits</strong>: SUGAR Charcoal Patrol Face Mask is a multi-tasker that detoxifies, reduces pores, smoothens, vitalizes, brightens &amp; tones up skin. Enriched with charcoal powder to remove excess sebum, green tea to reduce UV damage, hyaluronic acid to amp up hydration, glow &amp; your skin’s elasticity and coffee extract for deep exfoliation and hydration; this mask is a must-have.<br></span><br><span><strong>How To Use</strong>: Wash your face and use a skin toner. Remove the mask from the pouch and gently unfold. Separate the mask from the protecting plastic and carefully place over the eye area first and then smoothen out across the rest of your face. Tap the excess serum into your skin for better absorption. Keep the mask on for 15-20 minutes. Gently remove and tap the excess serum into your skin for better absorption.<br></span><br><span><strong>Net Weight</strong>: 24g<br></span><br><span><strong>List Of Ingredients</strong>: Water, Glycerin, Butylene Glycol, Dipropylene Glycol, Phenoxyethanol, PEG-40 Hydrogenated Castor Oil, Trideceth-9,Chlorphenesin, Aloe Barbadensis Leaf Water, Hydroxyethylcellulose, Carbomer, Tromethamine, Allantoin, Fragrance, Diospyros Kaki LeafExtract, Vitis Vinifera (Grape) Fruit Extract, Camellia Sinensis Leaf Extract, Castanea Crenata (Chestnut) Shell Extract, Zanthoxylum Schinifolium Leaf Extract, Coffea Arabica (Coffee) Seed Extract, Polygonum Cuspidatum Extract, Carthamus Tinctorius (Saower) FlowerExtract, Lavandula Angustifolia (Lavender) Extract, Salvia Sclarea (Clary) Extract, Hyacinthus Orientalis (Hyacinth) Extract, ChamomillaRecutita (Matricaria) Flower/Leaf Extract, Borago Ocinalis Extract, Centaurea Cyanus Flower Extract, 1,2-Hexanediol, Charcoal Powder,Sodium Hyaluronate, Ethylhexylglycerin</span></p>\n<p><span><strong>MRP</strong>: Rs. 99 (incl. all taxes)<br><br><strong>Country of Origin</strong>: Korea<br><br><strong>Company Name</strong>: SUGAR Cosmetics LLC<br><br><strong>Company Address</strong>: 8 The Green Suite A, Dover, DE 19901, USA</span></p>",
              vendor: "SUGAR Cosmetics",
              product_type: "Face Mask",
              created_at: "2019-08-20T09:32:39+05:30",
              handle: "charcoal-patrol-face-mask",
              updated_at: "2020-12-31T14:30:14+05:30",
              published_at: "2019-08-20T12:21:23+05:30",
              template_suffix: "",
              published_scope: "web",
              tags:
                "149, Anti ageing, Brightens skin, Calms skin, Deep clean, Deep Exfoliation, Detoxify Skin, Essentials, Face, Face mask, Fade blemishes, Heals Inflammation, Healthy glow, Improves skin texture, K Beauty, Korean Beauty, Mask, Minimize enlarged pores, Minimize wrinkle, Oil free look, Reduce hyperpigmentation, Reduces UV damage, Rejuvenates skin, Remove impurities, Removes Impurities, Sheet Mask, Skin, Soften skin, Soothe damaged skin, st_ concern: Oil control, st_ concern: Sun protection, st_bestseller:4, st_concern: Anti-ageing, st_popularityscore: 9003, sugar_type_0",
              admin_graphql_api_id: "gid://shopify/Product/3939315941459",
              variants: [
                {
                  id: 29463880302675,
                  product_id: 3939315941459,
                  title: "Default Title",
                  price: "99.00",
                  sku: "8904320701093",
                  position: 1,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "Default Title",
                  option2: null,
                  option3: null,
                  created_at: "2019-08-20T09:32:39+05:30",
                  updated_at: "2020-12-31T14:27:25+05:30",
                  taxable: false,
                  barcode: "",
                  grams: 24,
                  image_id: null,
                  weight: 24,
                  weight_unit: "g",
                  inventory_item_id: 30622839111763,
                  inventory_quantity: 3614,
                  old_inventory_quantity: 3614,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/29463880302675",
                },
              ],
              options: [
                {
                  id: 5144449417299,
                  product_id: 3939315941459,
                  name: "Title",
                  position: 1,
                  values: ["Default Title"],
                },
              ],
              images: [
                {
                  id: 13735855587411,
                  product_id: 3939315941459,
                  position: 1,
                  created_at: "2019-12-25T11:38:53+05:30",
                  updated_at: "2020-04-14T12:41:15+05:30",
                  alt: "SUGAR Cosmetics Charcoal Patrol Face Mask",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-charcoal-patrol-face-mask-12775752564819.jpg?v=1586848275",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13735855587411",
                },
                {
                  id: 14247392280659,
                  product_id: 3939315941459,
                  position: 2,
                  created_at: "2020-04-14T12:41:14+05:30",
                  updated_at: "2020-04-14T12:41:15+05:30",
                  alt: "SUGAR Cosmetics Charcoal Patrol Face Mask",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-charcoal-patrol-face-mask-14247388381267.jpg?v=1586848275",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14247392280659",
                },
                {
                  id: 13735858470995,
                  product_id: 3939315941459,
                  position: 3,
                  created_at: "2019-12-25T11:40:19+05:30",
                  updated_at: "2020-04-14T12:41:15+05:30",
                  alt: "SUGAR Cosmetics Charcoal Patrol Face Mask",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-charcoal-patrol-face-mask-12775752958035.jpg?v=1586848275",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13735858470995",
                },
                {
                  id: 13735859028051,
                  product_id: 3939315941459,
                  position: 4,
                  created_at: "2019-12-25T11:40:36+05:30",
                  updated_at: "2020-04-14T12:41:15+05:30",
                  alt: "SUGAR Cosmetics Charcoal Patrol Face Mask",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-charcoal-patrol-face-mask-12775752990803.jpg?v=1586848275",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13735859028051",
                },
                {
                  id: 13728649510995,
                  product_id: 3939315941459,
                  position: 5,
                  created_at: "2019-12-23T16:43:51+05:30",
                  updated_at: "2020-04-14T12:41:15+05:30",
                  alt: "SUGAR Cosmetics Charcoal Patrol Face Mask",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-charcoal-patrol-face-mask-12775753318483.jpg?v=1586848275",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13728649510995",
                },
                {
                  id: 13728648822867,
                  product_id: 3939315941459,
                  position: 6,
                  created_at: "2019-12-23T16:43:33+05:30",
                  updated_at: "2020-04-14T12:41:15+05:30",
                  alt: "SUGAR Cosmetics Charcoal Patrol Face Mask",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-charcoal-patrol-face-mask-12775753580627.jpg?v=1586848275",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13728648822867",
                },
              ],
              image: {
                id: 13735855587411,
                product_id: 3939315941459,
                position: 1,
                created_at: "2019-12-25T11:38:53+05:30",
                updated_at: "2020-04-14T12:41:15+05:30",
                alt: "SUGAR Cosmetics Charcoal Patrol Face Mask",
                width: 1025,
                height: 1400,
                src:
                  "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-charcoal-patrol-face-mask-12775752564819.jpg?v=1586848275",
                variant_ids: [],
                admin_graphql_api_id:
                  "gid://shopify/ProductImage/13735855587411",
              },
            },
            mediaText: null,
            redirectUrl: null,
          },
          {
            id: 151,
            sectionId: 16,
            webViewTitle: null,
            contentType: 3,
            mediaUrl: null,
            product_json: {
              id: 3939316695123,
              title: "Cheat Sheet Clarifying Mask",
              body_html:
                "It’s time to put your hands together for none other than the luxurious, the lavish, the grand SUGAR Sheet Mask. Packed individually with enriched formulas that help detoxify, cleanse, hydrate, vitalize &amp; brighten your skin; this serum-soaked mask is the quick fix you need at any downtime. Be it a work night, weekend, before stepping out or when you are watching TV, reading a book or even when you’re doing your hair, this is a great way to enjoy the ultimate at-home spa experience.\n<p><br><span><strong>Benefits</strong>: SUGAR Cheat Sheet Clarifying Mask controls oils, unclogs pores, controls blemishes, clarifies &amp; purifies the skin. Supplemented with rose water to maintain skin’s pH, tea tree extract to unclog pores &amp; salicylic acid that helps stop breakouts, this mask is just the thing! Also enriched with Centella Asiatica to boost collagen &amp; help calm inflammation plus witch hazel extract, rosemary &amp; lavender extract as a superior disinfectant for skin.<br></span><br><span><strong>How To Use</strong>: Wash your face and use a skin toner. Remove the mask from the pouch and gently unfold. Separate the mask from the protecting plastic and carefully place over the eye area first and then smoothen out across the rest of your face. Keep the mask on for 15-20 minutes. Gently remove and tap the excess serum into your skin for better absorption.</span><br><span><br><strong>Net Weight</strong>: 24g<br></span><br><span><strong>List Of Ingredients</strong>: Water, Glycerin, Butylene Glycol, Dipropylene Glycol, Phenoxyethanol, PEG-40 Hydrogenated Castor Oil, Trideceth-9,Chlorphenesin, Hamamelis Virginiana (Witch Hazel) Extract, Hydroxyethylcellulose, Carbomer, Tromethamine, Allantoin, Fragrance,Rose Extract, Wine Extract, Sucrose, 1,2-Hexanediol, Salicylic Acid, Melaleuca Alternifolia (Tea Tree) Leaf Oil, Mineral Salts,Ethylhexylglycerin, Centella Asiatica Extract, Houttuynia Cordata Extract, Glycine Soja (Soybean) Seed Extract, Pisum Sativum (Pea)Extract, Salvia Ocinalis (Sage) Extract, Lavandula Angustifolia (Lavender) Extract, Chamomilla Recutita (Matricaria) Extract,Rosmarinus Ocinalis (Rosemary) Extract, Cymbopogon Schoenanthus Extract</span></p>\n<p><span><strong>MRP</strong>: Rs. 99 (incl. all taxes)<br><br><strong>Country of Origin</strong>: Korea<br><br><strong>Company Name</strong>: SUGAR Cosmetics LLC<br><br><strong>Company Address</strong>: 8 The Green Suite A, Dover, DE 19901, USA</span></p>",
              vendor: "SUGAR Cosmetics",
              product_type: "Face Mask",
              created_at: "2019-08-20T09:34:25+05:30",
              handle: "cheat-sheet-clarifying-mask",
              updated_at: "2020-12-31T14:27:41+05:30",
              published_at: "2019-08-20T12:21:14+05:30",
              template_suffix: "",
              published_scope: "web",
              tags:
                "Absorbs Oil, Anti-Inflammatory, Boost skin collagen, Brightens skin, Centella Asiatica Extract, Chamomile Extract, Clarifies SKin, Clarifying, Cleanse Skin, Control Blemishes, Control Oils, Correct unevenness, Detoxify Skin, Essentials, Face mask, Hazel Extract, Hydrate Skin, K Beauty, Korean Beauty, Lavender Extract, Mask, Purifies Skin, Rose Water, Rosemary Extract, Salicylic Acid, Sebum Control, Sheet Mask, Skin care, Skin Exfoliator, st_ concern: Acne, st_ concern: Oil control, st_bestseller:16, st_concern: Brightening, st_concern: Hydration, st_popularityscore: 9015, Stop breakouts, sugar_type_0, Tea Tree Extract, Unclog Pores, Vitalize Skin",
              admin_graphql_api_id: "gid://shopify/Product/3939316695123",
              variants: [
                {
                  id: 29463891017811,
                  product_id: 3939316695123,
                  title: "Default Title",
                  price: "99.00",
                  sku: "8904320701079",
                  position: 1,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "Default Title",
                  option2: null,
                  option3: null,
                  created_at: "2019-08-20T09:34:25+05:30",
                  updated_at: "2020-12-31T14:27:41+05:30",
                  taxable: false,
                  barcode: "",
                  grams: 24,
                  image_id: null,
                  weight: 24,
                  weight_unit: "g",
                  inventory_item_id: 30622851137619,
                  inventory_quantity: 2525,
                  old_inventory_quantity: 2525,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/29463891017811",
                },
              ],
              options: [
                {
                  id: 5144450531411,
                  product_id: 3939316695123,
                  name: "Title",
                  position: 1,
                  values: ["Default Title"],
                },
              ],
              images: [
                {
                  id: 13735767834707,
                  product_id: 3939316695123,
                  position: 1,
                  created_at: "2019-12-25T11:06:37+05:30",
                  updated_at: "2020-04-14T12:43:13+05:30",
                  alt: "SUGAR Cosmetics Cheat Sheet Clarifying Mask",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-cheat-sheet-clarifying-mask-12775754203219.jpg?v=1586848393",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13735767834707",
                },
                {
                  id: 14247395098707,
                  product_id: 3939316695123,
                  position: 2,
                  created_at: "2020-04-14T12:43:12+05:30",
                  updated_at: "2020-04-14T12:43:13+05:30",
                  alt: "SUGAR Cosmetics Cheat Sheet Clarifying Mask",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-cheat-sheet-clarifying-mask-14247391920211.jpg?v=1586848393",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14247395098707",
                },
                {
                  id: 13735774290003,
                  product_id: 3939316695123,
                  position: 3,
                  created_at: "2019-12-25T11:08:27+05:30",
                  updated_at: "2020-04-14T12:43:13+05:30",
                  alt: "SUGAR Cosmetics Cheat Sheet Clarifying Mask",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-cheat-sheet-clarifying-mask-12775754367059.jpg?v=1586848393",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13735774290003",
                },
                {
                  id: 13735774879827,
                  product_id: 3939316695123,
                  position: 4,
                  created_at: "2019-12-25T11:08:42+05:30",
                  updated_at: "2020-04-14T12:43:13+05:30",
                  alt: "SUGAR Cosmetics Cheat Sheet Clarifying Mask",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-cheat-sheet-clarifying-mask-12775754465363.jpg?v=1586848393",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13735774879827",
                },
                {
                  id: 13728291553363,
                  product_id: 3939316695123,
                  position: 5,
                  created_at: "2019-12-23T15:01:54+05:30",
                  updated_at: "2020-04-14T12:43:13+05:30",
                  alt: "SUGAR Cosmetics Cheat Sheet Clarifying Mask",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-cheat-sheet-clarifying-mask-12775754498131.jpg?v=1586848393",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13728291553363",
                },
                {
                  id: 13728291029075,
                  product_id: 3939316695123,
                  position: 6,
                  created_at: "2019-12-23T15:01:37+05:30",
                  updated_at: "2020-04-14T12:43:13+05:30",
                  alt: "SUGAR Cosmetics Cheat Sheet Clarifying Mask",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-cheat-sheet-clarifying-mask-12775755448403.jpg?v=1586848393",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13728291029075",
                },
              ],
              image: {
                id: 13735767834707,
                product_id: 3939316695123,
                position: 1,
                created_at: "2019-12-25T11:06:37+05:30",
                updated_at: "2020-04-14T12:43:13+05:30",
                alt: "SUGAR Cosmetics Cheat Sheet Clarifying Mask",
                width: 1025,
                height: 1400,
                src:
                  "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-cheat-sheet-clarifying-mask-12775754203219.jpg?v=1586848393",
                variant_ids: [],
                admin_graphql_api_id:
                  "gid://shopify/ProductImage/13735767834707",
              },
            },
            mediaText: null,
            redirectUrl: null,
          },
          {
            id: 152,
            sectionId: 16,
            webViewTitle: null,
            contentType: 3,
            mediaUrl: null,
            product_json: {
              id: 4362664345683,
              title: "Bling Leader Illuminating Moisturizer",
              body_html:
                '<p><span style="font-weight: 400;" data-mce-fragment="1" data-mce-style="font-weight: 400;">A hydrating and lightweight moisturizer that gives your face the perfect glow. Enriched with Vitamin E and Shea Butter, </span><span style="font-weight: 400;" data-mce-fragment="1" data-mce-style="font-weight: 400;">SUGAR Bling Leader Illuminating Moisturizer</span><span style="font-weight: 400;" data-mce-fragment="1" data-mce-style="font-weight: 400;"> helps protect your skin against pollution and leaves it soft and smooth. This </span><span style="font-weight: 400;" data-mce-fragment="1" data-mce-style="font-weight: 400;">best buy illuminating moisturizer</span><span style="font-weight: 400;" data-mce-fragment="1" data-mce-style="font-weight: 400;"> is also transfer-proof and non-sticky.</span></p>\n<ul>\n<li>\n<strong><a href="https://in.sugarcosmetics.com/products/bling-leader-illuminating-moisturizer-01-gold-diggin-warm-gold-with-a-pearl-finish" target="_blank">01 Gold Diggin\'</a></strong> <span style="font-weight: 400;" data-mce-fragment="1" data-mce-style="font-weight: 400;">(Warm Gold with a pearl finish) – Keeps you and your persona, blinding!<br><br></span>\n</li>\n<li>\n<strong><a href="https://in.sugarcosmetics.com/products/bling-leader-illuminating-moisturizer-02-pink-trippin-cool-pink-with-a-pearl-finish" target="_blank">02 Pink Trippin\'</a></strong> <span style="font-weight: 400;" data-mce-fragment="1" data-mce-style="font-weight: 400;">(Cool Pink with a pearl finish) – Brings out the flirty and fun side of you.<br><br></span>\n</li>\n<li>\n<strong><a href="https://in.sugarcosmetics.com/products/bling-leader-illuminating-moisturizer-03-peach-poppin-warm-peach-with-a-pearl-finish" target="_blank">03 Peach Poppin\'</a></strong> <span style="font-weight: 400;" data-mce-fragment="1" data-mce-style="font-weight: 400;">(Warm Peach with a pearl finish) – For the diva in you.<br><br></span>\n</li>\n</ul>\n<p><span style="font-weight: 400;" data-mce-fragment="1" data-mce-style="font-weight: 400;"><strong>Additional details</strong>: </span><span style="font-weight: 400;" data-mce-fragment="1" data-mce-style="font-weight: 400;">The original SUGAR Bling Leader Illuminating Moisturizer </span><span style="font-weight: 400;" data-mce-fragment="1" data-mce-style="font-weight: 400;">is dermatologically tested and approved. Paraben-free and cruelty-free.</span></p>\n<p><span style="font-weight: 400;" data-mce-fragment="1" data-mce-style="font-weight: 400;"><strong>Net Volume</strong>: 25 ml</span></p>\n<p><span style="font-weight: 400;" data-mce-fragment="1" data-mce-style="font-weight: 400;"><strong>Bling Leader Illuminating Moisturizer ingredients</strong>: CYCLOPENTASILOXANE, DIMETHICONE CROSSPOLYMER, SILICA, MICA, METHYL METHACRYLATE CROSSPOLYMER, DIMETHICONE, ISONONYL ISONONANOATE, TOCOPHERYL ACETATE, CAPRYLYL GLYCOL, ETHYLHEXYLGLYCERIN, CI 77891, CI 77492, CI 77491, CI 77499</span></p>\n<p><span style="font-weight: 400;" data-mce-fragment="1" data-mce-style="font-weight: 400;"><strong>MRP</strong>: 499 (incl. all taxes)<br><br><strong>Country of Origin</strong>: PRC<br><br><strong>Company Name</strong>: SUGAR Cosmetics LLC.<br><br><strong>Company Address</strong>: 8 The Green Suite A, Dover, DE 19901, USA.</span></p>\n<h5>How to apply</h5>\n<p><strong><span style="font-weight: 400;" data-mce-style="font-weight: 400;">Apply SUGAR </span><span style="font-weight: 400;" data-mce-style="font-weight: 400;">Bling Leader Illuminating Moisturizer </span><span style="font-weight: 400;" data-mce-style="font-weight: 400;">directly to your skin for a radiant glow. Or mix it with moisturizer or foundation for a subtle glow. It can also be used as a highlighter. </span><span style="font-weight: 400;" data-mce-style="font-weight: 400;">Perfect for both workwear and a party look.</span></strong></p>\n<p><strong><span style="font-weight: 400;" data-mce-style="font-weight: 400;"><strong>Best paired with</strong>:</span></strong></p>\n<p><strong><span style="font-weight: 400;" data-mce-style="font-weight: 400;">Use this illuminating moisturizer as your <strong><a href="https://in.sugarcosmetics.com/products/face-fwd-highlighter-stick" data-mce-href="https://in.sugarcosmetics.com/products/face-fwd-highlighter-stick">highlighter for your face </a></strong>or under <strong><a href="https://in.sugarcosmetics.com/collections/foundation/products/ace-of-face-foundation-stick" data-mce-href="https://in.sugarcosmetics.com/collections/foundation/products/ace-of-face-foundation-stick">stick foundation </a></strong>to give your face a natural glow!</span></strong></p>\n<h5>Benefits</h5>\n<p>SUGAR Bling Leader Illuminating Moisturizer is powered by:<br> <br>- Vitamin E<br>(Protects skin from damage) <br>Learn more about why Vitamin E is an essential ingredient in your cosmetic product. <br> <br>- Shea butter<br>(Tones skin)<br> <br>- Iridescent particles<br>(Add instant glow)<br></p>\n<h5>Commonly asked questions</h5>\n<p>Q. Is it transfer-proof?<br>A. Yes, it doesn’t transfer and is non-sticky.<br> <br>Q. Can it be used as a highlighter?<br>A. Yes, you can use it as a highlighter as it gives an instant glow.<br> <br>Q. What’s the net volume?<br>A. It’s 25ml.<br> <br>Q. Can I mix it with my foundation?<br>A. Yes, you can mix SUGAR Bling Leader Illuminating Moisturizer with your foundation for a more natural glow.<br> <br>Q. Is it paraben-free?<br>A. It’s paraben-free, vegan and cruelty-free.<br></p>',
              vendor: "SUGAR Cosmetics",
              product_type: "Moisturizer",
              created_at: "2019-12-03T16:10:02+05:30",
              handle: "bling-leader-illuminating-moisturizer",
              updated_at: "2020-12-31T14:27:06+05:30",
              published_at: "2019-12-03T16:13:03+05:30",
              template_suffix: "",
              published_scope: "web",
              tags: "amp-hide, Moisturizer, sugar_type_1",
              admin_graphql_api_id: "gid://shopify/Product/4362664345683",
              variants: [
                {
                  id: 31256801181779,
                  product_id: 4362664345683,
                  title: "01 Gold Diggin' - warm gold with a pearl finish",
                  price: "499.00",
                  sku: "8904320704278",
                  position: 1,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "01 Gold Diggin' - warm gold with a pearl finish",
                  option2: null,
                  option3: null,
                  created_at: "2019-12-03T16:10:02+05:30",
                  updated_at: "2020-12-31T14:27:00+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13735619330131,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 32792555782227,
                  inventory_quantity: 3500,
                  old_inventory_quantity: 3500,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/31256801181779",
                },
                {
                  id: 31256801214547,
                  product_id: 4362664345683,
                  title: "02 Pink Trippin' - cool pink with a pearl finish",
                  price: "499.00",
                  sku: "8904320704285",
                  position: 2,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "02 Pink Trippin' - cool pink with a pearl finish",
                  option2: null,
                  option3: null,
                  created_at: "2019-12-03T16:10:02+05:30",
                  updated_at: "2020-12-31T14:27:06+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13728117850195,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 32792555814995,
                  inventory_quantity: 1345,
                  old_inventory_quantity: 1345,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/31256801214547",
                },
                {
                  id: 31256801247315,
                  product_id: 4362664345683,
                  title: "03 Peach Poppin' - warm peach with a pearl finish",
                  price: "499.00",
                  sku: "8904320704292",
                  position: 3,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "03 Peach Poppin' - warm peach with a pearl finish",
                  option2: null,
                  option3: null,
                  created_at: "2019-12-03T16:10:02+05:30",
                  updated_at: "2020-12-31T01:26:05+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 13728116506707,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 32792555847763,
                  inventory_quantity: 1131,
                  old_inventory_quantity: 1131,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/31256801247315",
                },
              ],
              options: [
                {
                  id: 5663618695251,
                  product_id: 4362664345683,
                  name: "Color",
                  position: 1,
                  values: [
                    "01 Gold Diggin' - warm gold with a pearl finish",
                    "02 Pink Trippin' - cool pink with a pearl finish",
                    "03 Peach Poppin' - warm peach with a pearl finish",
                  ],
                },
              ],
              images: [
                {
                  id: 13735614152787,
                  product_id: 4362664345683,
                  position: 1,
                  created_at: "2019-12-25T10:36:55+05:30",
                  updated_at: "2019-12-25T10:38:21+05:30",
                  alt: "SUGAR Cosmetics Bling Leader Illuminating Moisturizer",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-bling-leader-illuminating-moisturizer-13644982681683.jpg?v=1577250501",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13735614152787",
                },
                {
                  id: 13735619330131,
                  product_id: 4362664345683,
                  position: 2,
                  created_at: "2019-12-25T10:38:19+05:30",
                  updated_at: "2019-12-25T10:38:21+05:30",
                  alt:
                    "SUGAR Cosmetics Bling Leader Illuminating Moisturizer 01 Gold Diggin' - warm gold with a pearl finish",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-bling-leader-illuminating-moisturizer-01-gold-diggin-warm-gold-with-a-pearl-finish-13644974751827.jpg?v=1577250501",
                  variant_ids: [31256801181779],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13735619330131",
                },
                {
                  id: 13728117850195,
                  product_id: 4362664345683,
                  position: 3,
                  created_at: "2019-12-23T14:17:40+05:30",
                  updated_at: "2019-12-25T10:38:21+05:30",
                  alt:
                    "SUGAR Cosmetics Bling Leader Illuminating Moisturizer 02 Pink Trippin' - cool pink with a pearl finish",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-bling-leader-illuminating-moisturizer-02-pink-trippin-cool-pink-with-a-pearl-finish-13644977307731.jpg?v=1577250501",
                  variant_ids: [31256801214547],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13728117850195",
                },
                {
                  id: 13728116506707,
                  product_id: 4362664345683,
                  position: 4,
                  created_at: "2019-12-23T14:17:23+05:30",
                  updated_at: "2019-12-25T10:38:21+05:30",
                  alt:
                    "SUGAR Cosmetics Bling Leader Illuminating Moisturizer 03 Peach Poppin' - warm peach with a pearl finish",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-bling-leader-illuminating-moisturizer-03-peach-poppin-warm-peach-with-a-pearl-finish-13644979667027.jpg?v=1577250501",
                  variant_ids: [31256801247315],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13728116506707",
                },
              ],
              image: {
                id: 13735614152787,
                product_id: 4362664345683,
                position: 1,
                created_at: "2019-12-25T10:36:55+05:30",
                updated_at: "2019-12-25T10:38:21+05:30",
                alt: "SUGAR Cosmetics Bling Leader Illuminating Moisturizer",
                width: 1025,
                height: 1400,
                src:
                  "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-bling-leader-illuminating-moisturizer-13644982681683.jpg?v=1577250501",
                variant_ids: [],
                admin_graphql_api_id:
                  "gid://shopify/ProductImage/13735614152787",
              },
            },
            mediaText: null,
            redirectUrl: null,
          },
          {
            id: 153,
            sectionId: 16,
            webViewTitle: null,
            contentType: 3,
            mediaUrl: null,
            product_json: {
              id: 4345623183443,
              title: "Power Clay 3-Min Pore Cleansing Mask",
              body_html:
                "It's about time you pick your play as <strong>SUGAR Power Clay</strong> <strong>3-Min Pore Cleansing Mask</strong> makes its entry into the sensational SUGAR tribe. \n<p>A deep cleansing mask packed with Charcoal powder and Kaolin Clay that draws out impurities, absorbs excess oils, detoxifies skin and tightens pores. This mask is your go-to for glowing and refreshed skin.<br> <br> <strong>Benefits: SUGAR Power Clay 3-Min Pore Cleansing Mask </strong>contains Charcoal powder which deep cleans and draws impurities to the skin's surface and Kaolin Clay for effective purging of the pores by removing excess oil that can lead to breakouts. It also helps refine the appearance of acne-prone skin. It has no added fragrance and is vegan.<br> <br> <strong>How To Use: </strong>Apply<strong> </strong>a generous layer onto dry, clean skin. Let it sit for 3 minutes till it dries. Wipe off with a clean tissue or cloth. Rinse with warm water. Use twice or thrice a week for a clear and radiant skin.<br> <br> <strong>Net Volume</strong>: 50 ml<br> <br> <strong>List of Ingredients: </strong> WATER (AQUA), KAOLIN, GLYCERIN, GLYCERYL STEARATE, CHARCOAL POWDER, PHENOXYETHANOL, PENTYLENE GLYCOL, POLYSORBATE, CITRIC ACID.</p>\n<p><strong>MRP</strong><span>: Rs. 499 </span><span>(incl. all taxes)</span><br><br><strong>Country of Origin</strong><span>: PRC</span><span><br></span><br><strong>Company Name</strong><span>:</span><span> SUGAR Cosmetics LLC</span><br><br><strong>Company Address</strong><span>:</span><span> 8 The Green Suite A, Dover, DE 19901, USA</span></p>",
              vendor: "SUGAR Cosmetics",
              product_type: "Face Mask",
              created_at: "2019-11-11T18:53:34+05:30",
              handle: "power-clay-3-min-pore-cleansing-mask",
              updated_at: "2020-12-31T10:05:16+05:30",
              published_at: "2019-11-12T15:38:10+05:30",
              template_suffix: "",
              published_scope: "web",
              tags:
                "3 Minutes, 699, Absorbs oil, Adds Vitality, Alcohol Free, Anti-Aging, Calms skin, Charcoal, Charcoal Powder, Citric acid, Clay mask, Cleansing, Combat wrinkle, Deep Clean, Deep skin Hydration, Essentials, Evens skin tone, Face, Face mask, Fade blemishes, Fights acne, Fragrance free, Gluten free, Halal, Healthy looking skin, Improves skin elasticity, Increase firmness, Kaolin clay, Mask, Paraben Free, Pore cleansing, Pore tightening, Radiant skin, Rejuvenates skin, Remove impurities, Shrink pores, Skin, Skin care, Skin conditioner, Soften skin, Soothe damaged skin, st_ concern: Acne, st_ concern: Oil control, st_concern: Anti-ageing, st_concern: Brightening, st_concern: Hydration, st_range: Power Clay, sugar_type_0, Sulphate Free, Under 1000, Under 700, Vegan, Wash off, Washable, Youthful",
              admin_graphql_api_id: "gid://shopify/Product/4345623183443",
              variants: [
                {
                  id: 31135845679187,
                  product_id: 4345623183443,
                  title: "Default Title",
                  price: "499.00",
                  sku: "8904320703011",
                  position: 1,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "Default Title",
                  option2: null,
                  option3: null,
                  created_at: "2019-11-11T18:53:34+05:30",
                  updated_at: "2020-12-31T10:05:16+05:30",
                  taxable: false,
                  barcode: "",
                  grams: 50,
                  image_id: null,
                  weight: 50,
                  weight_unit: "g",
                  inventory_item_id: 32648606548051,
                  inventory_quantity: 1135,
                  old_inventory_quantity: 1135,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/31135845679187",
                },
              ],
              options: [
                {
                  id: 5641717874771,
                  product_id: 4345623183443,
                  name: "Title",
                  position: 1,
                  values: ["Default Title"],
                },
              ],
              images: [
                {
                  id: 13732141465683,
                  product_id: 4345623183443,
                  position: 1,
                  created_at: "2019-12-24T15:28:20+05:30",
                  updated_at: "2020-04-08T14:04:02+05:30",
                  alt: "SUGAR Cosmetics Power Clay 3-Min Pore Cleansing Mask",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-power-clay-3-min-pore-cleansing-mask-13470539645011.jpg?v=1586334842",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13732141465683",
                },
                {
                  id: 14227222462547,
                  product_id: 4345623183443,
                  position: 2,
                  created_at: "2020-04-08T14:04:01+05:30",
                  updated_at: "2020-04-08T14:04:02+05:30",
                  alt: "SUGAR Cosmetics Power Clay 3-Min Pore Cleansing Mask",
                  width: 540,
                  height: 738,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-power-clay-3-min-pore-cleansing-mask-14227220955219.jpg?v=1586334842",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14227222462547",
                },
                {
                  id: 13732138352723,
                  product_id: 4345623183443,
                  position: 3,
                  created_at: "2019-12-24T15:26:52+05:30",
                  updated_at: "2020-04-08T14:04:02+05:30",
                  alt: "SUGAR Cosmetics Power Clay 3-Min Pore Cleansing Mask",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-power-clay-3-min-pore-cleansing-mask-13470291394643.jpg?v=1586334842",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13732138352723",
                },
              ],
              image: {
                id: 13732141465683,
                product_id: 4345623183443,
                position: 1,
                created_at: "2019-12-24T15:28:20+05:30",
                updated_at: "2020-04-08T14:04:02+05:30",
                alt: "SUGAR Cosmetics Power Clay 3-Min Pore Cleansing Mask",
                width: 1025,
                height: 1400,
                src:
                  "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-power-clay-3-min-pore-cleansing-mask-13470539645011.jpg?v=1586334842",
                variant_ids: [],
                admin_graphql_api_id:
                  "gid://shopify/ProductImage/13732141465683",
              },
            },
            mediaText: null,
            redirectUrl: null,
          },
          {
            id: 154,
            sectionId: 16,
            webViewTitle: null,
            contentType: 3,
            mediaUrl: null,
            product_json: {
              id: 3939001172051,
              title: "Charcoal Patrol Bubble Mask",
              body_html:
                "It’s time to put your hands together for none other than the luxurious, the lavish, the grand SUGAR Sheet Mask. Packed individually with enriched formulas that help detoxify, cleanse, hydrate, vitalize &amp; brighten your skin; this serum-soaked mask is the quick fix you need at any downtime. Be it a work night, weekend, before stepping out or when you are watching TV, reading a book or even when you’re doing your hair, this is a great way to enjoy the ultimate at-home spa experience.\n<p><br><span><strong>Benefits</strong>: Enriched with lavender &amp; chamomile extract for distressed skin relief, aloe vera &amp; fruit acids for skin brightening, SUGAR Charcoal Patrol Bubble Mask is a treat for users as it deep cleanses, invigorates, purifies &amp; oxygenates the skin. The generated micro-bubbles dissolve away makeup impurities &amp; help in pore care. Infused with charcoal to exfoliate dead skin, green tea to provide UV protection &amp; grapefruit extract for detoxification, this mask is in the A-list for all.<br></span><br><span><strong>How To Use</strong>: Wash your face and use a skin toner. Unseal the mask and apply it on your face. When exposed to air, the mask begins to bubble and foam. Keep the mask on for 15-20 minutes. Remove the mask by rinsing thoroughly.<br></span><br><span><strong>Net Weight</strong>: 20g<br></span><br><span><strong>List Of Ingredients</strong>: Water/Aqua, Glycerin, Disiloxane, Polysorbate 80, Disodium Cocoamphodiacetate, Ascophyllum Nodosum Extract, Epilobium Angustifolium Flower/Leaf/Stem Extract, Butylene Glycol, Portulaca Oleracea Extract, Xanthan Gum, Chamomilla Recutita (Matricaria) Flower Extract, 1,2-Hexanediol, Zanthoxylum Piperitum Fruit Extract, Lonicera Japonica (Honeysuckle) Flower Extract, Citrus Paradisi (Grapefruit) Fruit Extract, Dimethicone, Dipotassium Glycyrrhizate, Lavandula Angustifolia (Lavender) Oil, Eucalyptus Globulus Leaf Extract, Freesia Refracta Extract, Jasminum O¬cinale (Jasmine) Flower Extract, Lavandula Angustifolia (Lavender) Extract, Lilium Candidum Flower Extract, Lilium Tigrinum Extract, Melaleuca Alternifolia (Tea tree) Extract, Mentha Rotundifolia Leaf Extract, Rosa Centifolia Flower Extract, Salvia O¬cinalis (Sage) Extract, Aloe Barbadensis Leaf Water, Camellia Sinensis Leaf Extract, Oxygen (1.3ppm), Pulsatilla Koreana Extract, Usnea Barbata (Lichen) Extract, Phenoxyethanol FREE FROM PARABEN, BENZOPHENONE, TRIETHANOLAMINE, ARTIFICIAL COLOURANT</span></p>\n<p><span><strong>MRP</strong>: Rs. 149 (incl. all taxes)<br><br><strong>Country of Origin</strong>: <span>Korea<br></span><br><strong>Company Name</strong>: SUGAR Cosmetics LLC<br><br><strong>Company Address</strong>: 8 The Green Suite A, Dover, DE 19901, USA</span></p>",
              vendor: "SUGAR Cosmetics",
              product_type: "Face Mask",
              created_at: "2019-08-19T18:27:40+05:30",
              handle: "charcoal-patrol-bubble-mask",
              updated_at: "2020-12-31T14:27:21+05:30",
              published_at: "2019-08-20T12:21:19+05:30",
              template_suffix: "",
              published_scope: "web",
              tags:
                "199, Aloe vera, Anitoxidant Rich, Antibacterial, Brightening, Bubble Mask, Calms, Chamomile extract, Charcoal, Charcoal Sheet, Cleanses, Complexion care, Dark Spot Reduction, Deep Cleansing, Detoxifying, Essentials, Exfoliates, Face mask, Fruit acids, Grapefruit Extract, Green tea, Hydrating, Invigorating, K Beauty, Korean Beauty, Lavender extract, Mask, Micro-bubbles, Moisturizing, Oxygenates, Paraben Free, Pimple Prevention, Purifies, Purifying, Relives, Revitalizing, Sebum Control, Serum, Skin brightening, Skin pH, st_ concern: Acne, st_ concern: Oil control, st_concern: Brightening, st_concern: Hydration, sugar_type_0, Tightens, Tightens Pores, UV Protection, Vegan, Vitalizing",
              admin_graphql_api_id: "gid://shopify/Product/3939001172051",
              variants: [
                {
                  id: 29462579773523,
                  product_id: 3939001172051,
                  title: "Default Title",
                  price: "149.00",
                  sku: "8904320701239",
                  position: 1,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "Default Title",
                  option2: null,
                  option3: null,
                  created_at: "2019-08-19T18:27:41+05:30",
                  updated_at: "2020-12-31T14:27:20+05:30",
                  taxable: false,
                  barcode: "",
                  grams: 20,
                  image_id: null,
                  weight: 20,
                  weight_unit: "g",
                  inventory_item_id: 30620994535507,
                  inventory_quantity: 7576,
                  old_inventory_quantity: 7576,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/29462579773523",
                },
              ],
              options: [
                {
                  id: 5144062034003,
                  product_id: 3939001172051,
                  name: "Title",
                  position: 1,
                  values: ["Default Title"],
                },
              ],
              images: [
                {
                  id: 13728626999379,
                  product_id: 3939001172051,
                  position: 1,
                  created_at: "2019-12-23T16:37:53+05:30",
                  updated_at: "2020-04-14T12:42:21+05:30",
                  alt: "SUGAR Cosmetics Charcoal Patrol Bubble Mask",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-charcoal-patrol-bubble-mask-12775750500435.jpg?v=1586848341",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13728626999379",
                },
                {
                  id: 14247393886291,
                  product_id: 3939001172051,
                  position: 2,
                  created_at: "2020-04-14T12:42:20+05:30",
                  updated_at: "2020-04-14T12:42:21+05:30",
                  alt: "SUGAR Cosmetics Charcoal Patrol Bubble Mask",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-charcoal-patrol-bubble-mask-14247387594835.jpg?v=1586848341",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14247393886291",
                },
                {
                  id: 13728625950803,
                  product_id: 3939001172051,
                  position: 3,
                  created_at: "2019-12-23T16:37:38+05:30",
                  updated_at: "2020-04-14T12:42:21+05:30",
                  alt: "SUGAR Cosmetics Charcoal Patrol Bubble Mask",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-charcoal-patrol-bubble-mask-12775751319635.jpg?v=1586848341",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13728625950803",
                },
                {
                  id: 13728625000531,
                  product_id: 3939001172051,
                  position: 4,
                  created_at: "2019-12-23T16:37:19+05:30",
                  updated_at: "2020-04-14T12:42:21+05:30",
                  alt: "SUGAR Cosmetics Charcoal Patrol Bubble Mask",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-charcoal-patrol-bubble-mask-12816912875603.jpg?v=1586848341",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13728625000531",
                },
                {
                  id: 13735842250835,
                  product_id: 3939001172051,
                  position: 5,
                  created_at: "2019-12-25T11:34:22+05:30",
                  updated_at: "2020-04-14T12:42:21+05:30",
                  alt: "SUGAR Cosmetics Charcoal Patrol Bubble Mask",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-charcoal-patrol-bubble-mask-12775752204371.jpg?v=1586848341",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13735842250835",
                },
                {
                  id: 13735842906195,
                  product_id: 3939001172051,
                  position: 6,
                  created_at: "2019-12-25T11:34:37+05:30",
                  updated_at: "2020-04-14T12:42:21+05:30",
                  alt: "SUGAR Cosmetics Charcoal Patrol Bubble Mask",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-charcoal-patrol-bubble-mask-12775753416787.jpg?v=1586848341",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13735842906195",
                },
              ],
              image: {
                id: 13728626999379,
                product_id: 3939001172051,
                position: 1,
                created_at: "2019-12-23T16:37:53+05:30",
                updated_at: "2020-04-14T12:42:21+05:30",
                alt: "SUGAR Cosmetics Charcoal Patrol Bubble Mask",
                width: 1025,
                height: 1400,
                src:
                  "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-charcoal-patrol-bubble-mask-12775750500435.jpg?v=1586848341",
                variant_ids: [],
                admin_graphql_api_id:
                  "gid://shopify/ProductImage/13728626999379",
              },
            },
            mediaText: null,
            redirectUrl: null,
          },
          {
            id: 155,
            sectionId: 16,
            webViewTitle: null,
            contentType: 3,
            mediaUrl: null,
            product_json: {
              id: 3939317579859,
              title: "Cheat Sheet Anti-Aging Mask",
              body_html:
                '<div class="col-md-12 video" style="text-align: left;">It’s time to put your hands together for none other than the luxurious, the lavish, the grand SUGAR Sheet Mask. Packed individually with enriched formulas that help detoxify, cleanse, hydrate, vitalize &amp; brighten your skin; this serum-soaked mask is the quick fix you need at any downtime. Be it a work night, weekend, before stepping out or when you are watching TV, reading a book or even when you’re doing your hair, this is a great way to enjoy the ultimate at-home spa experience.</div>\n<p><br><span><strong>Benefit</strong>: SUGAR Cheat Sheet Anti-aging Mask is carefully crafted to make sure you find the youthful glow you deserve. It improves elasticity, smoothens fine lines, revitalizes &amp; brightens skin. Enriched with sea buckthorn to aid in skin cell regeneration &amp; to boost skin’s elasticity, blueberry extract to reduce wrinkles and grapeseed oil for sun protection; this beauty is a real winner! Lemon juice, honey extract help in exfoliation &amp; adenosine, a yeast-derived ingredient energizes skin surface so it looks smoother and wrinkle-free.<br></span><br><span><strong>How To Use</strong>: Wash your face and use a skin toner. Remove the mask from the pouch and gently unfold. Separate the mask from the protecting plastic and carefully place over the eye area first and then smoothen out across the rest of your face. Keep the mask on for 15-20 minutes. Gently remove and tap the excess serum into your skin for better absorption.</span><br><span><br><strong>Net Weight</strong>: 24g<br></span><br><span><strong>List Of Ingredients</strong>: Water, Glycerin, Butylene Glycol, Dipropylene Glycol, Phenoxyethanol, PEG-40 Hydrogenated Castor Oil, Trideceth-9,Chlorphenesin, Citrus Limon (Lemon) Fruit Extract, Hydroxyethylcellulose, Carbomer, Tromethamine, Allantoin, Adenosine, Citrus Aurantium Bergamia (Bergamot) Fruit Oil, 1,2-Hexanediol, Honey extract, Royal Jelly Extract, Propolis Extract, Vitis Vinifera (Grape) Seed Oil, Sodium Hyaluronate, Hippophae Rhamnoides Water, Ethylhexylglycerin, Aristotelia Chilensis Fruit Extract, Rubus Idaeus (Raspberry) Fruit Extract, Rubus Fruticosus (Blackberry) Fruit Extract, Euterpe Oleracea Fruit Extract, Vaccinium Angustifolium (Blueberry) Fruit Extract, Vaccinium Macrocarpon (Cranberry) Fruit Extract</span></p>\n<p><span><strong>MRP</strong>: Rs. 99 (incl. all taxes)<br><br><strong>Country of Origin</strong>: Korea<br><br><strong>Company Name</strong>: SUGAR Cosmetics LLC<br><br><strong>Company Address</strong>: 8 The Green Suite A, Dover, DE 19901, USA</span></p>',
              vendor: "SUGAR Cosmetics",
              product_type: "Face Mask",
              created_at: "2019-08-20T09:35:38+05:30",
              handle: "cheat-sheet-anti-ageing-mask",
              updated_at: "2020-12-31T14:27:35+05:30",
              published_at: "2019-08-20T12:21:27+05:30",
              template_suffix: "",
              published_scope: "web",
              tags:
                "149, Adenosine Acid, Anti aging, Boosts Skin Elasticity, Brightens, Energize Skin, Essentials, Exfoliates, Face, Face mask, Honey Extract, Hyaluronic Acid, Hydrates, K Beauty, Korean beauty, Lemon Extract, Lightens Dark Circles, Mask, New Skin Cell, Nourishment, Reduces Wrinkles, Revitalizing, Rich Antioxidnat, Sea Buckthorn, Sheet Mask, Shrinks Pores, Skin, Skin care, Skin Firmness, Smoothens Fine Lines, Softens Skin, st_ concern: Anti-ageing, st_bestseller:19, st_concern: Brightening, st_concern: Hydration, st_popularityscore: 9018, sugar_type_0, Sun Protection",
              admin_graphql_api_id: "gid://shopify/Product/3939317579859",
              variants: [
                {
                  id: 29463895474259,
                  product_id: 3939317579859,
                  title: "Default Title",
                  price: "99.00",
                  sku: "8904320701086",
                  position: 1,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "Default Title",
                  option2: null,
                  option3: null,
                  created_at: "2019-08-20T09:35:38+05:30",
                  updated_at: "2020-12-31T14:27:35+05:30",
                  taxable: false,
                  barcode: "",
                  grams: 24,
                  image_id: null,
                  weight: 24,
                  weight_unit: "g",
                  inventory_item_id: 30622856904787,
                  inventory_quantity: 3569,
                  old_inventory_quantity: 3569,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/29463895474259",
                },
              ],
              options: [
                {
                  id: 5144451743827,
                  product_id: 3939317579859,
                  name: "Title",
                  position: 1,
                  values: ["Default Title"],
                },
              ],
              images: [
                {
                  id: 13728653508691,
                  product_id: 3939317579859,
                  position: 1,
                  created_at: "2019-12-23T16:45:37+05:30",
                  updated_at: "2020-04-14T12:41:49+05:30",
                  alt: "SUGAR Cosmetics Cheat Sheet Anti-Aging Mask",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-cheat-sheet-anti-aging-mask-12775753744467.jpg?v=1586848309",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13728653508691",
                },
                {
                  id: 14247392903251,
                  product_id: 3939317579859,
                  position: 2,
                  created_at: "2020-04-14T12:41:48+05:30",
                  updated_at: "2020-04-14T12:41:49+05:30",
                  alt: "SUGAR Cosmetics Cheat Sheet Anti-Aging Mask",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-cheat-sheet-anti-aging-mask-14247390085203.jpg?v=1586848309",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14247392903251",
                },
                {
                  id: 13728652820563,
                  product_id: 3939317579859,
                  position: 3,
                  created_at: "2019-12-23T16:45:20+05:30",
                  updated_at: "2020-04-14T12:41:49+05:30",
                  alt: "SUGAR Cosmetics Cheat Sheet Anti-Aging Mask",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-cheat-sheet-anti-aging-mask-12775753777235.jpg?v=1586848309",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13728652820563",
                },
                {
                  id: 13735868137555,
                  product_id: 3939317579859,
                  position: 4,
                  created_at: "2019-12-25T11:44:23+05:30",
                  updated_at: "2020-04-14T12:41:49+05:30",
                  alt: "SUGAR Cosmetics Cheat Sheet Anti-Aging Mask",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-cheat-sheet-anti-aging-mask-12775753875539.jpg?v=1586848309",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13735868137555",
                },
                {
                  id: 13735865712723,
                  product_id: 3939317579859,
                  position: 5,
                  created_at: "2019-12-25T11:42:54+05:30",
                  updated_at: "2020-04-14T12:41:49+05:30",
                  alt: "SUGAR Cosmetics Cheat Sheet Anti-Aging Mask",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-cheat-sheet-anti-aging-mask-12775753842771.jpg?v=1586848309",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13735865712723",
                },
                {
                  id: 13735868891219,
                  product_id: 3939317579859,
                  position: 6,
                  created_at: "2019-12-25T11:44:42+05:30",
                  updated_at: "2020-04-14T12:41:49+05:30",
                  alt: "SUGAR Cosmetics Cheat Sheet Anti-Aging Mask",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-cheat-sheet-anti-aging-mask-12775755022419.jpg?v=1586848309",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13735868891219",
                },
              ],
              image: {
                id: 13728653508691,
                product_id: 3939317579859,
                position: 1,
                created_at: "2019-12-23T16:45:37+05:30",
                updated_at: "2020-04-14T12:41:49+05:30",
                alt: "SUGAR Cosmetics Cheat Sheet Anti-Aging Mask",
                width: 1025,
                height: 1400,
                src:
                  "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-cheat-sheet-anti-aging-mask-12775753744467.jpg?v=1586848309",
                variant_ids: [],
                admin_graphql_api_id:
                  "gid://shopify/ProductImage/13728653508691",
              },
            },
            mediaText: null,
            redirectUrl: null,
          },
          {
            id: 156,
            sectionId: 16,
            webViewTitle: null,
            contentType: 3,
            mediaUrl: null,
            product_json: {
              id: 3939317579859,
              title: "Cheat Sheet Anti-Aging Mask",
              body_html:
                '<div class="col-md-12 video" style="text-align: left;">It’s time to put your hands together for none other than the luxurious, the lavish, the grand SUGAR Sheet Mask. Packed individually with enriched formulas that help detoxify, cleanse, hydrate, vitalize &amp; brighten your skin; this serum-soaked mask is the quick fix you need at any downtime. Be it a work night, weekend, before stepping out or when you are watching TV, reading a book or even when you’re doing your hair, this is a great way to enjoy the ultimate at-home spa experience.</div>\n<p><br><span><strong>Benefit</strong>: SUGAR Cheat Sheet Anti-aging Mask is carefully crafted to make sure you find the youthful glow you deserve. It improves elasticity, smoothens fine lines, revitalizes &amp; brightens skin. Enriched with sea buckthorn to aid in skin cell regeneration &amp; to boost skin’s elasticity, blueberry extract to reduce wrinkles and grapeseed oil for sun protection; this beauty is a real winner! Lemon juice, honey extract help in exfoliation &amp; adenosine, a yeast-derived ingredient energizes skin surface so it looks smoother and wrinkle-free.<br></span><br><span><strong>How To Use</strong>: Wash your face and use a skin toner. Remove the mask from the pouch and gently unfold. Separate the mask from the protecting plastic and carefully place over the eye area first and then smoothen out across the rest of your face. Keep the mask on for 15-20 minutes. Gently remove and tap the excess serum into your skin for better absorption.</span><br><span><br><strong>Net Weight</strong>: 24g<br></span><br><span><strong>List Of Ingredients</strong>: Water, Glycerin, Butylene Glycol, Dipropylene Glycol, Phenoxyethanol, PEG-40 Hydrogenated Castor Oil, Trideceth-9,Chlorphenesin, Citrus Limon (Lemon) Fruit Extract, Hydroxyethylcellulose, Carbomer, Tromethamine, Allantoin, Adenosine, Citrus Aurantium Bergamia (Bergamot) Fruit Oil, 1,2-Hexanediol, Honey extract, Royal Jelly Extract, Propolis Extract, Vitis Vinifera (Grape) Seed Oil, Sodium Hyaluronate, Hippophae Rhamnoides Water, Ethylhexylglycerin, Aristotelia Chilensis Fruit Extract, Rubus Idaeus (Raspberry) Fruit Extract, Rubus Fruticosus (Blackberry) Fruit Extract, Euterpe Oleracea Fruit Extract, Vaccinium Angustifolium (Blueberry) Fruit Extract, Vaccinium Macrocarpon (Cranberry) Fruit Extract</span></p>\n<p><span><strong>MRP</strong>: Rs. 99 (incl. all taxes)<br><br><strong>Country of Origin</strong>: Korea<br><br><strong>Company Name</strong>: SUGAR Cosmetics LLC<br><br><strong>Company Address</strong>: 8 The Green Suite A, Dover, DE 19901, USA</span></p>',
              vendor: "SUGAR Cosmetics",
              product_type: "Face Mask",
              created_at: "2019-08-20T09:35:38+05:30",
              handle: "cheat-sheet-anti-ageing-mask",
              updated_at: "2020-12-31T14:27:35+05:30",
              published_at: "2019-08-20T12:21:27+05:30",
              template_suffix: "",
              published_scope: "web",
              tags:
                "149, Adenosine Acid, Anti aging, Boosts Skin Elasticity, Brightens, Energize Skin, Essentials, Exfoliates, Face, Face mask, Honey Extract, Hyaluronic Acid, Hydrates, K Beauty, Korean beauty, Lemon Extract, Lightens Dark Circles, Mask, New Skin Cell, Nourishment, Reduces Wrinkles, Revitalizing, Rich Antioxidnat, Sea Buckthorn, Sheet Mask, Shrinks Pores, Skin, Skin care, Skin Firmness, Smoothens Fine Lines, Softens Skin, st_ concern: Anti-ageing, st_bestseller:19, st_concern: Brightening, st_concern: Hydration, st_popularityscore: 9018, sugar_type_0, Sun Protection",
              admin_graphql_api_id: "gid://shopify/Product/3939317579859",
              variants: [
                {
                  id: 29463895474259,
                  product_id: 3939317579859,
                  title: "Default Title",
                  price: "99.00",
                  sku: "8904320701086",
                  position: 1,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "Default Title",
                  option2: null,
                  option3: null,
                  created_at: "2019-08-20T09:35:38+05:30",
                  updated_at: "2020-12-31T14:27:35+05:30",
                  taxable: false,
                  barcode: "",
                  grams: 24,
                  image_id: null,
                  weight: 24,
                  weight_unit: "g",
                  inventory_item_id: 30622856904787,
                  inventory_quantity: 3569,
                  old_inventory_quantity: 3569,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/29463895474259",
                },
              ],
              options: [
                {
                  id: 5144451743827,
                  product_id: 3939317579859,
                  name: "Title",
                  position: 1,
                  values: ["Default Title"],
                },
              ],
              images: [
                {
                  id: 13728653508691,
                  product_id: 3939317579859,
                  position: 1,
                  created_at: "2019-12-23T16:45:37+05:30",
                  updated_at: "2020-04-14T12:41:49+05:30",
                  alt: "SUGAR Cosmetics Cheat Sheet Anti-Aging Mask",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-cheat-sheet-anti-aging-mask-12775753744467.jpg?v=1586848309",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13728653508691",
                },
                {
                  id: 14247392903251,
                  product_id: 3939317579859,
                  position: 2,
                  created_at: "2020-04-14T12:41:48+05:30",
                  updated_at: "2020-04-14T12:41:49+05:30",
                  alt: "SUGAR Cosmetics Cheat Sheet Anti-Aging Mask",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-cheat-sheet-anti-aging-mask-14247390085203.jpg?v=1586848309",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14247392903251",
                },
                {
                  id: 13728652820563,
                  product_id: 3939317579859,
                  position: 3,
                  created_at: "2019-12-23T16:45:20+05:30",
                  updated_at: "2020-04-14T12:41:49+05:30",
                  alt: "SUGAR Cosmetics Cheat Sheet Anti-Aging Mask",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-cheat-sheet-anti-aging-mask-12775753777235.jpg?v=1586848309",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13728652820563",
                },
                {
                  id: 13735868137555,
                  product_id: 3939317579859,
                  position: 4,
                  created_at: "2019-12-25T11:44:23+05:30",
                  updated_at: "2020-04-14T12:41:49+05:30",
                  alt: "SUGAR Cosmetics Cheat Sheet Anti-Aging Mask",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-cheat-sheet-anti-aging-mask-12775753875539.jpg?v=1586848309",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13735868137555",
                },
                {
                  id: 13735865712723,
                  product_id: 3939317579859,
                  position: 5,
                  created_at: "2019-12-25T11:42:54+05:30",
                  updated_at: "2020-04-14T12:41:49+05:30",
                  alt: "SUGAR Cosmetics Cheat Sheet Anti-Aging Mask",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-cheat-sheet-anti-aging-mask-12775753842771.jpg?v=1586848309",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13735865712723",
                },
                {
                  id: 13735868891219,
                  product_id: 3939317579859,
                  position: 6,
                  created_at: "2019-12-25T11:44:42+05:30",
                  updated_at: "2020-04-14T12:41:49+05:30",
                  alt: "SUGAR Cosmetics Cheat Sheet Anti-Aging Mask",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-cheat-sheet-anti-aging-mask-12775755022419.jpg?v=1586848309",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13735868891219",
                },
              ],
              image: {
                id: 13728653508691,
                product_id: 3939317579859,
                position: 1,
                created_at: "2019-12-23T16:45:37+05:30",
                updated_at: "2020-04-14T12:41:49+05:30",
                alt: "SUGAR Cosmetics Cheat Sheet Anti-Aging Mask",
                width: 1025,
                height: 1400,
                src:
                  "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-cheat-sheet-anti-aging-mask-12775753744467.jpg?v=1586848309",
                variant_ids: [],
                admin_graphql_api_id:
                  "gid://shopify/ProductImage/13728653508691",
              },
            },
            mediaText: null,
            redirectUrl: null,
          },
          {
            id: 157,
            sectionId: 16,
            webViewTitle: null,
            contentType: 3,
            mediaUrl: null,
            product_json: {
              id: 4352975765587,
              title: "Aquaholic Overnight Water Mask",
              body_html:
                '<p><strong>It </strong>soothes, replenishes skin &amp; relieves it from dehydration. It is infused with natural herb extracts to repair skin barrier functions and leave skin feeling softer.</p>\n<p><strong>How to Apply: </strong>Apply a layer of the mask onto freshly cleansed skin or post cleansing, toning &amp; moisturizing. Keep for 15-20 minutes. The product can also be applied by dotting and blending. Rinse thoroughly with lukewarm water or use a cotton pad to wipe off. Pat skin dry. Can also be applied in a layer or blended well into the skin and left overnight. </p>\n<p><strong>Benefits: SUGAR Aquaholic Overnight Water Mask </strong>has a superior sorbet texture that melts as soon as applied and instantly relieves skin from dehydration symptoms. It is enriched with herbal extracts &amp; spring water from Switzerland to deeply hydrate skin &amp; smoothen the appearance of fine lines. Daphne Feddei Extract aids in skin healing.<br><br> <strong>Additional Details: SUGAR Aquaholic range </strong>comprises of 4 hydrating and soothing products viz., <strong><a href="https://in.sugarcosmetics.com/products/aquaholic-priming-moisturizer" target="_blank" rel="noopener noreferrer">Aquaholic Priming Moisturizer</a>, <a href="https://in.sugarcosmetics.com/products/aquaholic-water-boost-mask" target="_blank" rel="noopener noreferrer">Aquaholic Water Boost Mask</a>, Aquaholic Overnight Water Mask, <a href="https://in.sugarcosmetics.com/products/aquaholic-hydrating-stick" target="_blank" rel="noopener noreferrer">Aquaholic Hydrating Stick</a>.<br><br></strong><strong></strong><strong>Net Weight</strong>: 50ml<strong><br><br> List of Ingredients: </strong> WATER, GLYCERIN, ISONONYL ISONONANOATE, PENTYLENE GLYCOL, SODIUM POLYACRYLATE, BUTYLENE GLYCOL, GLYCERYL STEARATE, PEG-100 STEARATE, SACCHARIDE ISOMERATE, SQUALANE, CAPRYLYL GLYCOL, XANTHAN GUM, DIPOTASSIUM GLYCYRRHIZATE, ASTRAGALUS MEMBRANACEUS ROOT EXTRACT, ALBIZIA JULIBRISSIN FLOWER EXTRACT, METHYL PALMITATE, SODIUM BENZOATE.</p>\n<p><strong>MRP</strong><span>: Rs. 1,099 (incl. all taxes)</span><br><br><strong>Country of Origin</strong><span>: </span><span>PRC</span><br><br><strong>Company Name</strong><span>:</span><span> SUGAR Cosmetics LLC</span><br><br><strong>Company Address</strong><span>:</span><span> 8 The Green Suite A, Dover, DE 19901, USA</span></p>',
              vendor: "SUGAR Cosmetics",
              product_type: "Water Mask",
              created_at: "2019-11-20T16:15:28+05:30",
              handle: "aquaholic-overnight-water-mask",
              updated_at: "2020-12-31T14:26:21+05:30",
              published_at: "2019-11-20T16:20:08+05:30",
              template_suffix: "",
              published_scope: "web",
              tags:
                "1299, Alcohol free, Aqua, Bedtime, Blurs Fine Lines, Cruelty Free, Daphne Feddei, Essentials, Flower Extract, Fresh, Gluten Free, Healing, Heals Skin, Healthy Skin, Herbal, Hydrate, Hydration, Mask, Moisturising, Moisturizing, Night Mask, Non Sticky, Nourishes, Overnight, Paraben Free, Quick Absorbing, Refreshing, Relax, Repairs, Replenish, Retains Moisture, Skin, Skin Care, Skin Protection, Sleeping Mask, Smooth, Soft, Soothens, Soothing, st_concern: Brightening, st_concern: Hydration, st_range: Aquaholic, sugar_type_0, Supple, Vegan, Water, Water Based, Water Mask, Water Sleeping Mask",
              admin_graphql_api_id: "gid://shopify/Product/4352975765587",
              variants: [
                {
                  id: 31195038253139,
                  product_id: 4352975765587,
                  title: "Default Title",
                  price: "1099.00",
                  sku: "8904320702120",
                  position: 1,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "Default Title",
                  option2: null,
                  option3: null,
                  created_at: "2019-11-20T16:15:28+05:30",
                  updated_at: "2020-12-31T14:26:21+05:30",
                  taxable: false,
                  barcode: "",
                  grams: 50,
                  image_id: null,
                  weight: 50,
                  weight_unit: "g",
                  inventory_item_id: 32716526878803,
                  inventory_quantity: 3700,
                  old_inventory_quantity: 3700,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/31195038253139",
                },
              ],
              options: [
                {
                  id: 5651271876691,
                  product_id: 4352975765587,
                  name: "Title",
                  position: 1,
                  values: ["Default Title"],
                },
              ],
              images: [
                {
                  id: 13727902597203,
                  product_id: 4352975765587,
                  position: 1,
                  created_at: "2019-12-23T12:35:55+05:30",
                  updated_at: "2020-04-22T12:58:59+05:30",
                  alt: "SUGAR Cosmetics Aquaholic Overnight Water Mask",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-aquaholic-overnight-water-mask-13548845236307.jpg?v=1587540539",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13727902597203",
                },
                {
                  id: 14280395227219,
                  product_id: 4352975765587,
                  position: 2,
                  created_at: "2020-04-22T12:58:59+05:30",
                  updated_at: "2020-04-22T12:58:59+05:30",
                  alt: "SUGAR Cosmetics Aquaholic Overnight Water Mask",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-aquaholic-overnight-water-mask-14280377925715.jpg?v=1587540539",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14280395227219",
                },
                {
                  id: 13727902564435,
                  product_id: 4352975765587,
                  position: 3,
                  created_at: "2019-12-23T12:35:36+05:30",
                  updated_at: "2020-04-22T12:58:59+05:30",
                  alt: "SUGAR Cosmetics Aquaholic Overnight Water Mask",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-aquaholic-overnight-water-mask-13548845269075.jpg?v=1587540539",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13727902564435",
                },
                {
                  id: 13727902203987,
                  product_id: 4352975765587,
                  position: 4,
                  created_at: "2019-12-23T12:35:20+05:30",
                  updated_at: "2020-04-22T12:58:59+05:30",
                  alt: "SUGAR Cosmetics Aquaholic Overnight Water Mask",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-aquaholic-overnight-water-mask-13548905168979.jpg?v=1587540539",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/13727902203987",
                },
              ],
              image: {
                id: 13727902597203,
                product_id: 4352975765587,
                position: 1,
                created_at: "2019-12-23T12:35:55+05:30",
                updated_at: "2020-04-22T12:58:59+05:30",
                alt: "SUGAR Cosmetics Aquaholic Overnight Water Mask",
                width: 1025,
                height: 1400,
                src:
                  "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-aquaholic-overnight-water-mask-13548845236307.jpg?v=1587540539",
                variant_ids: [],
                admin_graphql_api_id:
                  "gid://shopify/ProductImage/13727902597203",
              },
            },
            mediaText: null,
            redirectUrl: null,
          },
        ],
      },
      {
        id: 10,
        title: "TIPS AND TRICKS",
        sequence: 90,
        layoutType: 2,
        isTitleEnabled: 1,
        backgroundImg: null,
        backgroundColor: "#000000",
        text: null,
        contentType: 1,
        contentData: [
          {
            id: 138,
            sectionId: 10,
            webViewTitle: null,
            contentType: 1,
            mediaUrl:
              "https://d32baadbbpueqt.cloudfront.net/dashboard/1610023239SUGAR_Sheet_masks_for_hydrated_skin.jpg",
            product_json: null,
            mediaText: null,
            redirectUrl:
              "https://blog.sugarcosmetics.com/why-sheet-masks-are-a-must-in-your-wfh-skincare-routine/",
          },
          {
            id: 102,
            sectionId: 10,
            webViewTitle: null,
            contentType: 1,
            mediaUrl:
              "https://d32baadbbpueqt.cloudfront.net/dashboard/1610100787Tips-&-Tricks-1st-tile-banner_(1).jpg",
            product_json: null,
            mediaText: null,
            redirectUrl:
              "https://blog.sugarcosmetics.com/8-bold-lipsticks-you-must-try-this-winter/",
          },
          {
            id: 103,
            sectionId: 10,
            webViewTitle: null,
            contentType: 1,
            mediaUrl:
              "https://d32baadbbpueqt.cloudfront.net/dashboard/1610023316DIY_skincare_recipes_for_glowing_skin.jpg",
            product_json: null,
            mediaText: null,
            redirectUrl:
              "https://blog.sugarcosmetics.com/easy-homemade-skin-care-recipes-that-you-can-diy/",
          },
        ],
      },
      {
        id: 4,
        title: "JUST-IN",
        sequence: 100,
        layoutType: 3,
        isTitleEnabled: 1,
        backgroundImg: null,
        backgroundColor: "#fff9fc",
        text: null,
        contentType: 3,
        contentData: [
          {
            id: 194,
            sectionId: 4,
            webViewTitle: null,
            contentType: 3,
            mediaUrl: null,
            product_json: {
              id: 4732391817299,
              title: "Grand Finale SPF30 Setting Mist",
              body_html:
                "<p>The best of the best in makeup &amp; skincare has arrived!<br><br>Do you crave for a product that offers ultimate sun-protection whilst keeping your makeup in place all-day long? Well, we’ve got your back.<br><br>Behold, the game changer! The SUGAR Grand Finale SPF30 Setting Mist, an innovative way to wear sunscreen and the ultimate finishing touch to your beauty routine. This 2-in-1 makeup setting mist with SPF 30 is ideal for anyone who needs both long-lasting makeup &amp; great sun protection. Just a little spritz of this ultra-lightweight formula provides broad spectrum sun-protection from the damaging effects of sunrays whilst ensuring your makeup stays flawless from AM till PM!<br><br>What has us swooning over this 2-in-1 setting mist is that apart from featuring SPF 30 protection, it also packs in the goodness of ginger (zingiber officinale) root extracts that has potent antioxidant and anti-inflammatory properties and is known to hydrate skin. Moreover, it has the goodness of menthol lactate, known to offer a calming &amp; soothing effect on sun-damaged skin and helps strengthen the skin’s natural barrier. Total game changer, right?<br><br>All you need to do is simply spritz over your face, neck and décolleté, then go forth &amp; enjoy the sun, sea, or a picnic in the park, safe in the knowledge that your skin is shielded from the skin-ageing effects of harmful sunrays! What else you may ask? Well, SUGAR Grand Finale SPF30 Setting Mist comes packed in a travel-friendly size spray bottle, making it the perfect handbag must-have for easy on-the-go use.<br><br>So, flaunt those smokiest eyes and brightest red lip looks effortlessly as it’ll stay perfectly in place as you confidently soak up the sun!<br><br><strong>Key Benefits</strong>:<br>- Infused with a breakthrough fusion of active sunscreen ingredients &amp; UV defense technology to resist UVA, UVB skin damage<br>- The enriching formula forms an ultra-lightweight, non-sticky layer of sunscreen film on the skin and keeps it protected for hours together<br>- The goodness of homosalate specifically absorbs UVB rays that leads to skin damage while menthol lactate and ginger root extracts soothe, cool &amp; offer freshness to skin<br>- Super comfortable and breathable while working hard to make sure your makeup stays put <br>- Pigment free formula keeps skin looking dewy and fresh with every spritz<br>- Free from parabens and is cruelty-free<br><br><strong>Net</strong> <strong>Weight</strong>: 50ml<br><br><strong>Maximum Retail Price</strong>: Rs.699 (Incl. all taxes)<br><strong>Country of Origin</strong>: PRC<br><strong>Company Name</strong>: SUGAR Cosmetics LLC.<br><strong>Company Address</strong>: 8 The Green Suite A, Dover, DE 19901, USA.</p>\n<h5>Ingredients</h5>\n<p><strong>SUGAR Grand Finale SPF30 Setting Mist Ingredients</strong>: Alcohol, Octocrylene, Homosalate, Dimethicone/ Vinyl Dimethicone Crosspolymer, Ethylhexyl Salicylate, Cyclopentasiloxane, Butyl Methoxydibenzoylmethane, 4-Methylbenzylidene Camphor, Diethylhexyl 2,6-Naphthalate, Silica, Butylene Glycol, Parfum, Isoceteth-10, Menthol Lactate, Bisabolol, Zingiber Officinale (Ginger) Root Extract.</p>\n<h5>How to apply</h5>\n<p>- Place the bottle of the SUGAR Grand Finale SPF30 Setting Mist at arm’s length or about 8-10 inches away from your face.<br>- Generously spritz the mist all over your face and let it dry naturally.<br>- Apply liberally minutes before sun exposure and after applying your makeup.<br>- Do not rub or wipe.</p>\n<p><strong>Caution</strong>: Avoid direct contact with eyes, if contact occurs, rinse well with water.</p>\n<h5>Commonly Asked Questions</h5>\n<p>Q. Can it be used before makeup?<br>A. Yes, the SUGAR Grand Finale SPF30 Setting Mist can be used both- before and after your makeup.<br><br>Q. Is the product safe to use on skin?<br>A. Free from parabens and cruelty-free ingredients, this product is absolutely safe to use on skin.<br><br>Q. What is the volume of product in each bottle?<br>A. The volume of product in each bottle is 50ml.<br><br>Q. Is it easy to carry?<br>A. Absolutely! The setting mist comes in a travel-friendly size bottle for <br>on-the-go use &amp; storage.</p>",
              vendor: "SUGAR Cosmetics",
              product_type: "Setting Mist",
              created_at: "2021-01-11T15:36:01+05:30",
              handle: "grand-finale-spf30-setting-mist",
              updated_at: "2021-01-12T12:59:42+05:30",
              published_at: "2021-01-12T10:24:36+05:30",
              template_suffix: "",
              published_scope: "web",
              tags:
                "Affordable, Alcohol formula, allday use, Cruelty free, Dewy, Dewy look, Dewy Mist, Easy to Use, Face Mist, Face preparation, facial care, Fresh look, highlighting, hydrating, Long Lasting, Matte Finish, Matte look, matte mist, Mist, moisturizing, Nourishing Skin, Party Wear, pre makeup, Radiant Finish, Setting makeup, Setting Mist, skin mist, Soothing Skin, sugar_type_0, Ultra Fine Application, Wedding Wear, Weightless Matte Finish",
              admin_graphql_api_id: "gid://shopify/Product/4732391817299",
              variants: [
                {
                  id: 32366539800659,
                  product_id: 4732391817299,
                  title: "Default Title",
                  price: "699.00",
                  sku: "8904320704926",
                  position: 1,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "Default Title",
                  option2: null,
                  option3: null,
                  created_at: "2021-01-11T15:36:01+05:30",
                  updated_at: "2021-01-12T12:59:42+05:30",
                  taxable: false,
                  barcode: "",
                  grams: 0,
                  image_id: null,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 34148667883603,
                  inventory_quantity: 1058,
                  old_inventory_quantity: 1058,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/32366539800659",
                },
              ],
              options: [
                {
                  id: 6107618771027,
                  product_id: 4732391817299,
                  name: "Title",
                  position: 1,
                  values: ["Default Title"],
                },
              ],
              images: [
                {
                  id: 15637229109331,
                  product_id: 4732391817299,
                  position: 1,
                  created_at: "2021-01-12T10:30:01+05:30",
                  updated_at: "2021-01-12T12:27:23+05:30",
                  alt: "SUGAR Cosmetics Grand Finale SPF30 Setting Mist",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-grand-finale-spf30-setting-mist-15637215576147.jpg?v=1610434643",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15637229109331",
                },
                {
                  id: 15637695660115,
                  product_id: 4732391817299,
                  position: 2,
                  created_at: "2021-01-12T12:27:22+05:30",
                  updated_at: "2021-01-12T12:27:23+05:30",
                  alt: "SUGAR Cosmetics Grand Finale SPF30 Setting Mist",
                  width: 1080,
                  height: 1920,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-grand-finale-spf30-setting-mist-15637685502035.jpg?v=1610434643",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15637695660115",
                },
                {
                  id: 15637236940883,
                  product_id: 4732391817299,
                  position: 3,
                  created_at: "2021-01-12T10:33:27+05:30",
                  updated_at: "2021-01-12T12:27:23+05:30",
                  alt: "SUGAR Cosmetics Grand Finale SPF30 Setting Mist",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-grand-finale-spf30-setting-mist-15637215412307.jpg?v=1610434643",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15637236940883",
                },
                {
                  id: 15637220163667,
                  product_id: 4732391817299,
                  position: 4,
                  created_at: "2021-01-12T10:25:58+05:30",
                  updated_at: "2021-01-12T12:27:23+05:30",
                  alt: "SUGAR Cosmetics Grand Finale SPF30 Setting Mist",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-grand-finale-spf30-setting-mist-15637215838291.jpg?v=1610434643",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15637220163667",
                },
                {
                  id: 15637227241555,
                  product_id: 4732391817299,
                  position: 5,
                  created_at: "2021-01-12T10:29:27+05:30",
                  updated_at: "2021-01-12T12:27:23+05:30",
                  alt: "SUGAR Cosmetics Grand Finale SPF30 Setting Mist",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-grand-finale-spf30-setting-mist-15637215379539.jpg?v=1610434643",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15637227241555",
                },
              ],
              image: {
                id: 15637229109331,
                product_id: 4732391817299,
                position: 1,
                created_at: "2021-01-12T10:30:01+05:30",
                updated_at: "2021-01-12T12:27:23+05:30",
                alt: "SUGAR Cosmetics Grand Finale SPF30 Setting Mist",
                width: 1025,
                height: 1400,
                src:
                  "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-grand-finale-spf30-setting-mist-15637215576147.jpg?v=1610434643",
                variant_ids: [],
                admin_graphql_api_id:
                  "gid://shopify/ProductImage/15637229109331",
              },
            },
            mediaText: null,
            redirectUrl: null,
          },
          {
            id: 191,
            sectionId: 4,
            webViewTitle: null,
            contentType: 3,
            mediaUrl: null,
            product_json: {
              id: 4716738216019,
              title: "Beginner's Must-Have Kit",
              body_html:
                'Enter the enchanting world of beauty with the best of makeup and skin that we’ve handpicked just for you! Our new Beginners Must Have Kit has all the beauty essentials you’ll need to get started and look your flawless best. <br data-mce-fragment="1"><br data-mce-fragment="1">SUGAR\'s Beginners Must Have Kit comes with an intensely pigmented kohl to glam up your eyes, there is a bestselling lipstick with 3 colours to choose from, a mini blush with 3 lively shades to choose from, a mini highlighter or a bronzer to choose from, and finally a cheat sheet anti-aging mask to freshen up dull skin at the end of a tiring day. What\'s the best part about this beauty kit? All these travel-friendly products come in a cute see-through zipper pouch with pink panels - ideal to carry your makeup while travelling. This budget beauty kit is all you need to start your makeup journey and we highly recommend this one!<br data-mce-fragment="1"><br data-mce-fragment="1"><strong>Kit includes:</strong><br data-mce-fragment="1"><br data-mce-fragment="1">- Smudge Me Not Mini Liquid Lipstick x 1 (1.1ml) - Pick any 1 from 3 bestselling shades<br data-mce-fragment="1">- Stroke of Genius Heavy-Duty Kohl - 05 Black Magic x 1 (1.2g)<br data-mce-fragment="1">- SUGAR Cheat Sheet Anti-Aging Mask x 1 (24g)<br data-mce-fragment="1">- Contour De Force Mini Highlighter x 1 (4g) or Contour De Force Mini Bronzer x 1 (4g)<br data-mce-fragment="1">- Contour De Force Mini Blush x 1 (4g)<br data-mce-fragment="1"><br data-mce-fragment="1"><strong>Why you\'ll love this kit:</strong><br data-mce-fragment="1"><br data-mce-fragment="1">- Highly pigmented matte lipstick that stays put all day long<br data-mce-fragment="1">- Super creamy and rich kohl pencil that stays up to 8 hours<br data-mce-fragment="1">- Blendable blush with universally flattering tones<br data-mce-fragment="1">- Sheet mask that freshens skin in minutes<br data-mce-fragment="1">- Highlighter or a bronzer for that extra glow<br data-mce-fragment="1"><br data-mce-fragment="1">\n<p><strong>Maximum Retail Price</strong>: Rs. 999/- (incl. all taxes)<br data-mce-fragment="1"><br data-mce-fragment="1"><strong>SUGAR Smudge Me Not Liquid Lipstick</strong><br data-mce-fragment="1">Country of Origin: India<br data-mce-fragment="1">Company Name: Viva Cosmetics<br data-mce-fragment="1">Company Address: A/19 G4 Happy Valley, Nr Tikujiniwadi, Manpada, Thane W - 400610<br data-mce-fragment="1"><br data-mce-fragment="1"><strong>SUGAR Stroke of Genius Heavy-Duty Kohl</strong><br data-mce-fragment="1">Country of Origin: Germany<br data-mce-fragment="1">Company Name: Schwan Cosmetics Germany Gmbh &amp; Co.Kg<br data-mce-fragment="1">Company Address: Schwanweg 1, 90562 Heroldsberg - (Germany)<br data-mce-fragment="1"><br data-mce-fragment="1"><strong>SUGAR Contour De Force Mini Blush</strong><br data-mce-fragment="1">Country of Origin: Italy<br data-mce-fragment="1">Company Name: Regi India Cosmetics Pvt Ltd.<br data-mce-fragment="1">Company Address: C-08, Sara Ind Estate, Vill-Rampur, Selaqui, Dehradun 248197, Uttarakhand<br data-mce-fragment="1"><br data-mce-fragment="1"><strong>SUGAR Contour De Force Mini Highlighter</strong><br data-mce-fragment="1">Country of Origin: Italy<br data-mce-fragment="1">Company Name: Regi India Cosmetics Pvt Ltd.<br data-mce-fragment="1">Company Address: C-08, Sara Ind Estate, Vill-Rampur, Selaqui, Dehradun 248197, Uttarakhand.<br data-mce-fragment="1"><br data-mce-fragment="1"><strong>SUGAR Cheat Sheet Anti -Aging Mask</strong><br data-mce-fragment="1">Country of Origin: Korea<br data-mce-fragment="1">Company Name: SUGAR Cosmetics LLC<br data-mce-fragment="1">Company Address: 8 The Green Suite A, Dover, DE 19901, USA<br data-mce-fragment="1"><br data-mce-fragment="1"><strong>SUGAR Contour De Force Mini Bronzer</strong><br data-mce-fragment="1">Country of Origin: Italy<br data-mce-fragment="1">Company Name: Regi India Cosmetics Pvt Ltd.<br data-mce-fragment="1">Company Address: C-08, Sara Ind Estate, Vill-Rampur, Selaqui, Dehradun 248197, Uttarakhand</p>\n<h5>How to apply</h5>\n<p>- <strong>Smudge Me Not Liquid Lipstick</strong>: Apply straight from the wand. With the edge, trace your lip line and fill it in. Keep lips apart for a few seconds to allow the lipstick to dry.<br><br>- <strong>Stroke of Genius Heavy-Duty Kohl</strong>: Gently rim your lower and upper lash lines with this creamy Kohl. Open your eyes wide for precise application. Can also be used as a gel eyeliner.<br><br>- <strong>Contour De Force Mini Blush</strong>: Pick a little on a fluffy brush and start from the apples of your cheeks. Blend back and upwards towards your hairline.<br><br>- <strong>Contour De Force Mini Highlighter</strong>: Apply this multitasking highlighter to the parts of your face that light would naturally hit, to enhance its natural structure and add radiance to your complexion.<br><br>- <strong>Contour De Force Mini Bronzer</strong>: Lightly coat the end of your brush with some SUGAR Contour De Force Mini Bronzer powder and tap off the excess. Apply this best bronzer in the shape of a “3” from the top to the bottom of your face on both sides, starting with your forehead. Lightly sweep the brush along the outer sides of your upper forehead and along your hairline. Next, make a fish face and apply the bronzer powder to your cheekbones. Complete the “3” shape by sweeping your bronzer along your jawline.<br><br>- <strong>Cheat Sheet Anti-Aging Mask</strong>: Remove the mask from the pouch and gently unfold. Separate the mask from the protecting plastic and carefully place over the eye area first and then smoothen out across the rest of your face. Keep the mask on for 15-20 minutes. Gently remove and tap the excess serum into your skin for better absorption.</p>',
              vendor: "SUGAR Cosmetics",
              product_type: "Kit",
              created_at: "2020-11-26T16:14:44+05:30",
              handle: "beginners-must-have-kit",
              updated_at: "2021-01-13T12:50:51+05:30",
              published_at: "2021-01-11T15:57:14+05:30",
              template_suffix: "",
              published_scope: "web",
              tags:
                "Affordable, Anniversary, Birthday, Budget, Christmas, Cost Effective, Deals, Diwali, Economical, Eid, Eyeliner, Festive Makeup, Gift, Gifting, Gifts for her, Girlfriend, Kajal, Kohl, Lipstick, Love, Low Cost, Low Priced, Makeup Bloggers, Makeup Offers, Marriage, Matte Lipsticks, Mom, Money Saving, Mother, New Year, Nominal, Present, Presents for Her, Reasonable, Savings, Sheet Mask, Sister, Steal Deal, Traditional Makeup, Valentine, Valentine's Day, Value for Money, view_product, Wedding, Worth The Money, Worthy",
              admin_graphql_api_id: "gid://shopify/Product/4716738216019",
              variants: [
                {
                  id: 32321546944595,
                  product_id: 4716738216019,
                  title: "Default Title",
                  price: "999.00",
                  sku: "8904320715601",
                  position: 1,
                  inventory_policy: "deny",
                  compare_at_price: "1595.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "Default Title",
                  option2: null,
                  option3: null,
                  created_at: "2020-11-26T16:14:45+05:30",
                  updated_at: "2021-01-13T12:49:05+05:30",
                  taxable: false,
                  barcode: "",
                  grams: 0,
                  image_id: null,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 34103635738707,
                  inventory_quantity: 3584,
                  old_inventory_quantity: 3584,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/32321546944595",
                },
              ],
              options: [
                {
                  id: 6085737054291,
                  product_id: 4716738216019,
                  name: "Title",
                  position: 1,
                  values: ["Default Title"],
                },
              ],
              images: [
                {
                  id: 15493399773267,
                  product_id: 4716738216019,
                  position: 1,
                  created_at: "2020-12-10T18:24:03+05:30",
                  updated_at: "2020-12-10T18:27:31+05:30",
                  alt: "SUGAR Cosmetics Beginner's Must-Have Kit",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-beginner-s-must-have-kit-15493379784787.jpg?v=1607605051",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15493399773267",
                },
                {
                  id: 15493409570899,
                  product_id: 4716738216019,
                  position: 2,
                  created_at: "2020-12-10T18:25:57+05:30",
                  updated_at: "2020-12-10T18:27:31+05:30",
                  alt: "SUGAR Cosmetics Beginner's Must-Have Kit",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-beginner-s-must-have-kit-15493379850323.jpg?v=1607605051",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15493409570899",
                },
                {
                  id: 15493396693075,
                  product_id: 4716738216019,
                  position: 3,
                  created_at: "2020-12-10T18:23:27+05:30",
                  updated_at: "2020-12-10T18:38:39+05:30",
                  alt: "SUGAR Cosmetics Beginner's Must-Have Kit",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-beginner-s-must-have-kit-15493379653715.jpg?v=1607605719",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15493396693075",
                },
                {
                  id: 15493418713171,
                  product_id: 4716738216019,
                  position: 4,
                  created_at: "2020-12-10T18:27:30+05:30",
                  updated_at: "2020-12-10T18:38:39+05:30",
                  alt: "SUGAR Cosmetics Beginner's Must-Have Kit",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-beginner-s-must-have-kit-15493379883091.jpg?v=1607605719",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15493418713171",
                },
                {
                  id: 15493385257043,
                  product_id: 4716738216019,
                  position: 5,
                  created_at: "2020-12-10T18:21:27+05:30",
                  updated_at: "2020-12-10T18:38:39+05:30",
                  alt: "SUGAR Cosmetics Beginner's Must-Have Kit",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-beginner-s-must-have-kit-15493379555411.jpg?v=1607605719",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15493385257043",
                },
                {
                  id: 15493391548499,
                  product_id: 4716738216019,
                  position: 6,
                  created_at: "2020-12-10T18:22:33+05:30",
                  updated_at: "2020-12-10T18:27:31+05:30",
                  alt: "SUGAR Cosmetics Beginner's Must-Have Kit",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-beginner-s-must-have-kit-15493379719251.jpg?v=1607605051",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15493391548499",
                },
                {
                  id: 15493407113299,
                  product_id: 4716738216019,
                  position: 7,
                  created_at: "2020-12-10T18:25:25+05:30",
                  updated_at: "2020-12-10T18:27:31+05:30",
                  alt: "SUGAR Cosmetics Beginner's Must-Have Kit",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-beginner-s-must-have-kit-15493379948627.jpg?v=1607605051",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15493407113299",
                },
                {
                  id: 15493388435539,
                  product_id: 4716738216019,
                  position: 8,
                  created_at: "2020-12-10T18:22:00+05:30",
                  updated_at: "2020-12-10T18:27:31+05:30",
                  alt: "SUGAR Cosmetics Beginner's Must-Have Kit",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-beginner-s-must-have-kit-15493379915859.jpg?v=1607605051",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15493388435539",
                },
              ],
              image: {
                id: 15493399773267,
                product_id: 4716738216019,
                position: 1,
                created_at: "2020-12-10T18:24:03+05:30",
                updated_at: "2020-12-10T18:27:31+05:30",
                alt: "SUGAR Cosmetics Beginner's Must-Have Kit",
                width: 1025,
                height: 1400,
                src:
                  "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-beginner-s-must-have-kit-15493379784787.jpg?v=1607605051",
                variant_ids: [],
                admin_graphql_api_id:
                  "gid://shopify/ProductImage/15493399773267",
              },
            },
            mediaText: null,
            redirectUrl: null,
          },
          {
            id: 186,
            sectionId: 4,
            webViewTitle: null,
            contentType: 3,
            mediaUrl: null,
            product_json: {
              id: 4680398012499,
              title: "Tipsy Lips Moisturizing Balm",
              body_html:
                '<p><span data-mce-fragment="1">Need a nourishing product that serves up yumminess too? No worries! Presenting, SUGAR Tipsy Lips Moisturizing Lip Balm in 7 awesome flavours that are enriched with Vitamin E, Jojoba Oil, Shea Butter, and SPF. These beauties are colourless and pack a punch when it comes to keeping your lips supple and moisturized. You can\'t miss these.</span><br data-mce-fragment="1"></p>\n<ul>\n<li><span data-mce-fragment="1"><a href="https://in.sugarcosmetics.com/products/tipsy-lips-moisturizing-balm-01-mojito" target="_blank"><strong data-mce-fragment="1">01 Mojito</strong></a> - Like sitting by the pool, no matter where you are.<br><br></span></li>\n<li><span data-mce-fragment="1"><a href="https://in.sugarcosmetics.com/products/tipsy-lips-moisturizing-balm-02-cosmopolitan" target="_blank"><strong>02 Cosmopolitan</strong></a> - Gives you a sense of partying with your girls.<br><br></span></li>\n<li><span data-mce-fragment="1"><a href="https://in.sugarcosmetics.com/products/tipsy-lips-moisturizing-balm-03-pinacolada" target="_blank"><strong>03 Pinacolada</strong></a> - Like chilling by the beach, under sunshine.<br><br></span></li>\n<li><span data-mce-fragment="1"><a href="https://in.sugarcosmetics.com/products/tipsy-lips-moisturizing-balm-04-l-i-i-t" target="_blank"><strong>04 L.I.I.T</strong></a> - Party in a tiny pack!<br><br></span></li>\n<li><span data-mce-fragment="1"><a href="https://in.sugarcosmetics.com/products/tipsy-lips-moisturizing-balm-05-irish-coffee" target="_blank"><strong>05 Irish Coffee</strong></a> - Like waking up on a holiday.<br><br></span></li>\n<li><span data-mce-fragment="1"><a href="https://in.sugarcosmetics.com/products/tipsy-lips-moisturizing-balm-06-mango-margarita" target="_blank"><strong>06 Mango Margarita</strong></a> - Gives you the feeling of summer, all year long!<br><br></span></li>\n<li><span data-mce-fragment="1"><a href="https://in.sugarcosmetics.com/products/tipsy-lips-moisturizing-balm-07-bramble" target="_blank"><strong>07 Bramble</strong></a> - Gets you in the weekend mood, even on a weekday.</span></li>\n</ul>\n<p><br data-mce-fragment="1"><span data-mce-fragment="1"><strong>Net Volume</strong>: 4.5 gm</span><br data-mce-fragment="1"><br data-mce-fragment="1"><span data-mce-fragment="1"><strong>SUGAR Tipsy Lips Moisturizing Lip Balm Ingredients</strong>:<br data-mce-fragment="1">Petrolatum, Hydrogenated Polyisobutene, Hydrogenated Castor Oil, Octyl Palmitate, Isopropyl Myristate, Hydrogenated Vegetable Oil, Microcrystalline wax, Ozokerite, Tri-isostearoyl, Polyglycerol-3 Dimer Dilinoleate, Propyl Glycol, Synthetic Wax, Paraffin Wax, Butyrospermum Parkii (Shea) Butter, Propylene Glycol Dibenzonate, Synthetic Beewax, Polyethylene, Caprylic/Capric Triglyceride, Octyl Methoxycinnamate, Simmondsia Chinensis (Jojoba) Seed Oil, Tocopheryl Acetate, Phenoxyethanol, Ethylhexiglycerin, Camellia Japonica (Camilla) Seed Oil, Butylated Hydroxytoluene, Flavor, Approved Colours</span><br data-mce-fragment="1"><br data-mce-fragment="1"><span data-mce-fragment="1"><strong data-mce-fragment="1">Maximum Retail Price</strong>: Rs 199 (incl. of taxes)</span><br data-mce-fragment="1"><br data-mce-fragment="1"><span data-mce-fragment="1"><strong data-mce-fragment="1">Country of Origin</strong>: India</span><br data-mce-fragment="1"><br data-mce-fragment="1"><span data-mce-fragment="1"><strong data-mce-fragment="1">Company Name</strong>: Viva Cosmetics</span><br data-mce-fragment="1"><br data-mce-fragment="1"><span data-mce-fragment="1"><strong data-mce-fragment="1">Company Address</strong>: C-1 / 1 &amp; 2 RIC, Asangaon, Shahpur, Thane - 421601</span></p>\n<p data-mce-fragment="1"> </p>\n<h5 data-mce-fragment="1">How to apply</h5>\n<p data-mce-fragment="1"><span data-mce-fragment="1">Apply this best moisturizing lip balm directly onto lips after scrubbing or whenever they feel dry</span>.<br data-mce-fragment="1"><br data-mce-fragment="1"><span data-mce-fragment="1"><strong data-mce-fragment="1">Best paired with</strong>:</span><br data-mce-fragment="1"><br data-mce-fragment="1"><span data-mce-fragment="1">Wear the SUGAR Tipsy Lips Moisturizing Lip Balm under a matte or liquid lipstick to keep your lips hydrated. Perfect for both work and as party wear, never leave hydration behind!</span></p>\n<h5 data-mce-fragment="1">Benefits</h5>\n<p data-mce-fragment="1">SUGAR Tipsy Lips Moisturizing Lip Balm is powered by:<br data-mce-fragment="1"><br data-mce-fragment="1"><span data-mce-fragment="1">- Jojoba Oil (Moisturizes)</span><br data-mce-fragment="1"><span data-mce-fragment="1">- Vitamin E (Renews skin)<br></span><span data-mce-fragment="1">- Shea Butter (Anti-inflammatory)</span><br data-mce-fragment="1"><span data-mce-fragment="1">- SPF (Saves skin against UV rays)</span></p>\n<h5 data-mce-fragment="1">Commonly asked questions</h5>\n<p data-mce-fragment="1"><span data-mce-fragment="1">Q. How many variants does it come in?</span><br data-mce-fragment="1"><span data-mce-fragment="1">A. SUGAR Tipsy Lips Moisturizing Lip Balm comes in 7 awesome flavours.</span><br data-mce-fragment="1"><br data-mce-fragment="1"><span data-mce-fragment="1">Q. Do the flavours impart colour as well?</span><br data-mce-fragment="1"><span data-mce-fragment="1">A. No, the balms are colourless.</span><br data-mce-fragment="1"><br data-mce-fragment="1"><span data-mce-fragment="1">Q. What is the net volume of the product?</span><br data-mce-fragment="1"><span data-mce-fragment="1">A. The net volume is 4.5 gm.</span><br data-mce-fragment="1"><br data-mce-fragment="1"><span data-mce-fragment="1">Q. Where are the products made?</span><br data-mce-fragment="1"><span data-mce-fragment="1">A. They\'re made in India.</span><br data-mce-fragment="1"><br data-mce-fragment="1"><span data-mce-fragment="1">Q. Is it nourishing?</span><br data-mce-fragment="1"><span data-mce-fragment="1">A. Absolutely! It contains Jojoba Oil, Vitamin E, Shea Butter and also SPF. Read more about why </span><strong><a href="https://blog.sugarcosmetics.com/4-amazing-benefits-of-jojoba-oil/" data-mce-fragment="1" data-mce-href="https://blog.sugarcosmetics.com/4-amazing-benefits-of-jojoba-oil/" target="_blank">Jojoba Oil is a great ingredient</a></strong><span data-mce-fragment="1"> for your lips and skin.</span></p>',
              vendor: "SUGAR Cosmetics",
              product_type: "Balm",
              created_at: "2020-09-14T11:10:43+05:30",
              handle: "tipsy-lips-moisturizing-balm",
              updated_at: "2021-01-13T12:55:33+05:30",
              published_at: "2020-09-14T11:10:44+05:30",
              template_suffix: "",
              published_scope: "web",
              tags: "st_bestseller:18, st_popularityscore: 9017",
              admin_graphql_api_id: "gid://shopify/Product/4680398012499",
              variants: [
                {
                  id: 32222310727763,
                  product_id: 4680398012499,
                  title: "01 Mojito",
                  price: "199.00",
                  sku: "8904320706531",
                  position: 1,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "01 Mojito",
                  option2: null,
                  option3: null,
                  created_at: "2020-09-14T11:12:26+05:30",
                  updated_at: "2021-01-13T12:35:30+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 15057297801299,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 34004325204051,
                  inventory_quantity: 1464,
                  old_inventory_quantity: 1464,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/32222310727763",
                },
                {
                  id: 32222310760531,
                  product_id: 4680398012499,
                  title: "02 Cosmopolitan",
                  price: "199.00",
                  sku: "8904320706548",
                  position: 2,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "02 Cosmopolitan",
                  option2: null,
                  option3: null,
                  created_at: "2020-09-14T11:12:26+05:30",
                  updated_at: "2021-01-13T11:02:41+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 15045508071507,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 34004325236819,
                  inventory_quantity: 911,
                  old_inventory_quantity: 911,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/32222310760531",
                },
                {
                  id: 32222310793299,
                  product_id: 4680398012499,
                  title: "03 Pinacolada",
                  price: "199.00",
                  sku: "8904320706555",
                  position: 3,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "03 Pinacolada",
                  option2: null,
                  option3: null,
                  created_at: "2020-09-14T11:12:26+05:30",
                  updated_at: "2021-01-13T12:44:45+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 15057299996755,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 34004325269587,
                  inventory_quantity: 1873,
                  old_inventory_quantity: 1873,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/32222310793299",
                },
                {
                  id: 32222310826067,
                  product_id: 4680398012499,
                  title: "04 L.I.I.T",
                  price: "199.00",
                  sku: "8904320706562",
                  position: 4,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "04 L.I.I.T",
                  option2: null,
                  option3: null,
                  created_at: "2020-09-14T11:12:26+05:30",
                  updated_at: "2021-01-13T12:35:35+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 15045405933651,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 34004325302355,
                  inventory_quantity: 2095,
                  old_inventory_quantity: 2095,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/32222310826067",
                },
                {
                  id: 32222310858835,
                  product_id: 4680398012499,
                  title: "05 Irish Coffee",
                  price: "199.00",
                  sku: "8904320706579",
                  position: 5,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "05 Irish Coffee",
                  option2: null,
                  option3: null,
                  created_at: "2020-09-14T11:12:26+05:30",
                  updated_at: "2021-01-13T12:44:45+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 15045505974355,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 34004325335123,
                  inventory_quantity: 2098,
                  old_inventory_quantity: 2098,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/32222310858835",
                },
                {
                  id: 32222310891603,
                  product_id: 4680398012499,
                  title: "06 Mango Margarita",
                  price: "199.00",
                  sku: "8904320706586",
                  position: 6,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "06 Mango Margarita",
                  option2: null,
                  option3: null,
                  created_at: "2020-09-14T11:12:26+05:30",
                  updated_at: "2021-01-13T11:34:01+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 15045511839827,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 34004325367891,
                  inventory_quantity: 2379,
                  old_inventory_quantity: 2379,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/32222310891603",
                },
                {
                  id: 32222310924371,
                  product_id: 4680398012499,
                  title: "07 Bramble",
                  price: "199.00",
                  sku: "8904320706593",
                  position: 7,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "07 Bramble",
                  option2: null,
                  option3: null,
                  created_at: "2020-09-14T11:12:26+05:30",
                  updated_at: "2021-01-13T12:44:45+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: 15045402591315,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 34004325400659,
                  inventory_quantity: 462,
                  old_inventory_quantity: 462,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/32222310924371",
                },
              ],
              options: [
                {
                  id: 6034490458195,
                  product_id: 4680398012499,
                  name: "Color",
                  position: 1,
                  values: [
                    "01 Mojito",
                    "02 Cosmopolitan",
                    "03 Pinacolada",
                    "04 L.I.I.T",
                    "05 Irish Coffee",
                    "06 Mango Margarita",
                    "07 Bramble",
                  ],
                },
              ],
              images: [
                {
                  id: 15045508071507,
                  product_id: 4680398012499,
                  position: 1,
                  created_at: "2020-09-14T14:16:21+05:30",
                  updated_at: "2020-09-16T08:49:51+05:30",
                  alt:
                    "SUGAR Cosmetics Tipsy Lips Moisturizing Balm 02 Cosmopolitan",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-tipsy-lips-moisturizing-balm-02-cosmopolitan-15045393940563.jpg?v=1600226391",
                  variant_ids: [32222310760531],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15045508071507",
                },
                {
                  id: 15045405933651,
                  product_id: 4680398012499,
                  position: 2,
                  created_at: "2020-09-14T13:50:30+05:30",
                  updated_at: "2020-09-16T08:49:51+05:30",
                  alt:
                    "SUGAR Cosmetics Tipsy Lips Moisturizing Balm 04 L.I.I.T",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-tipsy-lips-moisturizing-balm-04-l-i-i-t-15045394628691.jpg?v=1600226391",
                  variant_ids: [32222310826067],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15045405933651",
                },
                {
                  id: 15045505974355,
                  product_id: 4680398012499,
                  position: 3,
                  created_at: "2020-09-14T14:15:49+05:30",
                  updated_at: "2020-09-16T08:49:51+05:30",
                  alt:
                    "SUGAR Cosmetics Tipsy Lips Moisturizing Balm 05 Irish Coffee",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-tipsy-lips-moisturizing-balm-05-irish-coffee-15045394956371.jpg?v=1600226391",
                  variant_ids: [32222310858835],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15045505974355",
                },
                {
                  id: 15045511839827,
                  product_id: 4680398012499,
                  position: 4,
                  created_at: "2020-09-14T14:17:13+05:30",
                  updated_at: "2020-09-16T08:49:51+05:30",
                  alt:
                    "SUGAR Cosmetics Tipsy Lips Moisturizing Balm 06 Mango Margarita",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-tipsy-lips-moisturizing-balm-06-mango-margarita-15045395054675.jpg?v=1600226391",
                  variant_ids: [32222310891603],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15045511839827",
                },
                {
                  id: 15045402591315,
                  product_id: 4680398012499,
                  position: 5,
                  created_at: "2020-09-14T13:49:22+05:30",
                  updated_at: "2020-09-16T08:49:51+05:30",
                  alt:
                    "SUGAR Cosmetics Tipsy Lips Moisturizing Balm 07 Bramble",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-tipsy-lips-moisturizing-balm-07-bramble-15045395480659.jpg?v=1600226391",
                  variant_ids: [32222310924371],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15045402591315",
                },
                {
                  id: 15057299996755,
                  product_id: 4680398012499,
                  position: 6,
                  created_at: "2020-09-16T08:49:50+05:30",
                  updated_at: "2020-09-16T08:49:51+05:30",
                  alt:
                    "SUGAR Cosmetics Tipsy Lips Moisturizing Balm 03 Pinacolada",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-tipsy-lips-moisturizing-balm-03-pinacolada-15057283776595.jpg?v=1600226391",
                  variant_ids: [32222310793299],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15057299996755",
                },
                {
                  id: 15057297801299,
                  product_id: 4680398012499,
                  position: 7,
                  created_at: "2020-09-16T08:49:16+05:30",
                  updated_at: "2020-09-16T08:49:51+05:30",
                  alt: "SUGAR Cosmetics Tipsy Lips Moisturizing Balm 01 Mojito",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-tipsy-lips-moisturizing-balm-01-mojito-15057279877203.jpg?v=1600226391",
                  variant_ids: [32222310727763],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15057297801299",
                },
              ],
              image: {
                id: 15045508071507,
                product_id: 4680398012499,
                position: 1,
                created_at: "2020-09-14T14:16:21+05:30",
                updated_at: "2020-09-16T08:49:51+05:30",
                alt:
                  "SUGAR Cosmetics Tipsy Lips Moisturizing Balm 02 Cosmopolitan",
                width: 1025,
                height: 1400,
                src:
                  "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-tipsy-lips-moisturizing-balm-02-cosmopolitan-15045393940563.jpg?v=1600226391",
                variant_ids: [32222310760531],
                admin_graphql_api_id:
                  "gid://shopify/ProductImage/15045508071507",
              },
            },
            mediaText: null,
            redirectUrl: null,
          },
          {
            id: 184,
            sectionId: 4,
            webViewTitle: null,
            contentType: 3,
            mediaUrl: null,
            product_json: {
              id: 4702988304467,
              title: "Graphic Jam 36HR Eyeliner - 01 Blackest Black",
              body_html:
                '<p>We’ve fallen eyes over heels over our new bold eyeliner that lasts up to 36Hrs and delivers intense black payoff! Effortless to apply with a precise tip, SUGAR\'s Graphic Jam 36HR Eyeliner glides on beautifully, leaving a richly pigmented, matte line up for attention. This waterproof eyeliner dries within seconds to give you bold black matte finish that does not fade away. Formulated with wax and silicone resin, this transfer proof beauty ensures exceptional longwear and glides on smoothly to deliver instant pigment, precision and style. Whether you opt for classic simple lines or prefer dramatic winged eyeliner looks, this hard-working formula lasts and lasts, so you dont need to ‘touch up’ during day or night!</p>\n<p>Maximum Retail Price: Rs 699 (incl. of taxes)<br>\nCountry of origin- PRC<br><span data-mce-fragment="1">Company Name - SUGAR Cosmetics LLC</span><br data-mce-fragment="1"><span data-mce-fragment="1">Company Address - 8 The Green, Suite A, Dover, DE 19901</span><br></p>\n<h5>Benefits</h5>\n<ul>\n<li>Offers an intense black colour that lasts up to 36 hours with just one stroke</li>\n<li>Has a smudge-proof; transfer-resistant and water-proof formula</li>\n<li>Quick drying and settles to elegant matte finish</li>\n<li>Is easy to apply and helps create sleek, bold lines with ease</li>\n</ul>\n<h5>Additional Details</h5>\nThe Graphic Jam 36HR Eyeliner is cruelty-free and vegan. This product is also free from parabens, dermatologically, ophthalmologically tested and 100% safe for your skin.\n<h5>Ingredients</h5>\nIsododecane, Trimethylsiloxysilicate, Iron Oxides Ci 77499, Dimethicone, Disteardimonium Hectorite, Black 2 Ci 77266 (Nano), Sorbitan Tristearate, Sorbitan Tristearate, Propylene Carbonate, Caprylyl Glycol, Phenoxyethanol, Ascorbyl Palmitate, Hexylene Glycol, BHT<br><br>\n<h5>How to apply</h5>\n<ul>\n<li><span>Line your top lid in a single stroke starting from the inner to the outer corner of your eye.</span></li>\n<li>Extend the line toward the tip of the brow to create a cat eye.</li>\n</ul>\n<h5>Commonly asked questions</h5>\n<p>Q. What\'s the finish like?<br><span>A. Super pigmented, ultra matte finish.</span><br><br><span>Q. Is it long-lasting?</span><br><span>A. With more than 36 hour of stay power, and being transfer-resistant and smudge-proof, we doubt you need any touch ups.</span><br><br><span>Q. How many shades does it come in?</span><br><span>A. This matte eyeliner is available in one shade - 01 BLACKEST BLACK.</span><br><br><span>Q. Will it irritate my eyes?</span><br><span>A. The eyeliner is dermatologically tested and is 100% safe for your skin.</span><br><br><span>Q. Where is it made?</span><br><span>A. It is formulated in PRC.</span></p>',
              vendor: "SUGAR Cosmetics",
              product_type: "Eyeliner",
              created_at: "2020-10-22T16:04:07+05:30",
              handle: "graphic-jam-36hr-eyeliner-01-blackest-black",
              updated_at: "2021-01-13T11:03:01+05:30",
              published_at: "2020-10-22T16:04:08+05:30",
              template_suffix: "",
              published_scope: "web",
              tags:
                "10 HR, 12 HR, 24 HR, 36 HR, 699, 9 to 5, Best Seller, Black, Bold, Budgeproof, Built in Brush, Cat Eye, Comfortable, Cruelty Free, Dermatologically Tested, Easy application, Easy Wing, Eye, Eyeliner, Gluten Free, High Coverage, Intense, Jet Black, Liquid Eyeliner, Long Lasting, Long wear, Longwear, Matte, Mineral Oil Free, No Fade, Ophthalmologically tested, Paraben Free, Precise Application, Rich matte finish, Saturated colour, Semi-Permanent, Silky Matte, Silky smooth formula, Smooth, Smooth Application, Smudgeproof, sugar_type_0, Transferproof, Travel friendly, Under 1000, Vegan, Vegetarian, Water resistant, With Brush",
              admin_graphql_api_id: "gid://shopify/Product/4702988304467",
              variants: [
                {
                  id: 32283958706259,
                  product_id: 4702988304467,
                  title: "Default Title",
                  price: "699.00",
                  sku: "8904320706302",
                  position: 1,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "Default Title",
                  option2: null,
                  option3: null,
                  created_at: "2020-10-22T16:04:07+05:30",
                  updated_at: "2021-01-13T11:03:01+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: null,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 34065975574611,
                  inventory_quantity: 7749,
                  old_inventory_quantity: 7749,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/32283958706259",
                },
              ],
              options: [
                {
                  id: 6066392793171,
                  product_id: 4702988304467,
                  name: "Title",
                  position: 1,
                  values: ["Default Title"],
                },
              ],
              images: [
                {
                  id: 15279034892371,
                  product_id: 4702988304467,
                  position: 1,
                  created_at: "2020-10-26T12:12:15+05:30",
                  updated_at: "2020-10-26T13:31:28+05:30",
                  alt:
                    "SUGAR Cosmetics Graphic Jam 36HR Eyeliner - 01 Blackest Black",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-graphic-jam-36hr-eyeliner-01-blackest-black-15278933278803.jpg?v=1603699288",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15279034892371",
                },
                {
                  id: 15279024865363,
                  product_id: 4702988304467,
                  position: 2,
                  created_at: "2020-10-26T12:11:09+05:30",
                  updated_at: "2020-10-26T13:31:28+05:30",
                  alt:
                    "SUGAR Cosmetics Graphic Jam 36HR Eyeliner - 01 Blackest Black",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-graphic-jam-36hr-eyeliner-01-blackest-black-15278933344339.jpg?v=1603699288",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15279024865363",
                },
                {
                  id: 15278970699859,
                  product_id: 4702988304467,
                  position: 3,
                  created_at: "2020-10-26T11:59:44+05:30",
                  updated_at: "2020-10-26T13:31:28+05:30",
                  alt:
                    "SUGAR Cosmetics Graphic Jam 36HR Eyeliner - 01 Blackest Black",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-graphic-jam-36hr-eyeliner-01-blackest-black-15278933311571.jpg?v=1603699288",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15278970699859",
                },
                {
                  id: 15279318859859,
                  product_id: 4702988304467,
                  position: 4,
                  created_at: "2020-10-26T13:31:27+05:30",
                  updated_at: "2020-10-26T13:31:28+05:30",
                  alt:
                    "SUGAR Cosmetics Graphic Jam 36HR Eyeliner - 01 Blackest Black",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-graphic-jam-36hr-eyeliner-01-blackest-black-15279310897235.jpg?v=1603699288",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15279318859859",
                },
                {
                  id: 15278969356371,
                  product_id: 4702988304467,
                  position: 5,
                  created_at: "2020-10-26T11:59:13+05:30",
                  updated_at: "2020-10-26T13:31:28+05:30",
                  alt:
                    "SUGAR Cosmetics Graphic Jam 36HR Eyeliner - 01 Blackest Black",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-graphic-jam-36hr-eyeliner-01-blackest-black-15278933409875.jpg?v=1603699288",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15278969356371",
                },
              ],
              image: {
                id: 15279034892371,
                product_id: 4702988304467,
                position: 1,
                created_at: "2020-10-26T12:12:15+05:30",
                updated_at: "2020-10-26T13:31:28+05:30",
                alt:
                  "SUGAR Cosmetics Graphic Jam 36HR Eyeliner - 01 Blackest Black",
                width: 1025,
                height: 1400,
                src:
                  "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-graphic-jam-36hr-eyeliner-01-blackest-black-15278933278803.jpg?v=1603699288",
                variant_ids: [],
                admin_graphql_api_id:
                  "gid://shopify/ProductImage/15279034892371",
              },
            },
            mediaText: null,
            redirectUrl: null,
          },
          {
            id: 96,
            sectionId: 4,
            webViewTitle: null,
            contentType: 3,
            mediaUrl: null,
            product_json: {
              id: 4720844374099,
              title: "GET SET GLOW KIT",
              body_html:
                'Beauty basics that do it all in a jiffy! SUGAR introduces an exciting set of makeup and skin rejuvenating essentials that offer you the brightest, glowiest skin you\'ve ever dreamed of. Whether you are a beauty maximalistic or a quickie facewash-and-sweep-of-lip balm kinda beginner minimalist, these goodies promise fresh, hydrated and healthy skin, and are a must in your daily getting ready routine. These radiant skin essentials come in cutesy pastel pouch that is perfect to show off wherever you head.<br data-mce-fragment="1"><br data-mce-fragment="1">SUGAR Get, Set, Glow Kit includes a refreshing priming moisturizer that breathes life into stressed skin, giving it a dewy, balanced finish along with an skin illuminating moisturizer that offers radiant finish. These products are packed with natural ingredients and are must-try. This beauty kit also includes refreshing sheet mask, pore cleansing masks to choose from along with skin soothing cleansing water with flower, herbal root and fruit extracts that effectively and gently cleans all makeup, skin impurities and gives fresh, revived skin!<br data-mce-fragment="1"><br data-mce-fragment="1">So all ladies out there, make your skin glow naturally, no matter how much time you have: one or two weeks, overnight, or even minutes with budget-friendly, all-in-one Get, Set, Glow Kit!<br data-mce-fragment="1"><br data-mce-fragment="1"><strong>Kit includes:</strong><br data-mce-fragment="1"><br data-mce-fragment="1">- Aquaholic Priming Moisturizer x 1 (30ml)<br data-mce-fragment="1">- Tipsy Lips Moisturizing Lip Balm - Pick any 1 variant from 2 (4.5gms)<br data-mce-fragment="1">- Swipe Right Cleansing Water x 1 (100ml)<br data-mce-fragment="1"><br data-mce-fragment="1">Pick any 1 of the following sheet masks:<br data-mce-fragment="1"><br data-mce-fragment="1">- Cheat Sheet Clarifying Mask x 1 (24g)<br data-mce-fragment="1">- Cheat Sheet Anti-Aging Mask x 1 (24g)<br data-mce-fragment="1"><br data-mce-fragment="1">Pick any 1 of the following bestsellers:<br data-mce-fragment="1"><br data-mce-fragment="1">- Bling Leader Illuminating Moisturizer  (25ml)<br data-mce-fragment="1">- Power Clay 3-Min Pore Cleansing Mask (50ml)<br data-mce-fragment="1"><br data-mce-fragment="1"><br data-mce-fragment="1"><strong>Why you\'ll love this kit:</strong><br data-mce-fragment="1"><br data-mce-fragment="1">- The moisturizers included in this kit prep, nourish and add that extra glow to your skin<br data-mce-fragment="1">- Sheet masks included effectively hydrate your complexion, leaving it looking more plumped and glowy<br data-mce-fragment="1">- Skin soothing cleansing water made up of active ingredients helps in deep cleansing, soothing and detoxification of skin<br data-mce-fragment="1">- Liven up your lips with skin soothing lip balms included in this kit that have the goodness of shea butter and jojoba oil. Just slip one into your bag before you hit the town!<br data-mce-fragment="1"><br data-mce-fragment="1"><strong>Maximum Retail Price: Rs.</strong> 1,695 (incl. all taxes)<br data-mce-fragment="1"><br data-mce-fragment="1"><strong>SUGAR Aquaholic Priming Moisturizer</strong><br data-mce-fragment="1">Country of Origin: PRC<br data-mce-fragment="1">Company Name: SUGAR Cosmetics LLC<br data-mce-fragment="1">Company Address: 8 The Green Suite A, Dover, DE 19901, USA<br data-mce-fragment="1"><br data-mce-fragment="1"><strong>SUGAR Cheat Sheet Masks</strong><br data-mce-fragment="1">Country of Origin: Korea<br data-mce-fragment="1">Company Name: SUGAR Cosmetics LLC<br data-mce-fragment="1">Company Address: 8 The Green Suite A, Dover, DE 19901, USA<br data-mce-fragment="1"><br data-mce-fragment="1"><strong>SUGAR Swipe Right Cleansing Water</strong><br data-mce-fragment="1">Country of Origin: Korea<br data-mce-fragment="1">Company Name: SUGAR Cosmetics LLC<br data-mce-fragment="1">Company Address: 8 The Green Suite A, Dover, DE 19901, USA<br data-mce-fragment="1"><br data-mce-fragment="1"><strong>SUGAR Tipsy Lips Moisturizing Lip Balm</strong><br data-mce-fragment="1">Country of Origin: India<br data-mce-fragment="1">Company Name: Viva Cosmetics<br data-mce-fragment="1">Company Address: C-1 / 1 &amp; 2 RIC, Asangaon, Shahpur, Thane - 421601<br data-mce-fragment="1"><br data-mce-fragment="1"><strong>SUGAR Bling Leader Illuminating Moisturizer</strong><br data-mce-fragment="1">Country of Origin: PRC<br data-mce-fragment="1">Company Name: SUGAR Cosmetics LLC.<br data-mce-fragment="1">Company Address: 8 The Green Suite A, Dover, DE 19901, USA<br data-mce-fragment="1"><br data-mce-fragment="1"><strong>SUGAR Power Clay 3-Min Pore Cleansing Mask</strong><br data-mce-fragment="1">Country of Origin: PRC<br data-mce-fragment="1">Company Name: SUGAR Cosmetics LLC<br data-mce-fragment="1">Company Address: 8 The Green Suite A, Dover, DE 19901, USA\n<h5>How to apply</h5>\n<p>- Apply a pea-sized amount of the priming moisturizer and gently massage into your face by using gentle upward and outward strokes. Use alone as a moisturizer or as a prep step before applying makeup. This 2 in 1 product acts as a primer and a moisturizer &amp; can be used every day.<br data-mce-fragment="1"><br data-mce-fragment="1">- Apply the illuminating moisturizer directly to your skin for a radiant glow. Or mix it with moisturizer or foundation for a subtle glow. It can also be used as a highlighter. Perfect for both workwear and a party look.<br data-mce-fragment="1"><br data-mce-fragment="1">- Apply a generous layer of pore cleansing mask onto dry, clean skin. Let it sit for 3 minutes till it dries. Wipe off with a clean tissue or cloth. Rinse with warm water. Use twice or thrice a week for clear skin.<br data-mce-fragment="1"><br data-mce-fragment="1">- You can use the sheet masks alternate days. After washing the face, gently apply toner to the face. Open the top part of pouch and take out the mask sheet. Unfold it softly and put it tightly on the face. Apply the mask about 15-20 minutes. After the essence is absorbed into skin, take off the sheet mask and apply essence or cream.<br data-mce-fragment="1"><br data-mce-fragment="1">- Apply the moisturizing lip balm directly onto lips after exfoliating or whenever your lips feel dry.</p>',
              vendor: "SUGAR Cosmetics",
              product_type: "Kit",
              created_at: "2020-12-07T12:08:09+05:30",
              handle: "get-set-glow-kit",
              updated_at: "2021-01-13T12:36:25+05:30",
              published_at: "2021-01-01T15:41:52+05:30",
              template_suffix: "",
              published_scope: "web",
              tags: "Kit, makeup kit, view_product",
              admin_graphql_api_id: "gid://shopify/Product/4720844374099",
              variants: [
                {
                  id: 32333447462995,
                  product_id: 4720844374099,
                  title: "Default Title",
                  price: "1299.00",
                  sku: "8904320715717",
                  position: 1,
                  inventory_policy: "deny",
                  compare_at_price: "1695.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "Default Title",
                  option2: null,
                  option3: null,
                  created_at: "2020-12-07T12:08:09+05:30",
                  updated_at: "2021-01-13T12:36:25+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: null,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 34115542024275,
                  inventory_quantity: 766,
                  old_inventory_quantity: 766,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/32333447462995",
                },
              ],
              options: [
                {
                  id: 6091392188499,
                  product_id: 4720844374099,
                  name: "Title",
                  position: 1,
                  values: ["Default Title"],
                },
              ],
              images: [
                {
                  id: 15477079277651,
                  product_id: 4720844374099,
                  position: 1,
                  created_at: "2020-12-07T12:11:31+05:30",
                  updated_at: "2020-12-07T15:48:07+05:30",
                  alt: "SUGAR Cosmetics Get Set Glow Kit",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-get-set-glow-kit-15477074067539.jpg?v=1607336287",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15477079277651",
                },
                {
                  id: 15477732704339,
                  product_id: 4720844374099,
                  position: 2,
                  created_at: "2020-12-07T14:52:01+05:30",
                  updated_at: "2020-12-07T15:48:07+05:30",
                  alt: "SUGAR Cosmetics GET SET GLOW KIT",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-get-set-glow-kit-15477717368915.jpg?v=1607336287",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15477732704339",
                },
                {
                  id: 15477080883283,
                  product_id: 4720844374099,
                  position: 3,
                  created_at: "2020-12-07T12:12:03+05:30",
                  updated_at: "2020-12-07T15:48:07+05:30",
                  alt: "SUGAR Cosmetics Get Set Glow Kit",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-get-set-glow-kit-15477077278803.jpg?v=1607336287",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15477080883283",
                },
                {
                  id: 15477082783827,
                  product_id: 4720844374099,
                  position: 4,
                  created_at: "2020-12-07T12:12:37+05:30",
                  updated_at: "2020-12-07T15:48:07+05:30",
                  alt: "SUGAR Cosmetics GET SET GLOW KIT",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-get-set-glow-kit-15477077475411.jpg?v=1607336287",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15477082783827",
                },
                {
                  id: 15477096710227,
                  product_id: 4720844374099,
                  position: 5,
                  created_at: "2020-12-07T12:16:32+05:30",
                  updated_at: "2020-12-07T15:48:07+05:30",
                  alt: "SUGAR Cosmetics GET SET GLOW KIT",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-get-set-glow-kit-15477078032467.jpg?v=1607336287",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15477096710227",
                },
                {
                  id: 15477902409811,
                  product_id: 4720844374099,
                  position: 6,
                  created_at: "2020-12-07T15:45:39+05:30",
                  updated_at: "2020-12-07T15:48:07+05:30",
                  alt: "SUGAR Cosmetics GET SET GLOW KIT",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-get-set-glow-kit-15477897461843.jpg?v=1607336287",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15477902409811",
                },
                {
                  id: 15477092843603,
                  product_id: 4720844374099,
                  position: 7,
                  created_at: "2020-12-07T12:15:26+05:30",
                  updated_at: "2020-12-07T15:48:07+05:30",
                  alt: "SUGAR Cosmetics GET SET GLOW KIT",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-get-set-glow-kit-15477079408723.jpg?v=1607336287",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15477092843603",
                },
                {
                  id: 15477104705619,
                  product_id: 4720844374099,
                  position: 8,
                  created_at: "2020-12-07T12:18:08+05:30",
                  updated_at: "2020-12-07T15:48:07+05:30",
                  alt: "SUGAR Cosmetics GET SET GLOW KIT",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-get-set-glow-kit-15477076918355.jpg?v=1607336287",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15477104705619",
                },
                {
                  id: 15477101527123,
                  product_id: 4720844374099,
                  position: 9,
                  created_at: "2020-12-07T12:17:36+05:30",
                  updated_at: "2020-12-07T15:48:07+05:30",
                  alt: "SUGAR Cosmetics GET SET GLOW KIT",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-get-set-glow-kit-15477076754515.jpg?v=1607336287",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15477101527123",
                },
                {
                  id: 15477909487699,
                  product_id: 4720844374099,
                  position: 10,
                  created_at: "2020-12-07T15:48:06+05:30",
                  updated_at: "2020-12-07T15:48:07+05:30",
                  alt: "SUGAR Cosmetics GET SET GLOW KIT",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-get-set-glow-kit-15477904441427.jpg?v=1607336287",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15477909487699",
                },
                {
                  id: 15477907849299,
                  product_id: 4720844374099,
                  position: 11,
                  created_at: "2020-12-07T15:47:32+05:30",
                  updated_at: "2020-12-07T15:48:07+05:30",
                  alt: "SUGAR Cosmetics GET SET GLOW KIT",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-get-set-glow-kit-15477905588307.jpg?v=1607336287",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15477907849299",
                },
                {
                  id: 15477728149587,
                  product_id: 4720844374099,
                  position: 12,
                  created_at: "2020-12-07T14:51:27+05:30",
                  updated_at: "2020-12-07T15:48:07+05:30",
                  alt: "SUGAR Cosmetics GET SET GLOW KIT",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-get-set-glow-kit-15477718089811.jpg?v=1607336287",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15477728149587",
                },
                {
                  id: 15477086158931,
                  product_id: 4720844374099,
                  position: 13,
                  created_at: "2020-12-07T12:13:28+05:30",
                  updated_at: "2020-12-07T15:48:07+05:30",
                  alt: "SUGAR Cosmetics GET SET GLOW KIT",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-get-set-glow-kit-15477079507027.jpg?v=1607336287",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15477086158931",
                },
                {
                  id: 15477895954515,
                  product_id: 4720844374099,
                  position: 14,
                  created_at: "2020-12-07T15:43:30+05:30",
                  updated_at: "2020-12-07T15:48:07+05:30",
                  alt: "SUGAR Cosmetics GET SET GLOW KIT",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-get-set-glow-kit-15477890383955.jpg?v=1607336287",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15477895954515",
                },
              ],
              image: {
                id: 15477079277651,
                product_id: 4720844374099,
                position: 1,
                created_at: "2020-12-07T12:11:31+05:30",
                updated_at: "2020-12-07T15:48:07+05:30",
                alt: "SUGAR Cosmetics Get Set Glow Kit",
                width: 1025,
                height: 1400,
                src:
                  "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-get-set-glow-kit-15477074067539.jpg?v=1607336287",
                variant_ids: [],
                admin_graphql_api_id:
                  "gid://shopify/ProductImage/15477079277651",
              },
            },
            mediaText: null,
            redirectUrl: null,
          },
          {
            id: 91,
            sectionId: 4,
            webViewTitle: null,
            contentType: 3,
            mediaUrl: null,
            product_json: {
              id: 4718380679251,
              title: "Bride Tribe Kit",
              body_html:
                'Now ace gorgeous beauty looks that complement any wedding aesthetic and get your Insta-worthy moments perfect between all busy wedding preparations. Flaunt the superbly fancy and all chic - Bride Tribe Kit by SUGAR. Maid of honour, bridesmaid or a close pal to the bride, this beauty kit is lovingly tailored for everyone’s beauty needs. It’s a customisable one and includes 6 awesome beauty essentials to look perfect for wedding or party!<br data-mce-fragment="1"><br data-mce-fragment="1">SUGAR Bride Tribe Kit includes a stunning mini lipstick perfect for a fabulous pout (4 shades to choose from), a setting powder (2 variants to pick from) for a flawless complexion, a pretty blush for that beautiful flush of colour to cheeks (3 shades to choose from), a radiant highlighter (3 shades to pick from) for that divine glow, a heavy-duty kohl (in black with silver glitter ) and a refreshing sheet mask (2 variants to choose from) that keeps your skin looking fresh all day. All these strikingly eye-catchy items come with a shimmery sequin pouch that exemplifies your overall look!<br data-mce-fragment="1"><br data-mce-fragment="1"><strong>Kit includes:</strong><br data-mce-fragment="1"><br data-mce-fragment="1">- Smudge Me Not Liquid Lipstick Mini x 1 (1.1ml)<br data-mce-fragment="1">- All Set To Go Banana Powder (7g) or Set The Tone Tinted Powder (15g) x 1<br data-mce-fragment="1">- Contour De Force Mini Blush x 1 (4g)<br data-mce-fragment="1">- Contour De Force Mini Highlighter x 1 (4g)<br data-mce-fragment="1">- Stroke Of Genius Heavy-Duty Kohl x 1 (1.2g)<br data-mce-fragment="1">- Cheat Sheet Clarifying Mask/Cheat Sheet Anti-Aging Mask X pick any 1 (24g each)<br data-mce-fragment="1"><br data-mce-fragment="1"><strong>Why you\'ll love this kit!</strong><br data-mce-fragment="1"><br data-mce-fragment="1">- Richly pigmented lipstick that stays all day long<br data-mce-fragment="1">- Intense on shine highlighter that gives a remarkable glow <br data-mce-fragment="1">- Multipurpose setting powder that lends a matte finish of your dreams<br data-mce-fragment="1">- A gorgeous blush that gives a perfect pop of colour to your cheeks<br data-mce-fragment="1">- Intense, bold kohl that enhances your eye makeup looks<br data-mce-fragment="1">- Ultra-hydrating sheet masks that keep your skin fresh and glowing<br data-mce-fragment="1"><br data-mce-fragment="1"><strong>Maximum Retail Price</strong>: Rs. 1499 (incl. all taxes)<br data-mce-fragment="1"><br data-mce-fragment="1"><strong data-mce-fragment="1">SUGAR Smudge Me Not Mini Liquid Lipstick</strong><br data-mce-fragment="1">Country of Origin: India<br data-mce-fragment="1">Company Name: Viva Cosmetics<br data-mce-fragment="1">Company Address: A/19 G4 Happy Valley, Nr Tikujiniwadi, Manpada, Thane W - 400610<br data-mce-fragment="1"><br data-mce-fragment="1"><strong>SUGAR Stroke Of Genius Heavy-Duty Kohl</strong><br data-mce-fragment="1">Country of Origin: Germany<br data-mce-fragment="1">Company Name: Schwan Cosmetics Germany Gmbh &amp; Co.Kg<br data-mce-fragment="1">Company Address: Schwanweg 1, 90562 Heroldsberg - (Germany)<br data-mce-fragment="1"><br data-mce-fragment="1"><strong>SUGAR Contour De Force Mini Highlighter</strong><br data-mce-fragment="1">Country of Origin: Italy<br data-mce-fragment="1">Company Name: Regi India Cosmetics Pvt Ltd.<br data-mce-fragment="1">Company Address: C-08, Sara Ind Estate, Vill-Rampur, Selaqui, Dehradun 248197, Uttarakhand.<br data-mce-fragment="1"> <br data-mce-fragment="1"><strong>SUGAR Contour De Force Mini Blush</strong><br data-mce-fragment="1">Country of Origin: Italy<br data-mce-fragment="1">Company Name: Regi India Cosmetics Pvt Ltd.<br data-mce-fragment="1">Company Address: C-08, Sara Ind Estate, Vill-Rampur, Selaqui, Dehradun 248197, Uttarakhand.<br data-mce-fragment="1"><br data-mce-fragment="1"><strong>SUGAR All Set To Go Banana Powder/ Set The Tone Tinted Powder</strong><br data-mce-fragment="1">Country of Origin: PRC<br data-mce-fragment="1">Company Name: SUGAR Cosmetics LLC<br data-mce-fragment="1">Company Address: 8 The Green Suite A, Dover, DE 19901, USA.<br data-mce-fragment="1"> <br data-mce-fragment="1"><strong>SUGAR Cheat Sheet Clarifying Mask/Cheat Sheet Anti-Aging Mask</strong><br data-mce-fragment="1">Country of Origin: Korea<br data-mce-fragment="1">Company Name: SUGAR Cosmetics LLC<br data-mce-fragment="1">Company Address: 8 The Green Suite A, Dover, DE 19901, USA<br data-mce-fragment="1"><br data-mce-fragment="1"><br data-mce-fragment="1"><strong>How to Apply:</strong><br data-mce-fragment="1"><br data-mce-fragment="1">- Apply the liquid lipstick straight from the wand. With the edge, trace your lip line and fill it in. Keep lips apart for a few seconds to allow the lipstick to dry.<br data-mce-fragment="1"><br data-mce-fragment="1">- Gently rim your lower and upper lash lines with the creamy kohl. Open your eyes wide for precise application.<br data-mce-fragment="1"><br data-mce-fragment="1">- Apply the multitasking highlighter to the parts of your face that light would naturally hit, to enhance its natural structure and add radiance to your complexion.<br data-mce-fragment="1"><br data-mce-fragment="1">- Tap the brush into the blush pan and flick the stem of the brush to knock off any excess product. Lightly tap on your cheek, and then blend using soft swirling motions.<br data-mce-fragment="1"><br data-mce-fragment="1">- Prep your face with a little primer or moisturizer. Apply concealer &amp; foundation if required. Tap the tinted powder\'s container base to release powder. Use the puff to dab a little product or swirl a brush into the powder for smooth application. Gently blow off the excess for a natural matte finish. For baking, let the powder set for around 3-5 minutes before dusting it off.<br data-mce-fragment="1"><br data-mce-fragment="1">- You can also try the Banana setting powder under eyes or over your face using a puff or a powder brush. <br data-mce-fragment="1"><br data-mce-fragment="1">- Sheet masks can be used anytime during the day to refresh your skin. After washing your face, gently apply some toner. Open the top part of pouch and take out the mask sheet. Unfold it softly and put it tightly on the face. Apply the mask for about 15-20 minutes. After the essence is absorbed into skin, take off the sheet mask and apply essence or cream.',
              vendor: "SUGAR Cosmetics",
              product_type: "Kit",
              created_at: "2020-11-30T18:59:19+05:30",
              handle: "bride-tribe-kit",
              updated_at: "2021-01-13T12:35:36+05:30",
              published_at: "2020-12-21T10:41:31+05:30",
              template_suffix: "",
              published_scope: "web",
              tags: "Kit, makeup kit, view_product",
              admin_graphql_api_id: "gid://shopify/Product/4718380679251",
              variants: [
                {
                  id: 32326349324371,
                  product_id: 4718380679251,
                  title: "Default Title",
                  price: "1499.00",
                  sku: "8904320715595",
                  position: 1,
                  inventory_policy: "deny",
                  compare_at_price: "2294.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "Default Title",
                  option2: null,
                  option3: null,
                  created_at: "2020-11-30T18:59:20+05:30",
                  updated_at: "2021-01-13T11:50:46+05:30",
                  taxable: true,
                  barcode: "",
                  grams: 0,
                  image_id: null,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 34108441821267,
                  inventory_quantity: 2896,
                  old_inventory_quantity: 2896,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/32326349324371",
                },
              ],
              options: [
                {
                  id: 6088030847059,
                  product_id: 4718380679251,
                  name: "Title",
                  position: 1,
                  values: ["Default Title"],
                },
              ],
              images: [
                {
                  id: 15446737420371,
                  product_id: 4718380679251,
                  position: 1,
                  created_at: "2020-12-01T09:57:47+05:30",
                  updated_at: "2020-12-01T10:14:02+05:30",
                  alt: "SUGAR Cosmetics Bride Tribe Kit",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-bride-tribe-kit-15446728015955.jpg?v=1606797842",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15446737420371",
                },
                {
                  id: 15446747349075,
                  product_id: 4718380679251,
                  position: 2,
                  created_at: "2020-12-01T10:06:20+05:30",
                  updated_at: "2020-12-01T10:14:02+05:30",
                  alt: "SUGAR Cosmetics Bride Tribe Kit",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-bride-tribe-kit-15446730375251.jpg?v=1606797842",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15446747349075",
                },
                {
                  id: 15446738403411,
                  product_id: 4718380679251,
                  position: 3,
                  created_at: "2020-12-01T09:58:21+05:30",
                  updated_at: "2020-12-01T10:14:02+05:30",
                  alt: "SUGAR Cosmetics Bride Tribe Kit",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-bride-tribe-kit-15446728704083.jpg?v=1606797842",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15446738403411",
                },
                {
                  id: 15446739124307,
                  product_id: 4718380679251,
                  position: 4,
                  created_at: "2020-12-01T09:58:52+05:30",
                  updated_at: "2020-12-01T10:14:02+05:30",
                  alt: "SUGAR Cosmetics Bride Tribe Kit",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-bride-tribe-kit-15446729162835.jpg?v=1606797842",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15446739124307",
                },
                {
                  id: 15446747873363,
                  product_id: 4718380679251,
                  position: 5,
                  created_at: "2020-12-01T10:06:53+05:30",
                  updated_at: "2020-12-01T10:14:02+05:30",
                  alt: "SUGAR Cosmetics Bride Tribe Kit",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-bride-tribe-kit-15446730080339.jpg?v=1606797842",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15446747873363",
                },
                {
                  id: 15446751838291,
                  product_id: 4718380679251,
                  position: 6,
                  created_at: "2020-12-01T10:10:54+05:30",
                  updated_at: "2020-12-01T10:14:02+05:30",
                  alt: "SUGAR Cosmetics Bride Tribe Kit",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-bride-tribe-kit-15446729719891.jpg?v=1606797842",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15446751838291",
                },
                {
                  id: 15446756196435,
                  product_id: 4718380679251,
                  position: 7,
                  created_at: "2020-12-01T10:14:01+05:30",
                  updated_at: "2020-12-01T10:14:02+05:30",
                  alt: "SUGAR Cosmetics Bride Tribe Kit",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-bride-tribe-kit-15446729424979.jpg?v=1606797842",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15446756196435",
                },
                {
                  id: 15446743187539,
                  product_id: 4718380679251,
                  position: 8,
                  created_at: "2020-12-01T10:03:01+05:30",
                  updated_at: "2020-12-01T10:14:02+05:30",
                  alt: "SUGAR Cosmetics Bride Tribe Kit",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-bride-tribe-kit-15446728441939.jpg?v=1606797842",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15446743187539",
                },
                {
                  id: 15446742958163,
                  product_id: 4718380679251,
                  position: 9,
                  created_at: "2020-12-01T10:02:29+05:30",
                  updated_at: "2020-12-01T10:14:02+05:30",
                  alt: "SUGAR Cosmetics Bride Tribe Kit",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-bride-tribe-kit-15446728474707.jpg?v=1606797842",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15446742958163",
                },
                {
                  id: 15446745972819,
                  product_id: 4718380679251,
                  position: 10,
                  created_at: "2020-12-01T10:05:48+05:30",
                  updated_at: "2020-12-01T10:14:02+05:30",
                  alt: "SUGAR Cosmetics Bride Tribe Kit",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-bride-tribe-kit-15446740041811.jpg?v=1606797842",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15446745972819",
                },
                {
                  id: 15446751510611,
                  product_id: 4718380679251,
                  position: 11,
                  created_at: "2020-12-01T10:10:25+05:30",
                  updated_at: "2020-12-01T10:14:02+05:30",
                  alt: "SUGAR Cosmetics Bride Tribe Kit",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-bride-tribe-kit-15446730801235.jpg?v=1606797842",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15446751510611",
                },
                {
                  id: 15446750330963,
                  product_id: 4718380679251,
                  position: 12,
                  created_at: "2020-12-01T10:09:48+05:30",
                  updated_at: "2020-12-01T10:14:02+05:30",
                  alt: "SUGAR Cosmetics Bride Tribe Kit",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-bride-tribe-kit-15446730899539.jpg?v=1606797842",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15446750330963",
                },
                {
                  id: 15446742106195,
                  product_id: 4718380679251,
                  position: 13,
                  created_at: "2020-12-01T10:01:56+05:30",
                  updated_at: "2020-12-01T10:14:02+05:30",
                  alt: "SUGAR Cosmetics Bride Tribe Kit",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-bride-tribe-kit-15446731391059.jpg?v=1606797842",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15446742106195",
                },
              ],
              image: {
                id: 15446737420371,
                product_id: 4718380679251,
                position: 1,
                created_at: "2020-12-01T09:57:47+05:30",
                updated_at: "2020-12-01T10:14:02+05:30",
                alt: "SUGAR Cosmetics Bride Tribe Kit",
                width: 1025,
                height: 1400,
                src:
                  "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-bride-tribe-kit-15446728015955.jpg?v=1606797842",
                variant_ids: [],
                admin_graphql_api_id:
                  "gid://shopify/ProductImage/15446737420371",
              },
            },
            mediaText: null,
            redirectUrl: null,
          },
          {
            id: 177,
            sectionId: 4,
            webViewTitle: null,
            contentType: 3,
            mediaUrl: null,
            product_json: {
              id: 2350278148179,
              title: "Smudge Me Not Liquid Lipstick Minis Set",
              body_html:
                '<p>Ladies, your wish is our command! We are here with the most-wanted, most wished for lipstick minis – And that too, as a CUSTOMISABLE SET. Yeah, you read it right. <strong>SUGAR Smudge Me Not Liquid Lipstick Minis Set</strong> is here!</p>\n<p>And, IT IS the treasure of your dreams. The cult-favourites, coveted colours of the “one-coat wonder” <strong>Smudge Me Not Liquid Lipstick</strong> range have become exponentially cuter now, in miniature form. Not just that, these teeny-tiny, super instagrammable versions of the lovable lip range give everyone bigger bang for the buck as they are sold in a specially-crafted, classy, black matte box that’s super stylish and travel friendly – the perfect box to gift to your girlfriends or the best treat for self-indulgence, you are never going to run out of options with this one, and that’s not all. With this on-the-go customizable fab box in your bag, say bye to all the lippie hassles and say hi to gorgeous, sorted lips!</p>\n<p>Now, you get everything you would ever need to get those ‘lips of your dreams’ in one chic place. With these stunning and bold lipcolours that pep your pout up, slay every day, all day!</p>\n<p>Checkout and choose from 24 cutesy minis with stunning shades that are perfect for all skin tones.</p>\n<ul>\n<li><strong>01 Brazen Raisin (Burgundy)</strong></li>\n<li><strong>02 Brink Of Pink (Plum Rose)</strong></li>\n<li><strong>03 Tan Fan (Mauve Nude)</strong></li>\n<li><strong>04 Plum Yum (Muted Plum)</strong></li>\n<li><strong>05 Rust Lust (Red Terracotta)</strong></li>\n<li><strong><span>06 Tangerine Queen (Orange Coral)</span></strong></li>\n<li><strong>07 Rethink Pink (Fuchsia)</strong></li>\n<li><strong>08 Wine And Shine (Sangria)</strong></li>\n<li><strong>09 Suave Mauve (<span>Mauve)</span></strong></li>\n<li><strong>10 Drop Dead Red (Red)</strong></li>\n<li><strong>12 Don Fawn (Yellow Brown)</strong></li>\n<li><strong>13 Wooed By Nude (Peach Nude)</strong></li>\n<li><strong>14 Teak Mystique (Warm Brown)</strong></li>\n<li><strong>17 Fiery Berry (Marsala)</strong></li>\n<li><strong>21 Aubergine Queen <span>(Blackened Burgundy)</span></strong></li>\n<li><strong>25 Very Mulberry<span> (Deep Berry)</span></strong></li>\n<li><strong>29 Scarlet Starlet<span> (Orange Red)</span></strong></li>\n<li><strong>30 Peony Genie <span>(Medium Pink)</span></strong></li>\n<li><strong><span>37 Hot Apricot (Peachy Nude)</span></strong></li>\n<li><strong><span>38 Dose Of Rose (Rosy Mauve)</span></strong></li>\n<li><strong><span>39 Pink Sync (Rosy Magenta)</span></strong></li>\n<li><strong><span>42 Toast Roast (Deep Reddish Brown)</span></strong></li>\n<li><strong><span>43 Hot Shot (Hot Pink / Dark Fuchsia Pink)</span></strong></li>\n<li><strong><span>44 Preach Peach (Peach Pink)</span></strong></li>\n</ul>\n<p>So ladies, Why have 1 when you can get your hands on 4?</p>\n<p><strong>NET VOLUME: (1.1ml/ 0.03 Fl.Oz.) X 4</strong></p>\n<p><span><strong>Note</strong>: Any single product in the kit/set cannot be replaced or returned.</span></p>\n<p><span><b data-stringify-type="bold">Maximum Retail Price:</b> RS. 999 (incl. all taxes)<br><b data-stringify-type="bold">Country of Origin</b>: India<br><b data-stringify-type="bold">Company Name</b>: Viva Cosmetics<br><b data-stringify-type="bold">Company Address</b>: A/19 G4 Happy Valley, Nr Tikujiniwadi, Manpada, Thane W - 400610</span></p>',
              vendor: "SUGAR Cosmetics",
              product_type: "Gifts & Sets",
              created_at: "2018-11-28T10:42:45+05:30",
              handle: "smudge-me-not-liquid-lipstick-minis-set",
              updated_at: "2021-01-13T13:05:08+05:30",
              published_at: "2019-09-19T13:23:46+05:30",
              template_suffix: "",
              published_scope: "web",
              tags:
                "10 HR, 12 HR, 24 HR, Affordable, Alcohol Free, amp-hide, Anniversary, Bestseller, Birthday, Bold, Budget, Budget Friendly, Classy, College, Combo, Cost Effective, Cruelty Free, Date-proof, Deals, Dermatologically Tested, Dry matte, Easy Application, Economical, Fadeproof, Gift, Gifting, Gifts for her, Girlfriend, Glides Effortlessly, Gluten Free, High Coverage, Intensely Pigmented, Kissproof, Lips, Lipstick, Lipstick Combo, Liquid Lip, Liquid Lipstick, Long Lasting, Longwear, Love, Low Cost, Low Priced, Marriage, Matte, Matte lipstick, Mineral Oil Free, Mini, Miniature, Mom, Money Saving, Mother, NO-OOS, Nominal, Non budge, One-coat wonder, Opaque, Pigmented, Present, Presents for Her, Reasonable, Save Money, Savings, Single Swipe, Sister, Small, Smudge Me Not, Smudgeproof, st_bestseller:1, st_gifting filter: Trending Gifts Minis Set, st_hot:, st_popularityscore: 9000, Steal Deal, sugar_type_2, Superstay, Transferproof, Travel Friendly, Ultra-matte, Valentine, Valentine's Day, Value for Money, Vegan, Vegetarian, view_product, Vitamin E, Water Resistant, Waterproof, Wedding, Wife, Worth The Money, Worthy, Zero featherings",
              admin_graphql_api_id: "gid://shopify/Product/2350278148179",
              variants: [
                {
                  id: 21206334177363,
                  product_id: 2350278148179,
                  title: "Default Title",
                  price: "799.00",
                  sku: "8904320700652",
                  position: 1,
                  inventory_policy: "deny",
                  compare_at_price: "999.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "Default Title",
                  option2: null,
                  option3: null,
                  created_at: "2018-11-28T10:42:46+05:30",
                  updated_at: "2021-01-13T13:00:18+05:30",
                  taxable: true,
                  barcode: "8904320700652",
                  grams: 0,
                  image_id: null,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 21708234489939,
                  inventory_quantity: 26858,
                  old_inventory_quantity: 26858,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/21206334177363",
                },
              ],
              options: [
                {
                  id: 3250281578579,
                  product_id: 2350278148179,
                  name: "Title",
                  position: 1,
                  values: ["Default Title"],
                },
              ],
              images: [
                {
                  id: 15520733331539,
                  product_id: 2350278148179,
                  position: 1,
                  created_at: "2020-12-16T19:57:23+05:30",
                  updated_at: "2020-12-16T20:34:01+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520724287571.png?v=1608131041",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15520733331539",
                },
                {
                  id: 15520736936019,
                  product_id: 2350278148179,
                  position: 2,
                  created_at: "2020-12-16T19:57:55+05:30",
                  updated_at: "2020-12-16T20:34:01+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520724582483.jpg?v=1608131041",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15520736936019",
                },
                {
                  id: 15520874758227,
                  product_id: 2350278148179,
                  position: 3,
                  created_at: "2020-12-16T20:17:26+05:30",
                  updated_at: "2020-12-16T20:34:01+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520725074003.jpg?v=1608131041",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15520874758227",
                },
                {
                  id: 15520876986451,
                  product_id: 2350278148179,
                  position: 4,
                  created_at: "2020-12-16T20:17:59+05:30",
                  updated_at: "2020-12-16T20:34:01+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520725663827.jpg?v=1608131041",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15520876986451",
                },
                {
                  id: 15520741130323,
                  product_id: 2350278148179,
                  position: 5,
                  created_at: "2020-12-16T19:58:30+05:30",
                  updated_at: "2020-12-16T20:34:01+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520730480723.jpg?v=1608131041",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15520741130323",
                },
                {
                  id: 15520885768275,
                  product_id: 2350278148179,
                  position: 6,
                  created_at: "2020-12-16T20:19:57+05:30",
                  updated_at: "2020-12-16T20:34:01+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520730775635.jpg?v=1608131041",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15520885768275",
                },
                {
                  id: 15520883605587,
                  product_id: 2350278148179,
                  position: 7,
                  created_at: "2020-12-16T20:19:23+05:30",
                  updated_at: "2020-12-16T20:34:01+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520731005011.jpg?v=1608131041",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15520883605587",
                },
                {
                  id: 15520819642451,
                  product_id: 2350278148179,
                  position: 8,
                  created_at: "2020-12-16T20:09:22+05:30",
                  updated_at: "2020-12-16T20:34:01+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520731562067.jpg?v=1608131041",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15520819642451",
                },
                {
                  id: 15520827670611,
                  product_id: 2350278148179,
                  position: 9,
                  created_at: "2020-12-16T20:10:29+05:30",
                  updated_at: "2020-12-16T20:34:01+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520731922515.jpg?v=1608131041",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15520827670611",
                },
                {
                  id: 15520837992531,
                  product_id: 2350278148179,
                  position: 10,
                  created_at: "2020-12-16T20:11:59+05:30",
                  updated_at: "2020-12-16T20:34:01+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520732676179.jpg?v=1608131041",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15520837992531",
                },
                {
                  id: 15520834191443,
                  product_id: 2350278148179,
                  position: 11,
                  created_at: "2020-12-16T20:11:27+05:30",
                  updated_at: "2020-12-16T20:34:01+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520733266003.jpg?v=1608131041",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15520834191443",
                },
                {
                  id: 15520822853715,
                  product_id: 2350278148179,
                  position: 12,
                  created_at: "2020-12-16T20:09:55+05:30",
                  updated_at: "2020-12-16T20:34:01+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520734347347.jpg?v=1608131041",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15520822853715",
                },
                {
                  id: 15520848674899,
                  product_id: 2350278148179,
                  position: 13,
                  created_at: "2020-12-16T20:13:26+05:30",
                  updated_at: "2020-12-16T20:34:01+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520736051283.jpg?v=1608131041",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15520848674899",
                },
                {
                  id: 15520899203155,
                  product_id: 2350278148179,
                  position: 14,
                  created_at: "2020-12-16T20:22:32+05:30",
                  updated_at: "2020-12-16T20:34:01+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520736673875.jpg?v=1608131041",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15520899203155",
                },
                {
                  id: 15520896516179,
                  product_id: 2350278148179,
                  position: 15,
                  created_at: "2020-12-16T20:22:01+05:30",
                  updated_at: "2020-12-16T20:34:01+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520737132627.jpg?v=1608131041",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15520896516179",
                },
                {
                  id: 15520912080979,
                  product_id: 2350278148179,
                  position: 16,
                  created_at: "2020-12-16T20:25:29+05:30",
                  updated_at: "2020-12-16T20:34:01+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520737984595.jpg?v=1608131041",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15520912080979",
                },
                {
                  id: 15520924926035,
                  product_id: 2350278148179,
                  position: 17,
                  created_at: "2020-12-16T20:27:25+05:30",
                  updated_at: "2020-12-16T20:34:01+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 600,
                  height: 819,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520738607187.png?v=1608131041",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15520924926035",
                },
                {
                  id: 15520893829203,
                  product_id: 2350278148179,
                  position: 18,
                  created_at: "2020-12-16T20:21:26+05:30",
                  updated_at: "2020-12-16T20:34:01+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520738738259.jpg?v=1608131041",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15520893829203",
                },
                {
                  id: 15520903725139,
                  product_id: 2350278148179,
                  position: 19,
                  created_at: "2020-12-16T20:23:28+05:30",
                  updated_at: "2020-12-16T20:34:01+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520739754067.jpg?v=1608131041",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15520903725139",
                },
                {
                  id: 15520793722963,
                  product_id: 2350278148179,
                  position: 20,
                  created_at: "2020-12-16T20:05:57+05:30",
                  updated_at: "2020-12-16T20:34:01+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520740081747.jpg?v=1608131041",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15520793722963",
                },
                {
                  id: 15520865714259,
                  product_id: 2350278148179,
                  position: 21,
                  created_at: "2020-12-16T20:15:56+05:30",
                  updated_at: "2020-12-16T20:34:01+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520740638803.jpg?v=1608131041",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15520865714259",
                },
                {
                  id: 15520746307667,
                  product_id: 2350278148179,
                  position: 22,
                  created_at: "2020-12-16T19:59:25+05:30",
                  updated_at: "2020-12-16T20:34:01+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520741785683.jpg?v=1608131041",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15520746307667",
                },
                {
                  id: 15520943308883,
                  product_id: 2350278148179,
                  position: 23,
                  created_at: "2020-12-16T20:30:30+05:30",
                  updated_at: "2020-12-16T20:34:01+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520749977683.jpg?v=1608131041",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15520943308883",
                },
                {
                  id: 15520869253203,
                  product_id: 2350278148179,
                  position: 24,
                  created_at: "2020-12-16T20:16:28+05:30",
                  updated_at: "2020-12-16T20:34:01+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520743161939.jpg?v=1608131041",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15520869253203",
                },
                {
                  id: 15520966443091,
                  product_id: 2350278148179,
                  position: 25,
                  created_at: "2020-12-16T20:34:00+05:30",
                  updated_at: "2020-12-16T20:34:01+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 600,
                  height: 819,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520743555155.png?v=1608131041",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15520966443091",
                },
                {
                  id: 15520963428435,
                  product_id: 2350278148179,
                  position: 26,
                  created_at: "2020-12-16T20:33:27+05:30",
                  updated_at: "2020-12-16T20:34:01+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520743882835.jpg?v=1608131041",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15520963428435",
                },
                {
                  id: 15520789299283,
                  product_id: 2350278148179,
                  position: 27,
                  created_at: "2020-12-16T20:05:24+05:30",
                  updated_at: "2020-12-16T20:34:01+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520744865875.jpg?v=1608131041",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15520789299283",
                },
                {
                  id: 15520915488851,
                  product_id: 2350278148179,
                  position: 28,
                  created_at: "2020-12-16T20:26:00+05:30",
                  updated_at: "2020-12-16T20:34:01+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 900,
                  height: 1229,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520745357395.jpg?v=1608131041",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15520915488851",
                },
                {
                  id: 15520918634579,
                  product_id: 2350278148179,
                  position: 29,
                  created_at: "2020-12-16T20:26:33+05:30",
                  updated_at: "2020-12-16T20:34:01+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 600,
                  height: 819,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520745947219.jpg?v=1608131041",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15520918634579",
                },
                {
                  id: 15520939475027,
                  product_id: 2350278148179,
                  position: 30,
                  created_at: "2020-12-16T20:29:58+05:30",
                  updated_at: "2020-12-16T20:34:01+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 600,
                  height: 819,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520746143827.png?v=1608131041",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15520939475027",
                },
                {
                  id: 15520936067155,
                  product_id: 2350278148179,
                  position: 31,
                  created_at: "2020-12-16T20:29:24+05:30",
                  updated_at: "2020-12-16T20:34:01+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 600,
                  height: 819,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520746864723.png?v=1608131041",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15520936067155",
                },
                {
                  id: 15520954974291,
                  product_id: 2350278148179,
                  position: 32,
                  created_at: "2020-12-16T20:32:05+05:30",
                  updated_at: "2020-12-16T20:34:01+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 600,
                  height: 819,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520748339283.png?v=1608131041",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15520954974291",
                },
                {
                  id: 15520951107667,
                  product_id: 2350278148179,
                  position: 33,
                  created_at: "2020-12-16T20:31:31+05:30",
                  updated_at: "2020-12-16T20:34:01+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 600,
                  height: 819,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520748699731.png?v=1608131041",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15520951107667",
                },
                {
                  id: 15520774389843,
                  product_id: 2350278148179,
                  position: 34,
                  created_at: "2020-12-16T20:03:31+05:30",
                  updated_at: "2020-12-16T20:34:01+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 600,
                  height: 819,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520748863571.jpg?v=1608131041",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15520774389843",
                },
                {
                  id: 15520804208723,
                  product_id: 2350278148179,
                  position: 35,
                  created_at: "2020-12-16T20:07:24+05:30",
                  updated_at: "2020-12-16T20:34:01+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 600,
                  height: 819,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520750207059.jpg?v=1608131041",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15520804208723",
                },
                {
                  id: 15520797524051,
                  product_id: 2350278148179,
                  position: 36,
                  created_at: "2020-12-16T20:06:30+05:30",
                  updated_at: "2020-12-16T20:34:01+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 600,
                  height: 819,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520750272595.jpg?v=1608131041",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15520797524051",
                },
                {
                  id: 15520759775315,
                  product_id: 2350278148179,
                  position: 37,
                  created_at: "2020-12-16T20:01:30+05:30",
                  updated_at: "2020-12-16T20:34:01+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 600,
                  height: 819,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520750567507.jpg?v=1608131041",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15520759775315",
                },
                {
                  id: 15520767017043,
                  product_id: 2350278148179,
                  position: 38,
                  created_at: "2020-12-16T20:02:37+05:30",
                  updated_at: "2020-12-16T20:34:01+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 600,
                  height: 819,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520751157331.jpg?v=1608131041",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15520767017043",
                },
                {
                  id: 15520763183187,
                  product_id: 2350278148179,
                  position: 39,
                  created_at: "2020-12-16T20:02:03+05:30",
                  updated_at: "2020-12-16T20:34:01+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 600,
                  height: 819,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520751321171.jpg?v=1608131041",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15520763183187",
                },
                {
                  id: 15520807747667,
                  product_id: 2350278148179,
                  position: 40,
                  created_at: "2020-12-16T20:07:57+05:30",
                  updated_at: "2020-12-16T20:34:01+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 600,
                  height: 819,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520752173139.jpg?v=1608131041",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15520807747667",
                },
                {
                  id: 15520863060051,
                  product_id: 2350278148179,
                  position: 41,
                  created_at: "2020-12-16T20:15:23+05:30",
                  updated_at: "2020-12-16T20:34:01+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 600,
                  height: 819,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520752959571.jpg?v=1608131041",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15520863060051",
                },
                {
                  id: 15520852475987,
                  product_id: 2350278148179,
                  position: 42,
                  created_at: "2020-12-16T20:13:58+05:30",
                  updated_at: "2020-12-16T20:34:01+05:30",
                  alt:
                    "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                  width: 600,
                  height: 819,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520753451091.jpg?v=1608131041",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15520852475987",
                },
              ],
              image: {
                id: 15520733331539,
                product_id: 2350278148179,
                position: 1,
                created_at: "2020-12-16T19:57:23+05:30",
                updated_at: "2020-12-16T20:34:01+05:30",
                alt: "SUGAR Cosmetics Smudge Me Not Liquid Lipstick Minis Set",
                width: 1025,
                height: 1400,
                src:
                  "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-smudge-me-not-liquid-lipstick-minis-set-15520724287571.png?v=1608131041",
                variant_ids: [],
                admin_graphql_api_id:
                  "gid://shopify/ProductImage/15520733331539",
              },
            },
            mediaText: null,
            redirectUrl: null,
          },
          {
            id: 174,
            sectionId: 4,
            webViewTitle: null,
            contentType: 3,
            mediaUrl: null,
            product_json: {
              id: 4666613760083,
              title: "The Boss Babe Kit",
              body_html:
                "<p><span>If you need a face that matches the attitude of your personality, here's the kit that's going to achieve you do just that! Presenting, the Boss Babe Kit with 5 stunning beauty must-haves and a clutch bag. Here's what's in it:</span><br><br><span>Matte As Hell Crayon Lipstick </span><br><br><span>Kohl Of Honour Intense Kajal</span><br><br><span>Wingman Waterproof Microliner </span><br><br><span>Sheet Mask</span><br><br><span>Mini Liquid Lipstick</span></p>\n<p><span>Maximum Retail Price: RS. 1,895 (incl. all taxes)<br><br>Matte As Hell Crayon Lipstick<br>Country of Origin: Germany<br>Company Name: Schwan Cosmetics Germany Gmbh &amp; Co.Kg<br>Company Address: Schwanweg 1, 90562 Heroldsberg - (Germany)<br><br>Kohl Of Honour Intense Kajal<br>Country of Origin: India<br>Company Name: Viva Cosmetics<br>Company Address: A/19 G4 Happy Valley, Nr Tikujiniwadi, Manpada, Thane W - 400610<br><br>Wingman Waterproof Microliner<br>Formulated in Italy<br>Company Name: SUGAR Cosmetics LLC<br>Company Address: 8 The Green Suite A, Dover, DE 19901, USA<br><br>Smudge Me Not Liquid Lipstick Minis<br>Country of Origin: India<br>Company Name: Viva Cosmetics<br>Company Address: A/19 G4 Happy Valley, Nr Tikujiniwadi, Manpada, Thane W - 400610<br><br>Sheet Mask<br>Country of Origin: Korea<br>Company Name: SUGAR Cosmetics LLC<br>Company Address: 8 The Green Suite A, Dover, DE 19901, USA\"<br></span></p>",
              vendor: "SUGAR Cosmetics",
              product_type: "Kit",
              created_at: "2020-08-31T18:23:11+05:30",
              handle: "boss-babe-kit",
              updated_at: "2020-12-16T04:46:06+05:30",
              published_at: "2020-10-21T10:32:24+05:30",
              template_suffix: "",
              published_scope: "web",
              tags:
                "Affordable, amp-hide, Anniversary, Birthday, Budget, Christmas, Cost Effective, Deals, Diwali, Economical, Eid, Eyeliner, Festive Makeup, Gift, Gifting, Gifts for her, Girlfriend, Kajal, Kit, Kohl, Lipstick, Love, Low Cost, Low Priced, Makeup Bloggers, makeup kit, Makeup Offers, Marriage, Matte Lipsticks, Mom, Money Saving, monsoon kit, Mother, New Year, Nominal, Present, Presents for Her, Reasonable, Savings, Sheet Mask, Sister, Steal Deal, Traditional Makeup, Travel kit, Travelling makeup kit, Valentine, Valentine's Day, Value for Money, Wedding, Wife, Worth The Money, Worthy",
              admin_graphql_api_id: "gid://shopify/Product/4666613760083",
              variants: [
                {
                  id: 32201115566163,
                  product_id: 4666613760083,
                  title: "Default Title",
                  price: "1299.00",
                  sku: "8904320713256",
                  position: 1,
                  inventory_policy: "deny",
                  compare_at_price: "1895.00",
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "Default Title",
                  option2: null,
                  option3: null,
                  created_at: "2020-08-31T18:23:11+05:30",
                  updated_at: "2020-12-16T04:40:45+05:30",
                  taxable: false,
                  barcode: "",
                  grams: 0,
                  image_id: null,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 33983129878611,
                  inventory_quantity: 1395,
                  old_inventory_quantity: 1395,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/32201115566163",
                },
              ],
              options: [
                {
                  id: 6013333569619,
                  product_id: 4666613760083,
                  name: "Title",
                  position: 1,
                  values: ["Default Title"],
                },
              ],
              images: [
                {
                  id: 14995532644435,
                  product_id: 4666613760083,
                  position: 1,
                  created_at: "2020-09-07T16:01:13+05:30",
                  updated_at: "2020-09-07T17:29:38+05:30",
                  alt: "SUGAR Cosmetics BOSS BABE KIT",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-boss-babe-kit-14995514294355.jpg?v=1599479978",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14995532644435",
                },
                {
                  id: 14995543359571,
                  product_id: 4666613760083,
                  position: 2,
                  created_at: "2020-09-07T16:04:14+05:30",
                  updated_at: "2020-09-07T17:41:34+05:30",
                  alt: "SUGAR Cosmetics BOSS BABE KIT",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-boss-babe-kit-14995514064979.jpg?v=1599480694",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14995543359571",
                },
                {
                  id: 14995529957459,
                  product_id: 4666613760083,
                  position: 3,
                  created_at: "2020-09-07T16:00:15+05:30",
                  updated_at: "2020-09-07T17:41:34+05:30",
                  alt: "SUGAR Cosmetics BOSS BABE KIT",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-boss-babe-kit-14995514359891.jpg?v=1599480694",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14995529957459",
                },
                {
                  id: 14995535134803,
                  product_id: 4666613760083,
                  position: 4,
                  created_at: "2020-09-07T16:01:45+05:30",
                  updated_at: "2020-09-07T17:32:47+05:30",
                  alt: "SUGAR Cosmetics BOSS BABE KIT",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-boss-babe-kit-14995513999443.jpg?v=1599480167",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14995535134803",
                },
                {
                  id: 14995528187987,
                  product_id: 4666613760083,
                  position: 5,
                  created_at: "2020-09-07T15:59:41+05:30",
                  updated_at: "2020-09-07T17:32:47+05:30",
                  alt: "SUGAR Cosmetics BOSS BABE KIT",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-boss-babe-kit-14995514130515.jpg?v=1599480167",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14995528187987",
                },
                {
                  id: 14995539820627,
                  product_id: 4666613760083,
                  position: 6,
                  created_at: "2020-09-07T16:03:09+05:30",
                  updated_at: "2020-09-07T17:32:47+05:30",
                  alt: "SUGAR Cosmetics BOSS BABE KIT",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-boss-babe-kit-14995514425427.jpg?v=1599480167",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14995539820627",
                },
                {
                  id: 14995541852243,
                  product_id: 4666613760083,
                  position: 7,
                  created_at: "2020-09-07T16:03:42+05:30",
                  updated_at: "2020-09-07T17:32:47+05:30",
                  alt: "SUGAR Cosmetics BOSS BABE KIT",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-boss-babe-kit-14995514490963.jpg?v=1599480167",
                  variant_ids: [],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/14995541852243",
                },
              ],
              image: {
                id: 14995532644435,
                product_id: 4666613760083,
                position: 1,
                created_at: "2020-09-07T16:01:13+05:30",
                updated_at: "2020-09-07T17:29:38+05:30",
                alt: "SUGAR Cosmetics BOSS BABE KIT",
                width: 1025,
                height: 1400,
                src:
                  "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-boss-babe-kit-14995514294355.jpg?v=1599479978",
                variant_ids: [],
                admin_graphql_api_id:
                  "gid://shopify/ProductImage/14995532644435",
              },
            },
            mediaText: null,
            redirectUrl: null,
          },
          {
            id: 172,
            sectionId: 4,
            webViewTitle: null,
            contentType: 3,
            mediaUrl: null,
            product_json: {
              id: 4677953978451,
              title: "Kohl Of Honour Intense Kajal",
              body_html:
                '<p><span>Ready to elevate your eye game to many notches above the ordinary? Step up to the world of extreme-wear, waterproof kajals with the SUGAR Kohl Of Honour Intense Kajal. Smudge and transfer-resistant, this breakthrough formula glides on smoothly for flawless, precise application and stays put for 12 straight hours. Ergonomically designed in an easy-to-use twist-up form, this wonder product delivers pigment-rich, high-impact colour that will fill your eyes with all the definition and drama you’ve ever known!</span></p>\n<ul>\n<li>\n<strong><a href="https://in.sugarcosmetics.com/products/kohl-of-honour-intense-kajal-01-black-out-black" target="_blank" rel="noopener noreferrer">01 Black Out (Black)</a></strong> – Every look intensified!<br><br>\n</li>\n<li>\n<strong><a href="https://in.sugarcosmetics.com/products/kohl-of-honour-intense-kajal-02-brown-bag-chocolate-brown" target="_blank" rel="noopener noreferrer">02 Brown Bag (Chocolate Brown)</a> - </strong>Mesmerizes while keeping things classy!<br><br>\n</li>\n<li>\n<strong><a href="https://in.sugarcosmetics.com/products/kohl-of-honour-intense-kajal-03-aqua-lung-metallic-mint-green" target="_blank" rel="noopener noreferrer">03 Aqua Lung (Metallic Mint Green)</a> - </strong>Like a deep dive into the ocean aka your<br><br>\n</li>\n<li>\n<strong><a href="https://in.sugarcosmetics.com/products/kohl-of-honour-intense-kajal-04-true-blue-navy-blue" target="_blank" rel="noopener noreferrer">04 True Blue (Navy Blue)</a> -<span> </span></strong><span>Cool, calm and fierce, all at the same time.</span>\n</li>\n</ul>\n<ul>\n<li>\n<strong><a href="https://in.sugarcosmetics.com/products/kohl-of-honour-intense-kajal-05-go-green-dark-green" target="_blank" rel="noopener noreferrer">05 Go Green (Dark Green)</a> - </strong>Flaunts your style like no other<strong>.<br><br></strong>\n</li>\n<li>\n<strong><a href="https://in.sugarcosmetics.com/products/kohl-of-honour-intense-kajal-06-blue-moon-metallic-baby-blue" target="_blank" rel="noopener noreferrer">06 Blue Moon (Metallic Baby Blue)</a> - </strong>For the diva in you!<br><br>\n</li>\n</ul>\n<p><span><strong>Additional Details</strong>: This product does not require an additional sharpener.</span></p>\n<p><span><strong>Net Weight</strong>: 0.25 gm.</span></p>\n<p><span><strong>SUGAR Kohl Of Honour Intense Kajal Ingredients</strong>: Cyclopentasiloxane, Hydrogenated Microcrystalline Wax, Iron Oxides, Polybutene, Synthetic Bees Wax, Trimethylsiloxysilicate, Black 2, Paraffin, Talc, Carnauba Wax, Triethoxycaprylylsilane, Tocopherol, Sunflower Seed Oil &amp; BHT.</span></p>\n<p><span><strong>Maximum Retail Price</strong>: Rs. 249 (incl. all taxes)</span></p>\n<p><span><strong>Country of Origin</strong>: India</span></p>\n<p><span><strong>Company Name</strong>: Viva Cosmetics</span></p>\n<p><span><strong>Company Address</strong>: A/19 G4 Happy Valley, Nr Tikujiniwadi, Manpada, Thane W - 400610</span></p>\n<h5>How to apply</h5>\n<p><span>Swivel up and glide on SUGAR Kohl Of Honour Intense Kajal, using short strokes. Build the desired shape on your lash line for a more controlled and smoother application. This best kajal can be applied to your upper/lower lash line and also on the waterline - you can even blend over the entire eyelid for a smokey eye makeup effect. Re-cap securely after use. This formula is designed to be used alone or over eyeshadow.</span></p>\n<p><span><strong>Best paired with</strong>:</span></p>\n<p><span>Go in with an </span><strong><a href="https://in.sugarcosmetics.com/collections/blend-the-rules-eyeshadow-palette/products/blend-the-rules-eyeshadow-palette-02-warrior-smokey">eyeshadow palette<span> </span></a></strong><span>and create a simple, smokey look. Add loads of </span><strong><a href="https://in.sugarcosmetics.com/products/lash-mob-limitless-mascara-01-black-with-a-bang">lengthening mascara<span> </span></a></strong><span>for long lashes. Finish with a </span><strong><a href="https://in.sugarcosmetics.com/products/face-fwd-highlighter-stick">pop of highlighter</a></strong><span>, pink blush to the cheeks, and a </span><strong><a href="https://in.sugarcosmetics.com/collections/nudes-lipsticks/products/matte-as-hell-crayon-lipstick-25-lily-aldrin-mauve-pink">nude lipstick</a></strong><span>.</span></p>\n<h5>Benefits</h5>\n<p><span>SUGAR Kohl of Honour Intense Kajal is powered by:<br>- Ultra-creamy texture (Glides on smoothly)<br>- 100% opacity (Super rich colour pay off)<br>- 12-hour stay power (No touch ups needed)</span></p>\n<h5>Commonly asked questions</h5>\n<p>Q. How long will it last on my eyes?<br>A. Up to 12 hours.<br><br>Q. Is it waterproof?<br>A. SUGAR Kohl Of Honour Intense Kajal is waterproof, smudgeproof, and even crackproof.<br><br>Q. Is the product paraben free?<br>A. Yes.<br><br>Q. How many shades does it come in?<br>A. Currently, just one.<br><br>Q. Does it come with a sharpener?<br>A. The product doesn\'t need a sharpener. It comes in a twist-up packaging.</p>',
              vendor: "SUGAR Cosmetics",
              product_type: "Kajal",
              created_at: "2020-09-11T10:30:39+05:30",
              handle: "kohl-of-honour-intense-kajal-1",
              updated_at: "2020-12-16T08:29:06+05:30",
              published_at: "2020-09-11T10:30:38+05:30",
              template_suffix: "",
              published_scope: "web",
              tags:
                "Kajal, Kohl Of Honour Intense Kajal, st_feature: Black Kajal, st_feature: Blue Kajal, st_feature: Brown Kajal, st_feature: Green Kajal, st_formulation: Kajal, st_type: Kajal and Kohl",
              admin_graphql_api_id: "gid://shopify/Product/4677953978451",
              variants: [
                {
                  id: 32218983071827,
                  product_id: 4677953978451,
                  title: "01 Black Out (Black)",
                  price: "249.00",
                  sku: "8904320705886",
                  position: 1,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "01 Black Out (Black)",
                  option2: null,
                  option3: null,
                  created_at: "2020-09-11T10:30:39+05:30",
                  updated_at: "2020-12-16T08:29:06+05:30",
                  taxable: true,
                  barcode: null,
                  grams: 0,
                  image_id: 15027209371731,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 34000997548115,
                  inventory_quantity: 9910,
                  old_inventory_quantity: 9910,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/32218983071827",
                },
                {
                  id: 32218983104595,
                  product_id: 4677953978451,
                  title: "02 Brown Bag (Chocolate Brown)",
                  price: "249.00",
                  sku: "8904320706081",
                  position: 2,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "02 Brown Bag (Chocolate Brown)",
                  option2: null,
                  option3: null,
                  created_at: "2020-09-11T10:30:39+05:30",
                  updated_at: "2020-12-16T02:04:32+05:30",
                  taxable: true,
                  barcode: null,
                  grams: 0,
                  image_id: 15027193872467,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 34000997580883,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/32218983104595",
                },
                {
                  id: 32218983137363,
                  product_id: 4677953978451,
                  title: "03 Aqua Lung (Metallic Mint Green)",
                  price: "249.00",
                  sku: "8904320706098",
                  position: 3,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "03 Aqua Lung (Metallic Mint Green)",
                  option2: null,
                  option3: null,
                  created_at: "2020-09-11T10:30:39+05:30",
                  updated_at: "2020-12-09T17:29:05+05:30",
                  taxable: true,
                  barcode: null,
                  grams: 0,
                  image_id: 15027191087187,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 34000997613651,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/32218983137363",
                },
                {
                  id: 32218983170131,
                  product_id: 4677953978451,
                  title: "04 True Blue (Navy Blue)",
                  price: "249.00",
                  sku: "8904320706104",
                  position: 4,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "04 True Blue (Navy Blue)",
                  option2: null,
                  option3: null,
                  created_at: "2020-09-11T10:30:39+05:30",
                  updated_at: "2020-12-09T17:29:05+05:30",
                  taxable: true,
                  barcode: null,
                  grams: 0,
                  image_id: 15027188564051,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 34000997646419,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/32218983170131",
                },
                {
                  id: 32218983202899,
                  product_id: 4677953978451,
                  title: "05 Go Green (Dark Green)",
                  price: "249.00",
                  sku: "8904320706111",
                  position: 5,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "05 Go Green (Dark Green)",
                  option2: null,
                  option3: null,
                  created_at: "2020-09-11T10:30:39+05:30",
                  updated_at: "2020-12-07T15:53:45+05:30",
                  taxable: true,
                  barcode: null,
                  grams: 0,
                  image_id: 15027202588755,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 34000997679187,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/32218983202899",
                },
                {
                  id: 32218983235667,
                  product_id: 4677953978451,
                  title: "06 Blue Moon (Metallic Baby Blue)",
                  price: "249.00",
                  sku: "8904320706128",
                  position: 6,
                  inventory_policy: "deny",
                  compare_at_price: null,
                  fulfillment_service: "manual",
                  inventory_management: "shopify",
                  option1: "06 Blue Moon (Metallic Baby Blue)",
                  option2: null,
                  option3: null,
                  created_at: "2020-09-11T10:30:39+05:30",
                  updated_at: "2020-12-16T01:33:55+05:30",
                  taxable: true,
                  barcode: null,
                  grams: 0,
                  image_id: 15027198722131,
                  weight: 0,
                  weight_unit: "kg",
                  inventory_item_id: 34000997711955,
                  inventory_quantity: 0,
                  old_inventory_quantity: 0,
                  requires_shipping: true,
                  admin_graphql_api_id:
                    "gid://shopify/ProductVariant/32218983235667",
                },
              ],
              options: [
                {
                  id: 6030198669395,
                  product_id: 4677953978451,
                  name: "color",
                  position: 1,
                  values: [
                    "01 Black Out (Black)",
                    "02 Brown Bag (Chocolate Brown)",
                    "03 Aqua Lung (Metallic Mint Green)",
                    "04 True Blue (Navy Blue)",
                    "05 Go Green (Dark Green)",
                    "06 Blue Moon (Metallic Baby Blue)",
                  ],
                },
              ],
              images: [
                {
                  id: 15027209371731,
                  product_id: 4677953978451,
                  position: 1,
                  created_at: "2020-09-11T10:39:09+05:30",
                  updated_at: "2020-09-11T10:49:04+05:30",
                  alt:
                    "SUGAR Cosmetics Kohl Of Honour Intense Kajal 01 Black Out (Black)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-kohl-of-honour-intense-kajal-01-black-out-black-15027177685075.jpg?v=1599801544",
                  variant_ids: [32218983071827],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15027209371731",
                },
                {
                  id: 15027193872467,
                  product_id: 4677953978451,
                  position: 2,
                  created_at: "2020-09-11T10:36:20+05:30",
                  updated_at: "2020-09-11T10:49:04+05:30",
                  alt:
                    "SUGAR Cosmetics Kohl Of Honour Intense Kajal 02 Brown Bag (Chocolate Brown)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-kohl-of-honour-intense-kajal-02-brown-bag-chocolate-brown-15027179126867.jpg?v=1599801544",
                  variant_ids: [32218983104595],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15027193872467",
                },
                {
                  id: 15027191087187,
                  product_id: 4677953978451,
                  position: 3,
                  created_at: "2020-09-11T10:35:47+05:30",
                  updated_at: "2020-09-11T10:49:04+05:30",
                  alt:
                    "SUGAR Cosmetics Kohl Of Honour Intense Kajal 03 Aqua Lung (Metallic Mint Green)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-kohl-of-honour-intense-kajal-03-aqua-lung-metallic-mint-green-15027179814995.jpg?v=1599801544",
                  variant_ids: [32218983137363],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15027191087187",
                },
                {
                  id: 15027188564051,
                  product_id: 4677953978451,
                  position: 4,
                  created_at: "2020-09-11T10:35:16+05:30",
                  updated_at: "2020-09-11T10:49:04+05:30",
                  alt:
                    "SUGAR Cosmetics Kohl Of Honour Intense Kajal 04 True Blue (Navy Blue)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-kohl-of-honour-intense-kajal-04-true-blue-navy-blue-15027180470355.jpg?v=1599801544",
                  variant_ids: [32218983170131],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15027188564051",
                },
                {
                  id: 15027202588755,
                  product_id: 4677953978451,
                  position: 5,
                  created_at: "2020-09-11T10:37:46+05:30",
                  updated_at: "2020-09-11T10:49:04+05:30",
                  alt:
                    "SUGAR Cosmetics Kohl Of Honour Intense Kajal 05 Go Green (Dark Green)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-kohl-of-honour-intense-kajal-05-go-green-dark-green-15027186008147.jpg?v=1599801544",
                  variant_ids: [32218983202899],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15027202588755",
                },
                {
                  id: 15027198722131,
                  product_id: 4677953978451,
                  position: 6,
                  created_at: "2020-09-11T10:37:14+05:30",
                  updated_at: "2020-09-11T10:49:04+05:30",
                  alt:
                    "SUGAR Cosmetics Kohl Of Honour Intense Kajal 06 Blue Moon (Metallic Baby Blue)",
                  width: 1025,
                  height: 1400,
                  src:
                    "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-kohl-of-honour-intense-kajal-06-blue-moon-metallic-baby-blue-15027186565203.jpg?v=1599801544",
                  variant_ids: [32218983235667],
                  admin_graphql_api_id:
                    "gid://shopify/ProductImage/15027198722131",
                },
              ],
              image: {
                id: 15027209371731,
                product_id: 4677953978451,
                position: 1,
                created_at: "2020-09-11T10:39:09+05:30",
                updated_at: "2020-09-11T10:49:04+05:30",
                alt:
                  "SUGAR Cosmetics Kohl Of Honour Intense Kajal 01 Black Out (Black)",
                width: 1025,
                height: 1400,
                src:
                  "https://cdn.shopify.com/s/files/1/0906/2558/products/sugar-cosmetics-kohl-of-honour-intense-kajal-01-black-out-black-15027177685075.jpg?v=1599801544",
                variant_ids: [32218983071827],
                admin_graphql_api_id:
                  "gid://shopify/ProductImage/15027209371731",
              },
            },
            mediaText: null,
            redirectUrl: null,
          },
        ],
      },
      {
        id: 9,
        title: "EXPLORE",
        sequence: 110,
        layoutType: 4,
        isTitleEnabled: 1,
        backgroundImg: null,
        backgroundColor: "#000000",
        text: null,
        contentType: 1,
        contentData: [
          {
            id: 89,
            sectionId: 9,
            webViewTitle: null,
            contentType: 1,
            mediaUrl:
              "https://d32baadbbpueqt.cloudfront.net/dashboard/1609482077Explore-Banner_Eyes-600X300.jpg",
            product_json: null,
            mediaText: null,
            redirectUrl:
              "https://in.sugarcosmetics.com/collections/eye-product-range",
          },
          {
            id: 90,
            sectionId: 9,
            webViewTitle: null,
            contentType: 1,
            mediaUrl:
              "https://d32baadbbpueqt.cloudfront.net/dashboard/1609482120Explore-Banner_Lips-600X300.jpg",
            product_json: null,
            mediaText: null,
            redirectUrl: "https://in.sugarcosmetics.com/collections/lips",
          },
          {
            id: 99,
            sectionId: 9,
            webViewTitle: null,
            contentType: 1,
            mediaUrl:
              "https://d32baadbbpueqt.cloudfront.net/dashboard/1609482145Explore-Banner_Face-600X300.jpg",
            product_json: null,
            mediaText: null,
            redirectUrl:
              "https://in.sugarcosmetics.com/collections/face-products-range",
          },
          {
            id: 100,
            sectionId: 9,
            webViewTitle: null,
            contentType: 1,
            mediaUrl:
              "https://d32baadbbpueqt.cloudfront.net/dashboard/1609482164Explore-Banner_Value-Set-600X300.jpg",
            product_json: null,
            mediaText: null,
            redirectUrl: "https://in.sugarcosmetics.com/collections/value-set",
          },
          {
            id: 88,
            sectionId: 9,
            webViewTitle: null,
            contentType: 1,
            mediaUrl:
              "https://d32baadbbpueqt.cloudfront.net/dashboard/1609482180Explore-Banner_Skin-600X300.jpg",
            product_json: null,
            mediaText: null,
            redirectUrl: "https://in.sugarcosmetics.com/collections/skin",
          },
          {
            id: 141,
            sectionId: 9,
            webViewTitle: null,
            contentType: 1,
            mediaUrl:
              "https://d32baadbbpueqt.cloudfront.net/dashboard/1601446927Gifting-card-explor.jpg",
            product_json: null,
            mediaText: null,
            redirectUrl:
              "https://in.sugarcosmetics.com/collections/sugar-cosmetics-gifts-boxes",
          },
        ],
      },
    ],
  },
};
export default function Home(props) {
  const router = useRouter();
  useEffect(() => {
    //  axios.post("https://dev.api.sugarcosmetics.com/appconfig/dev/homepageV2")
    //  .then(res => console.log(res))
    //  .catch(err => console.log(err))
    var config = {
      method: "post",
      url: "https://qa.api.sugarcosmetics.com/appconfig/qa/homepageV2",
      headers: {},
    };

    axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data));
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);

  console.log(router.asPath);
  // useEffect(() => {
  // return <Redirect to="/Components/Cart" as={`${router.asPath}`} />;
  if (router.asPath === "/cart") {
    return <Redirect to="/Components/Cart" as={`${router.asPath}`} />;
  } else if (router.asPath === "/categories") {
    return (
      <Redirect
        to="/Components/Categories"
        as={`${router.asPath}`}
        as="/categories"
      />
    );
  } else if (router.asPath === "/MyAccount") {
    return <Redirect to="/Components/MyAccount" as={`${router.asPath}`} />;
  } else if (router.asPath === "/more") {
    return <Redirect to="/Components/More" as={`${router.asPath}`} />;
  }
  if (router.asPath === "/search") {
    return <Redirect to="/Components/Search" as={`${router.asPath}`} />;
  } else if (router.asPath !== "/") {
    return <Redirect to="/Components/ProductPage" as={`${router.asPath}`} />;
  }
  // }, [])
  // console.log(router)

  // console.log(data)
  return (
    <>
      {/* <Head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous"/>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </Head> */}
      <div style={{ overflowX: "hidden", overflowY: "scroll" }}>
        <div>
          <Navbar />
        </div>
        {data &&
          data.resbody.sections?.map((item) => (
            <>
              {item.title === "Slider" && (
                <>
                  <div className="mt-5" key={uuidv4()}>
                    <div
                      id={`carouselExampleIndicators${item.id}`}
                      className="carousel slide"
                      data-ride="carousel"
                    >
                      <ol className="carousel-indicators">
                        {item.contentData?.map((slideElem, slideInd) => (
                          <>
                            {slideInd === 0 ? (
                              <li
                                data-target={`#carouselExampleIndicators${item.id}`}
                                data-slide-to="0"
                                className="active"
                              ></li>
                            ) : (
                              <li
                                data-target={`#carouselExampleIndicators${item.id}`}
                                data-slide-to={slideInd}
                              ></li>
                            )}
                          </>
                        ))}
                      </ol>
                      <div className="carousel-inner">
                        {item.contentData?.map((imgElem, imgInd) => (
                          <>
                            {imgInd === 0 ? (
                              <div className="carousel-item active">
                                <img
                                  className="d-block img-fluid"
                                  width="500"
                                  height="800"
                                  src={imgElem.mediaUrl}
                                  alt={imgElem.mediaUrl}
                                />
                              </div>
                            ) : (
                              <div className="carousel-item">
                                <img
                                  className="d-block img-fluid"
                                  width="500"
                                  height="800"
                                  src={imgElem.mediaUrl}
                                  alt={imgElem.mediaUrl}
                                />
                              </div>
                            )}
                          </>
                        ))}
                      </div>
                    </div>
                  </div>
                </>
              )}
              {item.title === "Refer Your Friends" && (
                <div>
                  <h3 className="my-3">Refer Your Friends</h3>
                  {item.contentData?.map((referImg) => (
                    <>
                      <div>
                        <img
                          className="d-block img-fluid"
                          width="500"
                          height="500"
                          src={referImg.mediaUrl}
                          alt={referImg.mediaUrl}
                        />
                      </div>
                    </>
                  ))}
                </div>
              )}

              {item.title === "HOT DEALS" && (
                <div>
                  <h3 className="my-3">HOT DEALS</h3>
                  <div>
                    <div style={{ overflowX: "hidden", color: "white" }}>
                      {/* <Link as="/product/lipstick/" href="/[products]/[ProductPage]" > */}
                      <div className={styles.containerBox}>
                        <div
                          className={styles.Box}
                          style={{ backgroundColor: "rgb(32, 32, 32)" }}
                        ></div>
                        <div
                          className={`${styles.Box} ${styles["stackTop"]} `}
                          style={{
                            backgroundColor: "rgb(32, 32, 32)",
                            display: "flex",
                          }}
                        >
                          {/* <SideImage contentData={item.contentData} /> */}
                          {item.contentData?.map((elem) => (
                            <div key={uuidv4()}>
                              <Link
                                as={`${elem.redirectUrl.split(".com")[1]}`}
                                href="/Components/ProductPage"
                              >
                                <div>
                                  <img
                                    src={elem.mediaUrl}
                                    className="d-block mx-2"
                                    width="320"
                                    height=""
                                    src={elem.mediaUrl}
                                    alt={elem.mediaUrl}
                                  />
                                </div>
                              </Link>
                            </div>
                          ))}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              )}

              {item.title === "BESTSELLERS" && (
                <div>
                  <h3 className="my-3">BESTSELLERS</h3>

                  {/* {item.contentData?.map((elem) => ( */}
                  <div key={uuidv4()}>
                    <SideImage contentData={item.contentData} />
                  </div>
                  {/* ))} */}
                </div>
              )}

              {item.title === "THIS OR THAT" && (
                <div>
                  <h3 className="my-3">THIS OR THAT</h3>
                  {item.contentData?.map((referImg) => (
                    <>
                      <div>
                        <img
                          className="d-block img-fluid mt-1"
                          width="500"
                          height="500"
                          src={referImg.mediaUrl}
                          alt={referImg.mediaUrl}
                        />
                      </div>
                    </>
                  ))}
                </div>
              )}

              {item.title === "SUPER SAVERS" && (
                <div>
                  <h3 className="my-3">SUPER SAVERS</h3>

                  {/* {item.contentData?.map((elem) => ( */}
                  <div key={uuidv4()}>
                    <SideImage contentData={item.contentData} />
                  </div>
                  {/* ))} */}
                </div>
              )}

{item.title === "BEST OF THE BEST" && (
                <div>
                  <h3 className="my-3">BEST OF THE BEST</h3>
                  {item.contentData?.map((referImg) => (
                    <>
                      <div>
                        <img
                          className="d-block img-fluid"
                          width="500"
                          height="500"
                          src={referImg.mediaUrl}
                          alt={referImg.mediaUrl}
                        />
                      </div>
                    </>
                  ))}
                </div>
              )}
  {item.title === "FRESH FACE LOOKS" && (
                <>
                <h3 className="my-3">FRESH FACE LOOKS</h3>
                  <div className="" key={uuidv4()}>
                    <div
                      id={`carouselExampleIndicators${item.id}`}
                      className="carousel slide"
                      data-ride="carousel"
                    >
                      <ol className="carousel-indicators">
                        {item.contentData?.map((slideElem, slideInd) => (
                          <>
                            {slideInd === 0 ? (
                              <li
                                data-target={`#carouselExampleIndicators${item.id}`}
                                data-slide-to="0"
                                className="active"
                              ></li>
                            ) : (
                              <li
                                data-target={`#carouselExampleIndicators${item.id}`}
                                data-slide-to={slideInd}
                              ></li>
                            )}
                          </>
                        ))}
                      </ol>
                      <div className="carousel-inner">
                        {item.contentData?.map((imgElem, imgInd) => (
                          <>
                            {imgInd === 0 ? (
                              <div className="carousel-item active">
                                <img
                                  className="d-block img-fluid"
                                  width="500"
                                  height="800"
                                  src={imgElem.mediaUrl}
                                  alt={imgElem.mediaUrl}
                                />
                              </div>
                            ) : (
                              <div className="carousel-item">
                                <img
                                  className="d-block img-fluid"
                                  width="500"
                                  height="800"
                                  src={imgElem.mediaUrl}
                                  alt={imgElem.mediaUrl}
                                />
                              </div>
                            )}
                          </>
                        ))}
                      </div>
                    </div>
                  </div>
                </>
              )}

{item.title === "SKINCARE BASICS" && (
                <div>
                  <h3 className="my-3">SKINCARE BASICS</h3>

                  {/* {item.contentData?.map((elem) => ( */}
                  <div key={uuidv4()}>
                    <SideImage contentData={item.contentData} />
                  </div>
                  {/* ))} */}
                </div>
              )}
              {item.title === "TIPS AND TRICKS" && (
                <>
                <h3 className="my-3">TIPS AND TRICKS</h3>
                  <div className="" key={uuidv4()}>
                    <div
                      id={`carouselExampleIndicators${item.id}`}
                      className="carousel slide"
                      data-ride="carousel"
                    >
                      <ol className="carousel-indicators">
                        {item.contentData?.map((slideElem, slideInd) => (
                          <>
                            {slideInd === 0 ? (
                              <li
                                data-target={`#carouselExampleIndicators${item.id}`}
                                data-slide-to="0"
                                className="active"
                              ></li>
                            ) : (
                              <li
                                data-target={`#carouselExampleIndicators${item.id}`}
                                data-slide-to={slideInd}
                              ></li>
                            )}
                          </>
                        ))}
                      </ol>
                      <div className="carousel-inner">
                        {item.contentData?.map((imgElem, imgInd) => (
                          <>
                            {imgInd === 0 ? (
                              <div className="carousel-item active">
                                <img
                                  className="d-block img-fluid"
                                  width="500"
                                  height="800"
                                  src={imgElem.mediaUrl}
                                  alt={imgElem.mediaUrl}
                                />
                              </div>
                            ) : (
                              <div className="carousel-item">
                                <img
                                  className="d-block img-fluid"
                                  width="500"
                                  height="800"
                                  src={imgElem.mediaUrl}
                                  alt={imgElem.mediaUrl}
                                />
                              </div>
                            )}
                          </>
                        ))}
                      </div>
                    </div>
                  </div>
                </>
              )}
              {item.title === "JUST-IN" && (
                <div>
                  <h3 className="my-3">JUST-IN</h3>

                  {/* {item.contentData?.map((elem) => ( */}
                  <div key={uuidv4()}>
                    <SideImage contentData={item.contentData} />
                  </div>
                  {/* ))} */}
                </div>
              )}
              {item.title === "EXPLORE" && (
                <div>
                  <h3 className="my-3">EXPLORE</h3>
                  {item.contentData?.map((referImg, expInd) => (
                    <>
                      <div>
                      {expInd === item.contentData.length-1 ? (
                              <img
                              className="d-block img-fluid mb-5"
                              width="500"
                              height="500"
                              src={referImg.mediaUrl}
                              alt={referImg.mediaUrl}
                            />
                            ) : (
                              <img
                          className="d-block img-fluid"
                          width="500"
                          height="500"
                          src={referImg.mediaUrl}
                          alt={referImg.mediaUrl}
                        />
                            )}
                        {/* <img
                          className="d-block img-fluid"
                          width="500"
                          height="500"
                          src={referImg.mediaUrl}
                          alt={referImg.mediaUrl}
                        /> */}
                      </div>
                    </>
                  ))}
                </div>
              )}

            </>
          ))}



        {/* <h3>HOT DEALS</h3>
        <div>
          <SideImage />
        </div> */}
        {/* <h3>BESTSELLERS</h3>
        <div>
          <SideImage />
        </div> */}
        {/* <h3>THIS OR THAT</h3>
        <div>
          <img src="https://via.placeholder.com/500X200" />
        </div> */}
        {/* <div>
          <img src="https://via.placeholder.com/500X200" />
        </div> */}
        {/* <h3>SUPER SAVERS</h3>
        <div><SideImage /></div> */}
        {/* <h3>BEST OF THE BEST</h3>
        <div>
          <img src="https://via.placeholder.com/500X200" />
        </div>
        <div>
          <img src="https://via.placeholder.com/500X200" />
        </div>
        <div>
          <img src="https://via.placeholder.com/500X200" />
        </div> */}
        {/* <h3>FRESH FACE LOOKS</h3>
        <div>
          <TopBannerCarousel />
        </div> */}
        {/* <h3>SKINCARE BASICS</h3>
        <div><SideImage /></div> */}
        {/* <h3>TIPS AND TRICKS</h3>
        <div>
          <TopBannerCarousel />
        </div> */}
        {/* <h3>JUST-IN</h3>
        <div><SideImage /></div> */}
        {/* <h3>EXPLORE</h3>
        <div>
          <img src="https://via.placeholder.com/500X200" />
        </div>
        <div>
          <img src="https://via.placeholder.com/500X200" />
        </div>
        <div>
          <img src="https://via.placeholder.com/500X200" />
        </div>
        <div>
          <img src="https://via.placeholder.com/500X200" />
        </div>
        <div>
          <img src="https://via.placeholder.com/500X200" />
        </div> */}
        {/* <div className="mb-5">
          <Link href="/Components/ProductPage" as="/product">
            <img src="https://via.placeholder.com/500X200" />
          </Link>
        </div> */}

        <div>
          <Footer />
        </div>
      </div>
    </>
  );
}


