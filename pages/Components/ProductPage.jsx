import React, {useRef} from 'react'
import ProductFooter from './ProductComponents/ProductFooter'
import ProductInfo from './ProductComponents/ProductInfo'
import ProductNavbar from './ProductComponents/ProductNavbar'
// import { disableBodyScroll, enableBodyScroll } from 'body-scroll-lock';
import {useRouter} from 'next/router'
export default function ProductPage() {
    console.log("checking")
    // const scrollRef = useRef();

    // const handleSetScroll = () => {
    //     // console.log(scrollRef.current)
    //     enableBodyScroll(scrollRef.current)
    // }
    // const handleRemoveScroll = () => {
    //     // console.log(scrollRef.current)
    //     disableBodyScroll(scrollRef.current)
    // }
    const router = useRouter()
    console.log(router.asPath)
    return (
        <div >
         <div 
            // onMouseLeave={handleSetScroll} 
            // onMouseEnter={handleRemoveScroll}
            >

            <ProductNavbar productName={router.asPath} />
            </div>
             <div style={{'marginTop':'520px'}} 
            // ref={scrollRef} 
            >
                <ProductInfo productName={router.asPath}/>
            </div>
            <div>
                <ProductFooter />
            </div>
        
       </div>
    )
}
