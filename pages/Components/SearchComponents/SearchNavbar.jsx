import React, {useRef, useEffect, useState} from 'react'
// import './SearchNavbar.css'
import styles from '../../../styles/SearchNavbar.module.css'
// import { Link } from 'react-router-dom'
import Link from 'next/link'
import KeyboardBackspaceRoundedIcon from '@material-ui/icons/KeyboardBackspaceRounded';
import ClearRoundedIcon from '@material-ui/icons/ClearRounded';
import FavoriteBorderRoundedIcon from '@material-ui/icons/FavoriteBorderRounded';
import LocalMallRoundedIcon from '@material-ui/icons/LocalMallRounded';

export default function SearchNavbar() {
    const inputRef = useRef();
    const [searchValue, setSearchValue]= useState('')

    useEffect(() => {
      inputRef.current.focus();
    })

   const handleSearchValue = (value) => {

    // console.log(value)
       setSearchValue(value)
   }

   const removeSearchValue = () => {
       setSearchValue('')
   }
    return (
        <div>
             
    <nav
      style={{ backgroundColor: "black" }}
      className="navbar fixed-top "
    >
        <div className="p">
        <div className="d-flex">
        <div className="px-2">
        <Link href="/" >
      {/* <img src="https://via.placeholder.com/20" className="px-4" /> */}
      <KeyboardBackspaceRoundedIcon style={{"color":"white", "fontSize":"28px"}}/>
      </Link>
      </div>
      <div className="">
          <div className="">

          <input type="text" value={searchValue} placeholder="Search..."  className={`${styles.cursorFocus} ${styles.searchBar} py-1`} ref={inputRef} onChange={(e) => handleSearchValue(e.target.value)} />
          </div>
      </div>
      <div className="d-flex pr-2" >
        <div className="px-1">
          {/* <Link to="/search" style={{"textDecoration":'none'}}> */}
          {/* <img src="https://via.placeholder.com/20" /> */}
          {/* </Link> */}
<ClearRoundedIcon style={{"color":"white", "fontSize":"28px"}} onClick={() => removeSearchValue()} />
        </div>
        <div className="px-1">
          {/* <img src="https://via.placeholder.com/20" /> */}
          <FavoriteBorderRoundedIcon style={{"color":"white", "fontSize":"28px"}} />
        </div>
        <div className="px-1">
        {/* <img src="https://via.placeholder.com/20" /> */}
        <LocalMallRoundedIcon  style={{"color":"white", "fontSize":"28px"}}  />
        </div>
      </div>
        </div>
     
        </div>
    </nav>
        </div>
    )
}
