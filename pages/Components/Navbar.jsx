import React from "react";
// import { Link } from "react-router-dom";
import Link from 'next/link'
import SearchRoundedIcon from '@material-ui/icons/SearchRounded';
import FavoriteBorderRoundedIcon from '@material-ui/icons/FavoriteBorderRounded';
import NotificationsNoneRoundedIcon from '@material-ui/icons/NotificationsNoneRounded';
import cart from '../../pages/Components/Cart'

export default function Navbar() {
  return (
   
    <nav
      style={{ backgroundColor: "black" }}
      className="navbar fixed-top "
    >
        <div className="d-flex justify-content-around">
        <div className="px-2 py-1">
          <Link href="/" as="/" style={{"textDecoration":'none'}}>
      <img src="/sugar-white-logo.png" className="" height="30px" />
          </Link>
      </div>
      <div className="d-flex " style={{"marginLeft":"9em"}}>
        <div className="px-1">
          <Link href="/Components/Search" as="/search">
          {/* <img src="https://via.placeholder.com/20" /> */}
          <SearchRoundedIcon style={{"color":"white", "fontSize":"30px"}} />
          </Link>
        </div>
        <div className="px-1">
          {/* <img src="https://via.placeholder.com/20" /> */}
          <FavoriteBorderRoundedIcon style={{"color":"white", "fontSize":"30px"}} />
        </div>
        <div className="px-1">
        {/* <img src="https://via.placeholder.com/20" /> */}
        <NotificationsNoneRoundedIcon style={{"color":"white", "fontSize":"30px"}} />
        </div>
      </div>
        </div>
     

    </nav>
  );
}
