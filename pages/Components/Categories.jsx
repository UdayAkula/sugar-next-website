import React from 'react'
import Footer from './Footer'
import Navbar from './Navbar'
import {manageItem} from '../Redux/Sidebar/action'
import { connect } from 'react-redux'
 function Categories(props) {
    return (
        <div>
            <div>
                <Navbar />
            </div>
            <div className="mt-5">

            Hello Categories
            </div>
            <div>
                <Footer />
            </div>
        </div>
    )
}
const mapStateToProps = state => ({
    overview: state.sidebarReducer.overview,
    affiliates:state.sidebarReducer.affiliates
    
  })
  const mapDispatchToProps = dispatch => ({
    manageItem: (payload) => dispatch(manageItem(payload))
  })
  
  export default connect(mapStateToProps, mapDispatchToProps)(Categories)