import React from 'react'
// import { Link } from "react-router-dom";
import Link from 'next/link'
import HomeIcon from '@material-ui/icons/Home';
import ViewHeadlineRoundedIcon from '@material-ui/icons/ViewHeadlineRounded';
// import ShoppingCartRoundedIcon from '@material-ui/icons/ShoppingCartRounded';
import AccountCircleRoundedIcon from '@material-ui/icons/AccountCircleRounded';
import MoreHorizRoundedIcon from '@material-ui/icons/MoreHorizRounded';
import LocalMallRoundedIcon from '@material-ui/icons/LocalMallRounded';

export default function Footer() {
    return (
      <>
        <div >
            <nav style={{"backgroundColor":"black"}} className="navbar  fixed-bottom d-flex justify-content-between">
            
 
  {/* <span className=""> */}

  <div className="px-3">
    <Link href="/" as="/">
    <div>
    <HomeIcon style={{"color":"white", "fontSize":"30px"}} />
    <div style={{"fontSize":""}}>
<small >Home</small>
</div>
</div>
    </Link> 
  </div>
  <div className="px-2">
    
  <Link href="/Components/Categories" as="/categories" >
    {/* <img src="https://via.placeholder.com/20" /> */}
    <div>
    <ViewHeadlineRoundedIcon style={{"color":"white", "fontSize":"30px"}}  />
    <div>
<small >Categories</small>

</div>
{/* <br/> */}
</div>
    </Link>
      
  </div>
  <div className="px-2">
  <Link href="/Components/Cart" as="/cart" >
    {/* <img src="https://via.placeholder.com/20" /> */}
    <div>
    <LocalMallRoundedIcon  style={{"color":"white", "fontSize":"30px"}}  />
<div>
<small >Cart</small>
</div>
    {/* <br/> */}
    </div>
    </Link>
      
  </div>
  <div className="px-3">
  <Link href="/Components/MyAccount" as='/MyAccount'>
    {/* <img src="https://via.placeholder.com/20" /> */}
    <div>
    <AccountCircleRoundedIcon style={{"color":"white", "fontSize":"30px"}}/>
    <div>

<small >My Account</small>
</div>
    {/* <br/> */}
    </div>
    </Link>
      
  </div>
  <div className="px-3">
  <Link href="/Components/More" as="/more">
    {/* <img src="https://via.placeholder.com/20" /> */}
    <div>
    <MoreHorizRoundedIcon style={{"color":"white", "fontSize":"30px"}}/>
    <div>
<small >More</small>

</div>
    {/* <br/> */}
    </div>
    </Link>
      
  </div>

</nav>
        </div>
        
        </>
    )
}
