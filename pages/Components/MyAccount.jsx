import React from 'react'
import Footer from './Footer'
import Navbar from './Navbar'

export default function MyAccount() {
    return (
        <div>
            <div>
                <Navbar />
            </div>
            <div className="mt-5">

            Hello My Account
            </div>
            <div>
                <Footer />
            </div>
        </div>
    )
}
